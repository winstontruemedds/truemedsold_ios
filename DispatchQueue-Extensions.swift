//
//  DispatchQueue-Extensions.swift
//  languagetry
//
//  Created by Office on 12/13/19.
//  Copyright © 2019 Office. All rights reserved.
//

import Foundation

public extension DispatchQueue {
    private static var onceTracker = [String]()

    ///    Execute the given `block` only once during app's lifecycle
    class func once(token: String, block: () -> Void) {
        objc_sync_enter(self);
        defer {
            objc_sync_exit(self)
        }

        if onceTracker.contains(token) { return }
        onceTracker.append(token)
        block()
    }
}
