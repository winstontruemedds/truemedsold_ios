//
//  HelpTableViewCell.swift
//  True_Meds
//


import UIKit

class HelpTableViewCell: UITableViewCell {

    //// Help Topic ////
    
    @IBOutlet weak var htopiclbl: UILabel!
    
    @IBOutlet weak var grarr: UIImageView!
    
    //// Help Issues ////
    
    @IBOutlet weak var hissuelbl: UILabel!
    
    @IBOutlet weak var hgrarr: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
