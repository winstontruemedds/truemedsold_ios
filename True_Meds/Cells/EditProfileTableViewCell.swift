//
//  EditProfileTableViewCell.swift
//  True_Meds
//


import UIKit

class EditProfileTableViewCell: UITableViewCell {
    
    
    

    @IBOutlet weak var eProPic: UIImageView!
    
    @IBOutlet weak var nametf: UITextField!
    
    @IBOutlet weak var emailtf: UITextField!
    
    @IBOutlet weak var agetf: UITextField!
    
    @IBOutlet weak var esavebtno: UIButton!
    
    @IBOutlet weak var ecancelbtno: UIButton!
    

    @IBOutlet weak var gendertf: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        eProPic.layer.cornerRadius = 90
        eProPic.clipsToBounds = true
        
        esavebtno.layer.borderWidth = 0.8
        esavebtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        esavebtno.layer.cornerRadius = 4.0
        
        ecancelbtno.layer.borderWidth = 0.8
        ecancelbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        ecancelbtno.layer.cornerRadius = 4.0
      
    }

   
    
   /* func loadImage(){
        if (UserDefaults.standard.value(forKey: "imageData") != nil){
            let data = UserDefaults.standard.object(forKey: "imageData")
            eProPic.image = UIImage(data: data as! Data)
            
            
        }
    }*/
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
   
   

}



