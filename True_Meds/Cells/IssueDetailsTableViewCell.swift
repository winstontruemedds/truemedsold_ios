//
//  IssueDetailsTableViewCell.swift
//  True_Meds
//


import UIKit
import SVGKit

class IssueDetailsTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var medissnlbl: UILabel!
    
    @IBOutlet weak var uploadbtno: UIButton!
    
    @IBOutlet weak var issuecollection: UICollectionView!
    
    @IBOutlet weak var issconswidth: NSLayoutConstraint!
    
    @IBOutlet weak var issuepickertf: UITextField!
    
    @IBOutlet weak var tellustv: UITextView!
    
    var array = [UIImage]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        issuecollection.delegate = self
        issuecollection.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    func fillCollectionView3(with array: [UIImage]) {
        
        self.array = array
        if array.count == 0
        {
            
        }
        else
        {
            issconswidth.constant = issconswidth.constant + 130
            self.issuecollection.reloadData()
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell2", for: indexPath) as! ImageCollectionViewCell2
        
        cell.issueimgs.image = array[indexPath.row]
        
        cell.issueimgs.layer.cornerRadius = 4.0
        cell.issueimgs.clipsToBounds = true
        
        cell.issuevisblur.layer.cornerRadius = 4.0
        cell.issuevisblur.clipsToBounds = true
        
        cell.issueimgdel.addTarget(self, action: #selector(delimg), for: .touchUpInside)
        cell.issueimgdel.tag = indexPath.row
        
        let svgimg: SVGKImage = SVGKImage(named: "circle_cross")
        cell.issueimgdel.setImage(svgimg.uiImage, for: .normal)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 120, height: 120)
    }
    
    
    @objc func delimg(sender:UIButton)
    {
        array.remove(at: sender.tag)
        issconswidth.constant = issconswidth.constant - 130
        self.issuecollection.reloadData()
        
        
        //self.delegateCustom?.getDataFromAnotherVC(temp: sender.tag)
    }
    
    
}
