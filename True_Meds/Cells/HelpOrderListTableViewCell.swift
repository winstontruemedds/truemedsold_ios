//
//  HelpOrderListTableViewCell.swift
//  True_Meds
//


import UIKit

class HelpOrderListTableViewCell: UITableViewCell {

    // Help with orders
    
    @IBOutlet weak var bgview3: UIView!
    
    @IBOutlet weak var orderId: UILabel!
    
    @IBOutlet weak var odatelbl: UILabel!
    
    @IBOutlet weak var presdeli: UILabel!
    
    @IBOutlet weak var savingslbl: UILabel!
    
    //
    
    @IBOutlet weak var odstatlbl: UILabel!
    
    @IBOutlet weak var cvalidlbl: UILabel!
    
    @IBOutlet weak var erroriden: UIImageView!
    
    @IBOutlet weak var totsav2: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
