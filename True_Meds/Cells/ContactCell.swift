//
//  ContactCell.swift
//  True_Meds
//


import UIKit

class ContactCell: UITableViewCell {

    @IBOutlet weak var cncName: UILabel!
    
    @IBOutlet weak var cncNumber: UILabel!
    
    @IBOutlet weak var invitebtno: UIButton!
    
    @IBOutlet weak var invitesentlbl: UILabel!
    
    @IBOutlet weak var installname: UILabel!
    
    @IBOutlet weak var installnumber: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
