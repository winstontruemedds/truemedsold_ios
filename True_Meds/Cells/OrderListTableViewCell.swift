//
//  OrderListTableViewCell.swift
//  True_Meds
//


import UIKit

class OrderListTableViewCell: UITableViewCell {

    @IBOutlet weak var bgview: UIView!
    
   
    
    @IBOutlet weak var orderId: UILabel!
    
    @IBOutlet weak var deliveredonlbl: UILabel!
    
    @IBOutlet weak var presvalidtill: UILabel!
    
    @IBOutlet weak var totalsavings: UILabel!
    
    //
    
    @IBOutlet weak var oldstatlbl: UILabel!
    
    @IBOutlet weak var cvalidlbl: UILabel!
    
    /// Past Order View ///
    
    
    @IBOutlet weak var popatlbl: UILabel!
    
    @IBOutlet weak var delionlbl: UILabel!
    
    @IBOutlet weak var povalidtill: UILabel!
    
    @IBOutlet weak var posavinglbl: UILabel!
    
    @IBOutlet weak var errorIden: UIImageView!
    
    @IBOutlet weak var paynowo: UIButton!
    
    @IBOutlet weak var totsavelbl: UILabel!
    
    @IBOutlet weak var pastotsavelbl: UILabel!
    
    @IBOutlet weak var pastostatlbl: UILabel!
    
    @IBOutlet weak var pastvalidlbl: UILabel!
    
    @IBOutlet weak var pasterrorid: UIImageView!
    
    @IBOutlet weak var ppaynow: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
