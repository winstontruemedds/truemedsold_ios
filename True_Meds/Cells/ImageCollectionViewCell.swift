//
//  ImageCollectionViewCell.swift
//  True_Meds
//


import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    
    /////  Upload Rx  /////
    
    @IBOutlet weak var imgs: UIImageView!
    
    @IBOutlet weak var delbtn: UIButton!
    
    @IBOutlet weak var ticketimg: UIImageView!
    
    @IBOutlet weak var delticketimg: UIButton!
    
    @IBOutlet weak var visblur: UIVisualEffectView!
    
    /////  Article Listing  /////
    
     @IBOutlet weak var artblur: UIImageView!
    
    @IBOutlet weak var artimg: UIImageView!
    
    @IBOutlet weak var arttitle: UILabel!
    
    @IBOutlet weak var artauthor: UILabel!
    
    @IBOutlet weak var artdatelbl: UILabel!
    
    
    /////  Article Details Related  /////
    
    @IBOutlet weak var relartimg: UIImageView!
    
    @IBOutlet weak var relartblur: UIImageView!
    
    @IBOutlet weak var relarttitle: UILabel!
    
    @IBOutlet weak var relauth: UILabel!
    
   
    /// Submit ticket ///
    
    @IBOutlet weak var subvisblur: UIVisualEffectView!
    
    
}
