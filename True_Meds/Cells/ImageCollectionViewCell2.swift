//
//  ImageCollectionViewCell2.swift
//  True_Meds
//


import UIKit

class ImageCollectionViewCell2: UICollectionViewCell {
    
    @IBOutlet weak var issueimgs: UIImageView!
    
    @IBOutlet weak var issueimgdel: UIButton!
    
    @IBOutlet weak var issuevisblur: UIVisualEffectView!
    
}
