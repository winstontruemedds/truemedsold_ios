//
//  SearchMedTableViewCell.swift
//  True_Meds
//


import UIKit

class SearchMedTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var discountPectLb: UILabel!
    
    @IBOutlet weak var mednamelbl: UILabel!
    
    @IBOutlet weak var varlbl: UILabel!
    
    @IBOutlet weak var addmedbtn: UIButton!
    
    @IBOutlet weak var quanstack: UIStackView!
    
    @IBOutlet weak var rembtno: UIButton!
    
    @IBOutlet weak var min: UIButton!
    
    @IBOutlet weak var qtylbl: UILabel!
    
    @IBOutlet weak var plus: UIButton!
    
    @IBOutlet weak var noslbl: UILabel!
    
    @IBOutlet weak var medmainlbl: UILabel!
    
    @IBOutlet weak var mediprice: UILabel!
    
    @IBOutlet weak var mrpPrice: UILabel!
    
    @IBOutlet weak var cancelView: UIView!
    
    @IBOutlet weak var nislbl: UILabel!
    
    @IBOutlet weak var rupeesymbol: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  

}
