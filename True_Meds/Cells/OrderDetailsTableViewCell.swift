//
//  OrderDetailsTableViewCell.swift
//  True_Meds
//


import UIKit

class OrderDetailsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var bgview: UIView!
    
    @IBOutlet weak var removebtno: UIButton!
    
    @IBOutlet weak var amstack: UIStackView!
    
    @IBOutlet weak var addquan: UIButton!
    
    @IBOutlet weak var minusquan: UIButton!
    
    @IBOutlet weak var quanlbl: UILabel!
    
    @IBOutlet weak var blurview: UIVisualEffectView!
    
    @IBOutlet weak var addmed: UIButton!
    
    //// order details ////
    
    @IBOutlet weak var submedname: UILabel!
    
    @IBOutlet weak var submedqty: UILabel!
    
    @IBOutlet weak var submedprice: UILabel!
    
    @IBOutlet weak var ormedname: UILabel!
    
    @IBOutlet weak var ormedprice: UILabel!
    
    //// order details summary ////
    
    @IBOutlet weak var summedname: UILabel!
    
    @IBOutlet weak var summedqty: UILabel!
    
    @IBOutlet weak var summedprice: UILabel!
    
    @IBOutlet weak var sumormedname: UILabel!
    
    @IBOutlet weak var sumormedprice: UILabel!
    
   //// Reorder details ////
    
    @IBOutlet weak var redsubname: UILabel!
    
    @IBOutlet weak var redsubqty: UILabel!
    
    @IBOutlet weak var redorgname: UILabel!
    
    @IBOutlet weak var redsubprice: UILabel!
    
    @IBOutlet weak var redorgprice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
