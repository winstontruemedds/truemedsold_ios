//
//  InfoSubstitutesTableViewCell.swift
//  True_Meds
//
//  Created by Office on 12/9/19.
//

import UIKit

class InfoSubstitutesTableViewCell: UITableViewCell {

    ////  Info On Substitutes Details  / / / /
    
    @IBOutlet weak var compositionlbl: UILabel!
    
    @IBOutlet weak var compositionviu: UIView!
    
    @IBOutlet weak var recmedname: UILabel!
    
    @IBOutlet weak var recmedisprice: UILabel!
    
    @IBOutlet weak var recmedprice: UILabel!
    
    @IBOutlet weak var recmedcompany: UILabel!
    
    @IBOutlet weak var presmedname: UILabel!
    
    @IBOutlet weak var presmedisprice: UILabel!
    
    @IBOutlet weak var presmedprice: UILabel!
    
    @IBOutlet weak var presmedcompany: UILabel!
    
    @IBOutlet weak var infocardviu: UIView!
    
    ////  Info On Substitutes List  / / / /
    
    @IBOutlet weak var subslistmainviu: UIView!
    
    @IBOutlet weak var ordernolbl: UILabel!
    
    @IBOutlet weak var subslistmedicine: UILabel!
    
    @IBOutlet weak var subslistprice: UILabel!
    
    @IBOutlet weak var subslistdatelbl: UILabel!
    
    @IBOutlet weak var subslistsaving: UILabel!
    
    @IBOutlet weak var subsliststatus: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
