//
//  DosageTableViewCell.swift
//  True_Meds
//


import UIKit

class DosageTableViewCell: UITableViewCell {

    
    @IBOutlet weak var medcinename: UILabel!
    
    @IBOutlet weak var packetsize: UILabel!
    
    @IBOutlet weak var summedcinename: UILabel!
    
    @IBOutlet weak var sumpacketsize: UILabel!
    
    @IBOutlet weak var morcount: UILabel!
    
    @IBOutlet weak var morimg: UIImageView!
    
    @IBOutlet weak var morchrono: UILabel!
    
    @IBOutlet weak var morfreq: UILabel!
    
    @IBOutlet weak var morview: UIView!
    
    @IBOutlet weak var aftcount: UILabel!
    
    @IBOutlet weak var aftimg: UIImageView!
    
    @IBOutlet weak var aftchrono: UILabel!
    
    @IBOutlet weak var aftfreq: UILabel!
    
    @IBOutlet weak var aftview: UIView!
    
    @IBOutlet weak var evecount: UILabel!
    
    @IBOutlet weak var eveimg: UIImageView!
    
    @IBOutlet weak var evechrono: UILabel!
    
    @IBOutlet weak var evefreq: UILabel!
    
    @IBOutlet weak var eveview: UIView!
    
    @IBOutlet weak var nigcount: UILabel!
    
    @IBOutlet weak var nigimg: UIImageView!
    
    @IBOutlet weak var nigchrono: UILabel!
    
    @IBOutlet weak var nigfreq: UILabel!
    
    @IBOutlet weak var nigview: UIView!
    
    @IBOutlet weak var onlcount: UILabel!
    
    @IBOutlet weak var onlimg: UIImageView!
    
    @IBOutlet weak var onlchrono: UILabel!
    
    @IBOutlet weak var onlfreq: UILabel!
    
    @IBOutlet weak var onlview: UIView!
    
    @IBOutlet weak var scroheight: NSLayoutConstraint!
    
    ///// order description /////
    
    @IBOutlet weak var morcount2: UILabel!
    
    @IBOutlet weak var morimg2: UIImageView!
    
    @IBOutlet weak var morchrono2: UILabel!
    
    @IBOutlet weak var morfreq2: UILabel!
    
    @IBOutlet weak var morview2: UIView!
    
    @IBOutlet weak var aftcount2: UILabel!
    
    @IBOutlet weak var aftimg2: UIImageView!
    
    @IBOutlet weak var aftchrono2: UILabel!
    
    @IBOutlet weak var aftfreq2: UILabel!
    
    @IBOutlet weak var aftview2: UIView!
    
    @IBOutlet weak var evecount2: UILabel!
    
    @IBOutlet weak var eveimg2: UIImageView!
    
    @IBOutlet weak var evechrono2: UILabel!
    
    @IBOutlet weak var evefreq2: UILabel!
    
    @IBOutlet weak var eveview2: UIView!
    
    @IBOutlet weak var nigcount2: UILabel!
    
    @IBOutlet weak var nigimg2: UIImageView!
    
    @IBOutlet weak var nigchrono2: UILabel!
    
    @IBOutlet weak var nigfreq2: UILabel!
    
    @IBOutlet weak var nigview2: UIView!
    
    @IBOutlet weak var onlcount2: UILabel!
    
    @IBOutlet weak var onlimg2: UIImageView!
    
    @IBOutlet weak var onlchrono2: UILabel!
    
    @IBOutlet weak var onlfreq2: UILabel!
    
    @IBOutlet weak var onlview2: UIView!
    
    @IBOutlet weak var scroheight2: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    

}
