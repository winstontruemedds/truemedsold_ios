//
//  PresImgTableViewCell.swift
//  True_Meds
//


import UIKit

class PresImgTableViewCell: UITableViewCell {

    @IBOutlet weak var RxImages: UIImageView!
    
    @IBOutlet weak var doctname: UILabel!
    
    @IBOutlet weak var doctspec: UILabel!
    
    @IBOutlet weak var doctexp: UILabel!
    
    @IBOutlet weak var doctqual: UILabel!
    
    @IBOutlet weak var doctimage: UIImageView!
    
    @IBOutlet weak var doctmantra: UILabel!
    
    @IBOutlet weak var star1: UIImageView!
    
    @IBOutlet weak var star2: UIImageView!
    
    @IBOutlet weak var star3: UIImageView!
    
    @IBOutlet weak var star4: UIImageView!
    
    @IBOutlet weak var star5: UIImageView!
    
    /// transaction view
    
    @IBOutlet weak var trdate: UILabel!
    
    @IBOutlet weak var trname: UILabel!
    
    @IBOutlet weak var trspent: UILabel!
    
    @IBOutlet weak var trearned: UILabel!
    
    @IBOutlet weak var trbalance: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
