//
//  MenuTableViewCell.swift
//  True_Meds
//


import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var mlbl: UILabel!
    
    @IBOutlet weak var menudesic: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
