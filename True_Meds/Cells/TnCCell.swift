//
//  TnCCell.swift
//  True_Meds
//


import UIKit

class TnCCell: UITableViewCell {

    @IBOutlet weak var tncheader: UILabel!
    
    @IBOutlet weak var tncdesc: UITextView!
    
    @IBOutlet weak var selectqtylbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
