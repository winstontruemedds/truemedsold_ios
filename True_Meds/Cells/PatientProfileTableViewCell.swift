//
//  PatientProfileTableViewCell.swift
//  True_Meds
//


import UIKit

class PatientProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var patpropic: UIImageView!
    
    @IBOutlet weak var reportnamelbl: UILabel!
    
    @IBOutlet weak var reportdatelbl: UILabel!
    
    @IBOutlet weak var bgview: UIView!
    
    @IBOutlet weak var apnamelbl: UILabel!
    
    @IBOutlet weak var agelbl: UILabel!
    
    @IBOutlet weak var genderlbl: UILabel!
    
    @IBOutlet weak var relationname: UILabel!
    
    @IBOutlet weak var greenarrimg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
