//
//  SelectOrderTicketTableViewCell.swift
//  True_Meds
//


import UIKit
import SVGKit

class SelectOrderTicketTableViewCell: UITableViewCell {

    @IBOutlet weak var mednlbl: UILabel!
    
    @IBOutlet weak var mednoslbl: UILabel!
    
    @IBOutlet weak var medamt: UILabel!
    
    @IBOutlet weak var medordcheck: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
