//
//  AgeGroupTableViewCell.swift
//  True_Meds
//


import UIKit

class AgeGroupTableViewCell: UITableViewCell {

    @IBOutlet weak var seniorbtno: UIButton!
    
    @IBOutlet weak var noseniorbtno: UIButton!
    
    @IBOutlet weak var qlbl3: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        seniorbtno.layer.borderWidth = 0.8
        seniorbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        seniorbtno.layer.cornerRadius = 4.0
        
        noseniorbtno.layer.borderWidth = 0.8
        noseniorbtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        noseniorbtno.layer.cornerRadius = 4.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
