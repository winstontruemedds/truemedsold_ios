//
//  LegalTableViewCell.swift
//  True_Meds
//


import UIKit

class LegalTableViewCell: UITableViewCell {

    /////  Legal  /////
    
    @IBOutlet weak var legic: UIImageView!
    
    @IBOutlet weak var legmainlbl: UILabel!
    
    @IBOutlet weak var legsublbl: UILabel!
    
    @IBOutlet weak var legrearr: UIImageView!
    
    
    /////  Privacy Policy  /////
    
    @IBOutlet weak var pheader: UILabel!
    
    @IBOutlet weak var pdesc: UILabel!
    
    /////  Terms & Conditions  /////
    
    
    
    /////  About Us  /////
    
    @IBOutlet weak var aboutheader: UILabel!
    
    @IBOutlet weak var aboutdesc: UILabel!
    
    
  
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
