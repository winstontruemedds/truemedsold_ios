//
//  OptionTableViewCell.swift
//  True_Meds
//


import UIKit

class OptionTableViewCell: UITableViewCell {

    @IBOutlet weak var langselbtno: UIButton!
    
    @IBOutlet weak var ageselbtno: UIButton!
    
    @IBOutlet weak var editprobtno: UIButton!
    
    @IBOutlet weak var questext: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        langselbtno.layer.borderWidth = 0.8
        langselbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        langselbtno.layer.cornerRadius = 4.0
        
        
        ageselbtno.layer.borderWidth = 0.8
        ageselbtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        ageselbtno.layer.cornerRadius = 4.0
        
        
        editprobtno.layer.borderWidth = 0.8
        editprobtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        editprobtno.layer.cornerRadius = 4.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
