//
//  InfoMedicineTableViewCell.swift
//  True_Meds
//


import UIKit
import SVGKit

class InfoMedicineTableViewCell: UITableViewCell{

   
    @IBOutlet weak var addqty: UIButton!
    
    @IBOutlet weak var minqty: UIButton!
    
    @IBOutlet weak var qtylbl: UILabel!
    
    @IBOutlet weak var addcartbtno: UIButton!
    
    @IBOutlet var stackviu: [UIStackView]!
    

    
    @IBOutlet weak var infomednamelbl: UILabel!
    
    @IBOutlet weak var infomedcomp: UILabel!
    
    @IBOutlet weak var infopresreq: UILabel!
    
    @IBOutlet weak var infocompos: UILabel!
    
    
    @IBOutlet weak var infomedsize: UILabel!
    
    @IBOutlet weak var infomedprice: UILabel!
    
    @IBOutlet weak var infopresamimg: UIImageView!
    
    ////
    
    @IBOutlet weak var morcount: UILabel!
    
    @IBOutlet weak var morimg: UIImageView!
    
    @IBOutlet weak var morchrono: UILabel!
    
    @IBOutlet weak var morfreq: UILabel!
    
    @IBOutlet weak var morview: UIView!
    
    @IBOutlet weak var morcons: NSLayoutConstraint!
    
    @IBOutlet weak var aftcount: UILabel!
    
    @IBOutlet weak var aftimg: UIImageView!
    
    @IBOutlet weak var aftchrono: UILabel!
    
    @IBOutlet weak var aftfreq: UILabel!
    
    @IBOutlet weak var aftview: UIView!
    
    @IBOutlet weak var aftcons: NSLayoutConstraint!
    
    @IBOutlet weak var evecount: UILabel!
    
    @IBOutlet weak var eveimg: UIImageView!
    
    @IBOutlet weak var evechrono: UILabel!
    
    @IBOutlet weak var evefreq: UILabel!
    
    @IBOutlet weak var eveview: UIView!
    
    @IBOutlet weak var evecons: NSLayoutConstraint!
    
    @IBOutlet weak var nigcount: UILabel!
    
    @IBOutlet weak var nigimg: UIImageView!
    
    @IBOutlet weak var nigchrono: UILabel!
    
    @IBOutlet weak var nigfreq: UILabel!
    
    @IBOutlet weak var nigview: UIView!
    
    @IBOutlet weak var nigcons: NSLayoutConstraint!
    
    @IBOutlet weak var onlcount: UILabel!
    
    @IBOutlet weak var onlimg: UIImageView!
    
    @IBOutlet weak var onlchrono: UILabel!
    
    @IBOutlet weak var onlfreq: UILabel!
    
    @IBOutlet weak var onlview: UIView!
    
    @IBOutlet weak var onlcons: NSLayoutConstraint!
    
    @IBOutlet weak var scroheight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    
    
    
    
    
}
