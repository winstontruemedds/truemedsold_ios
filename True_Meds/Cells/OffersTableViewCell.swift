//
//  OffersTableViewCell.swift
//  True_Meds
//


import UIKit

class OffersTableViewCell: UITableViewCell {

    @IBOutlet weak var titlelbl: UILabel!
    
    @IBOutlet weak var desclbl: UILabel!
    
    @IBOutlet weak var datelbl: UILabel!
    
    @IBOutlet weak var logoimg: UIImageView!
    
    @IBOutlet weak var applybtno: UIButton!
    
    @IBOutlet weak var viewdetailbtno: UIButton!
    
    @IBOutlet weak var offerviu: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
