//
//  Type.swift
//  True_Meds
//


import UIKit

class Type: NSObject {

//    var name: String
//    var image_url: String
//    var quantity: Int
//
//    init(name: String, image_url: String, quantity: Int) {
//        self.name = name
//        self.image_url = image_url
//        self.quantity = quantity
//    }
    
    
    var imgUrl : String!
    var name : String!
    var quantity : Int!
    
    init(dictionary: [String:Any])
    {
        imgUrl = dictionary["img_url"] as? String
        name = dictionary["name"] as? String
        quantity = dictionary["quantity"] as? Int
    }
    init(name: String, image_url: String, quantity: Int)
    {
        self.name = name
        self.imgUrl = image_url
        self.quantity = quantity
    }
    public class func modelsFromArray(array:[[String:Any]]) -> [Type]
    {
        var models:[Type] = []
        for item in array
        {
            models.append(Type.init(dictionary:item))
        }
        return models
    }
    
    
    
}
