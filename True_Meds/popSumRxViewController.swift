//
//  popSumRxViewController.swift
//  True_Meds
//
//  Created by Office on 12/31/19.
//

import UIKit


class popSumRxViewController: UIViewController {

    
    var rxImageUrl = String()
    
    @IBOutlet weak var rxImageView: UIImageView!
    
    @IBOutlet weak var cancelBtm: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelBtm.layer.cornerRadius = 20
        rxImageView.layer.cornerRadius = 20
        
       // rightBottom(object: rxImageView)
        showRxImage()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        showAnimate()
        // Do any additional setup after loading the view.
    }
    

    func rightBottom(object:UIImageView){
           if #available(iOS 11.0, *){
               object.clipsToBounds = false
               object.layer.cornerRadius = 20
               object.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner,.layerMinXMaxYCorner]
           }else{
               let rectShape = CAShapeLayer()
               rectShape.bounds = object.frame
               rectShape.position = object.center
               rectShape.path = UIBezierPath(roundedRect: object.bounds,    byRoundingCorners: [.topLeft , .topRight , .bottomLeft], cornerRadii: CGSize(width: 20, height: 20)).cgPath
               object.layer.mask = rectShape
           }
         
          
           
       }
    
    func showRxImage(){
        let imgurl:URL = URL(string: rxImageUrl)!
        rxImageView.sd_setImage(with: imgurl, completed: nil)
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
                  self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
        },completion: {(finished:Bool) in
            if (finished){
                self.view.removeFromSuperview()
            }
            
        })
        
    }
    
    @IBAction func closePress(_ sender: Any) {
        removeAnimate()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
