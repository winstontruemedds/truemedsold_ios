//
//  ECartPOP.swift
//  True_Meds
//
//  Created by Mangesh Toraskar on 19/11/19.
//

import UIKit

class ECartPOP: UIViewController {

    @IBOutlet weak var ecAddmedbtno: UIButton!
    
    @IBOutlet weak var ECtextlbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ecAddmedbtno.layer.borderWidth = 0.8
        ecAddmedbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        ecAddmedbtno.layer.cornerRadius = 4.0
        
        ECtextlbl.text = NSLocalizedString("Please add items to cart before placing an order", comment: "")
        
        ecAddmedbtno.setTitle(NSLocalizedString("Add Medicines", comment: ""), for: .normal)

        //self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        //self.view.alpha = 0
        UIView.animate(withDuration: 0.25, animations: {
       // self.view.alpha = 1.0
        self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            //self.view.alpha = 0
        },completion: {(finished:Bool) in
            if (finished){
                self.view.removeFromSuperview()
            }
        })
    }
        
    
    
    @IBAction func Addmedsbtna(_ sender: Any) {
        removeAnimate()
    }
    
}
