//
//  UnexpectedPOP.swift
//  True_Meds
//
//  Created by Office on 11/18/19.
//

import UIKit

class UnexpectedPOP: UIViewController {

    @IBOutlet weak var eeTryagainbtno: UIButton!
    
    @IBOutlet weak var UEtextlbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eeTryagainbtno.layer.borderWidth = 0.8
        eeTryagainbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        eeTryagainbtno.layer.cornerRadius = 4.0

        UEtextlbl.text = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
        
        eeTryagainbtno.setTitle(NSLocalizedString("Try Again", comment: ""), for: .normal)
        
        //self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        //self.view.alpha = 0
        UIView.animate(withDuration: 0.25, animations: {
       // self.view.alpha = 1.0
        self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            //self.view.alpha = 0
        },completion: {(finished:Bool) in
            if (finished){
                self.view.removeFromSuperview()
            }
        })
    }
    
   
    @IBAction func TryAgainbtna(_ sender: Any) {
        removeAnimate()
    }
    

}
