//
//  medTableViewCell.swift
//  summary
//
//  Created by Office on 11/12/19.
//  Copyright © 2019 Office. All rights reserved.
//

import UIKit

class medTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var pack: UILabel!
    @IBOutlet weak var mrp: UILabel!
    @IBOutlet weak var discountP: UILabel!
    @IBOutlet weak var qty: UILabel!
    @IBOutlet weak var medName: UILabel!
    @IBOutlet weak var mrpAfterDisc: UILabel!
    
    
    
    @IBOutlet weak var minus: UIButton!
    
    @IBOutlet weak var plus: UIButton!
    @IBOutlet weak var remove: UIButton!
    
    
    @IBOutlet weak var quantityLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
