//
//  summaryViewController.swift
//  summary
//
//  Created by Office on 11/4/19.
//  Copyright © 2019 Office. All rights reserved.
//

import UIKit
import Alamofire
import iOSDropDown
import SVGKit


class summaryViewController: UIViewController {
    

    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
   var resdict = NSDictionary()
    
    var Array = NSArray()
    var ArrayC = NSArray()
    var MedicineArray = NSArray()
    var collectionTag = Int()
    
    var OfferDetails = NSArray()
     var mssg = String()
    
    let baseUr = getAllPatientsOrderDetails()
    
    var localizedContent = NSLocalizedString(String(), comment: String())
    
    var myMutableDictUpdateQty = NSMutableDictionary()
    
     var orderID:CLong! = 0
    
    var StatusId:CLong! = 0
     
    var paymentId:CLong! = 0
    
    var offerId:CLong! = 0
    
    var deliveryChrges:CLong! = 0
    
    @IBOutlet weak var patientNmaeLbl: UILabel!
    
    @IBOutlet weak var addedPxView: UIView!
    
    @IBOutlet weak var offerView: UIView!
    @IBOutlet weak var offerViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var offerTitleLbl: UILabel!
    @IBOutlet weak var offerDiscriptionLbl: UILabel!
    

    @IBOutlet weak var estTotalView: UIView!
    @IBOutlet weak var totalView: UIView!
    
    
    
    
    @IBOutlet weak var estDelivery: UILabel!
    
    @IBOutlet weak var orderTypeDrop: DropDown!
    @IBOutlet weak var ordertypeDropBtn: UIButton!
    
    @IBOutlet weak var patientDetailsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var medicineDetailsViewHeight: NSLayoutConstraint!
   
    
      
       @IBOutlet weak var medicineBtn: UIButton!
       
       @IBOutlet weak var patientDetailsView: UIView!
       @IBOutlet weak var medicineDetailsView: UIView!
    
     fileprivate var tableViewCellCoordinator: [Int: IndexPath] = [:]
   
    
    
    @IBOutlet weak var rxCollectionView: UICollectionView!
    
    @IBOutlet weak var medicineTableView: UITableView!
    
   
    @IBOutlet weak var deliveryChargesLbl: UILabel!
    
    @IBOutlet weak var dMrp: UILabel!
    
  
    
    @IBOutlet weak var mrp: UILabel!
    @IBOutlet weak var dictP: UILabel!
    
    @IBOutlet weak var addLbl: UILabel!
    
    @IBOutlet weak var addChangeBtn: UIButton!
    @IBOutlet weak var discartAndConfirmBtnView: UIView!
    
    @IBOutlet weak var confirmOrderBtn: UIButton!
    @IBOutlet weak var dicardBtn: UIButton!
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var modeOfPaymentLbl: UILabel!
    
    @IBOutlet weak var modeOfPaymentDropView: UIView!
    
    var odhelpdictsumm = NSDictionary()
    

    override func viewDidLoad() {
        super.viewDidLoad()
      
        print(odhelpdictsumm)
        let svgimg: SVGKImage = SVGKImage(named: "back")
               backBtn.setImage(svgimg.uiImage, for: .normal)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
          self.rxCollectionView.collectionViewLayout = flowLayout
        
   
        
        
        if odhelpdictsumm.value(forKey: "orderId") != nil{
            orderID = odhelpdictsumm.value(forKey: "orderId") as? CLong
           
        }else{
            if UserDefaults.standard.value(forKey:"OrderId") != nil
            {
            orderID = UserDefaults.standard.value(forKey:"OrderId") as? CLong
            }
                                       
        }
         print(orderID!)
        
    
        if odhelpdictsumm.value(forKey: "statusId") != nil{
           StatusId = odhelpdictsumm.value(forKey: "statusId") as? CLong
        }
        print(StatusId)
        
        
  
        
        
        
        patientDetailsView.isHidden = false
        medicineDetailsView.isHidden = false

        //callSummaryDetails()
        
        getSummaryOrder()
        
        setDropDown()
        blueBtn(object: confirmOrderBtn)
        redBtn(object: dicardBtn)
        
        if StatusId == 1 || StatusId == 2 || StatusId == 39 || StatusId == 57{
              addChangeBtn.isHidden = true
            discartAndConfirmBtnView.isHidden = true
            modeOfPaymentLbl.isHidden = false
            modeOfPaymentDropView.isHidden = true

          }else{
              addChangeBtn.isHidden = false
              discartAndConfirmBtnView.isHidden = false
             modeOfPaymentLbl.isHidden = true
              modeOfPaymentDropView.isHidden = false

              }
        
        
       
      
        // Do any additional setup after loading the view.
    }
    
    
    
    
 
    
    
    override func viewDidLayoutSubviews() {
           super.viewDidLayoutSubviews()
           
           
           self.view.layoutIfNeeded()
       
       }
    
    
    
    func redBtn(object:UIButton){
           object.layer.cornerRadius = 6
           object.layer.borderWidth = 1
           object.layer.borderColor = UIColor.init(hex: 0xFF0000).cgColor
           object.setTitleColor(.init(hex: 0xFF0000), for: .normal)
           object.titleLabel?.font = UIFont.OpenSans(.semibold,size: 15)
          
       }
    
     func setDropDown(){
         orderTypeDrop.layer.borderWidth = 0
         orderTypeDrop.borderStyle = .none
         orderTypeDrop.textAlignment = .right
         orderTypeDrop.font =  UIFont.OpenSans(.semibold,size: 15)
         orderTypeDrop.arrowColor = UIColor.init(hex: 0x0071BC)
         orderTypeDrop.textColor = UIColor.init(hex: 0x0071BC)
         orderTypeDrop.optionArray = [NSLocalizedString("Online", comment: ""),NSLocalizedString("Cash on Delivery", comment: "")]
        
       
         orderTypeDrop.optionIds = [16,17]
      
        
         orderTypeDrop.selectedRowColor = UIColor.init(hex: 0x22B573)
         
            self.orderTypeDrop.listWillAppear() {
            print("//You can Do anything when iOS DropDown willAppear")

         }

        self.orderTypeDrop.listDidAppear() {
             print("//You can Do anything when iOS DropDown listDidAppear")

        }

        self.orderTypeDrop.listWillDisappear() {
             print("//You can Do anything when iOS DropDown listWillDisappear")

        }

         self.orderTypeDrop.listDidDisappear() {
            
            print("//You can Do anything when iOS DropDown listDidDisappear")

            if self.paymentId == 16
            {
             self.paymentId = 16
             self.orderTypeDrop.text = NSLocalizedString("Online", comment: "")
            }
            else
            {
            self.paymentId = 17
            self.orderTypeDrop.text = NSLocalizedString("Cash on Delivery", comment: "")
            }
            
        }

        
         orderTypeDrop.didSelect{(selectedText , index ,id) in
             // self.valueLabel.text = "Selected String: \(selectedText) \n index: \(index)"
            
            if self.paymentId == id && self.offerId != 0
           {
            self.paymentId = id
            print(id)
            self.savePaymentAndCouponForOrder()
            self.orderTypeDrop.listWillDisappear {
            self.orderTypeDrop.text = ""
                
            }
           }
           else if self.paymentId != id && self.offerId == 0
           {
           self.paymentId = id
           print(id)
           self.savePaymentAndCouponForOrder()
           self.orderTypeDrop.listWillDisappear {
           self.orderTypeDrop.text = ""
           }
           }
           else if self.paymentId == id
           {
                self.paymentId = id
                print(id)
                self.savePaymentAndCouponForOrder()
                self.orderTypeDrop.listWillDisappear {
                self.orderTypeDrop.text = ""
            }
           }
           else
           {
            self.orderTypeDrop.text = ""
            Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("This promo code is not applicable.", comment: ""), onVC: self)
        
           }
        }
         
     }
    

    func getSummaryOrder()
    {
        let parameters: Parameters = ["orderId": String(describing:orderID!)
        ] as [String : Any]
                
                ApiManager().requestApiWithDataType( methodType: HGET, urlString: getAllPatientsOrderDetails(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
                    
                    if cStatus == 200 || cStatus == 201
                    {
                     self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
                        
                        if self.resdict.value(forKey: "patientsDetails") != nil{
                            self.Array = self.resdict.value(forKey: "patientsDetails") as? NSArray ?? []
                            let dict = self.Array[0] as! NSDictionary
                            self.patientNmaeLbl.text = dict.value(forKey: "patientNmaeLbl") as? String ?? ""
                            self.ArrayC = dict.value(forKey: "images") as! NSArray
                            
                          
                            
                            print(" imageArray \(self.ArrayC)")
                            
                            
                                                        }else{
                                                            self.Array = []
                            
                                                        }
                                                   
                        if self.resdict.value(forKey: "medicineDetails") != nil{
                            self.MedicineArray = self.resdict.value(forKey: "medicineDetails") as? NSArray ?? []
                                                        }else{
                                                             self.MedicineArray = []
                                                        }
                                                        
                                                     
                                                        
                        if self.resdict.value(forKey: "TotalMrp") != nil{
                                                            
                            let mrp1 = self.resdict.value(forKey: "TotalMrp") as? String ?? ""
                                                            self.mrp.text = "MRP ₹ " +  mrp1
                                                            self.estTotalView.isHidden = false
                                                            self.totalView.isHidden = true
                                                        }else{
                                                            self.mrp.text = "MRP ₹ " +  "0"
                                                            self.estTotalView.isHidden = true
                                                            self.totalView.isHidden = false
                                                        }
                                                        
                        if self.resdict.value(forKey: "TotalMrpAfterDiscount") != nil{
                                                        let dmrp1 = self.resdict.value(forKey: "TotalMrpAfterDiscount") as? String ?? ""
                                                            self.dMrp.text = "₹ " +  dmrp1
                                                          
                                                                                      }else{
                                                            self.dMrp.text = "₹ " + "0"
                                                           
                                                                                      }
                                                        
                                                        if self.resdict.value(forKey: "AverageDiscountPercent") != nil{
                                                        let dictp1 = self.resdict.value(forKey: "AverageDiscountPercent") as? String ?? ""
                                                            self.dictP.text =  dictp1 + NSLocalizedString("% OFF", comment: "")
                                                                                      }else{
                                                            self.dictP.text =  "0" + NSLocalizedString("% OFF", comment: "")
                                                                                      }
                                                    
                                                        
                                                        if self.resdict.value(forKey: "offerId") != nil{
                                                            print(self.resdict)
                                                            self.offerId = self.resdict.value(forKey: "offerId") as? CLong ?? 0
                                                                }else{
                                                                    self.offerId = 0
                                                                }
                                                        
                                                        if self.resdict.value(forKey: "edd") != nil{
                                                            self.estDelivery.text = self.resdict.value(forKey: "edd") as? String ?? ""
                                                            }else{
                                                            self.estDelivery.text = ""
                                                            }
                                                        
                                                        
                                                        
                                                        if self.offerId == 0 {
                                                            
                                                            self.offerView.isHidden = true
                                                                                                                          self.offerViewHeight.constant = 60
                                                            
                                                           
                                                            }else{
                                                            
                                                            if UserDefaults.standard.value(forKey: "offertitel") as? String != ""
                                                               {
                                                                self.offerTitleLbl.text = UserDefaults.standard.value(forKey: "offertitel") as? String ?? ""
                                                                self.offerView.isHidden = false
                                                                self.offerViewHeight.constant = 140
                                                               }else{
                                                                self.offerView.isHidden = true
                                                                self.offerViewHeight.constant = 60
                                                               }
                                                               
                                                               
                                                               if UserDefaults.standard.value(forKey: "descriptionCoupen") != nil
                                                               {
                                                                self.offerDiscriptionLbl.text = UserDefaults.standard.value(forKey: "descriptionCoupen") as? String
                                                               }else{
                                                                   
                                                               }
                                                          
                                                            
                                                            if self.resdict.value(forKey: "OfferDetails") != nil{
                                                        self.OfferDetails = self.resdict.value(forKey: "OfferDetails") as? NSArray ?? []
                                                        print("offerDetails\(self.OfferDetails)")
                                                        let arr = self.OfferDetails[0] as? NSDictionary
                                                                                       
                                                        }else{
                                                        self.OfferDetails = []
                                                        }
                                                    }
                                                        
                                                     
                                                        
                                                   
                                                      
                                                      
                                                        
                                                         print("PatientCount\(self.Array.count)")
                                                         print("MedicineArraycount\(self.MedicineArray.count)")
                        
                        DispatchQueue.main.async {
                             if self.Array.count == 1 && self.MedicineArray.count == 0{
                                                print("Order Cancel")
                                    let dict = self.Array[0] as? NSDictionary
                                let imageArray = dict?.value(forKey: "images") as? NSArray ?? []
                                    if imageArray == []{
                                         self.navigationController?.popViewController(animated: false)
                                        NotificationCenter.default.post(name: Notification.Name("noItemFoundOnsummary"), object: nil)
                                           
                                                                                       }
                        }
                                                        
                                                       
//
//                      UserDefaults.standard.set(0, forKey: "offerId")
//                                                                              UserDefaults.standard.set("NotFromSummary", forKey: "Summary")
//                                                                              UserDefaults.standard.removeObject(forKey:"OrderId")
//                                                                                  UserDefaults.standard.synchronize()
                       
                                                            
                                                               
                                                            
                                                            
                                                            
                                                        }
                                                   
                                                       self.addLbl.text = self.resdict.value(forKey: "addressDetails") as? String ?? ""
                                                        
                                                        self.deliveryChrges = self.resdict.value(forKey: "deliveryCharges") as? CLong ?? 0
                                                        
                                                        if self.deliveryChrges != 0{
                                                           self.deliveryChargesLbl.text = "\(self.deliveryChrges)"
                                                        }else{
                                                           self.deliveryChargesLbl.text = NSLocalizedString("Free", comment: "")
                                                        }
                                                       
                                                        
                                                        self.paymentId =  self.resdict.value(forKey: "PaymentTypeId") as? CLong ?? 0
                                                        
                                                        if  self.paymentId  == 16{
                                                            self.orderTypeDrop.text = NSLocalizedString("Online", comment: "")
                                                            self.modeOfPaymentLbl.text = NSLocalizedString("Online", comment: "")
                                                            
                                                        }
                                                        if  self.paymentId  == 17{
                                                             self.orderTypeDrop.text = NSLocalizedString("Cash on Delivery", comment: "")
                                                           self.modeOfPaymentLbl.text = NSLocalizedString("Cash on Delivery", comment: "")
                                                        }
                                                       
                                                        
                                                      
                                            
                                                    DispatchQueue.main.async {
//
                                                        
                                                        if self.ArrayC.count == 0{
                                                             self.patientDetailsViewHeight.constant = 50
                                                            self.addedPxView.backgroundColor = .white
                                                            self.patientDetailsView.backgroundColor = .white
                                                            self.rxCollectionView.backgroundColor = .white
                                                                                                                                             
                                                        }else{
                                                             self.patientDetailsViewHeight.constant = 140
                                                            self.addedPxView.backgroundColor =  UIColor.init(hex: 0xE5F1F8)
                                                            self.patientDetailsView.backgroundColor = UIColor.init(hex: 0xE5F1F8)
                                                            self.rxCollectionView.backgroundColor = UIColor.init(hex: 0xE5F1F8)
                                                        }
                                                        
                                                        if self.MedicineArray.count == 0{
                                                            self.medicineDetailsViewHeight.constant =  100
                                                            self.medicineBtn.isHidden = true
                                                                                       
                                                            }else{
                                                               self.medicineBtn.isHidden = false
                                                            self.medicineDetailsViewHeight.constant = CGFloat( self.MedicineArray.count) * 140 + 50
                                                                                      }
                                                     
                                                        self.medicineTableView.reloadData()
                                                        self.rxCollectionView.reloadData()
                        }
                        
                    }
                    else if cStatus == 401
                    {
                        let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                        Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                    }
                    else if cStatus == 500
                    {
       let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
       
       self.addChild(popupVC)
       popupVC.view.frame = self.view.frame
       self.view.addSubview(popupVC.view)
       popupVC.didMove(toParent: self)
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                    
                }
    }
    
    
   
    
    
    func editMedicine(){
         let baseUrl = editMeds()
                     
        let queryStringParam  =  ["orderId": String(describing:orderID!)
                            ] as [String : Any]
            print(queryStringParam)
                        var urlComponent = URLComponents(string: baseUrl)!
                        let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value as? String) }
                          urlComponent.queryItems = queryItems
                          
                        let bodyParameters = myMutableDictUpdateQty
        
                      print(bodyParameters)
                        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
                       
                                
                        let headers = ["Content-Type": "application/json",
                                                            "transactionId":"a",
                                                            "Authorization":authtok]
                        
                
                          var request = URLRequest(url: urlComponent.url!)
                          request.httpMethod = "POST"
                          request.httpBody = try? JSONSerialization.data(withJSONObject: bodyParameters)
                          request.allHTTPHeaderFields = headers
                          
                          AF.request(request).responseJSON { response in
        //                    print(response)
                            switch response.result {
                                    case .success(let value):
                                        switch response.response?.statusCode {
                                case 200,201:
                                     print("200")
                                if let dict = value as? NSDictionary {
                                self.getSummaryOrder()
                                    if self.localizedContent == NSLocalizedString("Do you want to cancel this order?", comment: ""){
                                                                              print("oderrrrrrrrrr")
                                                                      
                                                                      UserDefaults.standard.set(0, forKey: "offerId")
                                                                      UserDefaults.standard.set("NotFromSummary", forKey: "Summary")
                                                                      UserDefaults.standard.removeObject(forKey:"OrderId")
                                                                      UserDefaults.standard.synchronize()
                                                                      for controller in self.navigationController!.viewControllers as Array {
                                                                          if controller.isKind(of: buyMeds.self) {
                                                                                                                                
                                                                      self.navigationController!.popToViewController(controller, animated: false)
                                                                                                  break
                                                                                                          }
                                                                                                                        }
                                                                                                                       
                                                                                                                                }
                                print(dict)
                                }
                                case 401:
                                                                       print("401")
                                                                       let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                                                       Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                                                                   case 400:
                                                                       print("400")
                                                                       let adddict = value as? NSDictionary
                                                                       if adddict?.allKeys.first as! String == "400"
                                                                       {
                                                                       Utility.showAlertWithTitle(title: "", andMessage: adddict?.value(forKey: "400") as? String, onVC: self)
                                                                       }
                                                                   case 500:
                                                                       print("500")
                    let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
                                                        
                    self.addChild(popupVC)
                   popupVC.view.frame = self.view.frame
                    self.view.addSubview(popupVC.view)
                    popupVC.didMove(toParent: self)
                                                                   default:
                                                                       print("may be 500")
                                                                       let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                                                                       Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                                                
                                                                   }
                                             case .failure(let error):
                                                 print(error)
                                             }
                                  
                              }
        
        
    }
    
  
        
    
    
//    func finalcalculationsidd(){
//           var mrp1 = Double()
//           var finaldiscount = Double()
//           var discountPrice = Double()
//
//           for i in 0..<MedicineArray.count{
//            //get values
//               let arr = MedicineArray[i] as! NSDictionary
//               let price = arr.value(forKey: "mrp") as? Double
//               let quantity = arr.value(forKey: "medicineQty") as? Double
//               let discountPercentage = arr.value(forKey: "discount") as? Double
//
//            //calculation
//               let dismulti = (100 - discountPercentage!)/100
//               let dicPrice = price! * dismulti
//
//               let dqp = dicPrice * quantity!
//               let qp = price! * quantity!
//
//               mrp1 = mrp1 + qp
//               discountPrice = discountPrice + dqp
//               finaldiscount = ((mrp1 - discountPrice)/mrp1) * 100
//
//           }
//
//           print("siddh\(mrp1)")
//           print("siddh\(discountPrice)")
//           print("siddh\(finaldiscount)")
//
//           dMrp.text = "₹ " + String(format: "%.2f", discountPrice)
//           totalDictPrice.text = "₹ " + String(format: "%.2f", discountPrice)
//           mrp.text = "MRP ₹ " + String(format: "%.2f", mrp1)
//           dictP.text = String(format: "%.0f", finaldiscount) + "% OFF "
//
//       }
            
        
    
    
    
    @objc func minusQty(sender:UIButton){
        print("Minus\(sender.tag)")
        let dict = MedicineArray[sender.tag] as? NSDictionary
        let Qty = dict?.value(forKey: "medicineQty") as? Double
        let updatedQty = Qty! - 1
        
        if updatedQty == 0{
           if self.Array.count == 1 && self.ArrayC.count == 0  && MedicineArray.count == 1{
                localizedContent = NSLocalizedString("Do you want to cancel this order?", comment: "")
            }else{
                localizedContent = NSLocalizedString("Do you want to remove this medicine?", comment: "")
            }
                           
                          let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
                          let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
                        { (action:UIAlertAction!) in
                           let dict = self.MedicineArray[sender.tag] as? NSDictionary
                           let updatedQty = 0
                           self.myMutableDictUpdateQty = NSMutableDictionary(dictionary: dict!)
                           self.myMutableDictUpdateQty.setValue(updatedQty, forKey: "medicineQty")
                           self.editMedicine()
                           print(self.myMutableDictUpdateQty)
                            
                         
                         
                       
                         }
                   alertVC.addAction(Action1);
                   let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
                   alertVC.addAction(Action2);

                   self.present(alertVC, animated: true, completion: nil)
                  
        }else{
            myMutableDictUpdateQty = NSMutableDictionary(dictionary: dict!)
                   myMutableDictUpdateQty.setValue(updatedQty, forKey: "medicineQty")
                  editMedicine()
                  print(myMutableDictUpdateQty)
        }
       
    }
    
    @objc func plusQty(sender:UIButton){
        print("plus\(sender.tag)")
        let dict = MedicineArray[sender.tag] as? NSDictionary
        let Qty = dict?.value(forKey: "medicineQty") as? Double
        let updatedQty = Qty! + 1
          myMutableDictUpdateQty = NSMutableDictionary(dictionary: dict!)
         myMutableDictUpdateQty.setValue(updatedQty, forKey: "medicineQty")
          editMedicine()
        print(myMutableDictUpdateQty)
        
    }
    
    @objc func removeMed(sender:UIButton){
        print("remove\(sender.tag)")
         
                
        if Array.count == 1  && ArrayC.count == 0  && MedicineArray.count == 1{
            localizedContent = NSLocalizedString("Do you want to cancel this order?", comment: "")
        }else{
            localizedContent = NSLocalizedString("Do you want to remove this medicine?", comment: "")
        }
                
              
               let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
               let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
             { (action:UIAlertAction!) in
                let dict = self.MedicineArray[sender.tag] as? NSDictionary
                               let updatedQty = 0
                self.myMutableDictUpdateQty = NSMutableDictionary(dictionary: dict!)
                self.myMutableDictUpdateQty.setValue(updatedQty, forKey: "medicineQty")
                self.editMedicine()
                print(self.myMutableDictUpdateQty)
                
               
              
            
              }
        alertVC.addAction(Action1);
        let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
        alertVC.addAction(Action2);

        self.present(alertVC, animated: true, completion: nil)
       
           
       }
    
    @objc func deletePx(sender:UIButton){
        
        print("deletePxItemNo\(sender.tag)")
        
       
        
       
            if Array.count == 1 && ArrayC.count == 1 && MedicineArray.count == 0{
              localizedContent = NSLocalizedString("Do you want to cancel this order?", comment: "")
                
            }else{
                localizedContent = NSLocalizedString("Do you want to remove this Rx?", comment: "")
            }
            
       
        
     
        
      
       let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
       let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
     { (action:UIAlertAction!) in
        
       
    
      
        let imgstr:NSDictionary = self.ArrayC[sender.tag] as! NSDictionary
       let baseUrl =  DeleteImgs()
       let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
        let queryStringParam  = ["orderId": String(describing:self.orderID!),"edit":false] as [String : Any]
        print(queryStringParam)
        

                      
        let bodyParameters = [imgstr.value(forKey: "imageId") as! CLong]
        
               print(bodyParameters)
        
                   let headers = ["Content-Type": "application/json",
                                                        "transactionId":"a",
                                                        "Authorization":authtok]
                    var items = [URLQueryItem]()
                    var myURL = URLComponents(string: baseUrl)
                    for (key,value) in queryStringParam {
                                      items.append(URLQueryItem(name: key, value: String(describing:value)))
                                  }
                                  
                      myURL?.queryItems = items
                                  
                                 
            
                      var request = URLRequest(url: (myURL?.url)!)
                      request.httpMethod = "POST"
                      request.httpBody = try? JSONSerialization.data(withJSONObject: bodyParameters)
                      request.allHTTPHeaderFields = headers
                      
                      AF.request(request).responseJSON { response in
    //                    print(response)
                        switch response.result {
                            
                              case .success(let value):
                                switch response.response?.statusCode {
                                                                       case 200,201:
                                                                           print("")
                               
                                //self.callSummaryDetails()
                                                                           self.getSummaryOrder()
                    if self.localizedContent == NSLocalizedString("Do you want to cancel this order?", comment: ""){
                                print("oderrrrrrrrrr")
                        
                        UserDefaults.standard.set(0, forKey: "offerId")
                        UserDefaults.standard.set("NotFromSummary", forKey: "Summary")
                        UserDefaults.standard.removeObject(forKey:"OrderId")
                        UserDefaults.standard.synchronize()
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: buyMeds.self) {
                                                                                  
                        self.navigationController!.popToViewController(controller, animated: false)
                                                    break
                                                            }
                                                                          }
                                                                         
                                                                                  }
                                if value is NSDictionary {
                                 
                                 let dict = value as? NSDictionary
                                    print(dict!)
                                  
                                                 
                                             }
                            case 401:
                                                                    print("401")
                                                                    let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                                                    Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                                                                case 400:
                                                                    print("400")
                                                                    let adddict = value as? NSDictionary
                                                                    if adddict?.allKeys.first as! String == "400"
                                                                    {
                                                                    Utility.showAlertWithTitle(title: "", andMessage: adddict?.value(forKey: "400") as? String, onVC: self)
                                                                    }
                                                                case 500:
                                                                    print("500")
//                                                                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                                                                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                    
                                    let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
                                                
                                                self.addChild(popupVC)
                                                popupVC.view.frame = self.view.frame
                                                self.view.addSubview(popupVC.view)
                                                popupVC.didMove(toParent: self)
                                                                default:
                                                                    print("may be 500")
                                                                    let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                                                                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                                             
                                                                }
                                         case .failure(let error):
                                             print(error)
                                         }
                              
                          }
                    
    
}
alertVC.addAction(Action1);
let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
alertVC.addAction(Action2);

self.present(alertVC, animated: true, completion: nil)
            
        }
    
    
    func confirmOrderService(){
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        actInd.startAnimating()
        
        let baseUrl = confirmOrder()
              print(baseUrl)
           let offerId = UserDefaults.standard.value(forKey: "offerId") as? CLong ?? 0
        let queryStringParam = ["orderId":String(describing:orderID!),
                                "paymentId":String(describing:paymentId!),
                                "offerId":String(describing:offerId)] as [String : Any]
              print(queryStringParam)
                     
                                 var urlComponent = URLComponents(string: baseUrl)!
                                 let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value as? String) }
                                   urlComponent.queryItems = queryItems
                                   
                                 let bodyParameters = ["":""]
                                 
                                 let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
                                
                                         
                                 let headers = ["Content-Type": "application/json",
                                                                     "transactionId":"a",
                                                                     "Authorization":authtok]
                                 
                         
                                   var request = URLRequest(url: urlComponent.url!)
                                   request.httpMethod = "POST"
                                   request.httpBody = try? JSONSerialization.data(withJSONObject: bodyParameters)
                                   request.allHTTPHeaderFields = headers
                                   
                                   AF.request(request).responseJSON { response in
                                     print(response)
                                    
                                UIApplication.shared.endIgnoringInteractionEvents()
                                self.actInd.stopAnimating()
                                    
                                     switch response.result {
                                             case .success(let value):
                                                switch response.response?.statusCode {
                                                                                       case 200,201:
                                                                                           print("")
                                            UserDefaults.standard.set(0, forKey: "offerId")
                                            UserDefaults.standard.set("", forKey: "applicableOn")
                                            UserDefaults.standard.set("", forKey: "offertitel")
                                            UserDefaults.standard.set("", forKey: "descriptionCoupen")
                                            UserDefaults.standard.synchronize()
                                                                                           
                                             if let dict = value as? NSDictionary {
                                                print(dict)
                                                
                                        if (dict.value(forKey: "201") as? String) != nil
                                        {
                                        self.mssg = dict.value(forKey: "201") as? String ?? "Order Received!"
                                                                                                     
                                            print(self.mssg )
                                                                                                 
                                        }
                                             
                                         UserDefaults.standard.set(0, forKey: "offerId")
                                         UserDefaults.standard.set("NotFromSummary", forKey: "Summary")
                                            UserDefaults.standard.removeObject(forKey:"OrderId")
                                                 UserDefaults.standard.synchronize()

                                                  let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouViewController") as? thankYouViewController
                                                vc?.savingsvalue2 = self.mssg 
                                                                                                                              //self.savingsvalue
                                                self.navigationController?.pushViewController(vc!, animated: false)
                                                              
                                                          }
                                                    case 401:
                                                        print("401")
                                                        let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                                        Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                                                    case 400:
                                                        print("400")
                                                        let adddict = value as? NSDictionary
                                                        if adddict?.allKeys.first as! String == "400"
                                                        {
                                                        Utility.showAlertWithTitle(title: "", andMessage: adddict?.value(forKey: "400") as? String, onVC: self)
                                                        }
                                                    case 500:
                                                        print("500")
//                                                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                                                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                    
                                                    let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
                                                                
                                                                self.addChild(popupVC)
                                                                popupVC.view.frame = self.view.frame
                                                                self.view.addSubview(popupVC.view)
                                                                popupVC.didMove(toParent: self)
                                                    default:
                                                        print("may be 500")
                                                        let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                                                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                                 
                                                    }
                                                    
                                                      case .failure(let error):
                                                          print(error)
                                                      }
                                           
                                       }
    }
    
    
    
    
    func DiscardOrderService(){
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        actInd.startAnimating()
        
           let baseUrl = discardOrder()
                 print(baseUrl)
              let offerId = UserDefaults.standard.value(forKey: "offerId") as? CLong ?? 0
           let queryStringParam = ["orderId":String(describing:orderID!)
                                   ] as [String : Any]
                 print(queryStringParam)
                        
                                    var urlComponent = URLComponents(string: baseUrl)!
                                    let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value as? String) }
                                      urlComponent.queryItems = queryItems
                                      
                                    let bodyParameters = ["":""]
                                    
                                    let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
                                   
                                            
                                    let headers = ["Content-Type": "application/json",
                                                                        "transactionId":"a",
                                                                        "Authorization":authtok]
                                    
                            
                                      var request = URLRequest(url: urlComponent.url!)
                                      request.httpMethod = "POST"
                                      request.httpBody = try? JSONSerialization.data(withJSONObject: bodyParameters)
                                      request.allHTTPHeaderFields = headers
                                      
                                      AF.request(request).responseJSON { response in
                                        print(response)
                                        
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                        self.actInd.stopAnimating()
                                        
                                        switch response.result {
                                                case .success(let value):
                                                    switch response.response?.statusCode {
                                                    case 200,201:
                                                        print("")
                                                if let dict = value as? NSDictionary {
                                                   print(dict)
                                                    
                                                  
                                                    UserDefaults.standard.set(0, forKey: "offerId")
                                                    UserDefaults.standard.set("NotFromSummary", forKey: "Summary")
                                                    UserDefaults.standard.removeObject(forKey:"OrderId")
                                                     UserDefaults.standard.synchronize()
                                                for controller in self.navigationController!.viewControllers as Array {
                                                         if controller.isKind(of: buyMeds.self) {
                                                             
                                                             self.navigationController!.popToViewController(controller, animated: false)
                                                             break
                                                         }
                                                     }
                                                    
                                                    
                                             
                                                                 
                                                             }
                                            case 401:
                                                print("401")
                                                let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                                            case 400:
                                                print("400")
                                                let adddict = value as? NSDictionary
                                                if adddict?.allKeys.first as! String == "400"
                                                {
                                                Utility.showAlertWithTitle(title: "", andMessage: adddict?.value(forKey: "400") as? String, onVC: self)
                                                }
                                            case 500:
                                                print("500")
//                                                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                                                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                        
                                                        
                                                        let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
                                                                    
                                                                    self.addChild(popupVC)
                                                                    popupVC.view.frame = self.view.frame
                                                                    self.view.addSubview(popupVC.view)
                                                                    popupVC.didMove(toParent: self)
                                            default:
                                                print("may be 500")
                                                let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                                                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                         
                                            }
                                            
                                                         case .failure(let error):
                                                             print(error)
                                                         }
                                              
                                          }
       }
       
       
       
       
    
     
    @IBAction func backPress(_ sender: Any) {
    
         self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func removeOfferPress(_ sender: Any) {
       
        localizedContent = NSLocalizedString("Do you want to remove Promo Code?", comment: "")
            
        let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
        let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
            { (action:UIAlertAction!) in
                
                self.offerId = 0
                
                    UserDefaults.standard.set(0, forKey: "offerId")
                    UserDefaults.standard.set("", forKey: "applicableOn")
                    UserDefaults.standard.set("", forKey: "offertitel")
                    UserDefaults.standard.set("", forKey: "descriptionCoupen")
                   UserDefaults.standard.synchronize()
                    
                    if self.offerId == 0 {
                        UIView.animate(withDuration: 2, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                        self.offerView.isHidden = true
                                        self.offerViewHeight.constant = 60
                                         })
                       
                      }else{
                        self.offerView.isHidden = false
                        self.offerViewHeight.constant = 140

                    }
                self.savePaymentAndCouponForOrder()
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);

            self.present(alertVC, animated: true, completion: nil)
            
    }
    
    
    
    @IBAction func confirmPress(_ sender: Any) {
        
        confirmOrderService()
        
    }
    
    @IBAction func discardPress(_ sender: Any) {
        
     localizedContent = NSLocalizedString("Are you sure you want to Discard this Order?", comment: "")
        
    let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
    let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
        { (action:UIAlertAction!) in
            
            self.DiscardOrderService()
            
        }
        alertVC.addAction(Action1);
        let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
        alertVC.addAction(Action2);

        self.present(alertVC, animated: true, completion: nil)
        
       
        
    }
    
    
    
    @IBAction func showPaymentDropPress(_ sender: Any) {
        orderTypeDrop.showList()
        
        
    }
    
    
    
      

      @IBAction func showMeds(_ sender: Any) {
        
          if medicineDetailsView.isHidden == true{
              UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                  self.medicineDetailsView.isHidden = false
                self.medicineTableView.alpha = 1
                self.medicineBtn.setImage(UIImage(named: "baseline_keyboard_arrow_up_black_36dp"), for: .normal)
                 
                  
              })
                
                 }else{
              UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                  self.medicineDetailsView.isHidden = true
                 self.medicineTableView.alpha = 0
                 
               
                 self.medicineBtn.setImage(UIImage(named: "baseline_keyboard_arrow_down_black_36dp"), for: .normal)
                             
                         })
                 
                 }
      }
    
    
    
    @IBAction func changeAddressPress(_ sender: Any) {
        
        
        let btnWidth = 150
        UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
        UserDefaults.standard.set("Order Summary", forKey: "backBtnText")
        UserDefaults.standard.set("fromSummary", forKey: "Summary")
         UserDefaults.standard.synchronize()
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "addressListViewController") as! addressListViewController
        navigationController?.pushViewController(vc, animated: false)
        
        
        
    }
    
      func savePaymentAndCouponForOrder(){
              let baseUrl = savePaymentAndCouponForOrderString()
                    print(baseUrl)
//                 let offerId = UserDefaults.standard.value(forKey: "offerId") as? CLong ?? 0
              let queryStringParam = ["orderId":String(describing:orderID!),
                                      "paymentId":String(describing:paymentId!),
                                      "offerId":String(describing:offerId!)] as [String : Any]
                    print(queryStringParam)
                           
                                       var urlComponent = URLComponents(string: baseUrl)!
                                       let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value as? String) }
                                         urlComponent.queryItems = queryItems
                                         
                                       let bodyParameters = ["":""]
                                       
                                       let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
                                      
                                               
                                       let headers = ["Content-Type": "application/json",
                                                                           "transactionId":"a",
                                                                           "Authorization":authtok]
                                       
                               
                                         var request = URLRequest(url: urlComponent.url!)
                                         request.httpMethod = "POST"
                                         request.httpBody = try? JSONSerialization.data(withJSONObject: bodyParameters)
                                         request.allHTTPHeaderFields = headers
                                         
                                         AF.request(request).responseJSON { response in
                                           print(response)
                                           switch response.result {
                                                   case .success(let value):
                                                      switch response.response?.statusCode {
                                                                                            case 200,201:
                                                                                                print("success")
                                                  
                                                case 401:
                                                              print("401")
                                                              let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                                              Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                                                          case 400:
                                                              print("400")
                                                              let adddict = value as? NSDictionary
                                                              if adddict?.allKeys.first as! String == "400"
                                                              {
                                                              Utility.showAlertWithTitle(title: "", andMessage: adddict?.value(forKey: "400") as? String, onVC: self)
                                                              }
                                                          case 500:
                                                              print("500")
//                                                          let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                                                          Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                          
                                                              let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
      
                                                              self.addChild(popupVC)
                                                              popupVC.view.frame = self.view.frame
                                                              self.view.addSubview(popupVC.view)
                                                              popupVC.didMove(toParent: self)
                                                          default:
                                                              print("may be 500")
                                                              let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                                                              Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                                       
                                                          }
                                                            case .failure(let error):
                                                                print(error)
                                                            }
                                                 
                                             }
          }
    

}


extension summaryViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var counts = 0
       
        if  MedicineArray.count != 0{
            counts = MedicineArray.count
        }else{
            counts = 1
           
        }
        return counts
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if MedicineArray.count != 0{

               let cell = tableView.dequeueReusableCell(withIdentifier: "medTableViewCell") as! medTableViewCell
               let Meddict = MedicineArray[indexPath.row] as? NSDictionary
               let discP = Meddict?.value(forKey: "discount") as? Double ?? 0
               let qty = Meddict?.value(forKey: "medicineQty") as? Double ?? 0
               cell.medName.text = Meddict?.value(forKey: "medicineName") as? String
               cell.discountP.text = String(format: "%.0f", discP) + "% OFF "
               cell.pack.text = Meddict?.value(forKey: "pack") as? String
               cell.qty.text = String(format: "%.0f", qty)
               
               if let mainprice = Meddict?.value(forKey: "mrp") as? String
                   {
                   cell.mrp.text = "MRP ₹\(mainprice)"
                   let IntPrice:Double = Double(mainprice)!
                   let discount:Double = discP / 100
                   let dicPrice =  IntPrice * discount
                   let afterDicPrice = IntPrice - dicPrice
                   cell.mrpAfterDisc.text = "₹" + String(format: "%.2f", afterDicPrice)
                   }
                           
               if let mainprice2 = Meddict?.value(forKey: "mrp") as? Double
                   {
                    cell.mrp.text = String(describing: "MRP ₹\(mainprice2)")
                    let discount:Double = discP / 100
                    let dicPrice =  mainprice2 * discount
                    let afterDicPrice = mainprice2 - dicPrice
                    cell.mrpAfterDisc.text = "₹" + String(format: "%.2f", afterDicPrice)
                   }
              
               
               if StatusId == 1 || StatusId == 2 || StatusId == 39 || StatusId == 57{
                   cell.minus.isHidden = true
                   cell.plus.isHidden = true
                cell.remove.setTitle(NSLocalizedString("QUANTITY", comment: ""), for: .normal)
                cell.remove.setTitleColor(.darkGray, for: .normal)
                   

               }else{
                   cell.minus.isHidden = false
                   cell.plus.isHidden = false
                  cell.remove.setTitle(NSLocalizedString("Remove", comment: ""), for: .normal)
                 cell.remove.setTitleColor(.red, for: .normal)
                
                cell.minus.addTarget(self, action: #selector(minusQty(sender:)), for: .touchUpInside)
                              cell.plus.addTarget(self, action: #selector(plusQty(sender:)), for: .touchUpInside)
                              cell.remove.addTarget(self, action: #selector(removeMed(sender:)), for: .touchUpInside)
                              
                              cell.minus.tag = indexPath.row
                               cell.plus.tag = indexPath.row
                               cell.remove.tag = indexPath.row
                   
               }
            
                return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "noMedTableViewCell") as! noMedTableViewCell
            cell.noMedLbl.text = NSLocalizedString("No Medicines Added", comment: "")
            return cell
        }
        
       
        
       
    }
    
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var Height = CGFloat()
        if MedicineArray.count != 0{
            Height = 140
        }else{
             Height = 50
        }
        return Height
    }
    
    
}






extension summaryViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
   

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           var itemCount = Int()
                 if ArrayC.count != 0{
                    itemCount = ArrayC.count
                 }else{
                      itemCount = 1
                 }
                 return itemCount
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if ArrayC.count != 0{
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pXCollectionViewCell", for: indexPath) as! pXCollectionViewCell
            let dictC = self.ArrayC[indexPath.row] as? NSDictionary
                             let urlString = dictC?.value(forKey: "imagePath") as! String
                             let imagUrlString = Live_IP + urlString
                           
                             let imgurl:URL = URL(string: imagUrlString)!
                             cell.PxImageView.sd_setImage(with: imgurl, completed: nil)
                             cell.layoutIfNeeded()
                             cell.deletePxBtn.tag = indexPath.row
                             cell.deletePxBtn.addTarget(self, action: #selector(deletePx(sender:)), for: .touchUpInside)
            
            if StatusId == 1 || StatusId == 2 || StatusId == 39 || StatusId == 57{
                cell.deletePxBtn.isHidden = true
            }else{
                cell.deletePxBtn.isHidden = false
            }
            
             return cell
                        }else{
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "noRxCollectionViewCell", for: indexPath) as! noRxCollectionViewCell
            cell.noRxLbl.text = NSLocalizedString("No Prescriptions Added", comment: "")
                              
              return cell
                        }
        
        
    }
    

    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var itemsize = CGSize()
        if ArrayC.count != 0{
                itemsize = CGSize(width: 100, height: 100)
            }else{
            itemsize = CGSize(width: collectionView.frame.width - 10, height: collectionView.frame.height)
                        }
        
        return itemsize
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dictC = self.ArrayC[indexPath.row] as? NSDictionary
                                    let urlString = dictC?.value(forKey: "imagePath") as! String
                                    let imagUrlString = Live_IP + urlString
        let popUpRx = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popSumRxViewController") as! popSumRxViewController
        popUpRx.rxImageUrl = imagUrlString
        self.addChild(popUpRx)
        popUpRx.view.frame = self.view.frame
        self.view.addSubview(popUpRx.view)
        popUpRx.didMove(toParent: self)
    }
    
    
    
    
    
    
}



