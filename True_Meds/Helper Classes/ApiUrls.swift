//
//  ApiUrls.swift
//  True_Meds
//


import UIKit
import Foundation

//
//var Live_IP = "https://stage-services.truemeds.in"


var Live_IP = "https://services.truemeds.in"


// POST Methods //


func GetUserMasters() -> String {
    return Live_IP + "/CustomerService/mobileMaster";
}

func GetOnboard() -> String {
    return Live_IP + "/CustomerService/getOnboardingImages";
}

func UpdateSettings() -> String {
    return Live_IP + "/CustomerService/updateUserSettings";
}

func VerifyOTP() -> String {
    return Live_IP + "/CustomerService/verifyOtp";
}

func SendOTP() -> String {
    return Live_IP + "/CustomerService/sendOtp";
}

func AddAddress() -> String {
    return Live_IP + "/CustomerService/saveAddress";
}

func checkPincode() -> String {
    return Live_IP + "/CustomerService/pincode"
}

func GetAllAddress() -> String {
    return Live_IP + "/CustomerService/getAllAddress";
}

func DeleteAddress() -> String {
    return Live_IP + "/CustomerService/deleteAddress";
}

func UpdateName() -> String {
    return Live_IP + "/CustomerService/updateProfile";
}

func UploadRx() -> String {
    return Live_IP + "/CustomerService/uploadImage";
}


func AddPatients() -> String {
    return Live_IP + "/CustomerService/addPatient";
}

func DeletePatients() -> String {
    return Live_IP + "/CustomerService/deletePatient"
}

func GetArticles() -> String {
    return Live_IP + "/ArticleService/getArticleListing"
}

func GetArticleDetails() -> String {
    return Live_IP + "/ArticleService/getArticleDetails"
}

func savePaymentType() -> String {
    return Live_IP + "/CustomerService/savePaymentType"
}

func Privacy() -> String {
    return Live_IP + "/CustomerService/getPP"
}

func TnC() -> String {
    return Live_IP + "/CustomerService/getTNC"
}

func GetAllOrders() -> String {
    return Live_IP + "/CustomerService/getAllOrders"
}

func AllAboutUs() -> String {
    return Live_IP + "/CustomerService/aboutUs"
}

func GetInfoOnMedicines() -> String {
    return Live_IP + "/CustomerService/getMyAllMedicines"
}

func GetOrderBills() -> String {
    return Live_IP + "/CustomerService/getOrderBill"
}

func getDosageDetails() -> String {
    return Live_IP + "/CustomerService/getDosageDetails"
}


func getOrderSummary() -> String {
    return Live_IP + "/CustomerService/getOrderSummary"
}

func GetHelpCat() -> String {
    return Live_IP + "/CustomerService/getHelpCategory"
}

func GetHelpCatDet() -> String {
    return Live_IP + "/CustomerService/getHelpCategoryDetails"
}

func RaiseOrderTicket() -> String {
    return Live_IP + "/CustomerService/raiseTicket"
}

func GetSearchMedData() -> String {
    return Live_IP + "/CustomerService/findMedicine"
}

func GetOrderIssues() -> String {
    return Live_IP + "/CustomerService/getOrderIssues"
}

func GetCustDetails() -> String {
    return Live_IP + "/CustomerService/getCustomerDetails"
}

func GetAllOffers() -> String {
    return Live_IP + "/CustomerService/getAllOffers"
}


func GetOfferDetails() -> String {
    return Live_IP + "/CustomerService/getOffersDetails"
}


func DeleteImgs() -> String {
    return Live_IP + "/CustomerService/deleteImage"
}


func GetAllPatients() -> String {
    return Live_IP + "/CustomerService/getAllPatient";
}

func GetAllLiveOrderUrl() -> String {
    return Live_IP + "/CustomerService/getOrderTrackOnHome";
}

func GetPatientDetails() -> String {
    return Live_IP + "/CustomerService/getPatientById";
}

func GetOrderStats() -> String {
    return Live_IP + "/CustomerService/getOrderStatus"
}

func RCancelOrders() -> String {
    return Live_IP + "/OrderManagementService/cancelOrder"
}

func GetDoctorsList() -> String {
    return Live_IP + "/CustomerService/doctor/all"
}

func GetWalletDetails() -> String {
    return Live_IP + "/CustomerService/wallet"
}

func GetMyTransactions() -> String {
    return Live_IP + "/CustomerService/wallet/info"
}

func saveReorderPaymentType() -> String {
    return Live_IP + "/CustomerService/reOrder"
}

func checkEditO() -> String {
    return Live_IP + "/CustomerService/editOrder"
}

func ReUploadRX() -> String {
    return Live_IP + "/CustomerService/reUploadImage"
}

func syncContacts() -> String {
    return Live_IP + "/CustomerService/referrals/check"
}

func sendReff() -> String {
    return Live_IP + "/CustomerService/referrals/send"
}

func ClaimReff() -> String {
    return Live_IP + "/CustomerService/redeem"
}


func GenerateChecksum() -> String {
    return Live_IP + "/ThirdPartyService/generateCheckSum"
}

func savePaytmResponse() -> String {
    return Live_IP + "/ThirdPartyService/savePaytmResponse"
}


func getAllPatientsOrderDetails() -> String {
    return Live_IP + "/OrderManagementService/getAllPatientsOrderDetails"
}


func saveMedicine() -> String {
    return Live_IP + "/CustomerService/saveMedsAndCreateOrder"
}

func saveAdd() -> String {
    return Live_IP + "/CustomerService/saveAddressForOrder"
}



func savePaymentAndCouponForOrderString() -> String {
    return Live_IP + "/CustomerService/savePaymentAndCouponForOrder"
}

func confirmOrder() -> String {
    return Live_IP + "/CustomerService/confirmOrder"
}

func discardOrder() -> String {
    return Live_IP + "/CustomerService/discardOrder"
}

func getCancelReasons() -> String {
    return Live_IP + "/OrderManagementService/getReasonsForOrderCancellation"
}

func editMeds() -> String
{
    return Live_IP + "/OrderManagementService/editMedicine"
}

func GetInfoOnSubsList() -> String {
    return Live_IP + "/CustomerService/getCustomerOrderlisting"
}

func GetInfoOnSubstitutes() -> String {
    return Live_IP + "/CustomerService/getMySubstitutesInfoByOrderId"  
}
