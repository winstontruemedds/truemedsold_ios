//
//  Utility.swift
//  True_Meds
//


import Foundation
import UIKit

class Utility: NSObject {
    
    class func isValidEmail(_ string : String) -> Bool
    {
        //let emailRegEx:String = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailRegEx:String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let isValid : Bool = emailTest.evaluate(with: string)
        return isValid
        
    }
    
    class func isValidURL(_ string : String) -> Bool
    {
        let urlRegEx:String = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        let isValid : Bool = urlTest.evaluate(with: string)
        return isValid
    }
    
    class func isValidPass(_ string : String) -> Bool
    {
        let passRegEx:String = "[a-zA-Z0-9@#$_-]{6,8}"
        let passTest = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        let isValid : Bool = passTest.evaluate(with: string)
        return isValid
        
    }
    
    class func isValidName(_ string : String) -> Bool
    {
        let passRegEx:String = "[a-zA-Z\\s]{1,35}"
        let passTest = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        let isValid : Bool = passTest.evaluate(with: string)
        return isValid
        
    }
    
    class func isValidName2(_ string : String) -> Bool
    {
        let passRegEx:String = "[a-zA-Z\\s]{1,35}"
        let passTest = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        let isValid : Bool = passTest.evaluate(with: string)
        return isValid
        
    }
    
    
    class func isValidAdd1(_ string : String) -> Bool
    {
        let add1RegEx:String = "[,-/#()a-zA-Z0-9]{1,30}"
        let add1Test = NSPredicate(format:"SELF MATCHES %@", add1RegEx)
        let isValid : Bool = add1Test.evaluate(with: string)
        return isValid
        
    }
    
    
    class func isValidAdd2(_ string : String) -> Bool
    {
        let add2RegEx:String = "[,-/#()a-zA-Z0-9]{1,50}"
        let add2Test = NSPredicate(format:"SELF MATCHES %@", add2RegEx)
        let isValid : Bool = add2Test.evaluate(with: string)
        return isValid
        
    }
    
    
    
    class func isValidMobNo(_ string : String) -> Bool
    {
        let passRegEx:String = "^[6-9]\\d{9}$"
        let passTest = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        let isValid : Bool = passTest.evaluate(with: string)
        return isValid
        
    }
    
    
    
    class func isValidPincode(_ string : String) -> Bool
    {
        let pinRegEx:String = "^[0-9]{6}$"
        let pinTest = NSPredicate(format:"SELF MATCHES %@", pinRegEx)
        let isValid : Bool = pinTest.evaluate(with: string)
        return isValid
        
    }
    
    
    class func showAlertWithTitle(title:String?,andMessage message:String?, onVC vc:UIViewController)
    {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil); alertVC.addAction(cancelAction);
        vc.present(alertVC, animated: true, completion: nil);
        
    }
    
    class func showNetworkAlert(title:String?,andMessage message:String?, onVC vc:UIViewController)
    {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil); alertVC.addAction(cancelAction);
        vc.present(alertVC, animated: true, completion: nil);
       
    }
    
    
    class func showAlertWithAction(title:String?,andMessage message:String?, onVC vc:UIViewController)
    {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let doneAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel) { (action:UIAlertAction!) in
        vc.navigationController?.popViewController(animated: true)
            
        }
        alertVC.addAction(doneAction);
        vc.present(alertVC, animated: true, completion: nil);
        
    }
    
    class func showAlertWithLogoutAction(title:String?,andMessage message:String?, onVC vc:UIViewController)
    {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let doneAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel)
        { (action:UIAlertAction!) in
            UserDefaults.standard.removeObject(forKey: "CustomerId")
            UserDefaults.standard.removeObject(forKey: "ageGroupId")
            UserDefaults.standard.removeObject(forKey: "languageId")
            UserDefaults.standard.removeObject(forKey: "custname")
            UserDefaults.standard.removeObject(forKey: "access_token")
            UserDefaults.standard.removeObject(forKey: "mobileNo")
            UserDefaults.standard.removeObject(forKey: "langpref")
            UserDefaults.standard.removeObject(forKey: "menunew")
            UserDefaults.standard.removeObject(forKey: "menulogin")
            UserDefaults.standard.synchronize()
            
            let vcs = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SplashVC") as? SplashVC
            
            vc.navigationController?.pushViewController(vcs!, animated: true)
            
        }
        alertVC.addAction(doneAction);
        vc.present(alertVC, animated: true, completion: nil);
        
    }
    
    class func showAlertWithFading(title:String?,andMessage message:String?, onVC vc:UIViewController)
    {
        let alert = UIAlertController(title:title, message: message, preferredStyle: .alert)
        vc.present(alert, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 1 second)
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
        }
    
    }
    
    
}

