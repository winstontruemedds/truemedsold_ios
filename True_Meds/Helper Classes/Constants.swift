//
//  Constants.swift
//  True_Meds
//
//  Created by Office on 9/26/19.
//

import Foundation

let GET:String = "GET"
let POST:String = "POST"

let HGET:String = "HGET"
let HPOST:String = "HPOST"

let BPOST:String = "BPOST"

let HBPOST:String = "HBPOST"

//MARK:- login constants
let DEPARTMENT:String = "department"
let USER_ID:String = "uid"
let USER_NAME:String = "name"
let IS_USER_LOGGED_IN = "isUserLoggedIn"
