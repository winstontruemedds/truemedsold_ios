//
//  trackCollectionViewCell.swift
//  orderStatus
//
//  Created by Mangesh Toraskar on 12/12/19.
//  Copyright © 2019 Mangesh Toraskar. All rights reserved.
//

import UIKit

class trackCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var orderNo: UILabel!
    
    @IBOutlet weak var datTimeLbl: UILabel!
    
    @IBOutlet weak var commentLbl: UILabel!
    
    @IBOutlet weak var eddLbl: UILabel!
    
    
    @IBOutlet weak var trackBtn: UIButton!
   
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
   func uderlinecellBtn(object:UIButton,UderlineString:String){
           let yourAttributes: [NSAttributedString.Key: Any] = [
                        
                        .foregroundColor: UIColor.init(hex: 0x0071BC),
                        .underlineStyle: NSUnderlineStyle.single.rawValue]
                    let attributeString = NSMutableAttributedString(string: UderlineString,
                                                                    attributes: yourAttributes)
                    object.setAttributedTitle(attributeString, for: .normal)
           object.titleLabel?.font = UIFont.OpenSans(.bold,size: 15)
             
         }
    
    func setupViews() {
      
        uderlinecellBtn(object: trackBtn, UderlineString: NSLocalizedString("TRACK", comment: ""))
        
    }
    
}

class paymentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var orderNo: UILabel!
    
    @IBOutlet weak var datTimeLbl: UILabel!
    
    @IBOutlet weak var commentLbl: UILabel!
    
    @IBOutlet weak var eddLbl: UILabel!
    
    
    @IBOutlet weak var payBtn: UIButton!
    
    
    
     override func awakeFromNib() {
           super.awakeFromNib()
           setupViews()
        payBtn.layer.cornerRadius = 4
       }
       
       func setupViews() {
          
           
       }
    
    
}

class editOrderCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var orderNo: UILabel!
       
       @IBOutlet weak var datTimeLbl: UILabel!
       
       @IBOutlet weak var commentLbl: UILabel!
       
       @IBOutlet weak var eddLbl: UILabel!
    
    @IBOutlet weak var editOrderBtn: UIButton!
    
    
   
    
    
    func uderlinecellBtn(object:UIButton,UderlineString:String){
             let yourAttributes: [NSAttributedString.Key: Any] = [
                        
                          .foregroundColor: UIColor.init(hex: 0xFF0000),
                          .underlineStyle: NSUnderlineStyle.single.rawValue]
                      let attributeString = NSMutableAttributedString(string: UderlineString,
                                                                      attributes: yourAttributes)
                      object.setAttributedTitle(attributeString, for: .normal)
             object.titleLabel?.font = UIFont.OpenSans(.bold,size: 15)
               
           }
    
    override func awakeFromNib() {
           super.awakeFromNib()
           setupViews()
       }
       
       func setupViews() {
      
           uderlinecellBtn(object: editOrderBtn, UderlineString:NSLocalizedString("EDIT ORDER", comment: ""))
       }
    
}
