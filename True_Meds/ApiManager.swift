//
//  ApiManager.swift
//  vasaibdirs
//
//  Created by Welborn Machado
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager: NSObject {
    
    func requestApiWithDataType(methodType:String,urlString:String,parameters:[String:AnyObject]? = nil,completionHandeler: @escaping (Any?,Int?,NSError?) -> Void){
    
        //var isSuccess: Bool?
        
        var cStatus: Int?
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
        
        let headers: HTTPHeaders = ["Content-Type": "application/json",
                                    "transactionId":"a",
                                    "Authorization":authtok]
                
        print(headers)
        
        switch methodType
        {
        case GET:

            AF.request(urlString, method: .get, parameters: parameters, encoding: JSONEncoding.default)
                .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                    print("Progress: \(progress.fractionCompleted)")
            }
                .responseString { response in
                    //debugPrint(response)
                    switch response.result
                    {
                    case .success(let JSON) :
                        if response.response?.statusCode == 200 || response.response?.statusCode == 201
                        {
                        cStatus = response.response?.statusCode
                            
                        completionHandeler(JSON,cStatus, nil)
                        }
                        else
                        {
                            cStatus = response.response?.statusCode
                            
                            completionHandeler(JSON,cStatus,nil)
                        }
                    case .failure(let error):
                        completionHandeler(nil,cStatus,error as NSError)
                    }
            }
            break
        case POST:
            
            AF.request(urlString, method:.post, parameters: parameters, encoding: JSONEncoding.default).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
                }
                .responseString { response in
                    switch response.result
                    {
                    case .success(let JSON) :
                        if response.response?.statusCode == 200 || response.response?.statusCode == 201
                        {
                            cStatus = response.response?.statusCode
                            completionHandeler(JSON,cStatus ,nil)
                        }
                        else
                        {
                            cStatus = response.response?.statusCode
                            completionHandeler(JSON,cStatus,nil)
                        }
                    case .failure(let error):
                        completionHandeler(nil,cStatus,error as NSError)
                    }
            }
        case HGET:
            AF.request(urlString, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString),headers:headers)
                .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                    print("Progress: \(progress.fractionCompleted)")
            }
                .responseString { response in
                    switch response.result
                    {
                    case .success(let JSON) :
                        if response.response?.statusCode == 200 || response.response?.statusCode == 201
                        {
                            cStatus = response.response?.statusCode
                            
                            completionHandeler(JSON,cStatus,nil)
                        }
                        else
                        {
                            cStatus = response.response?.statusCode
                            completionHandeler(JSON,cStatus,nil)
                        }
                    case .failure(let error):
                        completionHandeler(nil,cStatus,error as NSError)
                    }
            }
        case HPOST:
            
        AF.request(urlString, method:.post, parameters: parameters, encoding: URLEncoding(destination: .queryString),headers:headers).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
                }
                .responseString { response in
                    switch response.result
                    {
                    case .success(let JSON) :
                        if response.response?.statusCode == 200 || response.response?.statusCode == 201
                        {
                            cStatus = response.response?.statusCode
                            
                            completionHandeler(JSON,cStatus,nil)
                        }
                        else
                        {
                            cStatus = response.response?.statusCode
                            completionHandeler(JSON,cStatus,nil)
                        }
                    case .failure(let error):
                        completionHandeler(nil,cStatus,error as NSError)
                    }
            }
        
        case BPOST:
            
            AF.request(urlString, method:.post, parameters: parameters, encoding:JSONEncoding.default ,headers:headers).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
                }
                .responseString { response in
                    switch response.result
                    {
                    case .success(let JSON) :
                        if response.response?.statusCode == 200 || response.response?.statusCode == 201
                        {
                        cStatus = response.response?.statusCode
                            
                            completionHandeler(JSON,cStatus,nil)
                        }
                        else
                        {
                        cStatus = response.response?.statusCode
                        completionHandeler(JSON,cStatus,nil)
                        }
                    case .failure(let error):
                        completionHandeler(nil,cStatus,error as NSError)
                    }
            }
            
            break
        default :
            break
        }
    }
    
    struct CustomPostEncoding: ParameterEncoding {
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try URLEncoding().encode(urlRequest, with: parameters)
        let httpBody = NSString(data: request.httpBody!, encoding: String.Encoding.utf8.rawValue)!
        request.httpBody = httpBody.replacingOccurrences(of: "%5B%5D=", with: "=").data(using: .utf8)
        return request
    }
    }
    
    
    
    struct JSONStringArrayEncoding: ParameterEncoding {
        private let array: [[String : Any]]

        init(array: [[String : Any]]) {
            self.array = array
        }

        func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
            var urlRequest = try urlRequest.asURLRequest()

            let data = try JSONSerialization.data(withJSONObject: array, options: [])

            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }

            urlRequest.httpBody = data

            return urlRequest
        }
    }
    
}


