//
//  AppDelegate.swift
//  True_Meds
//
// Test1

import UIKit
import FirebaseMessaging
import UserNotifications
import Firebase
import SideMenu
import Flurry_iOS_SDK
import FBSDKCoreKit
import Siren
import AVFoundation
import Reachability


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
 
      let reachability = try! Reachability()
     
   
     func showAlertAppDelegate(title : String,message : String,buttonTitle : String,window: UIWindow){
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: nil))
            window.rootViewController?.present(alert, animated: true, completion: nil)
        }
    
    @objc func reachabilityChanged(note: Notification) {

      let reachability = note.object as! Reachability

      switch reachability.connection {
      case .wifi:
          print("Reachable via WiFi")
//        UIApplication.shared.endIgnoringInteractionEvents()
      case .cellular:
          print("Reachable via Cellular")
//        UIApplication.shared.endIgnoringInteractionEvents()
      case .none:
        print("Network not reachable")
//        UIApplication.shared.beginIgnoringInteractionEvents()
        
    
        
        let showAlert = UIAlertController(title: NSLocalizedString("Please check your internet connectivity", comment: ""), message: nil, preferredStyle: .alert)
        let imageView = UIImageView(frame: CGRect(x: 0, y: 90, width: 250, height: 140))
        imageView.image = UIImage(named: "Group 50")
        imageView.contentMode = .scaleAspectFit
        showAlert.view.addSubview(imageView)
        let height = NSLayoutConstraint(item: showAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 280)
        let width = NSLayoutConstraint(item: showAlert.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 280)
                
        showAlert.view.addConstraint(height)
        showAlert.view.addConstraint(width)

        showAlert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
            // your actions here...
        }))
                window!.rootViewController?.present(showAlert, animated: true, completion: nil)
        
      }
    }
    
    
    
    var window: UIWindow?
    
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
         Locale.setupInitialLanguage()

        window?.makeKeyAndVisible()

        Siren.shared.wail(performCheck: .onDemand, completion: nil)
        Siren.shared.apiManager = APIManager(countryCode: "IN")
        Siren.shared.presentationManager = PresentationManager(forceLanguageLocalization: .english)
        
          let siren = Siren.shared
        siren.rulesManager = RulesManager(globalRules: .critical)
        
         siren.wail { results in
             switch results {
             case .success(let updateResults):
                 print("AlertAction ", updateResults.alertAction)
                 print("Localization ", updateResults.localization)
                 print("Model ", updateResults.model)
                 print("UpdateType ", updateResults.updateType)
             case .failure(let error):
                 print(error.localizedDescription)
             }
         }
        
        //facebook
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        Settings.isAutoInitEnabled = true
        ApplicationDelegate.initializeSDK(nil)
        Settings.isAutoLogAppEventsEnabled = true
        
        Flurry.startSession("43GZQ5WT8JCH85XZJ5HW", with: FlurrySessionBuilder
               .init()
               .withCrashReporting(true)
               .withLogLevel(FlurryLogLevelAll))
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }
        

        //get application instance ID
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        
        application.registerForRemoteNotifications()
        
        
        return true
    }

    //faceBook
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = ApplicationDelegate.shared.application(app, open: url, options: options)

      // Add any custom logic here.
      
      return handled
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
       // application.registerForRemoteNotifications()
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
//        removeReachabilityObserver()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
          do{
            try reachability?.startNotifier()
          }catch{
            print("could not start reachability notifier")
          }
//        addReachabilityObserver()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    

}

extension AppDelegate: MessagingDelegate{
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        
        UserDefaults.standard.set(fcmToken, forKey: "FTOKEN")
        UserDefaults.standard.synchronize()
    }
    
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
        
    }
}


extension AppDelegate: UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // UIApplication.shared.applicationIconBadgeNumber = badgeCount + 1
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
    
        // Print full message.
        print(userInfo)
        
        let application = UIApplication.shared

        if(application.applicationState == .active){

        }

        if(application.applicationState == .inactive)
        {

//            guard let rootViewController = ViewController.show as? UINavigationController else {
//                return
//            }
//
//            guard let navigationController = rootViewController as? UINavigationController else {
//                return
//            }
//
//            let viewController2 = self.window!.rootViewController!.storyboard!.instantiateViewController(withIdentifier: "OrderListingViewController") as! OrderListingViewController
//
//            rootViewController.pushViewController(viewController2, animated: true)
//
//            //rootViewController.present(viewController2, animated: true, completion: nil)
            
            

        }
        
        completionHandler()
    }
    
    
    
    
    
    
    
    
}



