//
//  sectionTableViewCell.swift
//  orderStatus
//
//  Created by Mangesh Toraskar on 29/11/19.
//  Copyright © 2019 Mangesh Toraskar. All rights reserved.
//

import UIKit

class sectionTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var setionLbl: UILabel!
    @IBOutlet weak var sectionImg: UIImageView!
    @IBOutlet weak var dateTimeLbl: UILabel!
    
    @IBOutlet weak var openCloseBtn: UIButton!
    
    @IBOutlet weak var lineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
