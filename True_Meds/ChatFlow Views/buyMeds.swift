//
//  ViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/5/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK
import Siren

class buyMeds: UIViewController,FSPagerViewDataSource,FSPagerViewDelegate{
   

    @IBOutlet weak var trackCollectionView: UICollectionView!
    
    @IBOutlet weak var trackPageContr: UIPageControl!
    
  
    
    @IBOutlet weak var buyMedBtn: UIButton!
    
    @IBOutlet weak var exploreOtherBtn: UIButton!
    
    @IBOutlet weak var howItWorksBtn: UIButton!
    
    @IBOutlet weak var drTextView: UIView!
    
    var mastdict = NSDictionary()
    
    var imgsarr = NSArray()
    
    var liveOrdes = NSArray()
    
    var liveOrderDict = NSDictionary()
    
    var trackDate = String()
    var trackTime = String()
  
    @IBOutlet weak var bmlbl: UILabel!
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    var cameFrom = ""
    
    var resdict = NSDictionary()
    var resdict2 = NSDictionary()
    var resdict3 = NSDictionary()
    
    var addressDict = NSArray()
    var apatarr = NSArray()
    
    var langID:Int!
    var agegroupID:Int!
    
    var btnstr:String!
    
    @IBOutlet weak var walkpopviu: UIView!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    @IBOutlet weak var headerlbl: UILabel!
    
    @IBOutlet weak var wdescriptionlbl: UILabel!
    
    var indexx2:Int = 0
    
    @IBOutlet weak var wcrossbtno: UIButton!
    
    @IBOutlet weak var paysviu: UIView!
    
    var btn1Text = String()
    var btn2Text = String()
    var btn3Text = String()
    

    fileprivate let transformerNames = ["cross fading", "zoom out", "depth", "linear", "overlap", "ferris wheel", "inverted ferris wheel", "coverflow", "cubic"]
    fileprivate let transformerTypes: [FSPagerViewTransformerType] = [.crossFading,
                                                                      .zoomOut,
                                                                      .depth,
                                                                      .linear,
                                                                      .overlap,
                                                                      .ferrisWheel,
                                                                      .invertedFerrisWheel,
                                                                      .coverFlow,
                                                                      .cubic]
    
    fileprivate var typeIndex = 0 {
        didSet {
            let type = self.transformerTypes[0]
            self.pagerView.transformer = FSPagerViewTransformer(type:type)
            switch type {
            case .crossFading, .zoomOut, .depth:
                self.pagerView.itemSize = FSPagerView.automaticSize
                self.pagerView.decelerationDistance = 1
            case .linear,.overlap:
                let transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.pagerView.itemSize = FSPagerView.automaticSize
                
                self.pagerView.decelerationDistance = FSPagerView.automaticDistance
                
            case .ferrisWheel, .invertedFerrisWheel:
                self.pagerView.itemSize = CGSize(width: 180, height: 140)
                self.pagerView.decelerationDistance = FSPagerView.automaticDistance
            case .coverFlow:
                self.pagerView.itemSize = CGSize(width: 220, height: 170)
                self.pagerView.decelerationDistance = FSPagerView.automaticDistance
            case .cubic:
                let transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                self.pagerView.itemSize = self.pagerView.frame.size.applying(transform)
                self.pagerView.decelerationDistance = 1
            }
        }
    }
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.typeIndex = 0
        }
    }
    
    fileprivate var styleIndex = 0 {
        didSet {
            //Clean up
            self.pageControl.setStrokeColor(nil, for: .normal)
            self.pageControl.setStrokeColor(nil, for: .selected)
            self.pageControl.setFillColor(nil, for: .normal)
            self.pageControl.setFillColor(nil, for: .selected)
            self.pageControl.setImage(nil, for: .normal)
            self.pageControl.setImage(nil, for: .selected)
            self.pageControl.setPath(nil, for: .normal)
            self.pageControl.setPath(nil, for: .selected)
            
            switch self.styleIndex {
            case 0:
                let color = UIColor(red: 0/255.0, green: 113/255.0, blue: 188/255.0, alpha: 1.0)
                
                self.pageControl.setStrokeColor(color, for: .normal)
                self.pageControl.setStrokeColor(color, for: .selected)
                self.pageControl.setFillColor(color, for: .selected)
                
            
            default:
                break
            }
        }
    }
    
    fileprivate var alignmentIndex = 1 {
        didSet {
            self.pageControl.contentHorizontalAlignment = [.right,.center,.left][1]
        }
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let index = self.typeIndex
        self.typeIndex = index
        
    }
    
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return imgsarr.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let finimgdict:NSDictionary = imgsarr[index] as! NSDictionary
        let imgstr:String = finimgdict.value(forKey: "image") as! String
        let imgurl:URL = URL(string: imgstr)!
        let data = try? Data(contentsOf: imgurl)
        cell.imageView?.image = UIImage(data: data!)
        
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.clipsToBounds = true
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
        
        indexx2 = targetIndex
        
        let finimgdict2:NSDictionary = imgsarr[indexx2] as! NSDictionary
        headerlbl.text = finimgdict2.value(forKey: "header") as? String
        wdescriptionlbl.text = finimgdict2.value(forKey: "description") as? String
    
    }
    
    @IBOutlet weak var pageControl: FSPageControl! {
        didSet {
            self.pageControl.numberOfPages = self.imgsarr.count
            self.pageControl.contentHorizontalAlignment = .center
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            self.pageControl.hidesForSinglePage = true
            self.styleIndex = 0
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        startTimer()
        
       let flowLayout = UICollectionViewFlowLayout()
       flowLayout.scrollDirection = .horizontal
       
        trackCollectionView.isPagingEnabled = true
       
         self.trackCollectionView.collectionViewLayout = flowLayout
        
         
        Siren.shared.wail()
        
        navigationController?.navigationBar.isHidden = true
        cameFrom = ""
        
        wcrossbtno.layer.cornerRadius = 20.0
        
        visualEffectView.alpha = 0.3
        pageControl.currentPage = 0
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
        
       //let finimgdict2:NSDictionary = imgsarr[indexx2] as! NSDictionary
          // headerlbl.text = finimgdict2.value(forKey: "header") as? String
         //  wdescriptionlbl.text = finimgdict2.value(forKey: "description") as? String
       
        mastdict = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = mastdict.value(forKey: "3") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        bmlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        let btn3:NSDictionary = btnarr[2] as! NSDictionary
        btnstr = btn3.value(forKey: "text") as? String ?? ""
        
        btn1Text = btn1.value(forKey: "text") as? String ?? "Back"
        btn2Text = btn2.value(forKey: "text") as? String ?? "Back"
        btn3Text = btn3.value(forKey: "text") as? String ?? "Back"
        
        buyMedBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        exploreOtherBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        howItWorksBtn.setTitle(btn3.value(forKey: "text") as? String ?? "", for: .normal)
        leftbottomwithUderlineString(object: howItWorksBtn, UderlineString: btn3.value(forKey: "text") as? String ?? "")

        setDesign()
        
        if UserDefaults.standard.object(forKey: "access_token") != nil
        {
            getAllAddress()
            Getallpatients()
           
            
        }
        
        
        if UserDefaults.standard.object(forKey: "OrderId") != nil
        {
        UserDefaults.standard.removeObject(forKey: "OrderId")
        UserDefaults.standard.synchronize()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforUpdateSettings(notification:)), name: Notification.Name("UpdateMaster"), object: nil)
        
    }
    
    
    @objc func NotificationReceivedforPayment(notification: Notification) {
        paysuccesspop()

        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.paysviu.removeFromSuperview()
            self.visualEffectView.removeFromSuperview()
        }
    
    }
    
    
    @IBAction func closewalkthru(_ sender: Any) {
        walkpopviu.removeFromSuperview()
        visualEffectView.removeFromSuperview()
    }
    
    
    
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
         UserDefaults.standard.set(0, forKey: "offerId")
        UserDefaults.standard.removeObject(forKey: "OrderId")
        UserDefaults.standard.set("NotFromSummary", forKey: "Summary")
               UserDefaults.standard.set("", forKey: "offertitel")
               UserDefaults.standard.set("", forKey: "descriptionCoupen")
                UserDefaults.standard.synchronize()
        navigationController?.navigationBar.isHidden = true
        
      
       
        mastdict = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = mastdict.value(forKey: "3") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        bmlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        let btn3:NSDictionary = btnarr[2] as! NSDictionary
        
        btn1Text = btn1.value(forKey: "text") as? String ?? "Back"
        btn2Text = btn2.value(forKey: "text") as? String ?? "Back"
        btn3Text = btn3.value(forKey: "text") as? String ?? "Back"
        
        btnstr = btn3.value(forKey: "text") as? String ?? ""
        
        buyMedBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        exploreOtherBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        howItWorksBtn.setTitle(btn3.value(forKey: "text") as? String ?? "", for: .normal)

        leftbottomwithUderlineString(object: howItWorksBtn, UderlineString: btn3.value(forKey: "text") as? String ?? "")
        setDesign()
        
        if UserDefaults.standard.object(forKey: "access_token") != nil
        {
             trackCollectionView.isHidden = false
             trackPageContr.isHidden = false
            getAllAddress()
            Getallpatients()
            GetAllLiveOrders()
        }else{
            trackCollectionView.isHidden = true
            trackPageContr.isHidden = true
        }
        
        UserDefaults.standard.removeObject(forKey: "MedicineArr")
        UserDefaults.standard.synchronize()
        
        if cameFrom == ""{
           Out()
        }
        
      
        
    }
    
    
    func Out(){
        outRight(object: buyMedBtn)
        outRight(object: exploreOtherBtn)
        outRight(object: howItWorksBtn)
        outLeft(object: drTextView)
        outLeft(object: trackCollectionView)
        outLeft(object: trackPageContr)
        
      

    }

    override func viewDidAppear(_ animated: Bool) {
        print(cameFrom)
        

        if cameFrom == ""{
            In()
        }else{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.buyMedBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.exploreOtherBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.howItWorksBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.drTextView.transform = CGAffineTransform(translationX: -10, y: 0)
                self.trackCollectionView.transform = CGAffineTransform(translationX: 0, y: 0)
                 self.trackPageContr.transform = CGAffineTransform(translationX: 0, y: 0)
              
                
                
            }) { (_) in
              
            }
            
        }
        
        
    }
    
    
    func In(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.buyMedBtn.transform = self.buyMedBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.exploreOtherBtn.transform = self.exploreOtherBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.howItWorksBtn.transform = self.howItWorksBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.drTextView.transform = self.drTextView.transform.translatedBy(x: self.view.bounds.width, y: 0)
             self.trackCollectionView.transform = self.trackCollectionView.transform.translatedBy(x: self.view.bounds.width, y: 0)
             self.trackPageContr.transform = self.trackPageContr.transform.translatedBy(x: self.view.bounds.width, y: 0)
          
            
        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.buyMedBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.exploreOtherBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                 self.howItWorksBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.drTextView.transform = CGAffineTransform(translationX: -10, y: 0)
                 self.trackCollectionView.transform = CGAffineTransform(translationX: 0, y: 0)
                 self.trackPageContr.transform = CGAffineTransform(translationX: 0, y: 0)
              
                
            })
        }
    }
    
    
    
    @objc func NotificationReceivedforUpdateSettings(notification: Notification) {
          
           DispatchQueue.main.async {
               
               let temp:NSDictionary = notification.object as! NSDictionary
           
               self.mastdict = temp.value(forKey: "questions") as! NSDictionary
               
               let menuItems:NSArray = temp.value(forKey: "menuItems") as! NSArray
               let newuser:NSArray = menuItems.filter{!(($0 as! NSDictionary)["login"] as! Bool)} as NSArray
               
               let appintro:NSDictionary = temp.value(forKey: "appIntro") as! NSDictionary
               self.imgsarr = appintro.value(forKey: "screen") as! NSArray
               let finimgdict2:NSDictionary = self.imgsarr[self.indexx2] as! NSDictionary
               self.headerlbl.text = finimgdict2.value(forKey: "header") as? String
               self.wdescriptionlbl.text = finimgdict2.value(forKey: "description") as? String
               
               self.langID = UserDefaults.standard.value(forKey: "languageId") as! Int
               
               UserDefaults.standard.set(self.mastdict, forKey: "quesdict")
               UserDefaults.standard.set(newuser, forKey: "menunew")
               UserDefaults.standard.set(menuItems, forKey: "menulogin")
               
               UserDefaults.standard.synchronize()
               
              
               self.pagerView.reloadData()
               
               if UserDefaults.standard.object(forKey: "langpref") as! String == "English"
               {
              // Utility.showAlertWithTitle(title: "", andMessage: "Language will be changed only after restarting the application", onVC: self)
                   
               let alertVC = UIAlertController(title: "", message: "Language will be changed only after restarting the application", preferredStyle: .alert)
               let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil); alertVC.addAction(cancelAction);
                   self.present(alertVC, animated: true, completion: nil);
                   
               }
               else if UserDefaults.standard.object(forKey: "langpref") as! String == "Hindi"
               {
                //Utility.showAlertWithTitle(title: "", andMessage: "एप्लीकेशन बंद करके चालू करने के बाद ही भाषा बदलेगी", onVC: self)
                   
                   let alertVC = UIAlertController(title: "", message: "एप्लीकेशन बंद करके चालू करने के बाद ही भाषा बदलेगी", preferredStyle: .alert)
                   let cancelAction = UIAlertAction(title: "ठीक है", style: .cancel, handler: nil); alertVC.addAction(cancelAction);
                   self.present(alertVC, animated: true, completion: nil);
               
               }
               else if UserDefaults.standard.object(forKey: "langpref") as! String == "Marathi"
               {
              // Utility.showAlertWithTitle(title: "", andMessage: "एप्लीकेशन बंद करुण चालू केल्यावरच भाषा बदलली जातील", onVC: self)
                   
                   let alertVC = UIAlertController(title: "", message: "एप्लीकेशन बंद करुण चालू केल्यावरच भाषा बदलली जातील", preferredStyle: .alert)
                   let cancelAction = UIAlertAction(title: "ठीक आहे", style: .cancel, handler: nil); alertVC.addAction(cancelAction);
                   self.present(alertVC, animated: true, completion: nil);
                   
               }
               
           }
           
       }
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    
    
    func setDesign(){
        
        blueBtn(object: buyMedBtn)
        greenBtn(object: exploreOtherBtn)
        
        labelFont(object: bmlbl)
       
        
    }
    
    
     func setpagecontroller(){
        trackPageContr.numberOfPages = liveOrdes.count
         trackPageContr.currentPage = 0
        trackPageContr.pageIndicatorTintColor = UIColor.lightGray
        trackPageContr.currentPageIndicatorTintColor = UIColor.init(hex: 0x0071BC)
        
       
    }
    
    
    
    @IBAction func buyMedPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":buyMedBtn.titleLabel?.text,
        "full": "testing2",
        "question": bmlbl.text!
         ])
        
        let logParams = [
        "ans":buyMedBtn.titleLabel?.text,
        "full": "testing2",
        "question": bmlbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "buyMeds"
      let btnWidth = buyMedBtn.frame.size.width
      UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
      UserDefaults.standard.set(btn1Text, forKey: "backBtnText")
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
             self.buyMedBtn.transform = self.buyMedBtn.transform.translatedBy(x:0 , y: -(self.buyMedBtn.frame.origin.y - 20))
            self.exploreOtherBtn.transform = CGAffineTransform(translationX: self.view.bounds.width, y: 0)
            self.howItWorksBtn.transform = CGAffineTransform(translationX: self.view.bounds.width, y: 0)
            self.drTextView.transform = CGAffineTransform(translationX: 0, y: -self.view.bounds.height)
             self.trackCollectionView.transform = CGAffineTransform(translationX: 0, y: -self.view.bounds.height)
            self.trackPageContr.transform = CGAffineTransform(translationX: 0, y: -self.view.bounds.height)
         
            
            
       }) { (_) in
        self.enterMobileOtpView()
        }
    }
    
    
    
    @IBAction func exploreFeaturesPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":exploreOtherBtn.titleLabel?.text,
        "full": "testing2",
        "question": bmlbl.text!
         ])
        
        let logParams = [
        "ans":exploreOtherBtn.titleLabel?.text,
        "full": "testing2",
        "question": bmlbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "explore"
        let btnWidth = exploreOtherBtn.frame.size.width
        UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
        UserDefaults.standard.set(btn2Text, forKey: "backBtnText")
     
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.buyMedBtn.transform = self.buyMedBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.howItWorksBtn.transform = self.howItWorksBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.exploreOtherBtn.transform = self.exploreOtherBtn.transform.translatedBy(x:0 , y: -(self.exploreOtherBtn.frame.origin.y - 20))
            self.drTextView.transform = self.drTextView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
          self.trackCollectionView.transform = self.trackCollectionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
             self.trackPageContr.transform = self.trackPageContr.transform.translatedBy(x: 0, y: -self.view.bounds.height)
            
        }) { (_) in
            self.exploreFeaturesView()
        }
        
    }
    
    
    
    
    @IBAction func howItWorksPress(_ sender: Any) {
        cameFrom = "payforOrder"
        let btnWidth = howItWorksBtn.frame.size.width
        UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
        UserDefaults.standard.set(btn3Text, forKey: "backBtnText")
        
//        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//            self.howItWorksBtn.transform = self.howItWorksBtn.transform.translatedBy(x: 0, y: -(self.howItWorksBtn.frame.origin.y - 20))
//            self.exploreOtherBtn.transform = self.exploreOtherBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
//            self.buyMedBtn.transform = self.buyMedBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
//            self.drTextView.transform = self.drTextView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
//
//
//
//        }) { (_) in
//            self.ShowWalkpop()
//        }
        
        self.ShowWalkpop()
        
    }
    
    
    
    func enterMobileOtpView(){
        
        if UserDefaults.standard.object(forKey: "CustomerId") == nil || UserDefaults.standard.object(forKey: "access_token") == nil
        {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "enterMobileOtpViewController") as! enterMobileOtpViewController
        vc.agegroupID2 = agegroupID
        vc.langID2 = langID
        navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let vc = storyboard?.instantiateViewController(withIdentifier: "validPrescriptionViewController") as! validPrescriptionViewController

            navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func exploreFeaturesView(){
        
          if UserDefaults.standard.object(forKey: "access_token") == nil{
            let vc = storyboard?.instantiateViewController(withIdentifier: "exploreBeforeLoginViewController") as! exploreBeforeLoginViewController
                  
            navigationController?.pushViewController(vc, animated: false)
            
          }else{
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "exploreOtherFutureViewController") as! exploreOtherFutureViewController
               
            navigationController?.pushViewController(vc, animated: false)
        }
        
        
    
    }
    
   func getAllAddress()
   {
       let parameters = ["":""]
       
       ApiManager().requestApiWithDataType(methodType:HPOST, urlString: GetAllAddress(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
           
           if cStatus == 200 || cStatus == 201
           {
             self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
             self.addressDict = self.resdict.value(forKey: "AddressList") as! NSArray
            
            print(self.addressDict)
            
            if self.addressDict.count == 0
            {
                UserDefaults.standard.set("No", forKey: "isAddAvail")
                UserDefaults.standard.synchronize()
            }
            else
            {
                UserDefaults.standard.set("Yes", forKey: "isAddAvail")
                UserDefaults.standard.synchronize()
            }
           }
        else if cStatus == 401
        {
            let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
            Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
        }
        else if cStatus == 500
        {
//            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
            
            
            let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

            self.addChild(popupVC)
            popupVC.view.frame = self.view.frame
            self.view.addSubview(popupVC.view)
            popupVC.didMove(toParent: self)
            
        }
        else
        {
            let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
        }
           
       }
   }
    
    
     func GetAllLiveOrders(){
                let para2 = ["":""]
                   
                   ApiManager().requestApiWithDataType(methodType:HPOST, urlString: GetAllLiveOrderUrl(),parameters: para2 as [String : AnyObject]) { (response,cStatus, error) in

                                    if cStatus == 200 || cStatus == 201
                                    {
                                       let dict = convertStringToDictionary(json: response as! String)! as NSDictionary
           
                                        self.liveOrdes  = dict.value(forKey: "liveOrders") as? NSArray ?? []
                                        
                                        print(self.liveOrdes)
                                        
                                        if self.liveOrdes.count == 1{
                                          self.trackPageContr.isHidden = true
                                        }else{
                                          self.trackPageContr.isHidden = false
                                        }
                                        
                                        DispatchQueue.main.async {
                                           
                                            self.trackCollectionView.isHidden = false
                                            self.trackCollectionView.reloadData()
                                            self.setpagecontroller()
                                        }
                                       
                                    
                                    }
                    else if cStatus == 401
                    {
                        let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                        Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                    }
                    else if cStatus == 500
                    {
  
                        let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                        self.addChild(popupVC)
                        popupVC.view.frame = self.view.frame
                        self.view.addSubview(popupVC.view)
                        popupVC.didMove(toParent: self)
                    }
                    else
                    {
                        self.trackPageContr.isHidden = true
                        self.trackCollectionView.isHidden = true
                    }
                  }
               }
        
        
    
    
    
    
    func Getallpatients(){
            let para2 = ["showMyself":"false"]
               
               ApiManager().requestApiWithDataType(methodType:HPOST, urlString: GetAllPatients(),parameters: para2 as [String : AnyObject]) { (response,cStatus, error) in
                                
                                if cStatus == 200 || cStatus == 201
                                {
                                   self.resdict2 = convertStringToDictionary(json: response as! String)! as NSDictionary
       
                                    self.apatarr = self.resdict2.value(forKey: "PatientList") as! NSArray
                                    
                                    print(self.apatarr)
                                   
                                    if self.apatarr.count == 0
                                    {
                                        UserDefaults.standard.set("No", forKey: "isPatAvail")
                                        UserDefaults.standard.synchronize()
                                    }
                                    else
                                    {
                                    UserDefaults.standard.set("Yes", forKey: "isPatAvail")
                                    UserDefaults.standard.synchronize()
                                    }
                                }
                else if cStatus == 401
                {
                    let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                    Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                }
                else if cStatus == 500
                {
//                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    
                    let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                    self.addChild(popupVC)
                    popupVC.view.frame = self.view.frame
                    self.view.addSubview(popupVC.view)
                    popupVC.didMove(toParent: self)
                }
                else
                {
                    let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                }
              }
           }
    
    
    
    
    func ShowWalkpop()
    {
        let appintro:NSDictionary = UserDefaults.standard.dictionary(forKey: "appIntro") as! NSDictionary
//        let appintro:NSDictionary = self.getDict()
        imgsarr = appintro.value(forKey: "screen") as! NSArray
        pagerView.reloadData()
        
        walkpopviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(walkpopviu)
        
        let leadingConstraint = NSLayoutConstraint(item: walkpopviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: walkpopviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 10.0)
        
        let trailingConstraint = NSLayoutConstraint(item: walkpopviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: walkpopviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 10.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: walkpopviu, attribute: .bottom, relatedBy: .equal,
                                                      toItem: walkpopviu.superview, attribute: .bottom,
                                                      multiplier: 1.0, constant: -30.0)
        
        let horizontalConstraint2 = NSLayoutConstraint(item: walkpopviu, attribute: .top, relatedBy: .equal,
                                                      toItem: walkpopviu.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 30.0)
        
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,horizontalConstraint2])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    
    
    
}


extension UIColor {
    
    convenience init(hex: Int) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
    
}


extension UIViewController{
    
    func outRight(object:UIView){
         object.transform = object.transform.translatedBy(x: self.view.bounds.width, y: 0 )
    }
    func outLeft(object:UIView){
        object.transform = object.transform.translatedBy(x: -self.view.bounds.width, y: 0 )
    }
    func outUp(object:UIView){
        object.transform = object.transform.translatedBy(x: 0, y: -self.view.bounds.height )
    }
    func outDown(object:UIView){
        object.transform = object.transform.translatedBy(x: 0, y: self.view.bounds.height )
    }
    
    func labelFont(object:UILabel){
        object.font =  UIFont.OpenSans(.semibold,size: self.view.bounds.width / 16)
        print(self.view.bounds.width / 16)
       }
    
    func labelFont2(object:UILabel){
        object.font =  UIFont.OpenSans(.semibold,size: 20)
    }
    
    func blueBtn(object:UIButton){
        object.layer.cornerRadius = 6
        object.layer.borderWidth = 1
        object.layer.borderColor = UIColor.init(hex: 0x0071BC).cgColor
        object.setTitleColor(.init(hex: 0x0071BC), for: .normal)
        object.titleLabel?.font = UIFont.OpenSans(.semibold,size: 15)
       
    }
    
    
    func greenBtn(object:UIButton){
        object.layer.cornerRadius = 6
        object.layer.borderWidth = 1
        object.layer.borderColor = UIColor.init(hex: 0x22B573).cgColor
        object.setTitleColor(.init(hex: 0x22B573), for: .normal)
        object.titleLabel?.font = UIFont.OpenSans(.semibold,size: 15)
    }
    
    func backBtn(object:UIButton){
           
            object.layer.cornerRadius = 6
            object.layer.borderColor = UIColor.darkGray.cgColor
            object.layer.borderWidth = 1
            object.setTitleColor(.gray, for: .normal)
            object.titleLabel?.font = UIFont.OpenSans(.semibold,size: 15)
            object.widthAnchor.constraint(equalToConstant: UserDefaults.standard.value(forKey: "backBtnWidth") as! CGFloat).isActive = true
            object.setTitle( UserDefaults.standard.value(forKey: "backBtnText") as? String, for: .normal)
         
       }
   
    
    func uderlineLbl(object:UILabel,UderlineString:String){
        object.attributedText = NSAttributedString(string: UderlineString, attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        
    }
    
 
    
    func uderlineBtn(object:UIButton,UderlineString:String){
         let yourAttributes: [NSAttributedString.Key: Any] = [
                      //            .font: UIFont.systemFont(ofSize: 14),
                      .foregroundColor: UIColor.init(hex: 0x0071BC),
                      .underlineStyle: NSUnderlineStyle.single.rawValue]
                  let attributeString = NSMutableAttributedString(string: UderlineString,
                                                                  attributes: yourAttributes)
                  object.setAttributedTitle(attributeString, for: .normal)
         object.titleLabel?.font = UIFont.OpenSans(.bold,size: 15)
           
       }
    
    func leftbottomwithUderlineString(object:UIButton,UderlineString:String){
        if #available(iOS 11.0, *){
            
            object.clipsToBounds = false
            object.layer.cornerRadius = 10
            object.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        }else{
            let rectShape = CAShapeLayer()
            rectShape.bounds = object.frame
            rectShape.position = object.center
            rectShape.path = UIBezierPath(roundedRect: object.bounds,    byRoundingCorners: [.topLeft , .topRight , .bottomRight], cornerRadii: CGSize(width: 20, height: 20)).cgPath
            object.layer.mask = rectShape
        }
        let yourAttributes: [NSAttributedString.Key: Any] = [
            //            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.init(hex: 0x0071BC),
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: UderlineString,
                                                        attributes: yourAttributes)
        object.setAttributedTitle(attributeString, for: .normal)
        object.setTitleColor(.init(hex: 0x0071BC), for: .normal)
        object.titleLabel?.font = UIFont.OpenSans(.semibold,size: 15)
//        object.layer.borderWidth = 1
//        object.layer.borderColor = UIColor.gray.cgColor
    }
    
    
    public func setDict(dict: NSDictionary) {
        let data = NSKeyedArchiver.archivedData(withRootObject: dict)
        let userDefaults = UserDefaults.standard
        userDefaults.set(data, forKey:"appIntro")
    }


    public func getDict() -> NSDictionary {
        let data = UserDefaults.standard.object(forKey: "appIntro") as! NSData
        let object = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! NSDictionary
        return object;
    }
    
    
  
    
    
}


extension UIViewController{
    func hideKeyboardTapAround(){
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dissmissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dissmissKeyboard(){
        view.endEditing(true)
        
    }
}


extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

extension buyMeds:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    
    
    
    
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return liveOrdes.count
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          
        
        if let dict = liveOrdes[indexPath.row] as? NSDictionary{
            liveOrderDict = dict
            
        }
           let statusId = liveOrderDict.value(forKey: "statusId") as! CLong
             
          
          if statusId == 81 {
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "editOrderCollectionViewCell", for: indexPath) as! editOrderCollectionViewCell
                     cell.commentLbl.text = liveOrderDict.value(forKey: "status") as? String ?? ""
                                                   if let mydate:CLong = liveOrderDict.value(forKey: "date") as? CLong {
                                                             let mydates:CLong = mydate/1000
                                                             let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
                                                             let dayTimePeriodFormatter = DateFormatter()
                                                             dayTimePeriodFormatter.dateFormat = "dd MMMM yyyy"
                                                             trackDate = dayTimePeriodFormatter.string(from: date as Date)
                                                         }
                                                         
                                                       
                                                         if  let myTime:CLong = liveOrderDict.value(forKey: "modifiedOn") as? CLong {
                                                             let myTimes:CLong = myTime/1000
                                                             let Time = NSDate(timeIntervalSince1970: TimeInterval(myTimes))
                                                             let dayTimePeriodFormatter2 = DateFormatter()
                                                             dayTimePeriodFormatter2.dateFormat = "HH:mm"
                                                             trackTime = dayTimePeriodFormatter2.string(from: Time as Date)
                                                         }
                                
                                cell.editOrderBtn.tag = indexPath.item
                                cell.editOrderBtn.addTarget(self, action: #selector(editOrderPress(sender:)), for: .touchUpInside)
                                
                                             cell.datTimeLbl.text = "\(trackDate), \(trackTime)"
                                             let edd = liveOrderDict.value(forKey: "edd") as? String ?? ""
                                             cell.eddLbl.text = "\(edd)"
                                             let orderNo = liveOrderDict.value(forKey: "orderId") as? CLong ?? 0
                                             cell.orderNo.text = NSLocalizedString("ORDER NUMBER:", comment: "") + "\(orderNo)"
            cell.commentLbl.text = NSLocalizedString("Wrong prescription uploaded. Please select edit order to reupload the prescription.", comment: "")
                          return cell
           
          }
         else if statusId == 58 {
             let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paymentCollectionViewCell", for: indexPath) as! paymentCollectionViewCell
             cell.commentLbl.text = liveOrderDict.value(forKey: "status") as? String ?? ""
                               if let mydate:CLong = liveOrderDict.value(forKey: "date") as? CLong {
                                         let mydates:CLong = mydate/1000
                                         let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
                                         let dayTimePeriodFormatter = DateFormatter()
                                         dayTimePeriodFormatter.dateFormat = "dd MMMM yyyy"
                                         trackDate = dayTimePeriodFormatter.string(from: date as Date)
                                     }
                                     
                                   
                                     if  let myTime:CLong = liveOrderDict.value(forKey: "modifiedOn") as? CLong {
                                         let myTimes:CLong = myTime/1000
                                         let Time = NSDate(timeIntervalSince1970: TimeInterval(myTimes))
                                         let dayTimePeriodFormatter2 = DateFormatter()
                                         dayTimePeriodFormatter2.dateFormat = "HH:mm"
                                         trackTime = dayTimePeriodFormatter2.string(from: Time as Date)
                                     }
            
           
            let paymentValue = liveOrderDict.value(forKey: "paymentValue") as? Double ?? 0
            cell.payBtn.tag = indexPath.item
            cell.payBtn.addTarget(self, action: #selector(PayNowPress(sender:)), for: .touchUpInside)
            cell.payBtn.setTitle(NSLocalizedString("PAY", comment: "") + " ₹ \(paymentValue)", for: .normal)
            
                         cell.datTimeLbl.text = "\(trackDate), \(trackTime)"
                         let edd = liveOrderDict.value(forKey: "edd") as? String ?? ""
                         cell.eddLbl.text = "\(edd)"
                         let orderNo = liveOrderDict.value(forKey: "orderId") as? CLong ?? 0
                         cell.orderNo.text =  NSLocalizedString("ORDER NUMBER:", comment: "") + "\(orderNo)"
            
            
                    
                return cell
          }
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "trackCollectionViewCell", for: indexPath) as! trackCollectionViewCell
        if statusId == 60 {
            let statusSubstring = liveOrderDict.value(forKey: "statusSubstring") as? String ?? ""
                          cell.commentLbl.text = statusSubstring
        }else{
             cell.commentLbl.text = liveOrderDict.value(forKey: "status") as? String ?? ""
        }
               
        
       
        
                   if let mydate:CLong = liveOrderDict.value(forKey: "date") as? CLong {
                             let mydates:CLong = mydate/1000
                             let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
                             let dayTimePeriodFormatter = DateFormatter()
                             dayTimePeriodFormatter.dateFormat = "dd MMMM yyyy"
                             trackDate = dayTimePeriodFormatter.string(from: date as Date)
                         }
                         
                       
                         if  let myTime:CLong = liveOrderDict.value(forKey: "modifiedOn") as? CLong {
                             let myTimes:CLong = myTime/1000
                             let Time = NSDate(timeIntervalSince1970: TimeInterval(myTimes))
                             let dayTimePeriodFormatter2 = DateFormatter()
                             dayTimePeriodFormatter2.dateFormat = "HH:mm"
                             trackTime = dayTimePeriodFormatter2.string(from: Time as Date)
                         }
        
        cell.trackBtn.tag = indexPath.item
        cell.trackBtn.addTarget(self, action: #selector(trackMyOrderPress(sender:)), for: .touchUpInside)
        
        
               cell.datTimeLbl.text = "\(trackDate), \(trackTime)"
               let edd = liveOrderDict.value(forKey: "edd") as? String ?? ""
               cell.eddLbl.text = "\(edd)"
               let orderNo = liveOrderDict.value(forKey: "orderId") as? CLong ?? 0
               cell.orderNo.text =  NSLocalizedString("ORDER NUMBER:", comment: "") + "\(orderNo)"
               
       
                       
               return cell
          
         
      }
    
    
    
    @objc func trackMyOrderPress(sender:UIButton){

        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderHelpViewController") as? OrderHelpViewController
                       vc!.odhelpdict =  liveOrdes[sender.tag] as! NSDictionary
        print(liveOrdes[sender.tag] as! NSDictionary)
                       self.navigationController?.pushViewController(vc!, animated: true)
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StatusViewController") as? StatusViewController
//        vc!.odhelpdict4 = liveOrdes[sender.tag] as! NSDictionary
//        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
      
    
      @objc func PayNowPress(sender:UIButton){
   let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
          vc?.ordertype = "Billing Order"
          vc?.odhelpdict2 =  liveOrdes[sender.tag] as! NSDictionary
          self.navigationController?.pushViewController(vc!, animated: true)

            
        }
    
    
      @objc func editOrderPress(sender:UIButton){
      let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditOrderViewController") as? EditOrderViewController
      vc?.odhelpdict6 =  liveOrdes[sender.tag] as! NSDictionary
        vc?.isFrom = "buyMeds"
      self.navigationController?.pushViewController(vc!, animated: true)

             
         }
      
      
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           
           
          return CGSize(width: self.view.bounds.width, height: 140)
       }
       
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
    
       func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
           trackPageContr.currentPage = indexPath.item
       
       }
    
     
 

    
   
    
    
//    func startTimer() {
//
//        let timer =  Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
//    }
//
//
//    @objc func scrollAutomatically(_ timer1: Timer) {
//
//        if let coll  = trackCollectionView {
//            for cell in coll.visibleCells {
//                let indexPath: IndexPath? = coll.indexPath(for: cell)
//                if ((indexPath?.row)! < liveOrdes.count - 1){
//                    let indexPath1: IndexPath?
//                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
//
//                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
//                }
//                else{
//                    let indexPath1: IndexPath?
//                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
//                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
//                }
//
//            }
//        }
//    }
      
      
    
}



