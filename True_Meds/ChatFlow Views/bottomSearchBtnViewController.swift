//
//  bottomSearchBtnViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/27/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK

class bottomSearchBtnViewController: UIViewController {
    

    @IBOutlet weak var yesSerchandAddBtn: UIButton!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
     let bottomLine = CALayer()
    
    @IBOutlet weak var startsearchlbl: UILabel!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBtn()
        setDesign()
        
        visualEffectView.alpha = 0.3

        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "23") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        startsearchlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "inputs") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        
        searchBar.placeholder = btn1.value(forKey: "text") as? String ?? ""
        
    NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
        
       }
       
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    
       @objc func NotificationReceivedforPayment(notification: Notification) {
           paysuccesspop()

           let when = DispatchTime.now() + 3
           DispatchQueue.main.asyncAfter(deadline: when){
               self.paysviu.removeFromSuperview()
               self.visualEffectView.removeFromSuperview()
           }
        
       
       }
    
    func setDesign(){
        backBtn(object: yesSerchandAddBtn)
        labelFont(object: startsearchlbl)
    }
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    
//    @IBAction func gotoMenu(_ sender: Any) {
//        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
//        
//         let menu2 = SideMenuNavigationController(rootViewController: menu)
//        
//         menu2.statusBarEndAlpha = 0
//        
//         present(menu2, animated: true, completion: nil)
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "23") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        startsearchlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "inputs") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        
        searchBar.placeholder = btn1.value(forKey: "text") as? String ?? ""
    }
   
    
    func setBtn(){
       bottomLine.frame = CGRect(x: 10, y: searchBar.frame.height - 10, width: searchBar.frame.width - 10, height: 1)
                      bottomLine.backgroundColor = UIColor.black.cgColor
                     searchBar.barTintColor = UIColor.clear
                     searchBar.backgroundColor = UIColor.clear
                     searchBar.isTranslucent = true
                     searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
                      searchBar.layer.addSublayer(bottomLine)
    }
    
    
    
    
    
    @IBAction func searchBarPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":searchBar.placeholder,
        "full": "testing2",
        "question": startsearchlbl.text!
         ])
        
        let logParams = [
        "ans":searchBar.placeholder,
        "full": "testing2",
        "question": startsearchlbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "cartViewController") as! cartViewController
        vc.searplace = searchBar.placeholder
        navigationController?.pushViewController(vc, animated: false)
    
    }
    
    @IBAction func yesSearchAddPress(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
   
}
