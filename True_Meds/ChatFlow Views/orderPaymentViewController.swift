//
//  orderPaymentViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/13/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import Alamofire
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK


class orderPaymentViewController: UIViewController {
    
    

   
    
    @IBOutlet weak var offerView: UIView!
    
    @IBOutlet weak var offertitelLbl: UILabel!
    
    @IBOutlet weak var offerDiscripLbl: UILabel!
    
    @IBOutlet weak var activityTextLbl: UILabel!
    @IBOutlet weak var activityIndicate: UIActivityIndicatorView!
    
    @IBOutlet weak var savingView: UIView!
    
    @IBOutlet weak var savingLbl: UILabel!
    @IBOutlet weak var coinImageView: UIImageView!
    
    
    
    @IBOutlet weak var deliveryAddBtn: UIButton!
    
    @IBOutlet weak var questionView: UIView!

    @IBOutlet weak var onlinePaymentBtn: UIButton!
    
    @IBOutlet weak var cashOnDeliveryBtn: UIButton!
    
    @IBOutlet weak var applyPromoCodeBtn: UIButton!
    
    var cameFrom = ""
    var mssg:String!
    
    var savingPrice:Double!
    var savingPrct:Double!
    
  
    
    
    
    @IBOutlet weak var logoo: UIButton!
          
    @IBOutlet weak var menubtno: UIButton!
    
    var paymentID:String!
    
    var orderID:CLong! = 0
    
    var Offertitle:String! = ""
    var Discription:String! = ""
    
    
    var addID:CLong!
    
    var MedicineDict = ["productCode":String(),"quantity":String()]
    
    var MedicinesArr = [[String:Any]]()
    
    var savingsvalue:String!
    
    @IBOutlet weak var savepaylbl: UILabel!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
         activityTextLbl.isHidden = true
        cameFrom = ""
        setDesign()
        UserDefaults.standard.set(0, forKey: "offerId")
        UserDefaults.standard.set("", forKey: "applicableOn")
        
        visualEffectView.alpha = 0.3
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
        
        let svgimg3: SVGKImage = SVGKImage(named: "Coin icon")
        coinImageView.image = svgimg3.uiImage
        
        
        
      
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
        let medarr = UserDefaults.standard.value(forKey: "MedicineArr") as! [[String:String]]
            
        for i in 0..<medarr.count
         {
             let mydict:NSDictionary = medarr[i] as NSDictionary
            MedicineDict.updateValue(mydict.value(forKey: "productCode") as! String, forKey: "productCode")
            MedicineDict.updateValue(mydict.value(forKey: "quantity") as! String, forKey: "quantity")
            MedicinesArr.append(MedicineDict)
        }
        }
        
        savingPrice = UserDefaults.standard.value(forKey: "savingPrice") as? Double ?? 0
        savingPrct = UserDefaults.standard.value(forKey: "savingPrct") as? Double ?? 0
        
        if MedicinesArr.count == 0{
            savingLbl.text = NSLocalizedString("Minimum 25% OFF on all medicines", comment: "")
            
          
            
        }else{
            savingLbl.text =  NSLocalizedString("You will save min.", comment: "") + " ₹" + String(format: "%.0f", savingPrice) + " (" + String(format: "%.0f", savingPrct) + "%)" + NSLocalizedString(" OFF on this order", comment: "")
        }
        
        if UserDefaults.standard.value(forKey:"OrderId") != nil
        {
         orderID = UserDefaults.standard.value(forKey:"OrderId") as! CLong
        }
        print(orderID!)
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "20") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        savepaylbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        let btn3:NSDictionary = btnarr[2] as! NSDictionary
    
        onlinePaymentBtn.setTitle(btn1.value(forKey: "text") as? String ?? "", for: .normal)
        cashOnDeliveryBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        applyPromoCodeBtn.setTitle(btn3.value(forKey: "text") as? String ?? "", for: .normal)
    
       NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
    }
    
    @objc func NotificationReceivedforPayment(notification: Notification) {
        paysuccesspop()

        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.paysviu.removeFromSuperview()
            self.visualEffectView.removeFromSuperview()
        }
    
    }
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil || UserDefaults.standard.object(forKey: "order1") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        
        Offertitle = UserDefaults.standard.value(forKey: "offertitel") as? String ?? ""
        Discription = UserDefaults.standard.value(forKey: "descriptionCoupen") as? String ?? ""
               
               print("title\(Offertitle)")
               print("discription\(Discription)")
               offertitelLbl.text = Offertitle
               offerDiscripLbl.text = Discription
        
        if Offertitle == "" {
                  offerView.isHidden = true
                 
              }else{
                  offerView.isHidden = false
              }
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
            
            let dict:NSDictionary = maindict.value(forKey: "20") as! NSDictionary
            
            let text:String = dict.value(forKey: "question") as! String
            
            savepaylbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
            let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
            let btn1:NSDictionary = btnarr[0] as! NSDictionary
            let btn2:NSDictionary = btnarr[1] as! NSDictionary
            let btn3:NSDictionary = btnarr[2] as! NSDictionary
        
            onlinePaymentBtn.setTitle(btn1.value(forKey: "text") as? String ?? "", for: .normal)
            cashOnDeliveryBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
            applyPromoCodeBtn.setTitle(btn3.value(forKey: "text") as? String ?? "", for: .normal)
        
        if cameFrom == ""{
            Out()
        }
        
    }
    
    
    func Out(){
        outRight(object: onlinePaymentBtn)
        outRight(object: cashOnDeliveryBtn)
        outRight(object: applyPromoCodeBtn)
        outLeft(object: questionView)
        outRight(object: offerView)
        
       
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print(cameFrom)
        
        
        if cameFrom == ""{
            In()
        }else{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.onlinePaymentBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.cashOnDeliveryBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.applyPromoCodeBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.offerView.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
                self.savingView.transform = CGAffineTransform(translationX: 0, y: 0)
               
                
            })
        }
        
        
    }
    
    func In(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.onlinePaymentBtn.transform = self.onlinePaymentBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.cashOnDeliveryBtn.transform = self.cashOnDeliveryBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.applyPromoCodeBtn.transform = self.applyPromoCodeBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.offerView.transform = self.offerView.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: self.view.bounds.width, y: 0)
           
           
            
            
        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.onlinePaymentBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.cashOnDeliveryBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.applyPromoCodeBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.offerView.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
               
                
            })
        }
    }
    
    
    
    
    
    func setDesign(){
        //btns
     
        
        backBtn(object: deliveryAddBtn)
        blueBtn(object: onlinePaymentBtn)
        greenBtn(object: cashOnDeliveryBtn)
        blueBtn(object: applyPromoCodeBtn)
        labelFont(object: savepaylbl)
    }
    
    
    func savePaymentAndCouponForOrder(){
        let baseUrl = savePaymentAndCouponForOrderString()
              print(baseUrl)
           let offerId = UserDefaults.standard.value(forKey: "offerId") as? CLong ?? 0
        let queryStringParam = ["orderId":String(describing:orderID!),
                                "paymentId":paymentID!,
                                "offerId":String(describing:offerId)] as [String : Any]
              print(queryStringParam)
                     
                                 var urlComponent = URLComponents(string: baseUrl)!
                                 let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value as? String) }
                                   urlComponent.queryItems = queryItems
                                   
                                 let bodyParameters = ["":""]
                                 
                                 let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
                                
                                         
                                 let headers = ["Content-Type": "application/json",
                                                                     "transactionId":"a",
                                                                     "Authorization":authtok]
                                 
                         
                                   var request = URLRequest(url: urlComponent.url!)
                                   request.httpMethod = "POST"
                                   request.httpBody = try? JSONSerialization.data(withJSONObject: bodyParameters)
                                   request.allHTTPHeaderFields = headers
                                   
                                   AF.request(request).responseJSON { response in
                                     print(response)
                                     switch response.result {
                                             case .success(let value):
                                                switch response.response?.statusCode {
                                                                                      case 200,201:
                                                                                          print("")
                                             if let dict = value as? NSDictionary {
                                                print(dict)
                                                
                                                UserDefaults.standard.removeObject(forKey: "MedicineArr")

                                                self.MedicinesArr.removeAll()
                                             
                                           let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "summaryViewController") as? summaryViewController
                                           self.navigationController?.pushViewController(vc!, animated: false)
                                                              
                                                          }
                                                    case 401:
                                                        print("401")
                                                        let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                                        Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                                                    case 400:
                                                        print("400")
                                                        let adddict = value as? NSDictionary
                                                        if adddict?.allKeys.first as! String == "400"
                                                        {
                                                        Utility.showAlertWithTitle(title: "", andMessage: adddict?.value(forKey: "400") as? String, onVC: self)
                                                        }
                                                    case 500:
                                                        print("500")
//                                                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                                                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                    
                                                        let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                                                        self.addChild(popupVC)
                                                        popupVC.view.frame = self.view.frame
                                                        self.view.addSubview(popupVC.view)
                                                        popupVC.didMove(toParent: self)
                                                    default:
                                                        print("may be 500")
                                                        let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                                                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                                 
                                                    }
                                                      case .failure(let error):
                                                          print(error)
                                                      }
                                           
                                       }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func savePayments()
       {
        activityIndicate.startAnimating()
        activityTextLbl.isHidden = false
           UIApplication.shared.beginIgnoringInteractionEvents()
          
        let offerId = UserDefaults.standard.value(forKey: "offerId") as? CLong ?? 0
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
        let queryStringParam  =  ["orderId":String(describing:orderID!),"addressId":String(describing:addID!),"paymentId":paymentID!,"offerId":offerId
    //                       "customerId":custid
            ] as [String : Any]
         
           print(queryStringParam)
        
           let url =  savePaymentType()
           var items = [URLQueryItem]()
           var myURL = URLComponents(string: url)
           let session = URLSession.shared
           for (key,value) in queryStringParam {
               items.append(URLQueryItem(name: key, value: String(describing:value)))
           }
           myURL?.queryItems = items
           var request = URLRequest(url: (myURL?.url)!)
           let json = MedicinesArr
        print(MedicinesArr)
           let jsonData = try? JSONSerialization.data(withJSONObject: json)
           request.httpBody = jsonData
           request.httpMethod = "POST"
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("a", forHTTPHeaderField: "transactionId")
           request.addValue(authtok, forHTTPHeaderField: "Authorization")
           
           let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
               do {
                   //create json object from data
                print(response)
                   if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                       
                       DispatchQueue.main.async {
                           let httpResponse = response as? HTTPURLResponse
                           
                           if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                           {
                               UIApplication.shared.endIgnoringInteractionEvents()
                               let paydict:NSDictionary = json as NSDictionary
                            print(paydict)
                            if paydict["201"] != nil
                        {
                            if (paydict.value(forKey: "201") as? String) != nil
                        {
                            self.mssg = paydict.value(forKey: "201") as? String ?? "Order Received!"
                                                          
                            print(self.mssg ?? "")
                                                      
                        }
                            }else{
                                self.mssg = "Order Received!"
                            }
                            
                         UserDefaults.standard.removeObject(forKey: "MedicineArr")
                                              UserDefaults.standard.removeObject(forKey:"OrderId")
                                                     
                                              self.MedicinesArr.removeAll()
                                              UserDefaults.standard.set(0, forKey: "offerId")
                                               
                                                  self.activityIndicate.stopAnimating()
                                                  self.activityTextLbl.isHidden = true
                                              let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouViewController") as? thankYouViewController
                                              vc?.savingsvalue2 = self.mssg ?? ""
                                                                                 //self.savingsvalue
                                              self.navigationController?.pushViewController(vc!, animated: false)
                             
                               
                              // Utility.showAlertWithFading(title: "", andMessage: "Payment Method Saved Successsully", onVC: self)
    
                           }
                           else if httpResponse?.statusCode == 401
                           {
                               let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                               Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                                                  
                            
                           }
                           else if httpResponse?.statusCode == 500
                           {
                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                            
//                               let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
//
//                               self.addChild(popupVC)
//                               popupVC.view.frame = self.view.frame
//                               self.view.addSubview(popupVC.view)
//                               popupVC.didMove(toParent: self)
                           }
                           else
                           {
                               let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                               Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                           }
                       }
                       
                   }
               } catch let error {
                   print(error.localizedDescription)
               }
            }
           })
           task.resume()
           
     
       }
       
    
    
    
    
//
//        func savePayments(){
//
//              let baseUrl = savePaymentType()
//
//
//              let custid:String = String(describing:UserDefaults.standard.value(forKey: "CustomerId")!)
//
//            let queryStringParam  =  ["orderId":String(describing:orderID!),
//                       "addressId":String(describing:addID!),
//                       "paymentId":paymentID!
////                       "customerId":custid
//              ]
//
//
//              print(queryStringParam)
//
//              var urlComponent = URLComponents(string: baseUrl)!
//              let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value as? String) }
//              urlComponent.queryItems = queryItems
//              print(queryItems)
//              let parameters = MedicinesArr
//
//            print(parameters)
//
//
//              let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
//
//             let headers = ["Content-Type": "application/json",
//                                                "transactionId":"a",
//                                                "Authorization":authtok]
//
//
//              var request = URLRequest(url: urlComponent.url!)
//              request.httpMethod = "POST"
//              request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
//              request.allHTTPHeaderFields = headers
//            print(urlComponent.url!)
//              AF.request(request,).responseJSON{ response in
//
//
//
//                switch response.result {
//                        case .success(let value):
//                            if let json = value as? [String: Any] {
//                                print(json["Result"] as? Int ?? 1)
//                            }
//                        case .failure(let error):
//                            print(error)
//                        }
//
//                print(response)
//
//                let array = response.result as AnyObject? as! NSArray

//                let dict = response.result as! NSArray
//                let array = dict[0] as! NSDictionary
//                print(dict)
//                print(array)
//
//                      if response.response?.statusCode == 200 || response.response?.statusCode == 201{
//
//                        UserDefaults.standard.removeObject(forKey: "MedicineArr")
//                        UserDefaults.standard.removeObject(forKey:"OrderId")
//
//                        self.MedicinesArr.removeAll()
//                        let array :NSArray = response NSArray
//                        let paydict:NSDictionary = array[0] as NSDictionary
//
//                        if paydict["201"] != nil
//                        {
//                        if let payval = paydict.value(forKey: "201") as? String
//                        {
//                        self.savingsvalue = payval
//                        }
//                        }
//
//                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouViewController") as? thankYouViewController
//                        vc?.savingsvalue2 = "Order Received"
//                            //self.savingsvalue
//                            self.navigationController?.pushViewController(vc!, animated: false)
//
//                  }
//                else if response.response?.statusCode == 401
//                {
//                    let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
//                    Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
//                }
//                else if response.response?.statusCode == 500
//                {
//                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
//                }
//                else
//                {
//                    let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
//                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
//                }
//
//              }
//          }
//
//
//
    
    
    
    @IBAction func removeOfferBtnPress(_ sender: Any) {
        offerView.isHidden = true
        UserDefaults.standard.set(0, forKey: "offerId")
         UserDefaults.standard.set("", forKey: "applicableOn")
        UserDefaults.standard.set("", forKey: "offertitel")
        UserDefaults.standard.set("", forKey: "descriptionCoupen")
           UserDefaults.standard.synchronize()
        Offertitle = ""
        Discription = ""
        
    }
    
    
    
    @IBAction func onlinePaymentPress(_ sender: Any) {
        
        
        
        Analytics.logEvent("button_click", parameters: [
        "ans":onlinePaymentBtn.titleLabel?.text,
        "full": "testing2",
        "question": savepaylbl.text
         ])
        
        let logParams = [
        "ans":onlinePaymentBtn.titleLabel?.text,
        "full": "testing2",
        "question": savepaylbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
          cameFrom = "Me"
        let applicableOn =  UserDefaults.standard.value(forKey: "applicableOn") as! String
      
        if applicableOn == "Cod Payment" {
            Utility.showAlertWithTitle(title: "", andMessage: "This promo code not applicable on Online Payment ", onVC: self)
            print("This coupen is for cashOn delivery not applicable here")
        }else{
            
            self.paymentID = "16"
            self.savePaymentAndCouponForOrder()
           
           
        }
        
  
        
        
    }
    
    
    func onlinePaymentAnimate(){
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                    self.deliveryAddBtn.transform = self.deliveryAddBtn.transform.translatedBy(x: 0, y: -200)
                                    self.onlinePaymentBtn.transform = self.onlinePaymentBtn.transform.translatedBy(x: 0, y: -(self.onlinePaymentBtn.frame.origin.y - 20))
                                    self.cashOnDeliveryBtn.transform = self.cashOnDeliveryBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
                                    self.applyPromoCodeBtn.transform = self.applyPromoCodeBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
                                    self.offerView.transform = self.offerView.transform.translatedBy(x: self.view.bounds.width, y: 0)
                                    self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
                                    self.savingView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
                                    
                                    
                                    
                                })
        
    }
    
    
    
    @IBAction func cashOnDeliveryPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":cashOnDeliveryBtn.titleLabel?.text,
        "full": "testing2",
        "question": savepaylbl.text
         ])
        
        let logParams = [
        "ans":cashOnDeliveryBtn.titleLabel?.text,
        "full": "testing2",
        "question": savepaylbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "someOne"
        
        let applicableOn =  UserDefaults.standard.value(forKey: "applicableOn") as? String ?? ""
            
              if applicableOn == "Online Payment" {
                 Utility.showAlertWithTitle(title: "", andMessage: "This promo code not applicable on COD Payment ", onVC: self)
               print("This coupen is for onlinePayment delivery not applicable here")
              }else{
                self.paymentID = "17"
                self.savePaymentAndCouponForOrder()
                  
           
              }
        
    
        
    }
    
    func cashOnPressAnimation(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                          self.deliveryAddBtn.transform = self.deliveryAddBtn.transform.translatedBy(x: 0, y: -200)
                                          self.cashOnDeliveryBtn.transform = self.cashOnDeliveryBtn.transform.translatedBy(x: 0, y: -(self.cashOnDeliveryBtn.frame.origin.y - 20))
                                          self.onlinePaymentBtn.transform = self.onlinePaymentBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
                                          self.applyPromoCodeBtn.transform = self.applyPromoCodeBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
                                         self.offerView.transform = self.offerView.transform.translatedBy(x: self.view.bounds.width, y: 0)
                                          self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
                                          self.savingView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
                                        
                                          
                                          
                                      })
    }
    
    
    
    @IBAction func promoCodePress(_ sender: Any) {
        
          UserDefaults.standard.set(true, forKey: "applyCoupen")
        
        Analytics.logEvent("button_click", parameters: [
        "ans":applyPromoCodeBtn.titleLabel?.text,
        "full": "testing2",
        "question": savepaylbl.text
         ])
        
        let logParams = [
        "ans":applyPromoCodeBtn.titleLabel?.text,
        "full": "testing2",
        "question": savepaylbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "Both"
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.deliveryAddBtn.transform = self.deliveryAddBtn.transform.translatedBy(x: 0, y: -200)
            self.applyPromoCodeBtn.transform = self.applyPromoCodeBtn.transform.translatedBy(x: 0, y: -(self.applyPromoCodeBtn.frame.origin.y - 20))
             self.offerView.transform = self.offerView.transform.translatedBy(x: 0, y: -(self.offerView.frame.origin.y - 20))
            self.cashOnDeliveryBtn.transform = self.cashOnDeliveryBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.onlinePaymentBtn.transform = self.onlinePaymentBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
            self.savingView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
           
            
            
        }) { (_) in
            self.pushToPromo()
        }
    }
    

    func pushToPromo()
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OffersViewController") as? OffersViewController
      
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    
    
    
    @IBAction func backPress(_ sender: Any) {
        
        
      let vc = storyboard?.instantiateViewController(withIdentifier: "addressListViewController") as! addressListViewController
        navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    
    
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    
}

extension UIView {
  func addDashedBorder() {
    let color = UIColor.init(hex: 0x22B573).cgColor

    let shapeLayer:CAShapeLayer = CAShapeLayer()
    let frameSize = self.frame.size
    let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

    shapeLayer.bounds = shapeRect
    shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.strokeColor = color
    shapeLayer.lineWidth = 2
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round
    shapeLayer.lineDashPattern = [6,3]
    shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath

    self.layer.addSublayer(shapeLayer)
    }
}


extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.OpenSans(.semibold,size: 15)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)

        return self
    }

    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)

        return self
    }
}
