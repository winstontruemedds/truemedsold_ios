//
//  exploreOtherFutureViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/13/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import SVGKit
import FirebaseAnalytics
import Flurry_iOS_SDK

class exploreOtherFutureViewController: UIViewController {
    
    @IBOutlet weak var exploreOtherFeature: UIButton!
    
    @IBOutlet weak var questionView: UIView!
  
    @IBOutlet weak var helpBtn: UIButton!
    @IBOutlet weak var myOrderBtn: UIButton!
    @IBOutlet weak var walletBtn: UIButton!
    @IBOutlet weak var moreBtn: UIButton!
    
    var cameFrom = ""
    
    @IBOutlet weak var explorelbl: UILabel!
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    var btn4Text = String()
     
    @IBOutlet weak var paysviu: UIView!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cameFrom = ""
        setDesign()
        
        visualEffectView.alpha = 0.3
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "21") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        explorelbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        let btn3:NSDictionary = btnarr[2] as! NSDictionary
        let btn4:NSDictionary = btnarr[3] as! NSDictionary
        
        btn4Text = btn4.value(forKey: "text") as? String ?? "Back"
              
        
        helpBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        myOrderBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        walletBtn.setTitle(btn3.value(forKey: "text") as? String ?? "", for: .normal)
        moreBtn.setTitle(btn4.value(forKey: "text") as? String ?? "", for: .normal)
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
            
        }
        

        @objc func NotificationReceivedforPayment(notification: Notification) {
            paysuccesspop()

            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                self.paysviu.removeFromSuperview()
                self.visualEffectView.removeFromSuperview()
            }
        
        }
    
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "21") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        explorelbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        let btn3:NSDictionary = btnarr[2] as! NSDictionary
        let btn4:NSDictionary = btnarr[3] as! NSDictionary
        
        helpBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        myOrderBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        walletBtn.setTitle(btn3.value(forKey: "text") as? String ?? "", for: .normal)
        moreBtn.setTitle(btn4.value(forKey: "text") as? String ?? "", for: .normal)
        
        if cameFrom == ""{
            Out()
        }
        
    }
    
    
    func Out(){
        outRight(object: helpBtn)
        outRight(object: myOrderBtn)
        outRight(object: walletBtn)
        outRight(object: moreBtn)
        outLeft(object: questionView)
       
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print(cameFrom)
        
        
        if cameFrom == ""{
            In()
        }else{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.exploreOtherFeature.transform = CGAffineTransform(translationX: -10, y: 0)
                self.helpBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.myOrderBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.walletBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.moreBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
              
                
                
            })
        }
        
        
    }
    
    func In(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.helpBtn.transform = self.helpBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.myOrderBtn.transform = self.myOrderBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.walletBtn.transform = self.walletBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.moreBtn.transform = self.moreBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: self.view.bounds.width, y: 0)
        
            
            
        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.helpBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.myOrderBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.walletBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.moreBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
              
                
            })
        }
    }
    
    
    
    
    
    func setDesign(){
        //btns
        
        backBtn(object: exploreOtherFeature)
        
        blueBtn(object: helpBtn)
        greenBtn(object: myOrderBtn)
        blueBtn(object: walletBtn)
        greenBtn(object: moreBtn)
        labelFont(object: explorelbl)
        
    }
    
    
    
    
    @IBAction func exploreFeaturePress(_ sender: Any) {
        
        navigationController?.popViewController(animated: false)
    }
    
    
    
    
    
    
    
    @IBAction func helpPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":helpBtn.titleLabel?.text,
        "full": "testing2",
        "question": explorelbl.text!
         ])
        
        let logParams = [
        "ans":helpBtn.titleLabel?.text,
        "full": "testing2",
        "question": explorelbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "help"
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.exploreOtherFeature.transform = self.exploreOtherFeature.transform.translatedBy(x: 0, y: -200)
            self.helpBtn.transform = self.helpBtn.transform.translatedBy(x: 0, y: -(self.helpBtn.frame.origin.y - 20))
            self.myOrderBtn.transform = self.myOrderBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.walletBtn.transform = self.walletBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.moreBtn.transform = self.moreBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
          
            
            
        }) { (_) in
            self.HelppushTo()
        }
        
    }
    
    func HelppushTo(){
    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpViewController") as? HelpViewController
    self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    @IBAction func myOrderPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":myOrderBtn.titleLabel?.text,
        "full": "testing2",
        "question": explorelbl.text!
         ])
        
        let logParams = [
        "ans":myOrderBtn.titleLabel?.text,
        "full": "testing2",
        "question": explorelbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "myOrder"
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.exploreOtherFeature.transform = self.exploreOtherFeature.transform.translatedBy(x: 0, y: -200)
            self.myOrderBtn.transform = self.myOrderBtn.transform.translatedBy(x: 0, y: -(self.myOrderBtn.frame.origin.y - 20))
            self.helpBtn.transform = self.helpBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.walletBtn.transform = self.walletBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.moreBtn.transform = self.moreBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
         
            
            
        }) { (_) in
            self.OrderpushTo()
        }
        
        
    }
    
    func OrderpushTo(){
    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderListingViewController") as? OrderListingViewController
    self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    @IBAction func walletPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":walletBtn.titleLabel?.text,
        "full": "testing2",
        "question": explorelbl.text!
         ])
        
        let logParams = [
        "ans":walletBtn.titleLabel?.text,
        "full": "testing2",
        "question": explorelbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "wallet"
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.exploreOtherFeature.transform = self.exploreOtherFeature.transform.translatedBy(x: 0, y: -200)
            self.walletBtn.transform = self.walletBtn.transform.translatedBy(x: 0, y: -(self.walletBtn.frame.origin.y - 20))
            self.myOrderBtn.transform = self.myOrderBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.helpBtn.transform = self.helpBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.moreBtn.transform = self.moreBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
     
            
            
        }) { (_) in
            self.WalletpushTo()
        }
        
    }
    
    func WalletpushTo(){
    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MyWalletViewController") as? MyWalletViewController
    self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    @IBAction func morePress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":moreBtn.titleLabel?.text,
        "full": "testing2",
        "question": explorelbl.text!
         ])
        
        let logParams = [
        "ans":moreBtn.titleLabel?.text,
        "full": "testing2",
        "question": explorelbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "more"
        cameFrom = "Gallery"
           UserDefaults.standard.set(btn4Text, forKey: "backBtnText")
          let btnWidth = moreBtn.frame.size.width
          UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
    
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.exploreOtherFeature.transform = self.exploreOtherFeature.transform.translatedBy(x: 0, y: -200)
            self.moreBtn.transform = self.moreBtn.transform.translatedBy(x: 0, y: -(self.moreBtn.frame.origin.y - 20))
            self.myOrderBtn.transform = self.myOrderBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.helpBtn.transform = self.helpBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.walletBtn.transform = self.walletBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
         
            
            
        }) { (_) in
            self.MorepushTo()
        }
    
    }
    
    func MorepushTo(){
    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "moreExploreViewController") as? moreExploreViewController
    self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    
}
