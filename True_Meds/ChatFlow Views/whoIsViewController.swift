//
//  whoIsViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/9/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK

class whoIsViewController: UIViewController {

    @IBOutlet weak var yesBtn: UIButton! //back btn
    
    @IBOutlet weak var questionView: UIView!
   
    
    @IBOutlet weak var MeBtn: UIButton!
    @IBOutlet weak var someOneElseBtn: UIButton!
    @IBOutlet weak var bothBtn: UIButton!
    
     var cameFrom = ""
    
    @IBOutlet weak var whoislbl: UILabel!
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    var btn1Text = String()
    var btn2Text = String()
    var btn3Text = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cameFrom = ""
        setDesign()
        
        visualEffectView.alpha = 0.3
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
         
         let dict:NSDictionary = maindict.value(forKey: "6") as! NSDictionary
         
         let text:String = dict.value(forKey: "question") as! String
         
         whoislbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
         let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
         let btn1:NSDictionary = btnarr[0] as! NSDictionary
         let btn2:NSDictionary = btnarr[1] as! NSDictionary
         let btn3:NSDictionary = btnarr[2] as! NSDictionary
        
        btn1Text = btn1.value(forKey: "text") as? String ?? "Back"
        btn2Text = btn2.value(forKey: "text") as? String ?? "Back"
        btn3Text = btn3.value(forKey: "text") as? String ?? "Back"
         
         MeBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
         someOneElseBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
         bothBtn.setTitle(btn3.value(forKey: "text") as? String ?? "", for: .normal)
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
    }
    
    @objc func NotificationReceivedforPayment(notification: Notification) {
        paysuccesspop()

        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.paysviu.removeFromSuperview()
            self.visualEffectView.removeFromSuperview()
        }
    
    }
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "6") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        whoislbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        let btn3:NSDictionary = btnarr[2] as! NSDictionary
        
        MeBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        someOneElseBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        bothBtn.setTitle(btn3.value(forKey: "text") as? String ?? "", for: .normal)
        
        if cameFrom == ""{
            Out()
        }
        
    }
    
    
    func Out(){
        outRight(object: MeBtn)
        outRight(object: someOneElseBtn)
        outRight(object: bothBtn)
        outLeft(object: questionView)
       
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print(cameFrom)
        
     
        if cameFrom == ""{
            In()
        }else{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.MeBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.yesBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.someOneElseBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.bothBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
               
                
                
            })
        }
        
        
    }
    
    func In(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.MeBtn.transform = self.MeBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.someOneElseBtn.transform = self.someOneElseBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.bothBtn.transform = self.bothBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: self.view.bounds.width, y: 0)
           
            
            
        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.MeBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.someOneElseBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.bothBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
            
                
            })
        }
    }
    
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    
    func setDesign(){
        //btns
       backBtn(object: yesBtn)
       blueBtn(object: MeBtn)
       greenBtn(object: someOneElseBtn)
       blueBtn(object: bothBtn)
       labelFont(object: whoislbl)
    }
    
    

    
    @IBAction func yesPress(_ sender: Any) {
        
        navigationController?.popViewController(animated: false)
    }
  
    
    
    @IBAction func mePress(_ sender: Any) {
        Analytics.logEvent("button_click", parameters: [
        "ans":MeBtn.titleLabel?.text,
        "full": "testing2",
        "question": whoislbl.text!
         ])
        
        let logParams = [
        "ans":MeBtn.titleLabel?.text,
        "full": "testing2",
        "question": whoislbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "Me"
           UserDefaults.standard.set(cameFrom, forKey: "cameFrom")
        let btnWidth = MeBtn.frame.size.width
            UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
         UserDefaults.standard.set(btn1Text, forKey: "backBtnText")
        
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.yesBtn.transform = self.yesBtn.transform.translatedBy(x: 0, y: -200)
            self.MeBtn.transform = self.MeBtn.transform.translatedBy(x: 0, y: -(self.MeBtn.frame.origin.y - 20))
            self.someOneElseBtn.transform = self.someOneElseBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.bothBtn.transform = self.bothBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
           
           
            
        }) { (_) in
            self.pushTo()
        }
    }
    
    func pushTo(){
        let vc = storyboard?.instantiateViewController(withIdentifier: "cameraGalleryViewController") as! cameraGalleryViewController
        navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func someOneElsePress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":someOneElseBtn.titleLabel?.text,
        "full": "testing2",
        "question": whoislbl.text!
         ])
        
        let logParams = [
        "ans":someOneElseBtn.titleLabel?.text,
        "full": "testing2",
        "question": whoislbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "someOne"
        UserDefaults.standard.set(cameFrom, forKey: "cameFrom")
        let btnWidth = someOneElseBtn.frame.size.width
                     UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
         UserDefaults.standard.set(btn2Text, forKey: "backBtnText")
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.yesBtn.transform = self.yesBtn.transform.translatedBy(x: 0, y: -200)
            self.someOneElseBtn.transform = self.someOneElseBtn.transform.translatedBy(x: 0, y: -(self.someOneElseBtn.frame.origin.y - 20))
            self.MeBtn.transform = self.MeBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.bothBtn.transform = self.bothBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
            
            
        }) { (_) in
            self.pushToAddPatient()
        }
       
        
    }
    
    func pushToAddPatient(){
        
    if UserDefaults.standard.value(forKey:"isPatAvail") as? String == "No"
    {
    UserDefaults.standard.set("Yes", forKey: "cameFrompushpop")
    UserDefaults.standard.synchronize()
        
    let vc = storyboard?.instantiateViewController(withIdentifier: "AddPatientViewController") as! AddPatientViewController
    vc.isfromchat = "Yes"
    navigationController?.pushViewController(vc, animated: false)
    }
    else
    {
        UserDefaults.standard.set("No", forKey: "cameFrompushpop")
        UserDefaults.standard.synchronize()
        let vc = storyboard?.instantiateViewController(withIdentifier: "secondTimeAddPatientViewController") as! secondTimeAddPatientViewController
        
        navigationController?.pushViewController(vc, animated: false)
    }
        
    }
    
    @IBAction func bothPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":bothBtn.titleLabel?.text,
        "full": "testing2",
        "question": whoislbl.text!
         ])
        
        let logParams = [
        "ans":bothBtn.titleLabel?.text,
        "full": "testing2",
        "question": whoislbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
         cameFrom = "Both"
           UserDefaults.standard.set(cameFrom, forKey: "cameFrom")
        let btnWidth = bothBtn.frame.size.width
                   UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
         UserDefaults.standard.set(btn3Text, forKey: "backBtnText")
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.yesBtn.transform = self.yesBtn.transform.translatedBy(x: 0, y: -200)
            self.bothBtn.transform = self.bothBtn.transform.translatedBy(x: 0, y: -(self.bothBtn.frame.origin.y - 20))
            self.someOneElseBtn.transform = self.someOneElseBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.MeBtn.transform = self.MeBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
            
            
        }) { (_) in
            self.pushToAddPatient()
        }
    }
    
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    
    
}


