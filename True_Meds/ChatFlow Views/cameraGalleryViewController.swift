//
//  cameraGalleryViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/10/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import Alamofire
import SVGKit
import SideMenu
import FirebaseAnalytics
import AVFoundation
import Flurry_iOS_SDK

class cameraGalleryViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,DataEnteredDelegate2,DataEnteredDelegate3 {

    
    @IBOutlet weak var myselfBtn: UIButton!
    
    @IBOutlet weak var questionTView: UIView!
  
    let picker = UIImagePickerController()
    
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var galleryBtn: UIButton!
    
    var cameFrom = ""
    var orderID:CLong!
    var PatientID:CLong!
    var filenameID:Int! = 0
    var strBase64:String!
    var imageArray = [Any]()
    var imgIds = [CLong]()
    var resdict = NSDictionary()
    
    @IBOutlet weak var upldlbl: UILabel!
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    var btn1Text = String()
    var btn2Text = String()
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    @IBOutlet var activityView: UIView!
       
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        
       
        
         cameFrom = ""
         setDesign()
     activityFrameandPosition()
        visualEffectView.alpha = 0.3
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "8") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        upldlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        btn1Text = btn1.value(forKey: "text") as? String ?? "Back"
        btn2Text = btn2.value(forKey: "text") as? String ?? "Back"
        
        cameraBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        galleryBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
        
       NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
        
        }
        
        @objc func NotificationReceivedforPayment(notification: Notification) {
            paysuccesspop()

            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                self.paysviu.removeFromSuperview()
                self.visualEffectView.removeFromSuperview()
            }
        }
    
    
    
    func activityFrameandPosition(){

         self.activityView.frame = CGRect(x:0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
          
            self.activityView.layer.cornerRadius = 16
            self.activityView.layer.masksToBounds = true
           
        }
        
        
        func callActivity() {
            
            UIView.animate(withDuration: 0, animations: {
              
                self.view.addSubview(self.activityView)
                
            })
            
            
        }
        
  
    
    
    
    
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil || UserDefaults.standard.object(forKey: "order1") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
       
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
      
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "8") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        upldlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        btn1Text = btn1.value(forKey: "text") as? String ?? "Back"
        btn2Text = btn2.value(forKey: "text") as? String ?? "Back"
               
        
        cameraBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        galleryBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        
        if cameFrom == ""{
            Out()
        }
        
    }
    
    
    func Out(){
        outRight(object: cameraBtn)
        outRight(object: galleryBtn)
        outLeft(object: questionTView)
       
        
    }
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(cameFrom)
        
        
//        if cameFrom == ""{
//            In()
//        }
//        if cameFrom == "Camera" || cameFrom == "Gallery" {
//
//        }
//        if cameFrom == "back"{
//            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//
//                self.cameraBtn.transform = CGAffineTransform(translationX: 10, y: 0)
//                self.galleryBtn.transform = CGAffineTransform(translationX: 10, y: 0)
//                self.questionTView.transform = CGAffineTransform(translationX: -10, y: 0)
//                self.myselfBtn.transform = CGAffineTransform(translationX: 10, y: 0)
//
//
//            })
//         }
//
        
        if cameFrom == ""{
                          In()
                      }
        else{
                                  UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
          
                                        self.cameraBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                                        self.galleryBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                                        self.questionTView.transform = CGAffineTransform(translationX: -10, y: 0)
                                      self.myselfBtn.transform = CGAffineTransform(translationX: 10, y: 0)
          
          
                                  })
          }
        
    }
    
    func In(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.cameraBtn.transform = self.cameraBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.galleryBtn.transform = self.galleryBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.questionTView.transform = self.questionTView.transform.translatedBy(x: self.view.bounds.width, y: 0)
          
            
            
        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.cameraBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.galleryBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionTView.transform = CGAffineTransform(translationX: -10, y: 0)
             
                
            })
        }
    }
    
    
    func userDidEnterInformation2(info: String) {
       if info == "Empty"
        {   orderID = nil
            filenameID = 0
            imageArray.removeAll()
            imgIds.removeAll()
        }
    }
    
    func userDidEnterInformation3(imgarr: [Any], imgids: [CLong])
    {  print(imageArray)
        imageArray = imgarr
        imgIds = imgids
        print(imageArray)
    }
    
    func setDesign(){
        //btns
        backBtn(object: myselfBtn)
        blueBtn(object: cameraBtn)
        greenBtn(object: galleryBtn)
        labelFont(object: upldlbl)
    }

    @IBAction func mySelfPress(_ sender: Any) {
        if imgIds.count != 0 && imageArray.count != 0
        {
         deleteAll()
        }
        else
        {
        self.navigationController?.popViewController(animated: false)
        }
    }
    
    
    @IBAction func cameraPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":cameraBtn.titleLabel?.text,
        "full": "testing2",
        "question": upldlbl.text!
         ])
        
        let logParams = [
        "ans":cameraBtn.titleLabel?.text,
        "full": "testing2",
        "question": upldlbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized: // The user has previously granted access to the camera.
                  DispatchQueue.main.async {
                    self.cameraPress()
                  }
            
            case .notDetermined: // The user has not yet been asked for camera access.
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if granted {
                        DispatchQueue.main.async {
                            self.cameraPress()
                        }
                       
                    }
                }
            
            case .denied:
                // The user has previously denied access.
                 DispatchQueue.main.async {
                    Utility.showAlertWithTitle(title: "", andMessage: "Step 1: Go to Settings, and scroll down to the list of apps, find out the TrueMeds app.\nStep 2: Tap an app and you’ll see the permissions it wants. You can enable or disable camera permissions", onVC: self)
                        }
                return

            case .restricted: // The user can't grant access due to restrictions.
                return
        }
        
      
        
  
       
      
        
    }
    
    
    func cameraPress(){
         cameFrom = "Camera"
        UserDefaults.standard.set(btn1Text, forKey: "backBtnText")
         let btnWidth = cameraBtn.frame.size.width
               UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
           
           UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.cameraBtn.transform = self.cameraBtn.transform.translatedBy(x:0 , y: -(self.cameraBtn.frame.origin.y - 20))
               self.galleryBtn.transform = CGAffineTransform(translationX: self.view.bounds.width, y: 0)
               self.questionTView.transform = CGAffineTransform(translationX: -self.view.bounds.width, y:0 )
               self.myselfBtn.transform = self.myselfBtn.transform.translatedBy(x:0 , y: -self.view.bounds.height)
                
            
          }) { (_) in
                 self.picker.allowsEditing = false
                 self.picker.sourceType = .camera
                 self.picker.delegate = self
            
            self.addChild(self.picker)
            self.picker.didMove(toParent: self)
            self.view!.addSubview(self.picker.view!)
//                 self.present(self.picker, animated: true, completion: nil)
           }

    }
    
    @IBAction func galleryPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":galleryBtn.titleLabel?.text,
        "full": "testing2",
        "question": upldlbl.text!
         ])
        
        let logParams = [
        "ans":galleryBtn.titleLabel?.text,
        "full": "testing2",
        "question": upldlbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "Gallery"
       UserDefaults.standard.set(btn2Text, forKey: "backBtnText")
        let btnWidth = galleryBtn.frame.size.width
               UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.galleryBtn.transform = self.galleryBtn.transform.translatedBy(x:0 , y: -(self.galleryBtn.frame.origin.y - 20))
                    self.cameraBtn.transform = CGAffineTransform(translationX: self.view.bounds.width, y: 0)
                    self.questionTView.transform = CGAffineTransform(translationX: -self.view.bounds.width, y:0 )
                    self.myselfBtn.transform = self.myselfBtn.transform.translatedBy(x:0 , y: -self.view.bounds.height)
                 
               }) { (_) in
                

                      self.picker.allowsEditing = false
                      self.picker.sourceType = .photoLibrary
                      self.picker.delegate = self
                       
                      self.addChild(self.picker)
                      self.picker.didMove(toParent: self)
                      self.view!.addSubview(self.picker.view!)
                
                
//                      self.present(self.picker, animated: true, completion: {
//                      self.picker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
//                              })
                }
        
        
       
        
    }
    

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let images = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        {
            
            let imageData:NSData = images.jpegData(compressionQuality: 0.5) as! NSData
            strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
//             dismiss(animated: false, completion: nil)
            
            self.picker.view!.removeFromSuperview()
                              self.picker.removeFromParent()
            uploadRx()
            activity.startAnimating()
            callActivity()
            
            
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
         self.picker.view!.removeFromSuperview()
                   self.picker.removeFromParent()
        
//        if cameFrom == "Camera"{
//            dismiss(animated: false, completion: nil)
//
//        }else{
//            self.picker.view!.removeFromSuperview()
//            self.picker.removeFromParent()
//        }
        
        
       
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 2, options: .curveEaseOut, animations: {
                                                       
                   self.cameraBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                   self.galleryBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                   self.questionTView.transform = CGAffineTransform(translationX: -10, y: 0)
                   self.myselfBtn.transform = CGAffineTransform(translationX: 10, y: 0)
            })
       }
    


    

    func uploadRx()
    {
        
        let parameters = ["image":strBase64,
        "fileName":"IMG\(String(describing: filenameID!))",
        "orderId":orderID,
        "patientId":PatientID] as [String : Any]
        
          print(parameters)
        
        ApiManager().requestApiWithDataType(methodType:BPOST, urlString: UploadRx(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
            
            print(response)
            
            if cStatus == 200 || cStatus == 201
            {
                self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
                        
                if self.resdict["201"] != nil
                {
               // Utility.showAlertWithFading(title: "", andMessage: self.resdict.value(forKey: "201") as! String, onVC: self)
                }
                
                self.orderID = self.resdict.value(forKey: "OrderId") as! CLong
                UserDefaults.standard.set(self.orderID, forKey: "OrderId")
                UserDefaults.standard.set("true", forKey: "order1")
                UserDefaults.standard.synchronize()
                print(self.resdict.value(forKey: "OrderId") as! CLong)
                self.PatientID = self.resdict.value(forKey: "PatientId") as! CLong
                
                self.filenameID = self.filenameID + 1
                //self.isOpen = "yes"
                        
//                        if self.forothers == "yes"
//                        {
//                            self.imageArray2.append(presdict)
//
//                        }else
//                        {
                self.imgIds.append(self.resdict.value(forKey: "ImageId") as! CLong)
                self.imageArray.append(self.resdict)
                
            
                //siddh
//                let uD = UserDefaults.standard
//                uD.set(self.imgIds, forKey: "imgIds")
//                uD.set( self.imageArray, forKey: "imageArray")
//                uD.synchronize()
                        //}
            
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "morePreViewController") as? morePreViewController
                vc?.imgarray = self.imageArray
                vc?.imgids = self.imgIds
                vc?.delegate2 = self
                vc?.delegate3 = self
                
                self.activity.stopAnimating()
                self.activityView.removeFromSuperview()
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(vc!, animated: false)
                    self.cameFrom = "back"
                }
                
            }
            else if cStatus == 401
            {
                let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
            }
            else if cStatus == 500
            {
//                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                
               let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

               self.addChild(popupVC)
               popupVC.view.frame = self.view.frame
               self.view.addSubview(popupVC.view)
               popupVC.didMove(toParent: self)
            }
            else
            {
                let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
            }

     }
        
    }
    
    
    func deleteAll()
    {
        let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
        let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
        let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
        { (action:UIAlertAction!) in
        
            var para2 = ["orderId":CLong(self.orderID),"edit":false] as [String : Any]
            
            let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
            
            print(para2)
            
            let url =  DeleteImgs()
            
            var items = [URLQueryItem]()
            var myURL = URLComponents(string: url)
            
            let session = URLSession.shared
            
            for (key,value) in para2 {
                items.append(URLQueryItem(name: key, value: String(describing:value)))
            }
            
            myURL?.queryItems = items
            
            var request = URLRequest(url: (myURL?.url)!)
            
            let json = self.imgIds
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            
            request.httpBody = jsonData
            
            request.httpMethod = "POST"
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("a", forHTTPHeaderField: "transactionId")
            request.addValue(authtok, forHTTPHeaderField: "Authorization")
            
            //request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                if let data = data {
                
                do {
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                        DispatchQueue.main.async {
                            let httpResponse = response as? HTTPURLResponse
                            
                            if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                            {
                                
                                let deldict:NSDictionary = json as NSDictionary
                                
                                if deldict["201"] != nil
                                {
                                    self.imageArray.removeAll()
                                    self.imgIds.removeAll()
                                    //let alert = UIAlertController(title:"", message: deldict.value(forKey: "201") as? String, preferredStyle: .alert)
                                    
                                   // self.present(alert, animated: true, completion: nil);
                                    
//                                    let when = DispatchTime.now() + 2
//                                    DispatchQueue.main.asyncAfter(deadline: when){
//                                       alert.dismiss(animated: true, completion: nil)
                                       self.navigationController?.popViewController(animated: false)
                               //     }
                            
                                }
                            
                            }
                            else if httpResponse?.statusCode == 401
                            {
                                let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                            }
                            else if httpResponse?.statusCode == 500
                            {
                                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                
                                
//                                let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
//
//                                self.addChild(popupVC)
//                                popupVC.view.frame = self.view.frame
//                                self.view.addSubview(popupVC.view)
//                                popupVC.didMove(toParent: self)
                            }
                            else
                            {
                                let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                            }
                        }
                        
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
                }
            })
            task.resume()
            
            
            
            
        }
        alertVC.addAction(Action1);
        let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
        alertVC.addAction(Action2);
        
        self.present(alertVC, animated: true, completion: nil)
        
    }
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
        
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}

