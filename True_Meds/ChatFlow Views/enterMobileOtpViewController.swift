//
//  enterMobileOtpViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/6/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import PinCodeTextField
import TPKeyboardAvoiding
import Alamofire
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK

class enterMobileOtpViewController: UIViewController, UITextFieldDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource  {
    
    @IBOutlet weak var topConstains: NSLayoutConstraint!
    
    var seconds = 60
    var timer = Timer()
    var isTimerRunning = false
    var otpCode:String!
    
    @IBOutlet weak var txtOTP4: UITextField!
    @IBOutlet weak var txtOTP3: UITextField!
    @IBOutlet weak var txtOTP2: UITextField!
    @IBOutlet weak var txtOTP1: UITextField!
    
    @IBOutlet weak var topConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var iacceptLbl: UILabel!
    @IBOutlet weak var yesIm18Lbl: UILabel!
    
    @IBOutlet weak var lblImageView: UIView!
    @IBOutlet weak var mobileotpView: UIView!
    
     @IBOutlet weak var buyMedBtn: UIButton!
    
    @IBOutlet weak var otpView: UIView!
    
    @IBOutlet weak var mobileNo: PinCodeTextField!
    
//    @IBOutlet weak var otpNo: PinCodeTextField!
    
    @IBOutlet weak var iAcceptBtn: UIButton!
    @IBOutlet weak var yes18Btn: UIButton!
    
    @IBOutlet weak var generateOtpBtnView: UIView!
    
    @IBOutlet weak var generateOtpBtn: UIButton!
    
    @IBOutlet weak var entermoblbl: UILabel!
    
    var mobnoval:String!
    
    var otpval:String!
    
    var resdict = NSDictionary()
    var resdict2 = NSDictionary()
    var resdict3 = NSDictionary()
    var resdict4 = NSDictionary()
    
    var referarr = NSArray()
    
    var addressDict = NSArray()
    var apatarr = NSArray()
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    var langID2:Int!
    var agegroupID2:Int!
    
    @IBOutlet weak var tcpopviu: UIView!
    
    @IBOutlet weak var tnctv: UITextView!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    var rewCustId:CLong!
    
    @IBOutlet weak var inviteviu: UIView!
    
//    @IBOutlet weak var invitecoll: UICollectionView!
    
    @IBOutlet weak var claimrewbtno: UIButton!
    
    var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         hideKeyboardTapAround()
         setupOTPView()
        inviteviu.layer.cornerRadius = 4.0
        visualEffectView.alpha = 0.3
        claimrewbtno.layer.borderWidth = 0.8
        claimrewbtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        claimrewbtno.layer.cornerRadius = 4.0
        GetTnC()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
                target: self, action: #selector(didTapLabelTC))
        iacceptLbl.isUserInteractionEnabled = true
        iacceptLbl.addGestureRecognizer(tap)
        
        
       
        
        setDesign()
        mobileNo.delegate = self
//        otpNo.delegate = self
        mobileNo.keyboardType = .phonePad
        mobileNo.updatedUnderlineColor = UIColor.init(hex: 0x22B573)
        mobileNo.placeholderColor = UIColor.init(hex: 0x22B573)
//        otpNo.keyboardType = .phonePad
//        otpNo.updatedUnderlineColor = UIColor.init(hex: 0x22B573)
//        otpNo.placeholderColor = UIColor.init(hex: 0x22B573)
        
        otpView.isHidden = true
        setButtonImage()
        
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "13") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        entermoblbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        //let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
       // let btn3:NSDictionary = btnarr[2] as! NSDictionary
    
        generateOtpBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
       
    }
    
    
    
    func setupOTPView(){
               txtOTP1.backgroundColor = UIColor.clear
               txtOTP2.backgroundColor = UIColor.clear
               txtOTP3.backgroundColor = UIColor.clear
               txtOTP4.backgroundColor = UIColor.clear
        
     
               
               addBottomBorderTo(textField: txtOTP1)
               addBottomBorderTo(textField: txtOTP2)
               addBottomBorderTo(textField: txtOTP3)
               addBottomBorderTo(textField: txtOTP4)
               
               txtOTP1.delegate = self
               txtOTP2.delegate = self
               txtOTP3.delegate = self
               txtOTP4.delegate = self
              

               
    }
    
    
    func otpcodeToatl(){
        otpCode = txtOTP1.text! + txtOTP2.text! + txtOTP3.text! + txtOTP4.text!
        print(otpCode)
        if otpCode.count == 4 {
            
                if iAcceptBtn.currentImage == UIImage(named: "untick")
                 {
                 let localizedContent = NSLocalizedString("Please accept terms and conditions.", comment: "")
                 Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
                    txtOTP1.text = ""
                    txtOTP2.text = ""
                    txtOTP3.text = ""
                    txtOTP4.text = ""
                              
                 }
                 else if yes18Btn.currentImage == UIImage(named: "untick")
                 {
                 let localizedContent = NSLocalizedString("Sorry, Truemeds is only for people older than 18 years", comment: "")
                 Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
                    txtOTP1.text = ""
                    txtOTP2.text = ""
                    txtOTP3.text = ""
                    txtOTP4.text = ""
                 }
                 else
                 {
                  verifyOTP()
                 }
            
            
            
        }
        
    }
    
    
    func addBottomBorderTo(textField:UITextField) {
          let layer = CALayer()
          layer.backgroundColor = UIColor.gray.cgColor
          layer.frame = CGRect(x: 0.0, y: textField.frame.size.height - 2.0, width: textField.frame.size.width, height: 2.0)
          textField.layer.addSublayer(layer)
      }
    
    
      
       func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           if ((textField.text?.count)! < 1 ) && (string.count > 0) {
               if textField == txtOTP1 {
                   txtOTP2.becomeFirstResponder()
               }
               
               if textField == txtOTP2 {
                   txtOTP3.becomeFirstResponder()
               }
               
               if textField == txtOTP3 {
                   txtOTP4.becomeFirstResponder()
               }
               
               if textField == txtOTP4 {
                   txtOTP4.resignFirstResponder()
                   
               }
               
               textField.text = string
               otpcodeToatl()
               return false
           } else if ((textField.text?.count)! >= 1) && (string.count == 0) {
               if textField == txtOTP2 {
                   txtOTP1.becomeFirstResponder()
               }
               if textField == txtOTP3 {
                   txtOTP2.becomeFirstResponder()
               }
               if textField == txtOTP4 {
                   txtOTP3.becomeFirstResponder()
               }
               if textField == txtOTP1 {
                   txtOTP1.resignFirstResponder()
               }
               
               textField.text = ""
               return false
           } else if (textField.text?.count)! >= 1 {
               textField.text = string
               return false
           }
           
           return true
       }
    
    
    
    
    @IBAction func fourTextFieldend(_ sender: Any) {
        
        
    }
    
    
    
    
    @IBAction func claimReferalbonus(_ sender: UIButton) {
        if rewCustId == nil{
            Utility.showAlertWithTitle(title: "", andMessage: "please select at least one reference", onVC: self)
        }else{
          claimRef()
        }
        
        
    }
    
    @IBAction func skipinvite(_ sender: Any) {
        
//        self.getAllAddress()
//        self.allpatients()
//        self.allorders()
//        self.updateSetts()
        
        inviteviu.removeFromSuperview()
        visualEffectView.removeFromSuperview()
        
    }
    
    @objc func didTapLabelTC(sender: UITapGestureRecognizer)
    {
     TCpopview()
        
     view.endEditing(true)
    }
    
    
    @IBAction func closetcpop(_ sender: UIButton) {
        tcpopviu.removeFromSuperview()
        visualEffectView.removeFromSuperview()
    }
    
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    
  @IBAction func gotoMenu(_ sender: Any) {
      let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
      
       let menu2 = SideMenuNavigationController(rootViewController: menu)
      
       menu2.statusBarEndAlpha = 0
      
       present(menu2, animated: true, completion: nil)
  }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
       
     
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
            
        let dict:NSDictionary = maindict.value(forKey: "13") as! NSDictionary
            
        let text:String = dict.value(forKey: "question") as! String
            
        entermoblbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        //let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        // let btn3:NSDictionary = btnarr[2] as! NSDictionary
        
        generateOtpBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
     
      self.scrollView.transform = self.scrollView.transform.translatedBy(x: 0, y: self.view.bounds.height )
        
        
        uderlineLbl(object: iacceptLbl, UderlineString: NSLocalizedString("I accept terms and conditions", comment: ""))
               yesIm18Lbl.text = NSLocalizedString("Yes,I am 18+ years old", comment: "")
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
           
     self.scrollView.transform = self.scrollView.transform.translatedBy(x:0 , y: -self.view.bounds.height)
           
            
            
        }){ (_) in
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                 self.mobileNo.becomeFirstResponder()
                
            })
        }
        
       
        
    }
    
    
    @objc func keyboardWillAppear() {
               
                topConstains.constant = 10
                //Do something here
            }
            
            @objc func keyboardWillDisappear() {
                topConstains.constant = 100
                //Do something here
            }
    
    func setDesign(){
        
        blueBtn(object: generateOtpBtn)
    
        backBtn(object: buyMedBtn)
        
        labelFont(object: entermoblbl)
        
        
        
    }
    
    
    func setButtonImage(){
        
        iAcceptBtn.setImage(UIImage(named:"tick"), for: .normal)
        iAcceptBtn.setImage(UIImage(named:"untick"), for: .selected)
        yes18Btn.setImage(UIImage(named:"tick"), for: .normal)
        yes18Btn.setImage(UIImage(named:"untick"), for: .selected)
        
    }

    
    
    
    @IBAction func buyMedPress(_ sender: Any) {
        
         navigationController?.popViewController(animated: false)
    }
    
 
    
    
    
    @IBAction func generateOtpPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":generateOtpBtn.titleLabel?.text,
        "full": "testing2",
        "question": entermoblbl.text!
         ])
        
        let logParams = [
        "ans":generateOtpBtn.titleLabel?.text,
        "full": "testing2",
        "question": entermoblbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        if mobnoval == "" || mobnoval == nil
        {
         let localizedContent = NSLocalizedString("Please enter a valid mobile number", comment: "")
            Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
        else{
        if Utility.isValidMobNo(mobnoval) == false
        {
         let localizedContent = NSLocalizedString("Please enter a valid mobile number", comment: "")
            Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
        else if iAcceptBtn.currentImage == UIImage(named: "untick")
        {
        let localizedContent = NSLocalizedString("Please accept terms and conditions.", comment: "")
        Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
        }
        else if yes18Btn.currentImage == UIImage(named: "untick")
        {
        let localizedContent = NSLocalizedString("Sorry, Truemeds is only for people older than 18 years", comment: "")
        Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
        }
        else
        {
        
       // if Connectivity.isConnectedToInternet
      //  {
           //timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerRunning), userInfo: nil, repeats: true)
           getOTP()
       // }
      //  else
      //  {
            //    Utility.showNetworkAlert(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
      //  }
            
        }
        }
        
    }
    
    @IBAction func acceptPress(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            
        }) { (success) in
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
                sender.isSelected = !sender.isSelected
                sender.transform = .identity
            }, completion: nil)
        }
        

        
        
    }
    
    
    @IBAction func yes18Press(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            
        }) { (success) in
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
                sender.isSelected = !sender.isSelected
                sender.transform = .identity
            }, completion: nil)
        }
        
       // let vc = storyboard?.instantiateViewController(withIdentifier: "validPrescriptionViewController") as! validPrescriptionViewController
       // navigationController?.pushViewController(vc, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
//        var count:Int!
//
//        if collectionView == invitecoll
//        {
//        count = referarr.count
//        }else{
//        count = referarr.count
//        }
    
        return referarr.count
    
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
//        if collectionView == invitecoll
//        {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InviteCollectionVC2", for: indexPath) as! InviteCollectionVC2
        cell.layer.cornerRadius = 4.0
        cell.layer.borderWidth = 0.8
        cell.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        let rewdict:NSDictionary = referarr[indexPath.row] as! NSDictionary
        if let rewname = rewdict.value(forKey: "name") as? String
        {
        cell.referaluser2.text = rewname
        }
        if selectedIndexPath != nil && indexPath == selectedIndexPath {
            cell.backgroundColor = UIColor(hexString: "#0071BC")
            cell.referaluser2.textColor = UIColor.white
        }else{
                
         cell.backgroundColor = UIColor.white
         cell.referaluser2.textColor = UIColor(hexString: "#0071BC")
        }
         
       
        return cell
            
//        }
//        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
       {
//           if collectionView == invitecoll
//           {
            return CGSize(width: collectionView.bounds.size.width/3 - 5 , height: 40)
//           }
        
//           return CGSize(width: collectionView.bounds.size.width * 0.6, height: 100)
       }
       
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           
//           if collectionView == invitecoll
//           {
               self.selectedIndexPath = indexPath
               let cell = collectionView.cellForItem(at: indexPath) as! InviteCollectionVC2
               cell.backgroundColor = UIColor(hexString: "#0071BC")
               cell.referaluser2.textColor = UIColor.white
               let mydict:NSDictionary = referarr[indexPath.row] as! NSDictionary
               let rewcust:CLong = mydict.value(forKey: "customerId") as! CLong
               rewCustId = rewcust
           
//           }
    }
    
    func getOTP()
    {
        let parameters: Parameters = ["mobileNo": mobnoval!]
        
        ApiManager().requestApiWithDataType( methodType: HPOST, urlString: SendOTP(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
            
            if cStatus == 200 || cStatus == 201
            {
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerRunning), userInfo: nil, repeats: true)
                    UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            
                        self.otpView.isHidden = false
                        self.txtOTP1.becomeFirstResponder()
                        self.mobileNo.isUserInteractionEnabled = false
                    })
            }
            else if cStatus == 401
            {
                let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
            }
            else if cStatus == 500
            {
//                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                
                let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                self.addChild(popupVC)
                popupVC.view.frame = self.view.frame
                self.view.addSubview(popupVC.view)
                popupVC.didMove(toParent: self)
            }
            else
            {
                let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
            }
            
        }
    }
    
    
    func verifyOTP()
    {
        let fcmtoken:String = UserDefaults.standard.value(forKey: "FTOKEN") as! String
        let parameters = ["mobileNo": mobnoval!,"otp":otpCode!,"deviceKey":fcmtoken,"isIos":true] as [String : Any]
        print(parameters)
        ApiManager().requestApiWithDataType( methodType: HPOST, urlString:VerifyOTP(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
            
            print(response)
            
            if cStatus == 200 || cStatus == 201
            {
            
                self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
                
                let custdto:NSDictionary = self.resdict.value(forKey: "CustomerDto") as! NSDictionary
                  
                  let custid:CLong = custdto.value(forKey: "customerId") as! CLong
          
                  UserDefaults.standard.set(String(describing:custid), forKey: "CustomerId")
                  UserDefaults.standard.synchronize()
                  
                  if let custName = custdto.value(forKey: "customerName") as? String
                  {
                  UserDefaults.standard.set(custName, forKey: "custname")
                  UserDefaults.standard.synchronize()
                  }
                  
                  UserDefaults.standard.set(self.mobnoval!, forKey: "mobileNo")
                  UserDefaults.standard.synchronize()
                  
                 let resdict2:NSDictionary = self.resdict.value(forKey: "Response") as! NSDictionary
                  let accesstok:String = resdict2.value(forKey: "access_token") as! String
                  let tokentype:String = resdict2.value(forKey: "token_type") as! String
                  let finaccess:String = tokentype + " " + accesstok
                  UserDefaults.standard.set(finaccess, forKey: "access_token")
                  UserDefaults.standard.synchronize()
                  
                  self.referarr = self.resdict.value(forKey: "referArray") as! NSArray
                  print(self.referarr)
                print(self.referarr.count)
                  if self.referarr.count != 0
                  {
                  self.timer.invalidate()
                  self.InviteRewpop1()
                  
                  }
                  else
                  {
                      self.getAllAddress()
                      self.Getallpatients()
//                      self.allorders()
                      self.updateSetts()

//                      self.correctno = "correct"
//                      self.timer.invalidate()
//
//                      self.changerow()
                
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "validPrescriptionViewController") as? validPrescriptionViewController
                
                    self.navigationController?.pushViewController(vc!, animated: false)
                  }
            }
            else if cStatus == 401
            {
                let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
            }
            else if cStatus == 400
            {
                let adddict:NSDictionary = convertStringToDictionary(json: response as! String)! as NSDictionary
                if adddict.allKeys.first as! String == "400"
                {
                    Utility.showAlertWithTitle(title: "", andMessage: adddict.value(forKey: "400") as! String, onVC: self)
                }
//                let localizedContent = NSLocalizedString("Please enter correct otp", comment: "")
//                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                }

            else if cStatus == 500
            {
//                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                
                let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                self.addChild(popupVC)
                popupVC.view.frame = self.view.frame
                self.view.addSubview(popupVC.view)
                popupVC.didMove(toParent: self)
            }
            else
            {
                let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
            }
        }
    }
    
    
    func updateSetts()
    {
        let custid:String = String(describing:UserDefaults.standard.value(forKey: "CustomerId")!)
        agegroupID2 = UserDefaults.standard.value(forKey: "ageGroupId") as! Int
        langID2 = UserDefaults.standard.value(forKey: "languageId") as! Int
        let parameters = ["ageGroupId":CLong(agegroupID2),
                          "languageId":CLong(langID2),
                          "customerId":custid
            ] as [String : Any]
        
        ApiManager().requestApiWithDataType(methodType:HPOST, urlString: UpdateSettings(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
                        
            if cStatus == 200 || cStatus == 201
            {
               
            }
            else if cStatus == 401
            {
                let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
            }
            else if cStatus == 500
            {
//                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                
                let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                self.addChild(popupVC)
                popupVC.view.frame = self.view.frame
                self.view.addSubview(popupVC.view)
                popupVC.didMove(toParent: self)
            }
            else
            {
                let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
            }
        }
    }
    
    
    
    
    @objc func timerRunning(_ sender: UIButton)
    {
     seconds = seconds - 1
     if seconds == 1
     {
     timer.invalidate()
     seconds = 60
     generateOtpBtn.setTitle(NSLocalizedString("Resend OTP", comment: ""), for: .normal)
     generateOtpBtn.isUserInteractionEnabled = true
     self.mobileNo.isUserInteractionEnabled = true
     }
     else
     {
      generateOtpBtn.setTitle(String(describing: seconds) + " secs", for: .normal)
      generateOtpBtn.isUserInteractionEnabled = false
      }
     
    }
    
}

extension enterMobileOtpViewController: PinCodeTextFieldDelegate{
    
    
    func textFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool {
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
//            self.topConstraints.constant = -180
            
             self.generateOtpBtnView.isHidden = false
            
        }) //{ (success) in
//            UIView.animate(withDuration: 0.4, delay: 0, options: .curveLinear, animations: {
//                self.generateOtpBtnView.isHidden = false
//            }, completion: nil)
//        }
        
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: PinCodeTextField) -> Bool {
        print("textshould")
        
        return true
    }
    
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        
        
        
       if textField == mobileNo
       {
        
        mobnoval = textField.text ?? ""
        
        
        let value = textField.text ?? ""
        print("value changed: \(value)")
        let count = value.count
        print(count)
        
       }
        
//        if textField == otpNo
//        {
//         otpval = textField.text ?? ""
//
//         if otpval.count == 4
//         {
//          verifyOTP()
//         }
//         else
//         {
//
//         }
//
//        }
//
    }
    
    func textFieldDidBeginEditing(_ textField: PinCodeTextField) {
        print("textDid")
    
    }
    
   func getAllAddress()
   {
       let parameters = ["":""]
       
       ApiManager().requestApiWithDataType(methodType:HPOST, urlString: GetAllAddress(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
           
           if cStatus == 200 || cStatus == 201
           {
             self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
             self.addressDict = self.resdict.value(forKey: "AddressList") as! NSArray
            
            print(self.addressDict)
            
            if self.addressDict.count == 0
            {
                UserDefaults.standard.set("No", forKey: "isAddAvail")
            }
            else
            {
               // UserDefaults.standard.set(self.addressDict, forKey: "isAddAvail")
            }
            
           }
        else if cStatus == 401
        {
            let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
            Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
        }
        else if cStatus == 500
        {
//            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
            
            let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

            self.addChild(popupVC)
            popupVC.view.frame = self.view.frame
            self.view.addSubview(popupVC.view)
            popupVC.didMove(toParent: self)
        }
        else
        {
            let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
        }
           
       }
   }
    
    
    func Getallpatients(){
                let para2 = ["showMyself":"false"]
               
               ApiManager().requestApiWithDataType(methodType:HPOST, urlString: GetAllPatients(),parameters: para2 as [String : AnyObject]) { (response,cStatus, error) in
                                
                                if cStatus == 200 || cStatus == 201
                                {
                                   self.resdict3 = convertStringToDictionary(json: response as! String)! as NSDictionary
       
                                    self.apatarr = self.resdict3.value(forKey: "PatientList") as! NSArray
                                    
                                    print(self.apatarr)
                                   
                                    if self.apatarr.count == 0
                                    {
                                    UserDefaults.standard.set("No", forKey: "isPatAvail")
                                    }
                                    else
                                    {
                                    //UserDefaults.standard.set(self.apatarr as NSArray, forKey: "isPatAvail")
                                    }
                                
                                }
                else if cStatus == 401
                {
                    let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                    Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                }
                else if cStatus == 500
                {
//                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    
                    let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                    self.addChild(popupVC)
                    popupVC.view.frame = self.view.frame
                    self.view.addSubview(popupVC.view)
                    popupVC.didMove(toParent: self)
                }
                else
                {
                    let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                }
                }
           }
    
    
    func GetTnC(){
         let para2 = ["":""]
            
            ApiManager().requestApiWithDataType(methodType:POST, urlString: TnC(),parameters: para2 as [String : AnyObject]) { (response,cStatus, error) in
                             
                             if cStatus == 200 || cStatus == 201
                             {
                               self.resdict4 = convertStringToDictionary(json: response as! String)! as NSDictionary
    
                               let tncarr = self.resdict4.value(forKey: "Legals") as! NSArray
                               let fintncdict:NSDictionary = tncarr[0] as! NSDictionary
                               let header:String = fintncdict.value(forKey: "header") as! String
                               let desc:String = fintncdict.value(forKey: "description") as! String
                               self.tnctv.text = header + "\n\n" + desc
                             }
                else if cStatus == 401
                {
                    let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                    Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                }
                else if cStatus == 500
                {
//                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    
                    let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                    self.addChild(popupVC)
                    popupVC.view.frame = self.view.frame
                    self.view.addSubview(popupVC.view)
                    popupVC.didMove(toParent: self)
                }
                else
                {
                    let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                }
            }
        }
    
    
    func claimRef(){
             let para2 = ["customerId":rewCustId!,
                          "mobile":mobnoval!
                 ] as [String : Any]
             
            ApiManager().requestApiWithDataType(methodType:HPOST, urlString: ClaimReff(),parameters: para2 as [String : AnyObject]) { (response,cStatus, error) in
                             
                             if cStatus == 200 || cStatus == 201
                             {
                               // self.resdict3 = convertStringToDictionary(json: response as! String)! as NSDictionary
    
                                 self.inviteviu.removeFromSuperview()
                         self.visualEffectView.removeFromSuperview()
                                self.getAllAddress()
                                self.Getallpatients()
                                //                      self.allorders()
                                                      self.updateSetts()

                                //                      self.correctno = "correct"
                                //                      self.timer.invalidate()
                                //
                                //                      self.changerow()
                                                
                                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "validPrescriptionViewController") as? validPrescriptionViewController
                                                
                                                    self.navigationController?.pushViewController(vc!, animated: false)
                             
                             }
                else if cStatus == 401
                {
                    let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                    Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                }
                else if cStatus == 500
                {
//                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    
                    
                    let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                    self.addChild(popupVC)
                    popupVC.view.frame = self.view.frame
                    self.view.addSubview(popupVC.view)
                    popupVC.didMove(toParent: self)
                }
                else
                {
                    let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                }
                     }
        }
    
    
    func TCpopview()
    {
        tcpopviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(tcpopviu)
        
        let leadingConstraint = NSLayoutConstraint(item: tcpopviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: tcpopviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: tcpopviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: tcpopviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: tcpopviu, attribute: .bottom, relatedBy: .equal,
                                                      toItem: tcpopviu.superview, attribute: .bottom,
                                                      multiplier: 1.0, constant: -40.0)
        
        let horizontalConstraint2 = NSLayoutConstraint(item: tcpopviu, attribute: .top, relatedBy: .equal,
                                                       toItem: tcpopviu.superview, attribute: .top,
                                                       multiplier: 1.0, constant: 40.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,horizontalConstraint2])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    
    func InviteRewpop1()
    {
        view.endEditing(true)
        inviteviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(inviteviu)
        
        let leadingConstraint = NSLayoutConstraint(item: inviteviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: inviteviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: inviteviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: inviteviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: inviteviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: inviteviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: inviteviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: inviteviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }

}


