//
//  searchAndAddViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/10/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

protocol DataEnteredDelegate4: class {
    func userDidEnterInformation4(info: String,info2:Int)
}


import UIKit
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK

class searchAndAddViewController: UIViewController {
    
    weak var delegate4: DataEnteredDelegate4? = nil

    @IBOutlet weak var questionTView: UIView!
  
    @IBOutlet weak var yesSearchBtn: UIButton!
    
    @IBOutlet weak var noIwantBtn: UIButton!
    
    
    @IBOutlet weak var noBtn: UIButton! // backbtn
    
    @IBOutlet weak var searchaddlbl: UILabel!
    
    var cameFrom = ""
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    var namecount3:Int!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    var btn1Text = String()
    var btn2Text = String()
       
    override func viewDidLoad() {
        super.viewDidLoad()
        cameFrom = ""
        setDesign()
        
        visualEffectView.alpha = 0.3

        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "12") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        searchaddlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        btn1Text = btn1.value(forKey: "text") as? String ?? "Back"
        btn2Text = btn2.value(forKey: "text") as? String ?? "Back"
        
        yesSearchBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        noIwantBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
        
       NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
    
    }
    
    @objc func NotificationReceivedforPayment(notification: Notification) {
        paysuccesspop()

        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.paysviu.removeFromSuperview()
            self.visualEffectView.removeFromSuperview()
        }
    }
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "12") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        searchaddlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        yesSearchBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        noIwantBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        
        if cameFrom == ""{
            Out()
        }
        
    }
    
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    
    func Out(){
        outRight(object: yesSearchBtn)
        outRight(object: noIwantBtn)
        outLeft(object: questionTView)
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print(cameFrom)
        
        
        if cameFrom == ""{
            In()
        }else{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.yesSearchBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.noIwantBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionTView.transform = CGAffineTransform(translationX: -10, y: 0)
             
                
                
            })
        }
        
        
    }
    
    func In(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.yesSearchBtn.transform = self.yesSearchBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.noIwantBtn.transform = self.noIwantBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.questionTView.transform = self.questionTView.transform.translatedBy(x: self.view.bounds.width, y: 0)
       
            
            
        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.yesSearchBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.noIwantBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionTView.transform = CGAffineTransform(translationX: -10, y: 0)
            
                
            })
        }
    }
    
    
    
    func setDesign(){
        //btns
       
        backBtn(object: noBtn)
        blueBtn(object: yesSearchBtn)
        greenBtn(object: noIwantBtn)
      
        
    }
    
    @IBAction func noPress(_ sender: Any) {
    
        delegate4?.userDidEnterInformation4(info: "MultiBack",info2:namecount3)
        navigationController?.popViewController(animated: false)
       
    }
    
    
    @IBAction func yesSearchBtnPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
               "ans":yesSearchBtn.titleLabel?.text,
               "full": "testing2",
               "question": searchaddlbl.text
                ])
        
        let logParams = [
        "ans":yesSearchBtn.titleLabel?.text,
        "full": "testing2",
        "question": searchaddlbl.text
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        UserDefaults.standard.set(btn1Text, forKey: "backBtnText")
        let btnWidth = yesSearchBtn.frame.size.width
        UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
    
        let vc = storyboard?.instantiateViewController(withIdentifier: "bottomSearchBtnViewController") as! bottomSearchBtnViewController
        navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    @IBAction func noIwantBtnPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":noIwantBtn.titleLabel?.text,
        "full": "testing2",
        "question": searchaddlbl.text
         ])
        
        
        let logParams = [
        "ans":noIwantBtn.titleLabel?.text,
        "full": "testing2",
        "question": searchaddlbl.text
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        let btnWidth = noIwantBtn.frame.size.width
        UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
        UserDefaults.standard.set(btn2Text, forKey: "backBtnText")
        
    if UserDefaults.standard.value(forKey:"custname") == nil || UserDefaults.standard.value(forKey:"custname") as? String == ""
    {
    let vc = storyboard?.instantiateViewController(withIdentifier: "PeronalinfoViewController") as! PeronalinfoViewController
    navigationController?.pushViewController(vc, animated: false)
    }
    else if UserDefaults.standard.value(forKey:"isAddAvail") as? String == "No"
    {
       let vc = storyboard?.instantiateViewController(withIdentifier: "deliveryPincodeViewController") as! deliveryPincodeViewController
       navigationController?.pushViewController(vc, animated: false)
    }
    else
    {
        //Address Listing
        let vc = storyboard?.instantiateViewController(withIdentifier: "addressListViewController") as! addressListViewController
        navigationController?.pushViewController(vc, animated: false)
    }
        
    }
    
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }

    

}
