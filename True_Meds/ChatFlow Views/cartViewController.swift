//
//  cartViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/10/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseAnalytics
import Flurry_iOS_SDK

class cartViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {

    var resdict = NSDictionary()
    
    var discountPercentage:Double!
    
    @IBOutlet weak var activityInd: UIActivityIndicatorView!
    @IBOutlet weak var discountPectLbl: UILabel!
    @IBOutlet weak var cancelMRP: UILabel!
    
    @IBOutlet weak var cartempty: UIView!
    
    @IBOutlet weak var tableviewContainer: UIView!
   
    var savingPrice:Double!
    var savingPrct:Double!
    var orderID:CLong! = 0
    
   
    @IBOutlet weak var ifaddView: UIView!
    
    @IBOutlet weak var animateView: UIView!
    
    @IBOutlet weak var searchForMedicineBar: UISearchBar!
    
    let bottomLine = CALayer()
    
    @IBOutlet weak var searchtable: UITableView!
    
    @IBOutlet weak var searchtable2: UITableView!
    
    @IBOutlet weak var qtylistable: UITableView!
    
    @IBOutlet weak var medicnetextlbl: UILabel!
    
    var medict = NSDictionary()
    
    var selmedict = NSDictionary()
    
    var searchArray :Array<Dictionary<String,String>> = []
    
    var addedArray :Array<Dictionary<String,String>> = []
    
    var searchActive = false
    
    var filterArray = NSMutableArray()
    
    var dictionaryA = ["productCode":String(),"quantity":String(),"pack":String(),"medicineName":String(),"price":Double(),"discountPercentage":Double()] as [String : Any]
    
    var ArrayB = [[String:String]]()
    
    var mainarr = [Any]()
    
    var variantarr = NSArray()
    
    var variantarr2 = NSArray()
    
    var variantarr3 = NSArray()
    
    var variantdict = NSDictionary()
    
    var searchstrs:String!
    
    var price:Double!
    
    var tprice:Double = 0
    
    var price2:Double!
       
    var tprice2:Double = 0
    
    var indicator = UIActivityIndicatorView()
    
    let qtyarr = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"]
    
    var selectTag:IndexPath!
    
    var itemcount:Int = 0
    
    var qtycount:Int = 1
    
    @IBOutlet weak var totalprice: UILabel!
    
//    @IBOutlet weak var ftotalpricelbl: UILabel!
    
    @IBOutlet weak var itemcountlbl: UILabel!
    
    @IBOutlet weak var qtylistviu: UIView!
    
    @IBOutlet weak var qtyviublur: UIVisualEffectView!
    
    @IBOutlet weak var closeqtyviubtno: UIButton!
    
   
    
    @IBOutlet weak var Addmorebtno: UIButton!
    
    @IBOutlet weak var confmedbtn: UIButton!
    
    var searplace:String!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    @IBOutlet weak var emcartTryagin: UIButton!
    
    @IBOutlet weak var EMtextlbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cartempty.isHidden = true
        medicnetextlbl.isHidden = true
        EMtextlbl.text = NSLocalizedString("No Items Found", comment: "")
        
        emcartTryagin.layer.borderWidth = 0.8
        emcartTryagin.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        emcartTryagin.layer.cornerRadius = 4.0
        
        emcartTryagin.setTitle(NSLocalizedString("Try Again", comment: ""), for: .normal)
        
      setSearchBar()
      hideKeyboardTapAround()
      
      visualEffectView.alpha = 0.3
        
      searchtable2.isHidden = true
       
      
        closeqtyviubtno.layer.cornerRadius = 12.5
      qtylistviu.layer.cornerRadius = 4.0
      
        
      blueBtn(object: confmedbtn)
      blueBtn(object: Addmorebtno)
       
//      confmedbtn.layer.borderWidth = 0.8
//      confmedbtn.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
//      confmedbtn.layer.cornerRadius = 4.0
//
//      Addmorebtno.layer.borderWidth = 0.8
//      Addmorebtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
//      Addmorebtno.layer.cornerRadius = 4.0
      
        ifaddView.isHidden = true

        
      
      Addmorebtno.isHidden = false
        
     searchForMedicineBar.placeholder = searplace
        
        if UserDefaults.standard.value(forKey:"OrderId") != nil
        {
         orderID = UserDefaults.standard.value(forKey:"OrderId") as! CLong
        }
        print(orderID!)
        
      self.animateView.transform = self.animateView.transform.translatedBy(x: 0, y: self.view.bounds.height )
        
    NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
       }
       
       @objc func NotificationReceivedforPayment(notification: Notification) {
           paysuccesspop()

           let when = DispatchTime.now() + 3
           DispatchQueue.main.asyncAfter(deadline: when){
               self.paysviu.removeFromSuperview()
               self.visualEffectView.removeFromSuperview()
           }
       
       }
    
    
 
    @IBAction func cartTryAgainbtna(_ sender: Any) {
        self.cartempty.isHidden = true
        self.tableviewContainer.isHidden = false
        searchForMedicineBar.text = ""
        searchForMedicineBar.becomeFirstResponder()
    }
    
    
    @IBAction func closeqtylistviu(_ sender: UIButton) {
        qtylistviu.isHidden = true
        qtyviublur.isHidden = true
        self.searchtable2.isUserInteractionEnabled = true
       // self.viewDele?.getscreenBlur(temp: "hide")
    }
    
    @IBAction func AddMore(_ sender: UIButton) {
//       totalviu.isHidden = true
        searchtable2.isHidden = true
        searchtable.isHidden = false
        searchActive = true
        searchForMedicineBar.text = ""
        searchForMedicineBar.becomeFirstResponder()
        medicnetextlbl.isHidden = true
        searchForMedicineBar.isHidden = false
        searchForMedicineBar.isUserInteractionEnabled = true
        searchtable.reloadData()
    }
    
    @IBAction func confirmMeds(_ sender: Any) {
        
        UserDefaults.standard.set(self.savingPrice, forKey: "savingPrice")
         UserDefaults.standard.set(self.savingPrct, forKey: "savingPrct")
        
      Analytics.logEvent("button_click", parameters: [
        "ans":confmedbtn.titleLabel?.text,
        "full": "testing2",
        "question": "Cart"
         ])
        
        let logParams = [
        "ans":confmedbtn.titleLabel?.text,
        "full": "testing2",
        "question": "Cart"
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
     print(ArrayB)
        
        self.cartempty.isHidden = true
        self.tableviewContainer.isHidden = false
        
     if searchtable2.isHidden == false
     {
       searchtable2.isHidden = true
       searchForMedicineBar.isHidden = true
       searchForMedicineBar.isUserInteractionEnabled = false
       medicnetextlbl.isHidden = false
       variantarr3 = ArrayB as NSArray
       searchtable.isHidden = false
       searchtable.reloadData()
     }
     
        
//     if ifaddView.isHidden == true
//     {
//     if itemcount != 0
//     {
//       ifaddView.isHidden = false
//
//       searchtable.reloadData()
//       searchtable2.isHidden = true
//
//     }
//     }
     else
     {
     self.searchForMedicineBar.endEditing(true)
     
    //if UserDefaults.standard.value(forKey:"custname") == nil || UserDefaults.standard.value(forKey:"custname") as! String == ""
   // {
   //  let vc = storyboard?.instantiateViewController(withIdentifier: "PeronalinfoViewController") as! PeronalinfoViewController
 //    vc.ArrayB2 = ArrayB
   //  navigationController?.pushViewController(vc, animated: false)
    //}
        saveMeds()
        
        
       
        
        
        
        
    }
        
    }
    
    func setSearchBar(){
        bottomLine.frame = CGRect(x: 10, y: searchForMedicineBar.frame.height - 10, width: searchForMedicineBar.frame.width - 20, height: 1)
               bottomLine.backgroundColor = UIColor.black.cgColor
              searchForMedicineBar.barTintColor = UIColor.clear
              searchForMedicineBar.backgroundColor = UIColor.clear
              searchForMedicineBar.isTranslucent = true
              searchForMedicineBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
               searchForMedicineBar.layer.addSublayer(bottomLine)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count >= 2
        {
            self.mainarr = []
            self.searchtable2.isHidden = true
            self.searchtable.isHidden = false
            
            self.searchActive = true
            self.searchstrs = searchText
            
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(GetSearchData), object: nil)
            self.perform(#selector(GetSearchData), with: nil, afterDelay: 1.0)
    
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchForMedicineBar.endEditing(true)
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
       self.cartempty.isHidden = true
       self.tableviewContainer.isHidden = false
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var counts = 0
        
        if tableView == searchtable
        {
         if itemcount == 0 || searchActive == true
         {
         counts = mainarr.count
         }else
         {
         counts =  variantarr3.count
         }
        }
        if tableView == searchtable2
        {
        counts = variantarr.count
        }
        
        if tableView == qtylistable
        {
            counts = qtyarr.count
        }
        
        return counts
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == searchtable
        {
            if itemcount == 0 || searchActive == true
            {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchMedTableViewCell") as! SearchMedTableViewCell
        
            medict = mainarr[indexPath.row] as! NSDictionary
            cell.mednamelbl.text = medict.value(forKey: "motherBrand") as? String ?? ""
            cell.varlbl.text = String(describing:medict.value(forKey: "variantsCount")!) + " variants"
            
            return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SeAddMedTableViewCell") as! SearchMedTableViewCell
                
                variantdict = variantarr3[indexPath.row] as! NSDictionary
                
                print(variantdict)
                cell.noslbl.text = variantdict.value(forKey: "pack") as? String ?? ""
                cell.medmainlbl.text = variantdict.value(forKey: "medicineName") as? String ?? ""
                cell.qtylbl.text = variantdict.value(forKey: "quantity") as? String ?? ""
                let discPs = variantdict.value(forKey: "discountPercentage") as? String ?? "0"//siddh
                let discP = Double(discPs)!
                if discP == 0{
                    cell.discountPectLb.isHidden = true
                    cell.mrpPrice.isHidden = true
                    cell.cancelView.isHidden = true
                }else{
                    cell.discountPectLb.isHidden = false
                    cell.mrpPrice.isHidden = false
                    cell.cancelView.isHidden = false
                    cell.discountPectLb.text = String(format: "%.0f", discP) + NSLocalizedString("% OFF ", comment: "")
                }
                print(discP)
                cell.quanstack.isHidden = false
                cell.rembtno.isHidden = false
               
               

                
                if let mainprice = variantdict.value(forKey: "price") as? String
                {
                    
                    cell.mrpPrice.text =  NSLocalizedString("MRP ₹ ", comment: "") + "\(mainprice)"
                                               let IntPrice:Double = Double(mainprice)!
                    let discount:Double = discP / 100
                                               let dicPrice =  IntPrice * discount
                                               let afterDicPrice = IntPrice - dicPrice
                    cell.mediprice.text = String(format: "%.2f", afterDicPrice)
                }
                
                if let mainprice2 = variantdict.value(forKey: "price") as? Double
                {
                   
                    cell.mrpPrice.text = NSLocalizedString("MRP ₹ ", comment: "") + "\(mainprice2)"
                    let discount:Double = discP / 100
                                               let dicPrice =  mainprice2 * discount
                                               let afterDicPrice = mainprice2 - dicPrice
                                               cell.mediprice.text = String(format: "%.2f", afterDicPrice)
                }
                
                cell.rembtno.addTarget(self, action: #selector(remmedi), for: .touchUpInside)
                cell.rembtno.tag = indexPath.row
                
                cell.min.addTarget(self, action: #selector(minqty), for: .touchUpInside)
                cell.min.tag = indexPath.row
                
                cell.plus.tag = indexPath.row
                cell.plus.addTarget(self, action: #selector(addqty), for: .touchUpInside)
                
                return cell
            }
                
        }
        else if tableView == searchtable2
        {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "SeAddMedTableViewCell") as! SearchMedTableViewCell
            
                        variantdict = variantarr[indexPath.row] as! NSDictionary
                          print(variantdict)
                        cell.noslbl.text = variantdict.value(forKey: "pack") as! String
                        cell.medmainlbl.text = variantdict.value(forKey: "medicineName") as! String
                        
                        let discP = variantdict.value(forKey: "discountPercentage") as? Double ?? 0
            print(discP)
                                       if discP == 0{
                                           cell.discountPectLb.isHidden = true
                                           cell.mrpPrice.isHidden = true
                                           cell.cancelView.isHidden = true
                                       }else{
                                          cell.discountPectLb.isHidden = false
                                          cell.mrpPrice.isHidden = false
                                          cell.cancelView.isHidden = false
                                           cell.discountPectLb.text = String(format: "%.0f", discP) + NSLocalizedString("% OFF ", comment: "")
                                       }
                                       print(discP)
                        let nischeck:Bool = variantdict.value(forKey: "inStock") as! Bool
                        if nischeck == true
                        {
                         cell.addmedbtn.isHidden = false
                         cell.nislbl.isHidden = true
                         cell.mediprice.isHidden = false
                         cell.rupeesymbol.isHidden = false
                        }
                        else
                        {
                          cell.addmedbtn.isHidden = true
                          cell.nislbl.isHidden = false
                          cell.mediprice.isHidden = true
                          cell.rupeesymbol.isHidden = true
                          cell.mrpPrice.isHidden = true
                          cell.discountPectLb.isHidden = true
                          cell.cancelView.isHidden = true
                        }
            
                        cell.quanstack.isHidden = true
                        cell.rembtno.isHidden = true
                       
                        if ArrayB.count != 0
                        {
                            for i in 0..<ArrayB.count
                            {
                                if ArrayB[i].values.contains(cell.medmainlbl.text!){
                                    let mydicts:NSDictionary = ArrayB[i] as! NSDictionary
                                    let newqty:String = mydicts.value(forKey: "quantity") as! String
                                    cell.qtylbl.text = newqty
                                    cell.quanstack.isHidden = false
                                    cell.rembtno.isHidden = false
                                    cell.addmedbtn.isHidden = true
                                
                                    break;
                                    
                            }
                            
                        }
                    }
            
            
                        if let mainprice = variantdict.value(forKey: "price") as? String
                        {
                            cell.mrpPrice.text = NSLocalizedString("MRP ₹ ", comment: "") + "\(mainprice)"
                            let IntPrice:Double = Double(mainprice)!
                            let discount:Double = discP / 100
                            let dicPrice =  IntPrice * discount
                            let afterDicPrice = IntPrice - dicPrice
                            cell.mediprice.text = String(format: "%.2f", afterDicPrice)
                        }
            
                        if let mainprice2 = variantdict.value(forKey: "price") as? Double
                        {
                            cell.mrpPrice.text = NSLocalizedString("MRP ₹ ", comment: "") + "\(mainprice2)"
                            let discount:Double = discP / 100
                            let dicPrice =  mainprice2 * discount
                            let afterDicPrice = mainprice2 - dicPrice
                            cell.mediprice.text = String(format: "%.2f", afterDicPrice)
                        }
            
                        if cell.mediprice.text == "0.0"
                        {
                        cell.addmedbtn.isHidden = true
                        cell.nislbl.isHidden = false
                        cell.mediprice.isHidden = true
                        cell.rupeesymbol.isHidden = true
                        cell.mrpPrice.isHidden = true
                        cell.discountPectLb.isHidden = true
                          cell.cancelView.isHidden = true
                        }
            
            
                        cell.addmedbtn.addTarget(self, action: #selector(addmedi), for: .touchUpInside)
                        cell.addmedbtn.tag = indexPath.row
                        cell.addmedbtn.sizeToFit()
            
                        cell.rembtno.addTarget(self, action: #selector(remmedi), for: .touchUpInside)
                        cell.rembtno.tag = indexPath.row
            
                        cell.min.addTarget(self, action: #selector(minqty), for: .touchUpInside)
                        cell.min.tag = indexPath.row
            
                        cell.plus.tag = indexPath.row
                        cell.plus.addTarget(self, action: #selector(addqty), for: .touchUpInside)
            
                        return cell
        }
        else if tableView == qtylistable
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "quantitycell") as! TnCCell
        cell.selectqtylbl.text = qtyarr[indexPath.row]
          
        return cell
        }
      

        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
     if tableView == searchtable && itemcount == 0 || searchActive == true
     {
     //provide optional dict or delay operation
     searchActive = false
     selmedict = mainarr[indexPath.row] as! NSDictionary
     variantarr = selmedict.value(forKey: "variants") as! NSArray
     searchtable2.reloadData()
     searchtable2.isHidden = false
     searchtable.isHidden = true
     searchForMedicineBar.endEditing(true)

     }
        
    if tableView == qtylistable
    {
        let cell = self.searchtable2.cellForRow(at:selectTag) as! SearchMedTableViewCell
        cell.quanstack.isHidden = false
        cell.rembtno.isHidden = false
        cell.addmedbtn.isHidden = true
        let cell2 = self.qtylistable.cellForRow(at:indexPath) as! TnCCell
        
        cell.qtylbl.text = cell2.selectqtylbl.text!
        
        let variantdicts:NSDictionary = variantarr[selectTag.row] as! NSDictionary
        let vmediname = variantdicts.value(forKey: "medicineName") as! String
        
        if ArrayB.count != 0
        {
            for i in 0..<ArrayB.count
            {
                if ArrayB[i].values.contains(vmediname){
                    let mydicts:NSDictionary = ArrayB[i] as! NSDictionary
                    let newqty:String = mydicts.value(forKey: "quantity") as! String
                    let oldqty:String = cell.qtylbl.text!
                    let finqty:Int = Int(newqty)! + Int(oldqty)!

                    ArrayB[i].updateValue(String(describing:finqty), forKey: "quantity")
                    
                    break;

                } else {
        
                    itemcount = itemcount + 1
                    
                    if dictionaryA.count == 0
                    {
                        dictionaryA = ["productCode":String(),"quantity":String(),"pack":String(),"medicineName":String()]
                    }
                    else{
                        dictionaryA.updateValue(variantdicts.value(forKey: "medicineId") as! String, forKey: "productCode")
                        dictionaryA.updateValue(cell2.selectqtylbl.text!, forKey: "quantity")
                        dictionaryA.updateValue(variantdicts.value(forKey: "medicineName") as! String, forKey: "medicineName")
                        dictionaryA.updateValue(variantdicts.value(forKey: "pack") as! String, forKey: "pack")
                        dictionaryA.updateValue(variantdicts.value(forKey: "price") as! String, forKey: "price")
                        dictionaryA.updateValue("\(variantdicts.value(forKey: "discountPercentage") as? Double ?? 0)" , forKey: "discountPercentage")
                        
                    }
                    
                    ArrayB.append(dictionaryA as! [String : String])
        
                    print(ArrayB)
        
                    break;
        
                }
            }
        }
        else {
        itemcount = itemcount + 1

        if dictionaryA.count == 0
        {
            dictionaryA = ["productCode":String(),"quantity":String(),"pack":String(),"medicineName":String()]
        }
        else{
            dictionaryA.updateValue(variantdicts.value(forKey: "medicineId") as! String, forKey: "productCode")
            dictionaryA.updateValue(cell2.selectqtylbl.text!, forKey: "quantity")
            dictionaryA.updateValue(variantdicts.value(forKey: "medicineName") as! String, forKey: "medicineName")
            dictionaryA.updateValue(variantdicts.value(forKey: "pack") as! String, forKey: "pack")
            dictionaryA.updateValue(variantdicts.value(forKey: "price") as! String, forKey: "price")
            dictionaryA.updateValue("\(variantdicts.value(forKey: "discountPercentage") as? Double ?? 0)" , forKey: "discountPercentage")
        }

        ArrayB.append(dictionaryA as! [String : String])

        }
        print(variantdict)
        
        var discP = 0.0
        if let DString = variantdict.value(forKey: "discountPercentage") as? String{
           
            discP = Double(DString)!
           
        }
        if let DString = variantdict.value(forKey: "discountPercentage") as? Double{
                  
            discP = DString
                  
        }
        print(discP)
       
        if let mainprice = variantdicts.value(forKey: "price") as? String
        {
            let IntPrice:Double = Double(mainprice)!
            let discount:Double = discP / 100
            let dicPrice =  IntPrice * discount
            let afterDicPrice = IntPrice - dicPrice
            price = Double(afterDicPrice)
            price2 = IntPrice
        }
        
        if let mainprice2 = variantdicts.value(forKey: "price") as? Double
        {    let discount:Double = discP / 100
            let dicPrice =  mainprice2 * discount
            let mainprice21 = mainprice2 - dicPrice
            price = mainprice21
            price2 = mainprice2
        }
        
        let qtystrr:String = cell2.selectqtylbl.text!
        let myint:Int = Int(qtystrr)!
        
        tprice = tprice + Double(myint) * price
        tprice2 = tprice2 + Double(myint) * price2
        
        let diff = tprice2 - tprice
        let overallDis = diff/tprice2
        let overallPDis = overallDis * 100
        self.discountPectLbl.text = String(format: "%.0f", overallPDis) + NSLocalizedString("% OFF ", comment: "")
        
        savingPrice = diff
        savingPrct = overallPDis
        
       // let tfprice:Float = Float(tprice)
       // let ftfprice:Float = Float(tprice)
        
        totalprice.text = "₹ " + String(format: "%.2f", tprice)
        cancelMRP.text = NSLocalizedString("MRP ₹ ", comment: "") + String(format: "%.2f", tprice2)
      
        
        itemcountlbl.text = String(describing: itemcount) + NSLocalizedString(" items added ", comment: "")
        
       
        itemcountlbl.isHidden = false
       
        
         ifaddView.isHidden = false
        

        
        Addmorebtno.isHidden = false
        searchtable.isHidden = true
        variantarr3 = ArrayB as NSArray
        
        qtylistviu.isHidden = true
        qtyviublur.isHidden = true
        self.searchtable2.isUserInteractionEnabled = true
        
        //self.viewDele?.getscreenBlur(temp: "hide")
        
        searchtable.reloadData()
        
    }

    }
    
    @objc func addmedi(sender:UIButton)
    {
        if searchtable2.isHidden == false
        {
            qtylistviu.isHidden = false
            qtyviublur.isHidden = false
            self.searchtable2.isUserInteractionEnabled = false
            
            //self.viewDele?.getscreenBlur(temp: "unhide")
            
            let buttonPosition = sender.convert(CGPoint.zero, to: self.searchtable2)
            selectTag = self.searchtable2.indexPathForRow(at:buttonPosition)
            searchForMedicineBar.endEditing(true)

        }
        
        finalcalculationsidd()
        
    }
    
    @objc func remmedi(sender:UIButton)
    {
       
        if searchtable2.isHidden == false
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.searchtable2)
            let indexPath = self.searchtable2.indexPathForRow(at:buttonPosition)
            let cell = self.searchtable2.cellForRow(at: indexPath!) as! SearchMedTableViewCell
            
            cell.quanstack.isHidden = true
            cell.rembtno.isHidden = true
            cell.addmedbtn.isHidden = false
            
            let variantdicts:NSDictionary = variantarr[sender.tag] as! NSDictionary  /// 16 july changed here
            let vmediname:String = variantdicts.value(forKey: "medicineName") as! String
            
            print(ArrayB.count)
            
            for i in 0..<ArrayB.count
            {
                if ArrayB[i].values.contains(vmediname){
                    
                    ArrayB.remove(at: i)
                    
                    break;
                    
                } else {
                    // does not contain key
                }
            }
             var discP = 0.0
                    if let DString = variantdict.value(forKey: "discountPercentage") as? String{
                       
                        discP = Double(DString)!
                       
                    }
                    if let DString = variantdict.value(forKey: "discountPercentage") as? Double{
                              
                        discP = DString
                              
                    }
                    print(discP)
            if let mainprice = variantdicts.value(forKey: "price") as? String
            {
                 let IntPrice:Double = Double(mainprice)!
                 let discount:Double = discP / 100
                           let dicPrice =  IntPrice * discount
                           let afterDicPrice = IntPrice - dicPrice
                           price = Double(afterDicPrice)
                price2 = IntPrice
            }
            
            if let mainprice2 = variantdicts.value(forKey: "price") as? Double
            {     let discount:Double = discP / 100
                 let dicPrice =  mainprice2 * discount
                          let mainprice21 = mainprice2 - dicPrice
                          price = mainprice21
                
                price2 = mainprice2
            }
            
            
            if tprice == 0
            {
                
            }
            else
            {
                let qtystrr:String = cell.qtylbl.text!
                let myint:Int = Int(qtystrr)!
                
                tprice = tprice - Double(myint) * price
                tprice2 = tprice2 - Double(myint) * price2
                
               // tprice = tprice - price
            }
            let diff = tprice2 - tprice
                let overallDis = diff/tprice2
                let overallPDis = overallDis * 100
                self.discountPectLbl.text = String(format: "%.0f", overallPDis) + NSLocalizedString("% OFF ", comment: "")
            savingPrice = diff
                   savingPrct = overallPDis
            
            totalprice.text = "₹ " + String(format: "%.2f", tprice)
            cancelMRP.text = NSLocalizedString("MRP ₹ ", comment: "") + String(format: "%.2f", tprice2)
            
          
            
            itemcount = itemcount - 1
            
            itemcountlbl.text = String(describing: itemcount) + NSLocalizedString(" items added ", comment: "")
        
            if itemcount == 0
            {
            tprice = 0
        
            itemcountlbl.text = ""
            ifaddView.isHidden = true
            medicnetextlbl.isHidden = true
            searchForMedicineBar.isHidden = false
            searchForMedicineBar.isUserInteractionEnabled = true
          
           
            
            }
            
             variantarr3 = ArrayB as NSArray
           
            
        }
        
        if searchtable.isHidden == false && itemcount != 0
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.searchtable)
            let indexPath = self.searchtable.indexPathForRow(at:buttonPosition)
            let cell = self.searchtable.cellForRow(at: indexPath!) as! SearchMedTableViewCell
            
            let variantdicts:NSDictionary = variantarr3[sender.tag] as! NSDictionary
            let vmediname:String = variantdicts.value(forKey: "medicineName") as! String
            

            for i in 0..<ArrayB.count
            {
                if ArrayB[i].values.contains(vmediname){
                    
                    ArrayB.remove(at: i)
                    print(ArrayB)
                    
                    break;
                    
                } else {
                    // does not contain key
                }
            }
            
           var discP = 0.0
                   if let DString = variantdict.value(forKey: "discountPercentage") as? String{
                      
                       discP = Double(DString)!
                      
                   }
                   if let DString = variantdict.value(forKey: "discountPercentage") as? Double{
                             
                       discP = DString
                             
                   }
                   print(discP)
            if let mainprice = variantdicts.value(forKey: "price") as? String
            {
                let IntPrice:Double = Double(mainprice)!
                  let discount:Double = discP / 100
                           let dicPrice =  IntPrice * discount
                           let afterDicPrice = IntPrice - dicPrice
                           price = Double(afterDicPrice)
                price2 = IntPrice
            }
            
            if let mainprice2 = variantdicts.value(forKey: "price") as? Double
            {      let discount:Double = discP / 100
                  let dicPrice =  mainprice2 * discount
                          let mainprice21 = mainprice2 - dicPrice
                          price = mainprice21
                price2 = mainprice2
            }
            
            
            if tprice == 0
            {
                
            }else
            {
                let qtystrr:String = cell.qtylbl.text!
                let myint:Int = Int(qtystrr)!
                
                tprice = tprice - Double(myint) * price
                  tprice2 = tprice2 - Double(myint) * price2
                
            }
            
            let diff = tprice2 - tprice
                let overallDis = diff/tprice2
                let overallPDis = overallDis * 100
                self.discountPectLbl.text = String(format: "%.0f", overallPDis) + NSLocalizedString("% OFF ", comment: "")
            savingPrice = diff
                   savingPrct = overallPDis
            
            totalprice.text = "₹ " + String(format: "%.2f", tprice)
            cancelMRP.text = NSLocalizedString("MRP ₹ ", comment: "") + String(format: "%.2f", tprice2)
          
            
            itemcount = itemcount - 1
            
            itemcountlbl.text = String(describing: itemcount) + NSLocalizedString(" items added ", comment: "")
            
            if itemcount == 0
            {
                tprice = 0
                 tprice2 = 0
                let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "cartemp") as! ECartPOP

                self.addChild(popupVC)
                popupVC.view.frame = self.view.frame
                self.view.addSubview(popupVC.view)
                popupVC.didMove(toParent: self)
                itemcountlbl.text = ""
                Addmorebtno.isHidden = false
                medicnetextlbl.isHidden = true
                searchForMedicineBar.isHidden = false
                searchForMedicineBar.isUserInteractionEnabled = true
                ifaddView.isHidden = true
                searchtable.isHidden = true
                searchtable2.reloadData()
                searchtable2.isHidden = false
                variantarr3 = ArrayB as NSArray
                searchtable.reloadData()
            }
            else
            {
            
            variantarr3 = ArrayB as NSArray
            searchtable.reloadData()
            }
        }
        
        finalcalculationsidd()
        
    }
    
    
    @objc func addqty(sender:UIButton)
    {
        if searchtable2.isHidden == false
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.searchtable2)
            let indexPath = self.searchtable2.indexPathForRow(at:buttonPosition)
            let cell = self.searchtable2.cellForRow(at: indexPath!) as! SearchMedTableViewCell
            
            let variantdicts:NSDictionary = variantarr[sender.tag] as! NSDictionary
            let vmediname:String = variantdicts.value(forKey: "medicineName") as! String
            
            var quantity = Int(cell.qtylbl.text!)!
            quantity = quantity + 1
            
            cell.qtylbl.text = String(describing: quantity)
            
            for i in 0..<ArrayB.count
            {
                if ArrayB[i].values.contains(vmediname){
                    
                    ArrayB[i].updateValue(cell.qtylbl.text!, forKey: "quantity")
                    
                } else {
                    // does not contain key
                }
            }
            print(variantdict)
            var discP = 0.0
                   if let DString = variantdict.value(forKey: "discountPercentage") as? String{
                      
                       discP = Double(DString)!
                      
                   }
                   if let DString = variantdict.value(forKey: "discountPercentage") as? Double{
                             
                       discP = DString
                             
                   }
                   print(discP)
            if let mainprice = variantdicts.value(forKey: "price") as? String
            {
                let IntPrice:Double = Double(mainprice)!
                let discount:Double = discP / 100
                           let dicPrice =  IntPrice * discount
                           let afterDicPrice = IntPrice - dicPrice
                           price = Double(afterDicPrice)
                price2 = IntPrice
            }
            
            if let mainprice2 = variantdicts.value(forKey: "price") as? Double
            {   let discount:Double = discP / 100
                 let dicPrice =  mainprice2 * discount
                          let mainprice21 = mainprice2 - dicPrice
                          price = mainprice21
                price2 = mainprice2
            }
            
            
            if tprice == 0
            {
            }else
            {
                tprice = tprice + price
                tprice2 = tprice2 + price2
            }
            
            let diff = tprice2 - tprice
                let overallDis = diff/tprice2
                let overallPDis = overallDis * 100
                self.discountPectLbl.text = String(format: "%.0f", overallPDis) + NSLocalizedString("% OFF ", comment: "")
            savingPrice = diff
                   savingPrct = overallPDis
            //let tfprice:Float = Float(tprice)
            //let ftfprice:Float = Float(tprice)
            totalprice.text = "₹ " + String(format: "%.2f", tprice)
            cancelMRP.text = NSLocalizedString("MRP ₹ ", comment: "") + String(format: "%.2f", tprice2)
        
        }
        
        if searchtable.isHidden == false
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.searchtable)
            let indexPath = self.searchtable.indexPathForRow(at:buttonPosition)
            let cell = self.searchtable.cellForRow(at: indexPath!) as! SearchMedTableViewCell
            
            let variantdicts:NSDictionary = variantarr3[sender.tag] as! NSDictionary
            let vmediname:String = variantdicts.value(forKey: "medicineName") as! String
            
            var quantity = Int(cell.qtylbl.text!)!
            quantity = quantity + 1
            
            cell.qtylbl.text = String(describing: quantity)
            
            for i in 0..<ArrayB.count
            {
                if ArrayB[i].values.contains(vmediname){
                    
                    ArrayB[i].updateValue(cell.qtylbl.text!, forKey: "quantity")
                    
                } else {
                    // does not contain key
                }
            }
            
            var discP = 0.0
                  if let DString = variantdict.value(forKey: "discountPercentage") as? String{
                     
                      discP = Double(DString)!
                     
                  }
                  if let DString = variantdict.value(forKey: "discountPercentage") as? Double{
                            
                      discP = DString
                            
                  }
                  print(discP)
            
            if let mainprice = variantdicts.value(forKey: "price") as? String
            {
                let IntPrice:Double = Double(mainprice)!
                let discount:Double = discP / 100
                           let dicPrice =  IntPrice * discount
                           let afterDicPrice = IntPrice - dicPrice
                           price = Double(afterDicPrice)
                price2 = IntPrice
            }
            
            if let mainprice2 = variantdicts.value(forKey: "price") as? Double
            {     let discount:Double = discP / 100
                  let dicPrice =  mainprice2 * discount
                          let mainprice21 = mainprice2 - dicPrice
                          price = mainprice21
                price2 = mainprice2
            }
            
            
            if tprice == 0
            {
            }else
            {
                tprice = tprice + price
                tprice2 = tprice2 + price2

            }
            
            //let tfprice:Float = Float(tprice)
            //let ftfprice:Float = Float(tprice)
            let diff = tprice2 - tprice
                let overallDis = diff/tprice2
                let overallPDis = overallDis * 100
                self.discountPectLbl.text = String(format: "%.0f", overallPDis) + NSLocalizedString("% OFF ", comment: "")
            savingPrice = diff
                   savingPrct = overallPDis
            
            totalprice.text = "₹ " + String(format: "%.2f", tprice)
            cancelMRP.text = NSLocalizedString("MRP ₹ ", comment: "") + String(format: "%.2f", tprice2)
          
        }
        
        finalcalculationsidd()
        
    
    }
    
    
    @objc func minqty(sender:UIButton)
    {
        if searchtable2.isHidden == false
        {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.searchtable2)
        let indexPath = self.searchtable2.indexPathForRow(at:buttonPosition)
        let cell = self.searchtable2.cellForRow(at: indexPath!) as! SearchMedTableViewCell
       
        let variantdicts:NSDictionary = variantarr[sender.tag] as! NSDictionary
        let vmediname:String = variantdicts.value(forKey: "medicineName") as! String
        
        var quantity = Int(cell.qtylbl.text!)!
        if quantity == 1
        {
            

        }
        else
        {
        quantity = quantity - 1
        
        cell.qtylbl.text = String(describing: quantity)
        
        for i in 0..<ArrayB.count
        {
            if ArrayB[i].values.contains(vmediname){
                
                ArrayB[i].updateValue(cell.qtylbl.text!, forKey: "quantity")
                
            } else {
                // does not contain key
            }
        }
            var discP = 0.0
                   if let DString = variantdict.value(forKey: "discountPercentage") as? String{
                      
                       discP = Double(DString)!
                      
                   }
                   if let DString = variantdict.value(forKey: "discountPercentage") as? Double{
                             
                       discP = DString
                             
                   }
                   print(discP)
            if let mainprice = variantdicts.value(forKey: "price") as? String
            {
                 let IntPrice:Double = Double(mainprice)!
                 let discount:Double = discP / 100
                           let dicPrice =  IntPrice * discount
                           let afterDicPrice = IntPrice - dicPrice
                           price = Double(afterDicPrice)
                price2 = IntPrice
            }
            
            if let mainprice2 = variantdicts.value(forKey: "price") as? Double
            {     let discount:Double = discP / 100
                 let dicPrice =  mainprice2 * discount
                          let mainprice21 = mainprice2 - dicPrice
                          price = mainprice21
                price2 = mainprice2
            }
            
            if tprice == 0
            {
                totalprice.text = ""
                cancelMRP.text = ""
               
            }else
            {
                tprice = tprice - price
                tprice2 = tprice2 - price2
            }
            
            let diff = tprice2 - tprice
                let overallDis = diff/tprice2
                let overallPDis = overallDis * 100
                self.discountPectLbl.text = String(format: "%.0f", overallPDis) + NSLocalizedString("% OFF ", comment: "")
            savingPrice = diff
                   savingPrct = overallPDis
            
            totalprice.text = "₹ " + String(format: "%.2f", tprice)
            cancelMRP.text = NSLocalizedString("MRP ₹ ", comment: "") + String(format: "%.2f", tprice2)
          
            
            if itemcount == 0
            {
                itemcountlbl.text = ""
            }
            
            
            
        }
        
        }
        
        if searchtable.isHidden == false
        {
        
            let buttonPosition = sender.convert(CGPoint.zero, to: self.searchtable)
            let indexPath = self.searchtable.indexPathForRow(at:buttonPosition)
            let cell = self.searchtable.cellForRow(at: indexPath!) as! SearchMedTableViewCell
            
            let variantdicts:NSDictionary = variantarr3[sender.tag] as! NSDictionary
            let vmediname:String = variantdicts.value(forKey: "medicineName") as! String
            
            var quantity = Int(cell.qtylbl.text!)!
            if quantity == 1
            {
                
                
            }else
            {
                quantity = quantity - 1
                
                cell.qtylbl.text = String(describing: quantity)
                
                for i in 0..<ArrayB.count
                {
                    if ArrayB[i].values.contains(vmediname){
                        
                        ArrayB[i].updateValue(cell.qtylbl.text!, forKey: "quantity")
                        
                    } else {
                        // does not contain key
                    }
                }
                 var discP = 0.0
                       if let DString = variantdict.value(forKey: "discountPercentage") as? String{
                          
                           discP = Double(DString)!
                          
                       }
                       if let DString = variantdict.value(forKey: "discountPercentage") as? Double{
                                 
                           discP = DString
                                 
                       }
                       print(discP)
                if let mainprice = variantdicts.value(forKey: "price") as? String
                {
                     let IntPrice:Double = Double(mainprice)!
                      let discount:Double = discP / 100
                               let dicPrice =  IntPrice * discount
                               let afterDicPrice = IntPrice - dicPrice
                               price = Double(afterDicPrice)
                    price2 = IntPrice
                }
                
                if let mainprice2 = variantdicts.value(forKey: "price") as? Double
                {      let discount:Double = discP / 100
                      let dicPrice =  mainprice2 * discount
                              let mainprice21 = mainprice2 - dicPrice
                              price = mainprice21
                    price2 = mainprice2
                }
                
                if tprice == 0
                {
                    totalprice.text = ""
                    cancelMRP.text = ""
                 
                }else
                {
                    tprice = tprice - price
                    tprice2 = tprice2 - price2
                }
                
                let diff = tprice2 - tprice
                    let overallDis = diff/tprice2
                    let overallPDis = overallDis * 100
                    self.discountPectLbl.text = String(format: "%.0f", overallPDis) + NSLocalizedString("% OFF ", comment: "")
                savingPrice = diff
                       savingPrct = overallPDis
                
                totalprice.text = "₹ " + String(format: "%.2f", tprice)
                cancelMRP.text = NSLocalizedString("MRP ₹ ", comment: "") + String(format: "%.2f", tprice2)
            
                
                if itemcount == 0
                {
                    itemcountlbl.text = ""
                }
                
                
            }
            
        }
        
        finalcalculationsidd()
       
    }
    
    
    
    func finalcalculationsidd(){
        var mrp = Double()
        var finaldiscount = Double()
        var discountPrice = Double()
        
        for i in 0..<ArrayB.count{
            let arr = ArrayB[i] as NSDictionary
            let price = arr.value(forKey: "price") as! String
            let quantity = arr.value(forKey: "quantity") as! String
            let discountPercentage = arr.value(forKey: "discountPercentage") as! String
            
            let pInt = Double(price)
            let qInt = Double(quantity)
            let dInt = Double(discountPercentage)
            
            let dismulti = (100 - dInt!)/100
            let dicPrice = pInt! * dismulti
            
            let dqp = dicPrice * qInt!
            let qp = pInt! * qInt!
            
            mrp = mrp + qp
            discountPrice = discountPrice + dqp
            finaldiscount = ((mrp - discountPrice)/mrp) * 100
            
        }
        
        print("siddh\(mrp)")
        print("siddh\(discountPrice)")
        print("siddh\(finaldiscount)")
        
        totalprice.text = "₹ " + String(format: "%.2f", discountPrice)
        cancelMRP.text = NSLocalizedString("MRP ₹ ", comment: "") + String(format: "%.2f", mrp)
        self.discountPectLbl.text = String(format: "%.0f", finaldiscount) + NSLocalizedString("% OFF ", comment: "")
      
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
       
        
//        self.animateView.transform = self.animateView.transform.translatedBy(x: 0, y: self.view.bounds.height )
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
//            self.animateView.transform = self.animateView.transform.translatedBy(x:0 , y: -self.view.bounds.height)
             self.animateView.transform = CGAffineTransform(translationX: 0, y: 0)
            
            self.searchForMedicineBar.becomeFirstResponder()
            
        })
        
    }
    

    @IBAction func backPress(_ sender: Any) {
        
        if ArrayB.count != 0
        {
        let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
        let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
        let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
        { (action:UIAlertAction!) in
    
            self.navigationController?.popViewController(animated: false)
        
        }
                alertVC.addAction(Action1);
                let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
                alertVC.addAction(Action2);
                
                self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
        }
        
    }
    

    @objc func GetSearchData()
    {
        searchForMedicineBar.isLoading = true
        self.view.isUserInteractionEnabled = false
        
        let parameters = ["search":self.searchstrs!] as [String : Any]
        
        ApiManager().requestApiWithDataType( methodType: HPOST, urlString:GetSearchMedData(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
            
            
            print(response)
            
            if cStatus == 200 || cStatus == 201
            {
               self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
                
                self.discountPercentage = self.resdict.value(forKey: "discountPercentage") as? Double ?? 100
               
                self.discountPectLbl.text = String(format: "%.0f", self.discountPercentage!) + NSLocalizedString("% OFF ", comment: "")
                self.mainarr = self.resdict.value(forKey: "medicine") as! [Any]
                
                if self.mainarr.count == 0
                {
                self.searchForMedicineBar.endEditing(true)
                self.cartempty.isHidden = false
                self.tableviewContainer.isHidden = true
                }
                else
                {
                self.cartempty.isHidden = true
                self.tableviewContainer.isHidden = false
                }
                
                self.searchtable.reloadData()
                self.searchtable2.reloadData()
                self.searchForMedicineBar.isLoading = false
                self.view.isUserInteractionEnabled = true
            }
            else if cStatus == 401
            {
                let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
            }
            else if cStatus == 500
            {
//                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                
                let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                self.addChild(popupVC)
                popupVC.view.frame = self.view.frame
                self.view.addSubview(popupVC.view)
                popupVC.didMove(toParent: self)
            }
            else
            {
                let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
            }
        }
    }
    
    
    
    
    
    
    
    func saveMeds(){
        activityInd.startAnimating()
        view.isUserInteractionEnabled = false
        
        let baseUrl = saveMedicine()
        print(baseUrl)
        let queryStringParam  =  ["orderId": String(describing:orderID!)
                               ] as [String : Any]
        print(queryStringParam)
               
                           var urlComponent = URLComponents(string: baseUrl)!
                           let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value as? String) }
                             urlComponent.queryItems = queryItems
                             
                           let bodyParameters = ArrayB
                           
                           let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
                          
                                   
                           let headers = ["Content-Type": "application/json",
                                                               "transactionId":"a",
                                                               "Authorization":authtok]
                           
                   
                             var request = URLRequest(url: urlComponent.url!)
                             request.httpMethod = "POST"
                             request.httpBody = try? JSONSerialization.data(withJSONObject: bodyParameters)
                             request.allHTTPHeaderFields = headers
                             
                             AF.request(request).responseJSON { response in
                               print(response)
                               switch response.result {
                                       case .success(let value):
                                        self.activityInd.stopAnimating()
                                            self.view.isUserInteractionEnabled = true
                                        switch response.response?.statusCode {
                                        case 200,201:
                                            print("")
                                        if let dict = value as? NSDictionary {
                                            print(dict)
                                                                                                            
                                    let saveMedCreateOrderId = dict.value(forKey: "orderId") as? CLong
//                                        print(saveMedCreateOrderId)
                                                                                                            
                                    UserDefaults.standard.set(saveMedCreateOrderId, forKey: "OrderId")
                                    UserDefaults.standard.set(self.ArrayB, forKey: "MedicineArr")
                                        UserDefaults.standard.synchronize()

                                if UserDefaults.standard.value(forKey:"custname") == nil || UserDefaults.standard.value(forKey:"custname") as? String == ""
                                        {
                                        self.personalInfo()
                                        }
                                        else if UserDefaults.standard.value(forKey:"isAddAvail") as? String == "No"
                                        {
                                        self.deliveryPin()
                                        }
                                        else
                                        {
                                        self.addList()
                                        }
                                                 }
                                        case 401:
                                            print("401")
                                            let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                            Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                                        case 400:
                                            print("400")
                                            let adddict = value as? NSDictionary
                                            if adddict?.allKeys.first as! String == "400"
                                            {
                                            Utility.showAlertWithTitle(title: "", andMessage: adddict?.value(forKey: "400") as? String, onVC: self)
                                            }
                                        case 500:
                                            print("500")
                                            
//                                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                            
                                            let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                                            self.addChild(popupVC)
                                            popupVC.view.frame = self.view.frame
                                            self.view.addSubview(popupVC.view)
                                            popupVC.didMove(toParent: self)
                                        default:
                                            print("may be 500")
                                            let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                     
                                        }
                                        
                                      case .failure(let error):
                                                    print(error)
                                                }
                                 }
    }
    
    
    
    func personalInfo(){
        let vc = storyboard?.instantiateViewController(withIdentifier: "PeronalinfoViewController") as! PeronalinfoViewController
                                                      navigationController?.pushViewController(vc, animated: false)
    }
    
    func deliveryPin(){
        let vc = storyboard?.instantiateViewController(withIdentifier: "deliveryPincodeViewController") as! deliveryPincodeViewController
                                                         navigationController?.pushViewController(vc, animated: false)
    }
    
    func addList(){
        //Address Listing
                                                          let vc = storyboard?.instantiateViewController(withIdentifier: "addressListViewController") as! addressListViewController
                                                          navigationController?.pushViewController(vc, animated: false)
    }
    
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    
    
    
}


extension UISearchBar {
    var textField: UITextField? {
        if #available(iOS 13.0, *) {
            return searchTextField
        } else {
           if let textField = value(forKey: "searchField") as? UITextField {
                return textField
            }
            return nil
        }
    }

    private var searchIcon: UIImage? {
        let subViews = subviews.flatMap { $0.subviews }
        return  ((subViews.filter { $0 is UIImageView }).first as? UIImageView)?.image
    }

    private var activityIndicator: UIActivityIndicatorView? {
        return textField?.leftView?.subviews.compactMap{ $0 as? UIActivityIndicatorView }.first
    }

    var isLoading: Bool {
        get {

            return activityIndicator != nil
        } set {
            let _searchIcon = searchIcon
            if newValue {
                if activityIndicator == nil {
                    let _activityIndicator = UIActivityIndicatorView(style: .gray)
                    _activityIndicator.color = UIColor(hexString: "#0071BC")
                    _activityIndicator.startAnimating()
                    _activityIndicator.backgroundColor = UIColor.clear
                    self.setImage(UIImage(), for: .search, state: .normal)
                    textField?.leftView?.addSubview(_activityIndicator)
                    let leftViewSize = textField?.leftView?.frame.size ?? CGSize.zero
                    _activityIndicator.center = CGPoint(x: leftViewSize.width/2, y: leftViewSize.height/2)
                }
            } else {
                activityIndicator?.removeFromSuperview()
                self.setImage(_searchIcon, for: .search, state: .normal)
            }
        }
    }
}







