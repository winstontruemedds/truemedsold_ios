//
//  deliveryPincodeViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/11/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import PinCodeTextField
import Alamofire
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK

class deliveryPincodeViewController: UIViewController {
    

    @IBOutlet weak var topConstains: NSLayoutConstraint!
    
    @IBOutlet weak var bottonConstrains: NSLayoutConstraint!
    
    @IBOutlet weak var questionView: UIView!
    
    @IBOutlet weak var someOneElseBtn: UIButton!
    
    @IBOutlet weak var pinCodeTf: PinCodeTextField!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var patientDetailsView: UIView!
    
    @IBOutlet weak var addBtn: UIButton!
    
    var resdict = NSDictionary()
    
    var finpindict = NSDictionary()
    
    var finpinstr:String!

    @IBOutlet weak var updatepincodelbl: UILabel!
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        visualEffectView.alpha = 0.3
        
        hideKeyboardTapAround()
        setDesign()
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "16") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        updatepincodelbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        //let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        addBtn.setTitle(btn1.value(forKey: "text") as? String ?? "", for: .normal)
        
        pinCodeTf.delegate = self
        pinCodeTf.keyboardType = .phonePad
        pinCodeTf.updatedUnderlineColor = UIColor.init(hex: 0x0071BC)
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)

    NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
       }
       
       @objc func NotificationReceivedforPayment(notification: Notification) {
           paysuccesspop()

           let when = DispatchTime.now() + 3
           DispatchQueue.main.asyncAfter(deadline: when){
               self.paysviu.removeFromSuperview()
               self.visualEffectView.removeFromSuperview()
           }
       
       }
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
        
    override func viewWillAppear(_ animated: Bool) {
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "16") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        updatepincodelbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        //let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        addBtn.setTitle(btn1.value(forKey: "text") as? String ?? "", for: .normal)
        
NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        self.scrollView.transform = self.scrollView.transform.translatedBy(x: 0, y: self.view.bounds.height )
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.scrollView.transform = self.scrollView.transform.translatedBy(x:0 , y: -self.view.bounds.height)
            
            
            
        }){ (_) in
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.pinCodeTf.becomeFirstResponder()
                
            })
        }
        
    }
    
   @objc func keyboardWillAppear() {
      
       topConstains.constant = 100
       //Do something here
   }
   
   @objc func keyboardWillDisappear() {
       topConstains.constant = 200
       //Do something here
   }
    
    
    func setDesign(){
    
        blueBtn(object: addBtn)
        backBtn(object: someOneElseBtn)
        labelFont(object: updatepincodelbl)
    }
    
    


    @IBAction func donePress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":addBtn.titleLabel?.text,
        "full": "testing2",
        "question": updatepincodelbl.text!
         ])
        
        let logParams = [
        "ans":addBtn.titleLabel?.text,
        "full": "testing2",
        "question": updatepincodelbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        if finpinstr == "" || finpinstr == nil
        {
        let localizedContent = NSLocalizedString("Please provide a valid pincode", comment: "")
        Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
        else
        {
        if Utility.isValidPincode(finpinstr) == true
        {
         checkPin()
        }
        else
        {
        let localizedContent = NSLocalizedString("Please provide a valid pincode", comment: "")
        Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
        }
        
    }
    
    
    //backBtn
    
    @IBAction func someOneElsePress(_ sender: Any) {
        
        if UserDefaults.standard.value(forKey:"custname") == nil || UserDefaults.standard.value(forKey:"custname") as? String == ""
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: cartViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        else
        {
        navigationController?.popViewController(animated: false)
        }
        
    }
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }

}

extension deliveryPincodeViewController: PinCodeTextFieldDelegate{
    
    
    func textFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: PinCodeTextField) -> Bool {
        print("textshould")
        
        return true
    }
    
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        let value = textField.text ?? ""
        print("value changed: \(value)")
        if textField == pinCodeTf
        {
        finpinstr = textField.text ?? ""
        }

    }
    
    func textFieldDidBeginEditing(_ textField: PinCodeTextField) {
        print("textDid")
        
        if textField == pinCodeTf{
         
        }
    
    }
    
    
    
    func checkPin()
    {
        let parameters = ["pincode":finpinstr!]
        
        ApiManager().requestApiWithDataType(methodType:HPOST, urlString: checkPincode(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
        
         print(cStatus)
            
        if cStatus == 200 || cStatus == 201
        {
            self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
            print(self.resdict)
            
           
                if self.resdict.allKeys.first as! String == "400"
                 {
                     
                    Utility.showAlertWithTitle(title: "", andMessage:self.resdict.value(forKey: "400") as! String, onVC: self)
                     
                 }
            else
            {
                self.finpindict = self.resdict.value(forKey: "payload") as! NSDictionary
                
            }
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "deliveryAddViewController") as? deliveryAddViewController
            vc?.finpindict2 = self.finpindict
            vc?.finpinstr2 = self.finpinstr
            self.navigationController?.pushViewController(vc!, animated: false)
          }
        else if cStatus == 400
            {
            self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
            print(self.resdict)
                                  
            
            if self.resdict.allKeys.first as! String == "400"
                {
                                           
                Utility.showAlertWithTitle(title: "", andMessage:self.resdict.value(forKey: "400") as! String, onVC: self)
                                           
                }
                
                          
            }
        else if cStatus == 401
            {
                let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
            }
            else if cStatus == 500
            {
//                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                
                let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
                
                self.addChild(popupVC)
                popupVC.view.frame = self.view.frame
                self.view.addSubview(popupVC.view)
                popupVC.didMove(toParent: self)
            }
            else
            {
                let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
            }
        }
    }
  
    
}

