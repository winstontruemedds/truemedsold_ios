//
//  AddPatientViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire
import iOSDropDown


protocol  getArrayDelegate  {
    func getArrayFromPatient(temp:String)
    
}

protocol  getReload {
    func reloadPatients(temp:String)
    
}


class AddPatientViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    let reachability = Reachability()!
    
    var isfromchat:String!
    
    
    @IBOutlet weak var animateView: UIView!
    
    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var patnametf: UITextField!
    
    @IBOutlet weak var patagetf: UITextField!
    
    //@IBOutlet weak var patgentf: UITextField!
    
    //@IBOutlet weak var patreltf: UITextField!
    
    @IBOutlet weak var GenPicker: UIPickerView!
    
    @IBOutlet weak var RelationPicker: UIPickerView!
    
    @IBOutlet weak var toolbarr: UIToolbar!
    
    @IBOutlet weak var toolbarr2: UIToolbar!
    
    @IBOutlet weak var toolbarr3: UIToolbar!
    
    let genderarr = ["Male","Female","Others"]

    let allrelationarr = ["Grand Parent","Parent","Sibling","Spouse","Child","Grand Child","Other"]
    
    @IBOutlet weak var addpatientbtno: UIButton!
    
    @IBOutlet weak var addpatpopview: UIView!
    
    var seconds = 3
    
    var timer = Timer()
    
    var genderId : Int!
    
    var relationId:Int = 8
    
    var delegateCustom2 : getArrayDelegate?
    
    var reloadelegate : getReload?
    
    var dictionaryA = ["name":String(),"relation":String()]
    
    var ArrayB = [[String:String]]()
    
    @IBOutlet weak var addedpatimage: UIImageView!
    
    @IBOutlet weak var genderDrop: DropDown!
    
    @IBOutlet weak var relationDrop: DropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDropDown()
        patnametf.text = ""
        patagetf.text = ""
        
        
        //hideKeyboardTapAround()

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
//        patgentf.inputView = GenPicker
//        patgentf.inputAccessoryView = toolbarr2
        
        //patagetf.inputAccessoryView = toolbarr
        
        //patreltf.inputView = RelationPicker
        //patreltf.inputAccessoryView = toolbarr3
        
        addpatientbtno.layer.borderWidth = 0.8
        addpatientbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        addpatientbtno.layer.cornerRadius = 4.0
        
        addpatpopview.layer.cornerRadius = 4.0
        
        patnametf.addTarget(self, action: #selector(textFieldDidEndEditing), for:  UIControl.Event.editingDidEnd)
        
        patagetf.addTarget(self, action: #selector(textFieldDidChange), for:  UIControl.Event.editingChanged)
        
       // patreltf.addTarget(self, action: #selector(textFieldDidChange), for: .allEvents)
        
       
    }
    
    func setDropDown(){
        genderDrop.optionArray = ["Male","Female","Others"]
        genderDrop.optionIds = [8,9,10]
         genderDrop.selectedRowColor = UIColor.init(hex: 0x22B573)
        genderDrop.didSelect{(selectedText , index ,id) in
            // self.valueLabel.text = "Selected String: \(selectedText) \n index: \(index)"
            self.genderId = id
            
           
        }
        relationDrop.optionArray = ["Grand Parent","Parent","Sibling","Spouse","Child","Grand Child","Other"]
        relationDrop.optionIds = [1,2,3,4,6,7,8]
         relationDrop.selectedRowColor = UIColor.init(hex: 0x22B573)
        relationDrop.didSelect{(selectedText , index ,id) in
                   // self.valueLabel.text = "Selected String: \(selectedText) \n index: \(index)"
            self.relationId = id
                  
               }
      
    }
    
    
    @IBAction func genderPress(_ sender: Any) {
        genderDrop.showList()
        view.endEditing(true)
        
    }
    
    
    @IBAction func relationPress(_ sender: Any) {
        relationDrop.showList()
        view.endEditing(true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {

          self.animateView.transform = self.animateView.transform.translatedBy(x: 0, y: self.view.bounds.height )
          
          }
    
    
    override func viewDidAppear(_ animated: Bool) {
          
          UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
              
              self.animateView.transform = self.animateView.transform.translatedBy(x:0 , y: -self.view.bounds.height)
              
            self.patnametf.becomeFirstResponder()
              
          })
          
      }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       
        var Count:Int!
        
        if pickerView == GenPicker
        {
            Count = genderarr.count
        }
        else
        {
            Count = allrelationarr.count
        }
        
        return Count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        var title:String!
        
        if pickerView == GenPicker
        {
            title = genderarr[row]
            
        }
        else
        {
            title = allrelationarr[row]
        }
        
        return title
    }
    
    
    
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
//    {
//
//        if pickerView == GenPicker
//        {
//            patgentf.text = genderarr[row]
//        }
//        else
//        {
//            patreltf.text = allrelationarr[row]
//            if allrelationarr[row] == "Grand Parent"
//            {
//              relationId = 1
//            }
//            else if allrelationarr[row] == "Parent"
//            {
//              relationId = 2
//            }
//            else if allrelationarr[row] == "Sibling"
//            {
//              relationId = 3
//            }
//            else if allrelationarr[row] == "Spouse"
//            {
//                relationId = 4
//            }
//
//            else if allrelationarr[row] == "Child"
//            {
//                relationId = 6
//            }
//            else if allrelationarr[row] == "Grand Child"
//            {
//                relationId = 7
//            }
//            else if allrelationarr[row] == "Other"
//            {
//                relationId = 8
//            }
//
//        }
//
//
//    }
    

    @IBAction func backto(_ sender: Any) {
       self.delegateCustom2?.getArrayFromPatient(temp: "notadded")
       self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func donepick(_ sender: Any) {
        
        if patagetf.text == ""
        {
         let localizedContent = NSLocalizedString("Please enter valid age", comment: "")
         Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
        }else{
           let checkage:Int = Int(patagetf.text!)!
        if checkage < 1
        {
         patagetf.text = ""
         let localizedContent = NSLocalizedString("Please enter valid age", comment: "")
         Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
        }
        else if checkage > 150
        {
        patagetf.text = ""
        let localizedContent = NSLocalizedString("Input is over the allowable age limit for the app", comment: "")
        Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
        }
        else
        {
         view.endEditing(true)
        }
        }
    }
    
    
    @IBAction func donepick2(_ sender: Any) {
       
//        if patgentf.text == "Male"
//        {
//            genderId = 8
//            view.endEditing(true)
//        }
//
//        if patgentf.text == "Female"
//        {
//            genderId = 9
//            view.endEditing(true)
//        }
//
//        if patgentf.text == "Others"
//        {
//            genderId = 10
//            view.endEditing(true)
//        }
//
//        if patgentf.text == ""
//        {
//            patgentf.text = "Male"
//            genderId = 8
//            view.endEditing(true)
//        }
        
        
    }
    
    
    @IBAction func donepick3(_ sender: Any) {
        
//        if patreltf.text == ""
//        {
//         patreltf.text = "Grand Parent"
//         relationId = 1
//         view.endEditing(true)
//        }
//        else
//        {
//         view.endEditing(true)
//        }
    }
    
    
    
    
    @IBAction func addpatientact(_ sender: Any) {
       
        print("addPatientPress")
       view.endEditing(true)
        
       if relationId == 1 && genderId == 8
       {
        addedpatimage.image = UIImage(named: "gfather")
       }else if relationId == 1 && genderId == 9
       {
        addedpatimage.image = UIImage(named: "gmother")
       }else if relationId == 2 && genderId == 8
       {
        addedpatimage.image = UIImage(named: "father")
       }else if relationId == 2 && genderId == 9
       {
        addedpatimage.image = UIImage(named: "mother")
       }else if relationId == 3 && genderId == 8
       {
        addedpatimage.image = UIImage(named: "brother")
       }else if relationId == 3 && genderId == 9
       {
        addedpatimage.image = UIImage(named: "sister")
       }else if relationId == 4 && genderId == 8
       {
        addedpatimage.image = UIImage(named: "father")
       }else if relationId == 4 && genderId == 9
       {
        addedpatimage.image = UIImage(named: "mother")
       }else if relationId == 6 && genderId == 8
       {
        addedpatimage.image = UIImage(named: "brother")
       }else if relationId == 6 && genderId == 9
       {
        addedpatimage.image = UIImage(named: "sister")
       }else if relationId == 7 && genderId == 8
       {
        addedpatimage.image = UIImage(named: "father")
       }else if relationId == 7 && genderId == 9
       {
        addedpatimage.image = UIImage(named: "mother")
       }
       else if relationId == 8 && genderId == 8
       {
        addedpatimage.image = UIImage(named: "father")
       }
       else if relationId == 8 && genderId == 9
       {
        addedpatimage.image = UIImage(named: "mother")
       }
        
       if patnametf.text == "" || patnametf.text == nil || patagetf.text == "" || patagetf.text == nil || genderId == nil || relationId == nil
       {
        let localizedContent = NSLocalizedString("Please enter all fields", comment: "")
        Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
       }
       else
       {
        UIApplication.shared.beginIgnoringInteractionEvents()
        activity.startAnimating()
        addpatients()
        
       }
    
    }
    
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    
    @objc func updateTimer() {
        seconds -= 1
        if seconds == 0
        {
            timer.invalidate()
            addpatpopview.isHidden = true
            seconds = 2
            if isfromchat == "Yes" 
            {
            if UserDefaults.standard.value(forKey:"cameFrompushpop") as? String == "No"
            {
            self.navigationController?.popViewController(animated: false)
            }
            else
            {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "secondTimeAddPatientViewController") as? secondTimeAddPatientViewController
           
            self.navigationController?.pushViewController(vc!, animated: false)
            }
            }
            else
            {
            self.reloadelegate?.reloadPatients(temp: "")
            self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    @objc func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == patnametf
        {
            if Utility.isValidName2(patnametf.text!) == true
            {
            
            }
            else
            {
                patnametf.text = ""
                let localizedContent = NSLocalizedString("Please enter a valid name", comment: "")
                Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
            }
        }
    }
    
    
    @objc func textFieldDidChange(textField: UITextField)
    {
       
        if textField == patagetf
        {
            if  (patagetf.text?.count)! == 0
            {
                let localizedContent = NSLocalizedString("Please enter valid age", comment: "")
                Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
            }
            
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == patagetf
        {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        if updatedText == ""
        {
            
        }else{
        let checkage:Int = Int(updatedText)!
        if checkage < 1
            {
                patagetf.text = ""
                let localizedContent = NSLocalizedString("Please enter valid age", comment: "")
                Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
            }
            else if checkage > 150
            {
                patagetf.text = ""
                let localizedContent = NSLocalizedString("Input is over the allowable age limit for the app", comment: "")
                Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
            }
            }
            
        return updatedText.count <= 3
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == patnametf
        {
          patagetf.becomeFirstResponder()
        }
        return true
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    
    
    
    
    func addpatients(){
        let parameters = ["patientName":patnametf.text!,
                      "age":patagetf.text!,
                      "gender":CLong(genderId),
                      "relationId":CLong(relationId)
        ] as [String : Any]
        
         ApiManager().requestApiWithDataType(methodType:BPOST, urlString: AddPatients(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
                    
                    print(response)
                    
                    if cStatus == 200 || cStatus == 201
                    {
                       UserDefaults.standard.set("Yes", forKey: "isPatAvail")
                       UserDefaults.standard.synchronize()
                       self.addpatpopview.isHidden = false
                       self.runTimer()
                    UIApplication.shared.endIgnoringInteractionEvents()
                        self.activity.stopAnimating()
                    }
            else if cStatus == 401
            {
                let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
              UIApplication.shared.endIgnoringInteractionEvents()
               self.activity.stopAnimating()
            }
            else if cStatus == 500
            {
//                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                
                let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                self.addChild(popupVC)
                popupVC.view.frame = self.view.frame
                self.view.addSubview(popupVC.view)
                popupVC.didMove(toParent: self)
            }
            else
            {
                let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                UIApplication.shared.endIgnoringInteractionEvents()
                self.activity.stopAnimating()
            }
                
            }
        
       
        
    }
    
    
    
    
    
    
//    func addpatients2()
//    {
//        if Connectivity.isConnectedToInternet {
//
//        let parameters = ["patientName":patnametf.text!,
//                          "age":patagetf.text!,
//                          "gender":CLong(genderId),
//                          "relationId":CLong(relationId)
//            ] as [String : Any]
//
//        //let custid:String = String(describing:UserDefaults.standard.value(forKey: "CustomerId")!)
//
//        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
//
//        let para2 = ["":""]
//
//        let url =  AddPatients()
//
//        var items = [URLQueryItem]()
//        var myURL = URLComponents(string: url)
//
//        let session = URLSession.shared
//
//        for (key,value) in para2 {
//            items.append(URLQueryItem(name: key, value: value))
//        }
//
//        myURL?.queryItems = items
//
//        var request = URLRequest(url: (myURL?.url)!)
//
//        request.httpMethod = "POST"
//
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("a", forHTTPHeaderField: "transactionId")
//        request.addValue(authtok, forHTTPHeaderField: "Authorization")
//
//        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
//
//        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
//
//            do {
//
//                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
//
//                    DispatchQueue.main.async {
//                        let httpResponse = response as? HTTPURLResponse
//
//                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
//                        {
//                            self.addpatpopview.isHidden = false
//                            self.runTimer()
//                        }
//                        else
//                        {
//                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
//                        }
//                    }
//
//                }
//            } catch let error {
//                print(error.localizedDescription)
//            }
//        })
//        task.resume()
//    }
//    else
//    {
//    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
//    }
//    }
//
    

}

extension UITextField {
    
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
