//
//  moreExploreViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/13/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import SVGKit
import FirebaseAnalytics
import Flurry_iOS_SDK

class moreExploreViewController: UIViewController {
     
    @IBOutlet weak var moreBtn: UIButton!  // backbtn
    
    @IBOutlet weak var healthArticalBtn: UIButton!
    @IBOutlet weak var trueMedsDoctorBtn: UIButton!
    @IBOutlet weak var infoMyMedicineBtn: UIButton!
    @IBOutlet weak var sabscribeAndSaveBtn: UIButton!


    @IBOutlet weak var questionView: UIView!

    var cameFrom = ""
    
    @IBOutlet weak var morexplorelbl: UILabel!
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    @IBOutlet weak var paysviu: UIView!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cameFrom = ""
        setDesign()
        
        visualEffectView.alpha = 0.3
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "22") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        morexplorelbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        let btn3:NSDictionary = btnarr[2] as! NSDictionary
        let btn4:NSDictionary = btnarr[3] as! NSDictionary
        
        healthArticalBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        trueMedsDoctorBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        infoMyMedicineBtn.setTitle(btn3.value(forKey: "text") as? String ?? "", for: .normal)
        sabscribeAndSaveBtn.setTitle(btn4.value(forKey: "text") as? String ?? "", for: .normal)
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
            
        }
        

        @objc func NotificationReceivedforPayment(notification: Notification) {
            paysuccesspop()

            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                self.paysviu.removeFromSuperview()
                self.visualEffectView.removeFromSuperview()
            }
        
        }
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "22") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        morexplorelbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        let btn3:NSDictionary = btnarr[2] as! NSDictionary
        let btn4:NSDictionary = btnarr[3] as! NSDictionary
        
        healthArticalBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        trueMedsDoctorBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        infoMyMedicineBtn.setTitle(btn3.value(forKey: "text") as? String ?? "", for: .normal)
        sabscribeAndSaveBtn.setTitle(btn4.value(forKey: "text") as? String ?? "", for: .normal)
        
        if cameFrom == ""{
            Out()
        }
        
    }
    
    
    func Out(){
        outRight(object: healthArticalBtn)
        outRight(object: trueMedsDoctorBtn)
        outRight(object: infoMyMedicineBtn)
        outRight(object: sabscribeAndSaveBtn)
        outLeft(object: questionView)
       
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print(cameFrom)
        
        
        if cameFrom == ""{
            In()
        }else{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.moreBtn.transform = CGAffineTransform(translationX: -10, y: 0)
                self.healthArticalBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.trueMedsDoctorBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.infoMyMedicineBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.sabscribeAndSaveBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
              
                
                
            })
        }
        
        
    }
    
    func In(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.healthArticalBtn.transform = self.healthArticalBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.trueMedsDoctorBtn.transform = self.trueMedsDoctorBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.infoMyMedicineBtn.transform = self.infoMyMedicineBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.sabscribeAndSaveBtn.transform = self.sabscribeAndSaveBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: self.view.bounds.width, y: 0)
          
            
            
        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.healthArticalBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.trueMedsDoctorBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.infoMyMedicineBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.sabscribeAndSaveBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
               
                
            })
        }
    }
    
    
    
    
    
    func setDesign(){
        
        //btns
        backBtn(object: moreBtn)
        blueBtn(object: healthArticalBtn)
        greenBtn(object: trueMedsDoctorBtn)
        blueBtn(object: infoMyMedicineBtn)
        greenBtn(object: sabscribeAndSaveBtn)
        labelFont(object: morexplorelbl)
    }
    
    
    
    
    
    @IBAction func morePress(_ sender: Any) {
        
        navigationController?.popViewController(animated: false)
    }
    
    
    
    
    
    @IBAction func healthArticalPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":healthArticalBtn.titleLabel?.text,
        "full": "testing2",
        "question": morexplorelbl.text!
         ])
        
        let logParams = [
        "ans":healthArticalBtn.titleLabel?.text,
        "full": "testing2",
        "question": morexplorelbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "help"
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.moreBtn.transform = self.moreBtn.transform.translatedBy(x: 0, y: -200)
            self.healthArticalBtn.transform = self.healthArticalBtn.transform.translatedBy(x: 0, y: -(self.healthArticalBtn.frame.origin.y - 20))
            self.trueMedsDoctorBtn.transform = self.trueMedsDoctorBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.infoMyMedicineBtn.transform = self.infoMyMedicineBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.sabscribeAndSaveBtn.transform = self.sabscribeAndSaveBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
         
            
            
        }) { (_) in
            self.ArticlepushTo()
        }
    }
    
    
    func ArticlepushTo(){
    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ArticlesViewController") as? ArticlesViewController
    self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    @IBAction func trueMedsDocPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":trueMedsDoctorBtn.titleLabel?.text,
        "full": "testing2",
        "question": morexplorelbl.text!
         ])
        
        let logParams = [
        "ans":trueMedsDoctorBtn.titleLabel?.text,
        "full": "testing2",
        "question": morexplorelbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "myOrder"
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.moreBtn.transform = self.moreBtn.transform.translatedBy(x: 0, y: -200)
            self.trueMedsDoctorBtn.transform = self.trueMedsDoctorBtn.transform.translatedBy(x: 0, y: -(self.trueMedsDoctorBtn.frame.origin.y - 20))
            self.healthArticalBtn.transform = self.healthArticalBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.infoMyMedicineBtn.transform = self.infoMyMedicineBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.sabscribeAndSaveBtn.transform = self.sabscribeAndSaveBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
           
            
            
        }) { (_) in
            self.DoctorspushTo()
        }
    }
    
    
    func DoctorspushTo(){
    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TMDoctorsViewController") as? TMDoctorsViewController
    self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
  
    @IBAction func infoMyMedicine(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":infoMyMedicineBtn.titleLabel?.text,
        "full": "testing2",
        "question": morexplorelbl.text!
         ])
        
        let logParams = [
        "ans":infoMyMedicineBtn.titleLabel?.text,
        "full": "testing2",
        "question": morexplorelbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "wallet"
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.moreBtn.transform = self.moreBtn.transform.translatedBy(x: 0, y: -200)
            self.infoMyMedicineBtn.transform = self.infoMyMedicineBtn.transform.translatedBy(x: 0, y: -(self.infoMyMedicineBtn.frame.origin.y - 20))
            self.trueMedsDoctorBtn.transform = self.trueMedsDoctorBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.healthArticalBtn.transform = self.healthArticalBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.sabscribeAndSaveBtn.transform = self.sabscribeAndSaveBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
         
            
            
        }) { (_) in
            self.InfoMedspushTo()
        }
    }
    
    
    func InfoMedspushTo(){
    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "InfoMedicineViewController") as? InfoMedicineViewController
    self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    @IBAction func sabscribeAndSavePress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":sabscribeAndSaveBtn.titleLabel?.text,
        "full": "testing2",
        "question": morexplorelbl.text!
         ])
        
        let logParams = [
        "ans":sabscribeAndSaveBtn.titleLabel?.text,
        "full": "testing2",
        "question": morexplorelbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        cameFrom = "more"
//        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//            self.moreBtn.transform = self.moreBtn.transform.translatedBy(x: 0, y: -200)
//            self.sabscribeAndSaveBtn.transform = self.sabscribeAndSaveBtn.transform.translatedBy(x: 0, y: -(self.sabscribeAndSaveBtn.frame.origin.y - 20))
//            self.trueMedsDoctorBtn.transform = self.trueMedsDoctorBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
//            self.healthArticalBtn.transform = self.healthArticalBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
//            self.infoMyMedicineBtn.transform = self.infoMyMedicineBtn.transform.translatedBy(x: self.view.bounds.width, y: 0)
//            self.questionView.transform = self.questionView.transform.translatedBy(x: 0, y: -self.view.bounds.height)
//
//
//
//        }) { (_) in
            self.SubscribepushTo()
        //}
    }
    
    func SubscribepushTo()
    {
    let localizedContent = NSLocalizedString("This feature is coming soon", comment: "")
    Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
    }
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
}
