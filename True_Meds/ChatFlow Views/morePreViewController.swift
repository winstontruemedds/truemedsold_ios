//
//  morePreViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/10/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

protocol DataEnteredDelegate2: class {
    func userDidEnterInformation2(info:String)
}

protocol DataEnteredDelegate3: class {
    func userDidEnterInformation3(imgarr:[Any],imgids:[CLong])
}

import UIKit
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK

class morePreViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    weak var delegate2: DataEnteredDelegate2? = nil
    
    weak var delegate3: DataEnteredDelegate3? = nil
    
    @IBOutlet weak var sincamgallbtno: UIButton!
    
    @IBOutlet weak var questionTView: UIView!
   
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    
    var imgarray = [Any]()
    var imgids = [CLong]()

    var cameFrom = ""
    
    @IBOutlet weak var imageColl: UICollectionView!
    
    @IBOutlet weak var morepreslbl: UILabel!
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    var btn1Text = String()
    var btn2Text = String()
       
    override func viewDidLoad() {
        super.viewDidLoad()
       
        cameFrom = ""
        setDesign()
        
        visualEffectView.alpha = 0.3
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "9") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        morepreslbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        btn1Text = btn1.value(forKey: "text") as? String ?? "Back"
        btn2Text = btn2.value(forKey: "text") as? String ?? "Back"
              
        
        yesBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        noBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)

        imageColl.reloadData()

        NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
        
        }
        
        @objc func NotificationReceivedforPayment(notification: Notification) {
            paysuccesspop()

            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                self.paysviu.removeFromSuperview()
                self.visualEffectView.removeFromSuperview()
            }
        }
        
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil || UserDefaults.standard.object(forKey: "order1") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "9") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        morepreslbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        yesBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        noBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        
        if cameFrom == ""{
            Out()
        }
        
    }
    
    
    func Out(){
        outRight(object: yesBtn)
        outRight(object: noBtn)
        outLeft(object: questionTView)
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print(cameFrom)
        
        if cameFrom == ""{
            In()
        }else{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.yesBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.noBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionTView.transform = CGAffineTransform(translationX: -10, y: 0)
            })
        }
        
        
    }
    
    func In(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.yesBtn.transform = self.yesBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.noBtn.transform = self.noBtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.questionTView.transform = self.questionTView.transform.translatedBy(x: self.view.bounds.width, y: 0)

        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.yesBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.noBtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionTView.transform = CGAffineTransform(translationX: -10, y: 0)
                
            })
        }
    }
    
    
    @IBAction func sincamgallback(_ sender: Any) {
        self.delegate3?.userDidEnterInformation3(imgarr: imgarray, imgids: imgids)
        navigationController?.popViewController(animated: false)
    }
    
    
    func setDesign(){
        //btns
        backBtn(object: sincamgallbtno)
        blueBtn(object: yesBtn)
        greenBtn(object: noBtn)
        labelFont(object: morepreslbl)
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgarray.count
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
     {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
       let imgstr:NSDictionary = imgarray[indexPath.row] as! NSDictionary
        
        let imgurl:URL = URL(string: imgstr.value(forKey: "ImageUrl") as! String)!
        cell.imgs.layer.cornerRadius = 4.0
        cell.imgs.clipsToBounds = true
        
        cell.visblur.layer.cornerRadius = 4.0
        cell.visblur.clipsToBounds = true
        
        cell.imgs.sd_setImage(with: imgurl, completed: nil)
        cell.delbtn.addTarget(self, action: #selector(delimg), for: .touchUpInside)
        cell.delbtn.tag = indexPath.row
        
        let svgimg: SVGKImage = SVGKImage(named: "circle_cross")
        cell.delbtn.setImage(svgimg.uiImage, for: .normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 100, height: 100)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    @IBAction func yesPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":yesBtn.titleLabel?.text,
        "full": "testing2",
        "question": morepreslbl.text!
         ])
        
        let logParams = [
        "ans":yesBtn.titleLabel?.text,
        "full": "testing2",
        "question": morepreslbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        
        self.delegate3?.userDidEnterInformation3(imgarr: imgarray, imgids: imgids)
        navigationController?.popViewController(animated: false)
        let btnWidth = yesBtn.frame.size.width
        UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
         UserDefaults.standard.set(btn1Text, forKey: "backBtnText")
    }
    
    @IBAction func noPress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":noBtn.titleLabel?.text,
        "full": "testing2",
        "question": morepreslbl.text!
         ])
        
        let logParams = [
        "ans":noBtn.titleLabel?.text,
        "full": "testing2",
        "question": morepreslbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        let btnWidth = noBtn.frame.size.width
        UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
         UserDefaults.standard.set(btn2Text, forKey: "backBtnText")
        
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "searchAndAddViewController") as! searchAndAddViewController
        navigationController?.pushViewController(vc, animated: false)
        
    }
    

    @objc func delimg(sender:UIButton)
       {
           let localizedContent = NSLocalizedString("Are you sure you want to delete this Rx?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
               
               let imgstr:NSDictionary = self.imgarray[sender.tag] as! NSDictionary
               let odID:CLong = imgstr.value(forKey: "OrderId") as! CLong
               
               var para2 = ["orderId":odID,"edit":false] as [String : Any]
               
               let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
               
               print(para2)
               
               let url =  DeleteImgs()
               
               var items = [URLQueryItem]()
               var myURL = URLComponents(string: url)
               
               let session = URLSession.shared
               
               for (key,value) in para2 {
                   items.append(URLQueryItem(name: key, value: String(describing:value)))
               }
               
               myURL?.queryItems = items
               
               var request = URLRequest(url: (myURL?.url)!)
               
               let json = [imgstr.value(forKey: "ImageId") as! CLong]
               
               let jsonData = try? JSONSerialization.data(withJSONObject: json)
               
               request.httpBody = jsonData
               
               request.httpMethod = "POST"
               
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue("a", forHTTPHeaderField: "transactionId")
               request.addValue(authtok, forHTTPHeaderField: "Authorization")
               
               //request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
               
               let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                   if let data = data {
                   do {
                       //create json object from data
                       if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                           
                           DispatchQueue.main.async {
                               let httpResponse = response as? HTTPURLResponse
                               
                               if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                               {
                                   //self.imgconstr.constant = self.imgcollection.contentSize.width - 126
                                   
                                   let deldict:NSDictionary = json as NSDictionary
                                   
                                   print(deldict)
                                
                                   if deldict["201"] != nil
                                   {
                                       self.imgarray.remove(at: sender.tag)
                                       self.imgids.remove(at: sender.tag)
                                    
                                    print(self.imgarray)
                                    
                                       let alert = UIAlertController(title:"", message: deldict.value(forKey: "201") as? String, preferredStyle: .alert)
                                       
                                       self.present(alert, animated: true, completion: nil);
                                       
                                       let when = DispatchTime.now() + 2
                                       DispatchQueue.main.asyncAfter(deadline: when){
                                           alert.dismiss(animated: true, completion: nil)
                                        if self.imgarray.count == 0
                                        {
                                            
                                          self.delegate2?.userDidEnterInformation2(info:"Empty")
                                          self.navigationController?.popViewController(animated: false)
                                        }
                                       }
                               
                                   }
                               
                                   self.imageColl.reloadData()
                                   
                               }
                               else if httpResponse?.statusCode == 401
                               {
                                   let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                   Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                               }
                               else if httpResponse?.statusCode == 500
                               {
                                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                
//                                   let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
//
//                                   self.addChild(popupVC)
//                                   popupVC.view.frame = self.view.frame
//                                   self.view.addSubview(popupVC.view)
//                                   popupVC.didMove(toParent: self)
                               }
                               else
                               {
                                   let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                                   Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                               }
                           }
                           
                           
                       }
                   } catch let error {
                       print(error.localizedDescription)
                   }
                }
               })
               task.resume()
               
               
               
               
           }
           alertVC.addAction(Action1);
           let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
           alertVC.addAction(Action2);
           
           self.present(alertVC, animated: true, completion: nil)
           
       }
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    
}
