//
//  deliveryAddViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/11/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import iOSDropDown
import SkyFloatingLabelTextField
import TPKeyboardAvoiding
import Alamofire
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK

class deliveryAddViewController: UIViewController,UITextFieldDelegate{
    
    
    @IBOutlet weak var topConstains: NSLayoutConstraint!
    
    @IBOutlet weak var bottonConstrains: NSLayoutConstraint!
    
    @IBOutlet weak var questionView: UIView!
    
    @IBOutlet weak var someOneElseBtn: UIButton!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var patientDetailsView: UIView!
   
    @IBOutlet weak var doneBtn: UIButton!
    
    var finpindict2 = NSDictionary()
    
    @IBOutlet weak var addAddlbl: UILabel!
    
    @IBOutlet weak var homecheckbtn: UIButton!
    
    @IBOutlet weak var officecheckbtn: UIButton!
    
    @IBOutlet weak var othercheckbtn: UIButton!
    
    @IBOutlet weak var extratf: UITextField!
    
    @IBOutlet weak var mainlinetf: UITextField!
    
    @IBOutlet weak var landmarktf: UITextField!
    
    @IBOutlet weak var extratfheight: NSLayoutConstraint!
    
    @IBOutlet weak var extratfline: UILabel!
    
    var finpinstr2:String!
    
    var AddressId:CLong!
     var orderID:CLong! = 0
    
    var resdict = NSDictionary()
    
    @IBOutlet weak var arealbl: UILabel!
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.value(forKey:"OrderId") != nil
                               {
                                orderID = UserDefaults.standard.value(forKey:"OrderId") as! CLong
                               }
                               print(orderID!)
        
        mainlinetf.keyboardType = UIKeyboardType.asciiCapable
        landmarktf.keyboardType = UIKeyboardType.asciiCapable
        extratf.keyboardType = UIKeyboardType.asciiCapable
       
        hideKeyboardTapAround()
        
        extratf.text = "Home"
        
        setDesign()
        
        visualEffectView.alpha = 0.3
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "17") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        addAddlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        
        doneBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        let inptarr:NSArray = dict.value(forKey: "inputs") as! NSArray
        let inp1:NSDictionary = inptarr[0] as! NSDictionary
        let inp2:NSDictionary = inptarr[1] as! NSDictionary
        
        mainlinetf.placeholder = inp1.value(forKey: "text") as? String ?? "BuyMedicine"
        landmarktf.placeholder = inp2.value(forKey: "text") as? String ?? "BuyMedicine"

        let city:String = finpindict2.value(forKey: "city") as! String
        let country:String = finpindict2.value(forKey: "country") as! String
        // let region:String = finpindict.value(forKey: "region") as! String
        let state:String = finpindict2.value(forKey: "state") as! String
           
        arealbl.text = city + ", " + state
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
        }
        
        @objc func NotificationReceivedforPayment(notification: Notification) {
            paysuccesspop()

            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                self.paysviu.removeFromSuperview()
                self.visualEffectView.removeFromSuperview()
            }
        
        }
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    
    @IBAction func homebtna(_ sender: Any) {
        extratfheight.constant = 0
        extratfline.isHidden = true
        extratf.text = "Home"
        homecheckbtn.setImage(UIImage(named:"circular_checked"), for: .normal)
        officecheckbtn.setImage(UIImage(named:"circular_unchecked"), for: .normal)
        othercheckbtn.setImage(UIImage(named:"circular_unchecked"), for: .normal)
    }
    
    
    @IBAction func officebtna(_ sender: Any) {
        extratfheight.constant = 0
        extratfline.isHidden = true
        extratf.text = "Office"
        homecheckbtn.setImage(UIImage(named:"circular_unchecked"), for: .normal)
        officecheckbtn.setImage(UIImage(named:"circular_checked"), for: .normal)
        othercheckbtn.setImage(UIImage(named:"circular_unchecked"), for: .normal)
    }
    
    
    @IBAction func otherbtna(_ sender: Any) {
        extratfheight.constant = 40
        extratfline.isHidden = false
        extratf.text = ""
        homecheckbtn.setImage(UIImage(named:"circular_unchecked"), for: .normal)
        officecheckbtn.setImage(UIImage(named:"circular_unchecked"), for: .normal)
        othercheckbtn.setImage(UIImage(named:"circular_checked"), for: .normal)
    }
    
    
    @objc func keyboardWillAppear() {
        topConstains.constant = -60
        //Do something here
    }
    
    @objc func keyboardWillDisappear() {
        topConstains.constant = 30
        //Do something here
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "17") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        addAddlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        
        doneBtn.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        let inptarr:NSArray = dict.value(forKey: "inputs") as! NSArray
        let inp1:NSDictionary = inptarr[0] as! NSDictionary
        let inp2:NSDictionary = inptarr[1] as! NSDictionary
        
        mainlinetf.placeholder = inp1.value(forKey: "text") as? String ?? "BuyMedicine"
        landmarktf.placeholder = inp2.value(forKey: "text") as? String ?? "BuyMedicine"

        let city:String = finpindict2.value(forKey: "city") as! String
        let country:String = finpindict2.value(forKey: "country") as! String
        // let region:String = finpindict.value(forKey: "region") as! String
        let state:String = finpindict2.value(forKey: "state") as! String
           
        arealbl.text = city + ", " + state
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        self.scrollView.transform = self.scrollView.transform.translatedBy(x: 0, y: self.view.bounds.height )
        
    }
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.scrollView.transform = self.scrollView.transform.translatedBy(x:0 , y: -self.view.bounds.height)
            
        })//{ (_) in
        //            UIView.animate(withDuration: 2, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
        //                self.nameTf.becomeFirstResponder()
        //
        //            })
        //        }
        
    }
    

    
    func setDesign(){
       backBtn(object: someOneElseBtn)
       blueBtn(object: doneBtn)
       labelFont(object: addAddlbl)
    }
    
    
       func textFieldShouldReturn(_ textField: UITextField) -> Bool {
              if textField == mainlinetf
              {
                landmarktf.becomeFirstResponder()
              }
              if textField == landmarktf{
                landmarktf.resignFirstResponder()
              }
              if textField == extratf{
                mainlinetf.becomeFirstResponder()
              }
              return true
          }
    

    @IBAction func donePress(_ sender: Any) {
      
      Analytics.logEvent("button_click", parameters: [
        "ans":doneBtn.titleLabel?.text,
        "full": "testing2",
        "question": addAddlbl.text!
         ])
        
        let logParams = [
        "ans":doneBtn.titleLabel?.text,
        "full": "testing2",
        "question": addAddlbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        
      if extratf.text! == "" || mainlinetf.text! == "" || landmarktf.text! == ""
      {
        let localizedContent = NSLocalizedString("Please enter all fields", comment: "")
        Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
      }
      else
      {
       AddNewAddress()
      }
    
    }
    
    
    
    @IBAction func someOnePress(_ sender: Any) {
        navigationController?.popViewController(animated: false)
        
    }
    
    
   func saveAddress(){
                              
           
           let baseUrl = saveAdd()
           print(baseUrl)
           let queryStringParam  =  ["orderId": String(describing:orderID!),"addressId": String(describing:AddressId!)
                                  ] as [String : Any]
           print(queryStringParam)
                  
                              var urlComponent = URLComponents(string: baseUrl)!
                              let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value as? String) }
                                urlComponent.queryItems = queryItems
                                
                              let bodyParameters = ["":""]
                              
                              let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
                             
                                      
                              let headers = ["Content-Type": "application/json",
                                                                  "transactionId":"a",
                                                                  "Authorization":authtok]
                              
                      
                                var request = URLRequest(url: urlComponent.url!)
                                request.httpMethod = "POST"
                                request.httpBody = try? JSONSerialization.data(withJSONObject: bodyParameters)
                                request.allHTTPHeaderFields = headers
                                
                                AF.request(request).responseJSON { response in
                                  print(response)
                                  switch response.result {
                                          case .success(let value):
                                            switch response.response?.statusCode {
                                                                                   case 200,201:
                                                                                       print("")
                                          if let dict = value as? NSDictionary {
                                             print(dict)
                                           if UserDefaults.standard.value(forKey: "Summary") as! String == "fromSummary"{
                                                 let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "summaryViewController") as? summaryViewController
                                                    self.navigationController?.pushViewController(vc!, animated: false)
                                           }else{
                                               let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "orderPaymentViewController") as? orderPaymentViewController
                                                                                     vc?.addID = self.AddressId
                                                                                        self.navigationController?.pushViewController(vc!, animated: false)
                                           }
                                           
                                         
                                        
                                                           
                                                       }
                                                case 401:
                                                                                           print("401")
                                                                                           let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                                                                           Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                                                                                       case 400:
                                                                                           print("400")
                                                                                           let adddict = value as? NSDictionary
                                                                                           if adddict?.allKeys.first as! String == "400"
                                                                                           {
                                                                                           Utility.showAlertWithTitle(title: "", andMessage: adddict?.value(forKey: "400") as? String, onVC: self)
                                                                                           }
                                                                                       case 500:
                                                                                           print("500")
                                                
//                                                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                                                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                           let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                                                                                           self.addChild(popupVC)
                                                                                           popupVC.view.frame = self.view.frame
                                                                                           self.view.addSubview(popupVC.view)
                                                                                           popupVC.didMove(toParent: self)
                                                                                       default:
                                                                                           print("may be 500")
                                                                                           let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                                                                                           Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                                                                    
                                                                                       }
                                                                                       
                                                   case .failure(let error):
                                                       print(error)
                                                   }
                                        
                                    }
       }
       
       
    
    
 
    func AddNewAddress()
    {
            let parameters = ["addressType": extratf.text!,
            "addressline1":mainlinetf.text!,
            "landmark": landmarktf.text!,
            "pincode":finpinstr2!]
            
            ApiManager().requestApiWithDataType(methodType:BPOST, urlString: AddAddress(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
                
                if cStatus == 200 || cStatus == 201
                {
                  self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
                  print(self.resdict)
                  self.AddressId = self.resdict.value(forKey: "addressId") as! CLong
                    print(self.AddressId!)
                    self.saveAddress()
                    
                
                    
                }
                else if cStatus == 400
                 {
                    let adddict:NSDictionary = convertStringToDictionary(json: response as! String) as! NSDictionary
                    if adddict.allKeys.first as! String == "400"
                    {
                     Utility.showAlertWithTitle(title: "", andMessage: adddict.value(forKey: "400") as! String, onVC: self)
                                                 
                    }
                }
                else if cStatus == 401
                {
                    let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                    Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                }
                else if cStatus == 500
                {
//                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
                                
                                self.addChild(popupVC)
                                popupVC.view.frame = self.view.frame
                                self.view.addSubview(popupVC.view)
                                popupVC.didMove(toParent: self)
                }
                else
                {
                    let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                }
    }

    }
    
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    
}
