//
//  someonemorePreViewController.swift
//  True_Meds
//
//  Created by Welborn Machado on 02/10/19.
//

protocol DataEnteredDelegate: class {
    func userDidEnterInformation(info: Int, fileId: Int, orderId:CLong, patientId:CLong)
}

protocol DataEnteredDelegate5: class {
    func userDidEnterInformation5(orderId:CLong, imgsId:[CLong])
}

protocol DataEnteredDelegate7: class {
    func userDidEnterInformation7(imgarr:[Any],imgids:[CLong])
}


import UIKit
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK

class someonemorePreViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,DataEnteredDelegate4 {

    weak var delegate: DataEnteredDelegate? = nil
    
    weak var delegate5: DataEnteredDelegate5? = nil
    
      weak var delegate7: DataEnteredDelegate7? = nil
    
    @IBOutlet weak var questionTView: UIView!
    
     @IBOutlet weak var yesBtn2: UIButton!
     @IBOutlet weak var noBtn2: UIButton!
     
     var imgarray = [Any]()
     var imgids = [CLong]()
     var spimageArray = [Any]()
     var spimgids = [Any]()
    
     var cameFrom = ""
    
    @IBOutlet weak var camgallbtno: UIButton!
    
     @IBOutlet weak var imageColl2: UICollectionView!
     
     @IBOutlet weak var morepreslbl2: UILabel!
     
     @IBOutlet weak var logoo: UIButton!
     
     @IBOutlet weak var menubtno: UIButton!
    
     var namecount2:Int!
    
    var filenameID2:Int!
    
    var orderID2:CLong!
    
    var PatientID2:CLong!
    
    var selectedpat3 = [Any]()
    
    var btn1Text = String()
    var btn2Text = String()
      
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    var patientName:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cameFrom = ""
        setDesign()
        
        
        print(namecount2)
        
        visualEffectView.alpha = 0.3

        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "11") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        let escaping:String = text.replacingOccurrences(of: "\\n", with: "\n")
        let patarrfor = selectedpat3[namecount2] as! NSDictionary
        
        if let name = patarrfor.value(forKey:"patientName") as? String
        {
          patientName = " " + name + " "
        }
        else
        {
          patientName = ""
        }
        
        morepreslbl2.text = escaping.replacingOccurrences(of: "<Name>", with: patientName)
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        btn1Text = btn1.value(forKey: "text") as? String ?? "Back"
        btn2Text = btn2.value(forKey: "text") as? String ?? "Back"
               
        
        yesBtn2.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
        noBtn2.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
        }
        
        @objc func NotificationReceivedforPayment(notification: Notification) {
            paysuccesspop()

            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                self.paysviu.removeFromSuperview()
                self.visualEffectView.removeFromSuperview()
            }
        
        }
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil || UserDefaults.standard.object(forKey: "order1") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }

    
    func userDidEnterInformation4(info: String,info2: Int) {
        if info == "MultiBack"
        {
            let imgarr = UserDefaults.standard.value(forKey: "PatientPres") as! [Any]
            let totalpat = UserDefaults.standard.value(forKey: "TotalPatients") as! Int
        
            self.imgarray = imgarr[totalpat - 1] as! [Any]
            
            self.imageColl2.reloadData()
        }
    }

    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        spimageArray.append(imgarray)
        spimgids.append(imgids)
        
        UserDefaults.standard.set(spimageArray as [Any], forKey: "PatientPres")
              UserDefaults.standard.set(spimgids as [Any], forKey: "PatientIDS")
              UserDefaults.standard.synchronize()
       
        
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
               
               let dict:NSDictionary = maindict.value(forKey: "11") as! NSDictionary
               
               let text:String = dict.value(forKey: "question") as! String
               
               let escaping:String = text.replacingOccurrences(of: "\\n", with: "\n")
               
               //morepreslbl2.text = escaping.replacingOccurrences(of: "<Name>", with: patientName)
               let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
               let btn1:NSDictionary = btnarr[0] as! NSDictionary
               let btn2:NSDictionary = btnarr[1] as! NSDictionary
               
               yesBtn2.setTitle(btn1.value(forKey: "text") as? String ?? "BuyMedicine", for: .normal)
               noBtn2.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        
        if cameFrom == ""{
            Out()
        }
        
    }
    
    
    func Out(){
        outRight(object: yesBtn2)
        outRight(object: noBtn2)
        outLeft(object: questionTView)
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print(cameFrom)
        
        
        if cameFrom == ""{
            In()
        }else{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.yesBtn2.transform = CGAffineTransform(translationX: 10, y: 0)
                self.noBtn2.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionTView.transform = CGAffineTransform(translationX: -10, y: 0)
            })
        }
        
        
    }
    
    func In(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.yesBtn2.transform = self.yesBtn2.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.noBtn2.transform = self.noBtn2.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.questionTView.transform = self.questionTView.transform.translatedBy(x: self.view.bounds.width, y: 0)

        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.yesBtn2.transform = CGAffineTransform(translationX: 10, y: 0)
                self.noBtn2.transform = CGAffineTransform(translationX: 10, y: 0)
                self.questionTView.transform = CGAffineTransform(translationX: -10, y: 0)
                
            })
        }
    }
    
    func setDesign(){
        
        //btns
        backBtn(object: camgallbtno)
        blueBtn(object: yesBtn2)
        greenBtn(object: noBtn2)
        labelFont(object: morepreslbl2)
    }

    
    @IBAction func camgallback(_ sender: Any) {
        self.delegate7?.userDidEnterInformation7(imgarr: imgarray, imgids: imgids)
        if namecount2 < selectedpat3.count
        {
        navigationController?.popViewController(animated: false)
            
        }
        else
        {
        
        delegate5?.userDidEnterInformation5(orderId: orderID2, imgsId: imgids)
        navigationController?.popViewController(animated: false)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgarray.count
    }
    
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
     {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
       let imgstr:NSDictionary = imgarray[indexPath.row] as! NSDictionary
        
        let imgurl:URL = URL(string: imgstr.value(forKey: "ImageUrl") as! String)!
        cell.imgs.layer.cornerRadius = 4.0
        cell.imgs.clipsToBounds = true
        
        cell.visblur.layer.cornerRadius = 4.0
        cell.visblur.clipsToBounds = true
        
        cell.imgs.sd_setImage(with: imgurl, completed: nil)
        cell.delbtn.addTarget(self, action: #selector(delimg2), for: .touchUpInside)
        cell.delbtn.tag = indexPath.row
        
        let svgimg: SVGKImage = SVGKImage(named: "circle_cross")
        cell.delbtn.setImage(svgimg.uiImage, for: .normal)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 100, height: 100)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    @IBAction func yesPress2(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
               "ans":yesBtn2.titleLabel?.text,
               "full": "testing2",
               "question": morepreslbl2.text!
                ])
        
        let logParams = [
        "ans":yesBtn2.titleLabel?.text,
        "full": "testing2",
        "question": morepreslbl2.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        let btnWidth = yesBtn2.frame.size.width
             UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
         UserDefaults.standard.set(btn1Text, forKey: "backBtnText")
    self.delegate7?.userDidEnterInformation7(imgarr: imgarray, imgids: imgids)
        navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func noPress2(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":noBtn2.titleLabel?.text,
        "full": "testing2",
        "question": morepreslbl2.text!
         ])
        
        let logParams = [
        "ans":noBtn2.titleLabel?.text,
        "full": "testing2",
        "question": morepreslbl2.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        UserDefaults.standard.set(btn2Text, forKey: "backBtnText")
        
        let btnWidth = noBtn2.frame.size.width
        UserDefaults.standard.set(btnWidth, forKey: "backBtnWidth")
        //spimageArray.append(imgarray)
        //spimgids.append(imgids)
        
        print(namecount2)
        
        namecount2 = UserDefaults.standard.value(forKey: "nameCount") as? Int ?? namecount2
        
        UserDefaults.standard.removeObject(forKey: "nameCount")
        UserDefaults.standard.synchronize()
        
        
        if namecount2 != selectedpat3.count{
          namecount2 = namecount2 + 1
        }
        
        
         print(namecount2)
        
        if namecount2 == selectedpat3.count
        {
        //UserDefaults.standard.set(spimageArray as [Any], forKey: "PatientPres")
       // UserDefaults.standard.set(spimgids as [Any], forKey: "PatientIDS")
        //UserDefaults.standard.synchronize()
            
        print(self.spimgids)
            
        let vc = storyboard?.instantiateViewController(withIdentifier: "searchAndAddViewController") as! searchAndAddViewController
            //vc.namecount = namecount2
           // vc.selectedpat2 = selectedpat3
        navigationController?.pushViewController(vc, animated: false)
        }
        else
        {
        let vc = storyboard?.instantiateViewController(withIdentifier: "someonecameraGalleryViewController") as! someonecameraGalleryViewController
        vc.namecount = namecount2
        vc.selectedpat2 = selectedpat3
        vc.spimgids2 = spimgids
        navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    
    
    @objc func delimg2(sender:UIButton)
    {
        let localizedContent = NSLocalizedString("Are you sure you want to delete this Rx?", comment: "")
        let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
        let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
        { (action:UIAlertAction!) in
        
        let imgstr:NSDictionary = self.imgarray[sender.tag] as! NSDictionary
        let odID:CLong = imgstr.value(forKey: "OrderId") as! CLong
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        var para2 = ["orderId":odID,"edit":false] as [String : Any]
        
        let url =  DeleteImgs()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        let json = [imgstr.value(forKey: "ImageId") as! CLong]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        request.httpBody = jsonData
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        //request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                        let httpResponse = response as? HTTPURLResponse
                        
                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                        {
                        
                            let deldict:NSDictionary = json as NSDictionary
                            
                            if deldict["201"] != nil
                            {
                                self.imgarray.remove(at: sender.tag)
                                self.imgids.remove(at: sender.tag)
                                
                                print(self.imgarray)
                                 print( self.imgids)
                                
                                
                                let alert = UIAlertController(title:"", message: deldict.value(forKey: "201") as! String, preferredStyle: .alert)
                                
                                self.present(alert, animated: true, completion: nil)
                                
                                let when = DispatchTime.now() + 2
                                DispatchQueue.main.asyncAfter(deadline: when){
                                    // your code with delay
                                    alert.dismiss(animated: true, completion: nil)
                                    if self.imgarray.count == 0
                                    {
                                        print(self.namecount2)
                                        self.namecount2 = self.namecount2 - 1
                                        UserDefaults.standard.set(self.namecount2, forKey: "nameCount")
                                        print(self.namecount2)
                                        self.delegate7?.userDidEnterInformation7(imgarr: self.imgarray, imgids: self.imgids)
                                      self.navigationController?.popViewController(animated: false)
                                    }
                                
                                }
                                
                            }
                            
                            self.imageColl2.reloadData()
                            
                        }
                        else if httpResponse?.statusCode == 401
                        {
                            let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                            Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                        }
                        else if httpResponse?.statusCode == 500
                        {
                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                            
//                            let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
//                            
//                            self.addChild(popupVC)
//                            popupVC.view.frame = self.view.frame
//                            self.view.addSubview(popupVC.view)
//                            popupVC.didMove(toParent: self)
                        }
                        else
                        {
                            let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
        
        }
        alertVC.addAction(Action1);
        let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
        alertVC.addAction(Action2);
        
        //UIApplication.shared.keyWindow?.rootViewController?.present(alertVC, animated: true, completion: nil)
    
        self.present(alertVC, animated: true, completion: nil)
        
    }
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    
}

