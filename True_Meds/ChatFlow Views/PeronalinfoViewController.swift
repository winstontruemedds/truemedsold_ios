//
//  PeronalinfoViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/11/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import iOSDropDown
import SkyFloatingLabelTextField
import TPKeyboardAvoiding
import Alamofire
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK

class PeronalinfoViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var topConstains: NSLayoutConstraint!
    
    @IBOutlet weak var bottonConstrains: NSLayoutConstraint!
    
    @IBOutlet weak var questionView: UIView!
    
    @IBOutlet weak var someOneElseBtn: UIButton!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var doneBtn: UIButton!
    
    @IBOutlet weak var patientDetailsView: UIView!
   
    var ArrayB2 = [[String:String]]()

    @IBOutlet weak var nametf: UITextField!
    
    @IBOutlet weak var agetf: UITextField!
    
   // @IBOutlet weak var gendertf: UITextField!
    
    @IBOutlet weak var genderddtf: DropDown!
    
    
    var resdict = NSDictionary()
    
    var genId:CLong!
    
    let pickarr = ["Male","Female","Others"]
    
    var picktxt:String!
    
    @IBOutlet weak var cgenpicker: UIPickerView!
    
    @IBOutlet weak var toolbr: UIToolbar!
    
    @IBOutlet weak var updatenamelbl: UILabel!
    
    @IBOutlet weak var logoo: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardTapAround()
        setDesign()
        setDropDown()
        nametf.text = ""
        agetf.text = ""
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "14") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        updatenamelbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        //let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        doneBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        
        //gendertf.inputView = cgenpicker
        //gendertf.inputAccessoryView = toolbr
        
        agetf.inputAccessoryView = toolbr
        
        nametf.addTarget(self, action: #selector(textFieldDidEndEditing), for:  UIControl.Event.editingDidEnd)
               
        agetf.addTarget(self, action: #selector(textFieldDidChange), for:  UIControl.Event.editingChanged)
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
    }
    
    
    
    

    
    @objc func keyboardWillAppear() {
                  
        topConstains.constant = 10
                   //Do something here
    }
               
    @objc func keyboardWillDisappear() {
        topConstains.constant = 150
                   //Do something here
    }
    
    
    
    
    func setDropDown(){
        genderddtf.optionArray = ["Male","Female","Others"]
        genderddtf.optionIds = [8,9,10]
       
        genderddtf.selectedRowColor = UIColor.init(hex: 0x22B573)
        
        genderddtf.didSelect{(selectedText , index ,id) in
            // self.valueLabel.text = "Selected String: \(selectedText) \n index: \(index)"
           
           self.genId = id
            print(index)
            print(self.genId!)
        }
        genderddtf.listWillDisappear {
            // print(self.dropDown.text!)
        }
        
        
    }
    
    @objc func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == nametf
        {
            if Utility.isValidName2(nametf.text!) == true
            {
            
            }
            else
            {
                nametf.text = ""
                let localizedContent = NSLocalizedString("Please enter a valid name", comment: "")
                Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
            }
        }
    }
    
    
    @objc func textFieldDidChange(textField: UITextField)
    {
       
        if textField == agetf
        {
            if  (agetf.text?.count)! == 0
            {
                let localizedContent = NSLocalizedString("Please enter valid age", comment: "")
                Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
            }
            
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          
          if textField == agetf
          {
          let currentText = textField.text ?? ""
          guard let stringRange = Range(range, in: currentText) else { return false }
          
          let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
          
          if updatedText == ""
          {
              
          }else{
          let checkage:Int = Int(updatedText)!
          if checkage < 1
              {
                  agetf.text = ""
                  let localizedContent = NSLocalizedString("Please enter valid age", comment: "")
                  Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
              }
              else if checkage > 150
              {
                  agetf.text = ""
                  let localizedContent = NSLocalizedString("Input is over the allowable age limit for the app", comment: "")
                  Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
              }
              }
              
          return updatedText.count <= 3
          }
          return true
      }
    
    @IBAction func ageDone(_ sender: Any) {
        
        if agetf.text == ""
               {
                let localizedContent = NSLocalizedString("Please enter valid age", comment: "")
                Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
               }else{
               let checkage:Int = Int(agetf.text!)!
               if checkage < 1
               {
                agetf.text = ""
                let localizedContent = NSLocalizedString("Please enter valid age", comment: "")
                Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
               }
               else if checkage > 150
               {
               agetf.text = ""
               let localizedContent = NSLocalizedString("Input is over the allowable age limit for the app", comment: "")
               Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
               }
               else
               {
                view.endEditing(true)
               }
               }
        
        
    }
    
    
    
    @IBAction func genderBtnPress(_ sender: Any) {
        genderddtf.showList()
    }
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    
    
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    @IBAction func genderpickerdone(_ sender: Any) {
//        gendertf.endEditing(true)
//        gendertf.text = picktxt
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickarr.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        picktxt = pickarr[row]
        if picktxt == "Male"
        {
        genId = 8
        }
        else if picktxt == "Female"
        {
        genId = 9
        }
        else
        {
        genId = 10
        }
    
        return pickarr[row]
    }
    
   
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           if textField == nametf
           {
             agetf.becomeFirstResponder()
           }
           return true
       }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "14") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        updatenamelbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        //let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        doneBtn.setTitle(btn2.value(forKey: "text") as? String ?? "", for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        self.scrollView.transform = self.scrollView.transform.translatedBy(x: 0, y: self.view.bounds.height )
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.scrollView.transform = self.scrollView.transform.translatedBy(x:0 , y: -self.view.bounds.height)
            
            self.nametf.becomeFirstResponder()
            
        })//{ (_) in
        //            UIView.animate(withDuration: 2, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
        //                self.nameTf.becomeFirstResponder()
        //
        //            })
        //        }
        
    }
    
 
    
    
    func setDesign(){
        backBtn(object: someOneElseBtn)
        blueBtn(object: doneBtn)
        labelFont(object: updatenamelbl)
    }
    

    //doneBtnPress
    @IBAction func addBtnPress(_ sender: Any) {
        Analytics.logEvent("button_click", parameters: [
        "ans":doneBtn.titleLabel?.text,
        "full": "testing2",
        "question": updatenamelbl.text!
         ])
        
        let logParams = [
        "ans":doneBtn.titleLabel?.text,
        "full": "testing2",
        "question": updatenamelbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        
        if nametf.text == "" || nametf.text == nil || agetf.text == "" || agetf.text == nil || genId == nil
             {
              let localizedContent = NSLocalizedString("Please enter all fields", comment: "")
              Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
             }
             else
             {
               updateName()
             }

       
    }
    
    
    @IBAction func someOnePress(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    
    
    func updateName()
    {
        let parameters = ["customerName":nametf.text!,
                "mobileNo":"",
                "ageGroupId":nil ,
                "ageGroupName":nil,
                "languageId":nil,
                "languageName":nil,
                "emailAddress":nil,
                "profileImage":nil,
                "profileImageUrl":nil,
                "age":CLong(agetf.text!),
                "gender":genId,
                "genderName":"",
                "addressId":nil
        ] as [String : Any?]
        
        ApiManager().requestApiWithDataType(methodType:BPOST, urlString: UpdateName(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
        
            
        if cStatus == 200 || cStatus == 201
        {
            self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
            print(self.resdict)
            UserDefaults.standard.set(self.resdict.value(forKey: "customerName"), forKey: "custname")
            UserDefaults.standard.synchronize()
            //let localizedContent = NSLocalizedString("Profile saved successfully", comment: "")
            //Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "deliveryPincodeViewController") as? deliveryPincodeViewController
            
            self.navigationController?.pushViewController(vc!, animated: false)
        }
        else if cStatus == 401
            {
                let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
            }
            else if cStatus == 500
            {
//                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                
                let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                self.addChild(popupVC)
                popupVC.view.frame = self.view.frame
                self.view.addSubview(popupVC.view)
                popupVC.didMove(toParent: self)
            }
            else
            {
                let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
            }
        }
    }

}
