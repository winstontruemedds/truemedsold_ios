//
//  secondTimeAddPatientViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/11/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import Reachability
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK

class secondTimeAddPatientViewController: UIViewController {
    
     var selectedpat = [Any]()
     var patstatus = [Bool]()
     var selectedIndex = [Int]()
     var patientDict = NSArray()
     var cameFrom = ""
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var questionView: UIView!
    
    @IBOutlet weak var someOneElseBtn: UIButton!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
  
    @IBOutlet weak var donebtno: UIButton!
    
    @IBOutlet weak var selectpatlbl: UILabel!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    @IBOutlet weak var addmorebtno: UIButton!
    
    
    var resdict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        cameFrom = ""
        setDesign()
        
        visualEffectView.alpha = 0.3
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "7") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        selectpatlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        //let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        donebtno.setTitle(btn1.value(forKey: "text") as? String ?? "", for: .normal)
        
    NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
    }
    
    @objc func NotificationReceivedforPayment(notification: Notification) {
        paysuccesspop()

        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.paysviu.removeFromSuperview()
            self.visualEffectView.removeFromSuperview()
        }
    
    }
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        UserDefaults.standard.removeObject(forKey: "OrderId")
         UserDefaults.standard.removeObject(forKey: "nameCount")
        
        UserDefaults.standard.synchronize()
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "7") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        selectpatlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        //let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        donebtno.setTitle(btn1.value(forKey: "text") as? String ?? "", for: .normal)
        
        allpatients3()
        if cameFrom == ""{
            Out()
        }
        
    }
    
    
    func Out(){
        outLeft(object: questionView)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print(cameFrom)
        
        
        if cameFrom == ""{
            In()
        }else{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
               
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
             
                
                
            })
        }
        
        
    }
    
    func In(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
         
            self.questionView.transform = self.questionView.transform.translatedBy(x: self.view.bounds.width, y: 0)
      
            
            
        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
    
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
    
                
            })
        }
    }
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    func setDesign(){
        //btns
        backBtn(object: someOneElseBtn)
        blueBtn(object: donebtno)
        labelFont(object: selectpatlbl)
    }
    
    
    func allpatients3(){
             let para2 = ["showMyself":"false"]
            
            ApiManager().requestApiWithDataType(methodType:HPOST, urlString: GetAllPatients(),parameters: para2 as [String : AnyObject]) { (response,cStatus, error) in
                             
                             print(response)
                             
                if cStatus == 200 || cStatus == 201
                             {
                               let dict  = convertStringToDictionary(json: response as! String)! as NSDictionary
                                
                                self.patientDict = dict.value(forKey: "PatientList") as! NSArray
                                
                                if self.patientDict.count == 0
                                {
                                UserDefaults.standard.set("No", forKey: "isPatAvail")
                                UserDefaults.standard.synchronize()
                                
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: whoIsViewController.self) {
                                        self.navigationController!.popToViewController(controller, animated: false)
                                        break
                                    }
                                }
                                }
    
                                self.collectionView.reloadData()
                            
                             }
                         else if cStatus == 401
                         {
                             let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                             Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                         }
                         else if cStatus == 500
                         {
                            
//                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                            
                             let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                             self.addChild(popupVC)
                             popupVC.view.frame = self.view.frame
                             self.view.addSubview(popupVC.view)
                             popupVC.didMove(toParent: self)
                         }
                         else
                         {
                             let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                             Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                         }
                     }
            
            
        }
        
    
    //addmorepress
    @IBAction func addPatientPress(_ sender: Any) {
        
     Analytics.logEvent("button_click", parameters: [
        "ans":addmorebtno.titleLabel?.text,
        "full": "testing2",
        "question": selectpatlbl.text!
         ])
        
        let logParams = [
        "ans":addmorebtno.titleLabel?.text,
        "full": "testing2",
        "question": selectpatlbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
       
    if UserDefaults.standard.value(forKey:"cameFrompushpop") as? String == "Yes"
    {
     self.navigationController?.popViewController(animated: false)
    }
    else
    {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddPatientViewController") as! AddPatientViewController
        vc.isfromchat = "Yes"
        navigationController?.pushViewController(vc, animated: false)
    }
        
    }
    
    
    @IBAction func donePress(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":donebtno.titleLabel?.text,
        "full": "testing2",
        "question": selectpatlbl.text!
         ])
        
        let logParams = [
        "ans":donebtno.titleLabel?.text,
        "full": "testing2",
        "question": selectpatlbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        
         let cameFrom = UserDefaults.standard.value(forKey: "cameFrom") as! String
          if cameFrom == "Both"{
      selectedpat.insert(["patientName":UserDefaults.standard.value(forKey: "custname"), "patientId":nil], at: 0)
                        
          }
        
        if selectedpat.count == 0
        {
        
        let localizedContent = NSLocalizedString("Please select atleast one patient", comment: "")
        Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
        else
        {
        let vc = storyboard?.instantiateViewController(withIdentifier: "someonecameraGalleryViewController") as! someonecameraGalleryViewController
        vc.selectedpat2 = selectedpat
        vc.namecount = 0
        navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    
    @IBAction func someOneElsePress(_ sender: Any) {
        
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: whoIsViewController.self) {
        
                    self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
    }
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    

}

extension secondTimeAddPatientViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return patientDict.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell3", for: indexPath) as! ImageCollectionViewCell3
        
                          let reldict = patientDict[indexPath.row] as! NSDictionary
                          if let relation = reldict.value(forKey: "relationName") as? String
                          {
                            cell.myprelation.text = relation //NSLocalizedString(relation, comment: "")
                          }
        
                          if let name = reldict.value(forKey: "patientName") as? String
                          {
                              cell.mypatientname.text = name
                          }
        
                          cell.bgview.layer.cornerRadius = 4.0
                          cell.bgview.layer.borderWidth = 0.8
                          cell.bgview.layer.borderColor = UIColor(hexString: "#22B573").cgColor
                          cell.deletepatient.tag = indexPath.row
                          cell.deletepatient.addTarget(self, action: #selector(deletePatient), for: .touchUpInside)
        
                          if selectedIndex.count == 0
                          {
                              cell.bgview.backgroundColor = UIColor.white
                              cell.mypatientname.textColor = UIColor(hexString: "#22B573")
                              cell.myprelation.textColor = UIColor(hexString: "#22B573")
                              cell.deletepatient.setTitleColor(UIColor(hexString: "#22B573"), for: .normal)
                          }else{
                              if selectedIndex.contains(indexPath.row)
                              {
                                  cell.bgview.backgroundColor = UIColor(hexString: "#22B573")
                                  cell.mypatientname.textColor = UIColor.white
                                  cell.myprelation.textColor = UIColor.white
                                 cell.deletepatient.setTitleColor(UIColor.white, for: .normal)
        
                              }else
                              {
                                  cell.bgview.backgroundColor = UIColor.white
                                  cell.mypatientname.textColor = UIColor(hexString: "#22B573")
                                  cell.myprelation.textColor =  UIColor(hexString: "#22B573")
                                  cell.deletepatient.setTitleColor(UIColor(hexString: "#22B573"), for: .normal)
                              }
                          }
        
                return cell
    }
    
    
    
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
      {
          
          return CGSize(width:150, height: 100)
      }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    if selectedIndex.contains(indexPath.row)
    {
        while selectedIndex.contains(indexPath.row) {
            if let itemToRemoveIndex = selectedIndex.firstIndex(of: indexPath.row) {
                selectedIndex.remove(at: itemToRemoveIndex)
                selectedpat.remove(at: itemToRemoveIndex)
                
            }
        }
    }else
    {
        selectedpat.append(patientDict[indexPath.row])
        
        selectedIndex.append(indexPath.row)
    }
    
    self.collectionView.reloadData()
        
    }

      @objc func deletePatient(sender:UIButton)
      {
          let localizedContent = NSLocalizedString("Are you sure, you want to delete this patient's details?", comment: "")
          let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
          let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
          { (action:UIAlertAction!) in

             // if Connectivity.isConnectedToInternet {

              if self.selectedIndex.contains(sender.tag)
              {
                  while self.selectedIndex.contains(sender.tag) {
                      if let itemToRemoveIndex = self.selectedIndex.firstIndex(of: sender.tag) {
                          self.selectedIndex.remove(at: itemToRemoveIndex)
                          self.selectedpat.remove(at: itemToRemoveIndex)
                      }
                  }
              }

          //self.isUserInteractionEnabled = false
          let parr = self.patientDict[sender.tag] as! NSDictionary
          let patID = parr.value(forKey: "patientId") as! CLong

          let custid:String = String(describing:UserDefaults.standard.value(forKey: "CustomerId")!)

          let parameters = ["customerId":custid,
                            "patientId":patID
              ] as [String : Any]

            ApiManager().requestApiWithDataType( methodType: HPOST, urlString: DeletePatients(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
                
                if cStatus == 200 || cStatus == 201
                {
                    self.allpatients3()
                    //let localizedContent = NSLocalizedString("Patient details deleted successfully", comment: "")
                   // Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
                }
                else if cStatus == 401
                {
                    let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                    Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                }
                else if cStatus == 500
                {
//                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    
                    let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                    self.addChild(popupVC)
                    popupVC.view.frame = self.view.frame
                    self.view.addSubview(popupVC.view)
                    popupVC.didMove(toParent: self)
                }
                else
                {
                    let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                }
            }
            
//              }
//              else
//              {
//                  Utility.showNetworkAlert(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
//              }
          }
          alertVC.addAction(Action1);
          let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
          alertVC.addAction(Action2);
          self.present(alertVC, animated: true, completion: nil);

      }


    
}
