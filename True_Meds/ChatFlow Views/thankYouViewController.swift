//
//  thankYouViewController.swift
//  True_Meds
//
//  Created by siddhesh redkar on 10/3/19.
//

import UIKit
import SVGKit
import SideMenu
import FirebaseAnalytics
import Flurry_iOS_SDK

class thankYouViewController: UIViewController {
    
    
    
    @IBOutlet weak var orderimageView: UIImageView!
    
    @IBOutlet weak var lbl1: UILabel!
    
    @IBOutlet weak var lbl3: UILabel!
    
    @IBOutlet weak var donebtn: UIButton!
    
    var cameFrom = ""
    
    @IBOutlet weak var logoo: UIButton!
       
    @IBOutlet weak var menubtno: UIButton!

    var savingsvalue2:String!
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         setDesign()
         UserDefaults.standard.set("", forKey: "applicableOn")
         visualEffectView.alpha = 0.3
        
         let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
    
         let dict:NSDictionary = maindict.value(forKey: "18") as! NSDictionary
         let text:String = dict.value(forKey: "question") as! String
         lbl1.text = savingsvalue2
         let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
         let btn1:NSDictionary = btnarr[0] as! NSDictionary
         donebtn.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
         let desarr:NSArray = dict.value(forKey: "description") as! NSArray
         let des1:NSDictionary = desarr[0] as! NSDictionary
         
         let text2:String = des1.value(forKey: "text") as! String
         lbl3.text = text2.replacingOccurrences(of: "\\n", with: "\n")
         
        
         let svgimg3: SVGKImage = SVGKImage(named: "Thankyou2")
         
         orderimageView.image = svgimg3.uiImage
    
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
        }
        
        @objc func NotificationReceivedforPayment(notification: Notification) {
            paysuccesspop()

            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                self.paysviu.removeFromSuperview()
                self.visualEffectView.removeFromSuperview()
            }
        
        }
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil 
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    @IBAction func OrderDone(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
               "ans":donebtn.titleLabel?.text,
               "full": "testing2",
               "question": lbl1.text
                ])
        
        let logParams = [
        "ans":donebtn.titleLabel?.text,
        "full": "testing2",
        "question": lbl1.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        UserDefaults.standard.removeObject(forKey: "order1")
    
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: buyMeds.self) {
                
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
          
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
             let dict:NSDictionary = maindict.value(forKey: "18") as! NSDictionary
             let text:String = dict.value(forKey: "question") as! String
             lbl1.text = savingsvalue2
             let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
             let btn1:NSDictionary = btnarr[0] as! NSDictionary
             donebtn.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
             let desarr:NSArray = dict.value(forKey: "description") as! NSArray
             let des1:NSDictionary = desarr[0] as! NSDictionary
             
             let text2:String = des1.value(forKey: "text") as! String
             lbl3.text = text2.replacingOccurrences(of: "\\n", with: "\n")
        
          if cameFrom == ""{
              Out()
          }
          
      }
    
    
    override func viewDidAppear(_ animated: Bool) {
           print(cameFrom)
           
        
           if cameFrom == ""{
               In()
           }else{
               UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                   
                   self.donebtn.transform = CGAffineTransform(translationX: 10, y: 0)
                 
                   self.orderimageView.transform = CGAffineTransform(translationX: -10, y: 0)
                  
                   
                   
               })
           }
           
           
       }
    
    func setDesign(){
        //btns
      
       blueBtn(object: donebtn)
       labelFont(object: lbl1)
       labelFont2(object: lbl3)
    }
    
    func Out(){
        
         outRight(object: donebtn)
         outLeft(object: orderimageView)
         self.lbl1.transform = self.lbl1.transform.translatedBy(x: 0, y: self.view.bounds.height )
        // self.lbl2.transform = self.lbl2.transform.translatedBy(x: 0, y: self.view.bounds.height )
         self.lbl3.transform = self.lbl3.transform.translatedBy(x: 0, y: self.view.bounds.height )
         self.lbl1.alpha = 0
         //self.lbl2.alpha = 0
         self.lbl3.alpha = 0
         
     }
   
    
    func In(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
          
            self.donebtn.transform = self.donebtn.transform.translatedBy(x: -self.view.bounds.width, y: 0)
            self.orderimageView.transform = self.orderimageView.transform.translatedBy(x: self.view.bounds.width, y: 0)
            
            self.lbl1.alpha = 1
                         self.lbl1.transform = self.lbl1.transform.translatedBy(x: 0, y: -self.view.bounds.height)
                     
                         self.lbl3.alpha = 1
                         self.lbl3.transform = self.lbl3.transform.translatedBy(x: 0, y: -self.view.bounds.height)
           }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.donebtn.transform = CGAffineTransform(translationX: 10, y: 0)
                self.orderimageView.transform = CGAffineTransform(translationX: -10, y: 0)
                self.lbl1.transform = CGAffineTransform(translationX: -10, y: 0)
                            
                self.lbl3.transform = CGAffineTransform(translationX: -10, y: 0)
               
            })
            }
    }

    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }

    

}
