//
//  addressListViewController.swift
//  truemedsAnimation
//
//  Created by siddhesh redkar on 9/13/19.
//  Copyright © 2019 XYZ. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import SVGKit
import SideMenu
import FirebaseAnalytics
import Alamofire
import Flurry_iOS_SDK

class addressListViewController: UIViewController {
    
    var addressDict = NSArray()
    var cameFrom = ""
    
    @IBOutlet weak var logoo: UIButton!
       
    @IBOutlet weak var menubtno: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var questionView: UIView!
    
    @IBOutlet weak var someOneElseBtn: UIButton!
    
    @IBOutlet weak var seladdlbl: UILabel!
    
    var AddressId:CLong!
    
    var orderID:CLong! = 0
     
    
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
       
    @IBOutlet weak var paysviu: UIView!
    
    @IBOutlet weak var addmorebtno: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cameFrom = ""
        setDesign()
        
        if UserDefaults.standard.value(forKey:"OrderId") != nil
                         {
                          orderID = UserDefaults.standard.value(forKey:"OrderId") as! CLong
                         }
                         print(orderID!)
        
        visualEffectView.alpha = 0.3

        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "15") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        seladdlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        
        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        logoo.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPayment(notification:)), name: Notification.Name("PaymentSuccess"), object: nil)
        }
        
        @objc func NotificationReceivedforPayment(notification: Notification) {
            paysuccesspop()

            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                self.paysviu.removeFromSuperview()
                self.visualEffectView.removeFromSuperview()
            }
            
            
          
        
        }
    
    @IBAction func TMhome(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MedicineArr") != nil
        {
           let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
           let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
           let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
           { (action:UIAlertAction!) in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }
            alertVC.addAction(Action1);
            let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
            alertVC.addAction(Action2);
            
            self.present(alertVC, animated: true, completion: nil)
        }
        else
        {
          for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let maindict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        let dict:NSDictionary = maindict.value(forKey: "15") as! NSDictionary
        
        let text:String = dict.value(forKey: "question") as! String
        
        seladdlbl.text = text.replacingOccurrences(of: "\\n", with: "\n")
        
        alladd()
        if cameFrom == ""{
            Out()
        }
        
    }
    
    @IBAction func gotoMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
         let menu2 = SideMenuNavigationController(rootViewController: menu)
        
         menu2.statusBarEndAlpha = 0
        
         present(menu2, animated: true, completion: nil)
    }
    
    func Out(){
        
        outLeft(object: questionView)
       
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print(cameFrom)
        
        
        if cameFrom == ""{
            In()
        }else{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
               
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
             
                
                
            })
        }
        
        
    }
    
    func In(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.questionView.transform = self.questionView.transform.translatedBy(x: self.view.bounds.width, y: 0)
           
            
            
        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
              
                self.questionView.transform = CGAffineTransform(translationX: -10, y: 0)
             
                
            })
        }
    }
    
    @IBAction func SomeoneElsePress(_ sender: Any) {
        
        if UserDefaults.standard.value(forKey: "Summary") as! String == "fromSummary"{
            
            for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: summaryViewController.self) {
                                        
                                        self.navigationController!.popToViewController(controller, animated: false)
                                        break
                                    }
                                }
            
        }else{
            if UserDefaults.standard.object(forKey: "MedicineArr") != nil
                  {
                     let localizedContent = NSLocalizedString("Are you sure you want to cancel this order?", comment: "")
                     let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
                     let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
                     { (action:UIAlertAction!) in
                      for controller in self.navigationController!.viewControllers as Array {
                          if controller.isKind(of: buyMeds.self) {
                              
                              self.navigationController!.popToViewController(controller, animated: false)
                              break
                          }
                      }
                      }
                      alertVC.addAction(Action1);
                      let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
                      alertVC.addAction(Action2);
                      
                      self.present(alertVC, animated: true, completion: nil)
                  }
                  else
                  {
                    for controller in self.navigationController!.viewControllers as Array {
                          if controller.isKind(of: searchAndAddViewController.self) {
                              
                              self.navigationController!.popToViewController(controller, animated: false)
                              break
                          }
                      }
                  }
            
        }
        
        
        
     
    }
    
    
    func setDesign(){
        //btns
        backBtn(object: someOneElseBtn)
        labelFont(object: seladdlbl)
    }
    
    
    @IBAction func Addmoreadd(_ sender: Any) {
        
        Analytics.logEvent("button_click", parameters: [
        "ans":addmorebtno.titleLabel?.text,
        "full": "testing2",
        "question": seladdlbl.text
         ])
        
        let logParams = [
        "ans":addmorebtno.titleLabel?.text,
        "full": "testing2",
        "question": seladdlbl.text!
         ];

        Flurry.logEvent("button_click", withParameters: logParams);
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "deliveryPincodeViewController") as? deliveryPincodeViewController
        
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
     func alladd(){
             let parameters = ["":""]
            
            ApiManager().requestApiWithDataType(methodType:HPOST, urlString: GetAllAddress(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
                            
                if cStatus == 200 || cStatus == 201
                {
                let adddict:NSDictionary = convertStringToDictionary(json: response as! String)! as NSDictionary
                self.addressDict = adddict.value(forKey: "AddressList") as! NSArray
                if self.addressDict.count == 0
                {
                UserDefaults.standard.set("No", forKey: "isAddAvail")
                UserDefaults.standard.synchronize()
                self.navigationController?.popViewController(animated: false)
                }
                self.collectionView.reloadData()
                print(self.addressDict)
                }
                else if cStatus == 401
                {
                    let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                    Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                }
                else if cStatus == 500
                {
//                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    
                    let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                    self.addChild(popupVC)
                    popupVC.view.frame = self.view.frame
                    self.view.addSubview(popupVC.view)
                    popupVC.didMove(toParent: self)
                }
                else
                {
                    let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                }
            }
        }
    
    
    @objc func deleteAddress(sender:UIButton)
    {
        let localizedContent = NSLocalizedString("Are you sure you want to delete the address?", comment: "")
                 let alertVC = UIAlertController(title: "", message: localizedContent, preferredStyle: .alert)
                 let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
                 { (action:UIAlertAction!) in
               
                    let addarr = self.addressDict[sender.tag] as! NSDictionary
                            let addID = addarr.value(forKey: "addressId") as! CLong
                           
                           let custid:String = String(describing:UserDefaults.standard.value(forKey: "CustomerId")!)
                       
                           let parameters = ["customerId":custid,
                                             "addressId":addID
                               ] as [String : Any]
                           
                           ApiManager().requestApiWithDataType(methodType:HPOST, urlString: DeleteAddress(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
                                
                               print(response!)
                               if cStatus == 200 || cStatus == 201
                               {
                                   self.alladd()
                               }
                               else if cStatus == 401
                               {
                                   let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                   Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                               }
                               else if cStatus == 500
                               {
//                                let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                
                                   let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                                   self.addChild(popupVC)
                                   popupVC.view.frame = self.view.frame
                                   self.view.addSubview(popupVC.view)
                                   popupVC.didMove(toParent: self)
                               }
                               else
                               {
                                   
                                   let adddict:NSDictionary = convertStringToDictionary(json: response as! String)! as NSDictionary
                                   if adddict.allKeys.first as! String == "400"
                                    {
                                      Utility.showAlertWithTitle(title: "", andMessage: adddict.value(forKey: "400") as! String, onVC: self)
                                    }
                                 
                               }
                           }
                    
                    
                    
                    
                  }
        
        
        
                  alertVC.addAction(Action1);
                  let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
                  alertVC.addAction(Action2);
                  
                  self.present(alertVC, animated: true, completion: nil)
        
       
    }
    
    
    
    
    
    func saveAddressAtList(){
                           
        
        let baseUrl = saveAdd()
        print(baseUrl)
        let queryStringParam  =  ["orderId": String(describing:orderID!),"addressId": String(describing:AddressId!)
                               ] as [String : Any]
        print(queryStringParam)
               
                           var urlComponent = URLComponents(string: baseUrl)!
                           let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value as? String) }
                             urlComponent.queryItems = queryItems
                             
                           let bodyParameters = ["":""]
                           
                           let authtok:String = UserDefaults.standard.value(forKey: "access_token") as? String ?? ""
                          
                                   
                           let headers = ["Content-Type": "application/json",
                                                               "transactionId":"a",
                                                               "Authorization":authtok]
                           
                   
                             var request = URLRequest(url: urlComponent.url!)
                             request.httpMethod = "POST"
                             request.httpBody = try? JSONSerialization.data(withJSONObject: bodyParameters)
                             request.allHTTPHeaderFields = headers
                             
                             AF.request(request).responseJSON { response in
                               print(response)
                               switch response.result {
                                       case .success(let value):
                                        switch response.response?.statusCode {
                                                                              case 200,201:
                                                                                  print("")
                                       if let dict = value as? NSDictionary {
                                          print(dict)
                                        if UserDefaults.standard.value(forKey: "Summary") as! String == "fromSummary"{
                                              let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "summaryViewController") as? summaryViewController
                                                 self.navigationController?.pushViewController(vc!, animated: false)
                                        }else{
                                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "orderPaymentViewController") as? orderPaymentViewController
                                            vc?.addID = self.AddressId
                                    self.navigationController?.pushViewController(vc!, animated: false)
                                        }
                                        
                                  
                                     
                                                        
                                                    }
                                            case 401:
                                                                                                                       print("401")
                                                                                                                       let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                                                                                                                       Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                                                                                                                   case 400:
                                                                                                                       print("400")
                                                                                                                       let adddict = value as? NSDictionary
                                                                                                                       if adddict?.allKeys.first as! String == "400"
                                                                                                                       {
                                                                                                                       Utility.showAlertWithTitle(title: "", andMessage: adddict?.value(forKey: "400") as? String, onVC: self)
                                                                                                                       }
                                                                                                                   case 500:
                                                                                                                       print("500")
//                                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
//                                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                                                       let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                                                                                                                       self.addChild(popupVC)
                                                                                                                       popupVC.view.frame = self.view.frame
                                                                                                                       self.view.addSubview(popupVC.view)
                                                                                                                       popupVC.didMove(toParent: self)
                                                                                                                   default:
                                                                                                                       print("may be 500")
                                                                                                                       let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                                                                                                                       Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                                                                                                
                                                                                                                   }
                                                case .failure(let error):
                                                    print(error)
                                                }
                                     
                                 }
    }
    
    
    
   
    
    func paysuccesspop()
    {
        view.endEditing(true)
        paysviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(paysviu)
        
        let leadingConstraint = NSLayoutConstraint(item: paysviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: paysviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: paysviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: paysviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: paysviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: paysviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: paysviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    

}


extension addressListViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return addressDict.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
           return 1
       }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell3", for: indexPath) as! ImageCollectionViewCell3
        
        let addarr = addressDict[indexPath.row] as! NSDictionary
               
               if let adtype = addarr.value(forKey: "addressType") as? String
               {
                cell.addrestype.text = addarr.value(forKey: "addressType") as! String
               }
               
                //cell.addrestype.text = addarr.value(forKey: "addressType") as! String
                 let landmark:String = addarr.value(forKey: "landmark") as! String
                 let line1:String = addarr.value(forKey: "addressline1") as! String
                 let pincode:String = addarr.value(forKey: "pincode") as! String
                 let city:String = addarr.value(forKey: "cityName") as! String
                 let state:String = addarr.value(forKey: "stateName") as! String
               
               cell.fulladd.text = line1 + ", " + landmark + ", " + city + ", " + state + " -" + pincode
               
               cell.deleteAdd.tag = indexPath.row
               cell.deleteAdd.addTarget(self, action: #selector(deleteAddress), for: .touchUpInside)
               
               cell.bgview2.layer.cornerRadius = 4.0
               cell.bgview2.layer.borderWidth = 0.8
               cell.bgview2.layer.borderColor = UIColor(hexString: "#22B573").cgColor
               
//               if selectedIndex2.count == 0
//               {
//                   cell.bgview2.backgroundColor = UIColor.white
//                   cell.addrestype.textColor = UIColor(hexString: "#22B573")
//                   cell.fulladd.textColor = UIColor.black
//               }else
//               {
//                   if selectedIndex2.contains(indexPath.row)
//                   {
//                       cell.bgview2.backgroundColor = UIColor(hexString: "#22B573")
//                       cell.addrestype.textColor = UIColor.white
//                       cell.fulladd.textColor = UIColor.white
//                   }else
//                   {
//                       cell.bgview2.backgroundColor = UIColor.white
//                       cell.addrestype.textColor = UIColor(hexString: "#22B573")
//                       cell.fulladd.textColor = UIColor.black
//                   }
//               }
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
       {
           
           return CGSize(width: collectionView.bounds.size.width * 0.6, height: 100)
       }
     
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    let tempadict:NSDictionary = addressDict[indexPath.row] as! NSDictionary
    AddressId = tempadict.value(forKey: "addressId") as! CLong
     saveAddressAtList()
   
        
        
    }
    
}
