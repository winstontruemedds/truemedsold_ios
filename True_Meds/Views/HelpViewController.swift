//
//  HelpViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class HelpViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    let reachability = Reachability()!
    
    
    
    @IBOutlet weak var contactLbl: UILabel!
    
    @IBOutlet weak var backbtno: UIButton!
    
    var helpcatlist = NSArray()
    
    @IBOutlet weak var helptopictable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        getHelp()
        uderlineLbl(object: contactLbl, UderlineString: "9987410969")
        
        let tap2 : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(caller))
        self.contactLbl.isUserInteractionEnabled = true
        self.contactLbl.addGestureRecognizer(tap2)
    }
     
    
    
  @objc func caller(){
        if let url = URL(string: "tel://\("+919987410969")"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            Utility.showAlertWithTitle(title: "", andMessage: "Incorrect Mobile Number", onVC: self)
        }
    }
    
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helpcatlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpTableViewCell") as! HelpTableViewCell
        let finhelpcat:NSDictionary = helpcatlist[indexPath.row] as! NSDictionary
        cell.htopiclbl.text = finhelpcat.value(forKey: "categoryName") as! String
        
        let svgimg2: SVGKImage = SVGKImage(named: "green_arrow")
        cell.grarr.image = svgimg2.uiImage
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let finhelpcat:NSDictionary = helpcatlist[indexPath.row] as! NSDictionary
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpDetailsViewController") as? HelpDetailsViewController
        vc!.hid = String(describing:finhelpcat.value(forKey: "categoryId")!)
        vc!.detailstr = finhelpcat.value(forKey: "categoryName") as! String
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    

    @IBAction func backto(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    

    func getHelp()
    {
        if Connectivity.isConnectedToInternet {
        
        let url = GetHelpCat()
        
        let serviceurl = URL(string: url)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: serviceurl!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    let dict:NSDictionary = json as NSDictionary
                    self.helpcatlist = dict.value(forKey: "Category") as! NSArray
                    
                    DispatchQueue.main.async {
                    self.helptopictable.reloadData()
                     
                    self.helptopictable.tableFooterView = UIView()
                        
                    }
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
