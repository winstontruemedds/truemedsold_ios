//
//  WalkthroughViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import SDWebImage
import KDCircularProgress
import Reachability
import Alamofire

class WalkthroughViewController: UIViewController,FSPagerViewDataSource,FSPagerViewDelegate {

    let reachability = Reachability()!
    
    @IBOutlet weak var nxtbtn: UIButton!
    
    @IBOutlet weak var progress: KDCircularProgress!
    
    var arrImages : [String] = []
    
    var custdict = NSDictionary()
    
    var quesdict = NSDictionary()
    
    var mastdicts = NSDictionary()
    
    var langDict = NSDictionary()
    
    var imgsarr = NSArray()
    
    @IBOutlet weak var arrbtno: UIButton!
    
    var indexx:Int = 0
    
    var agegroupID:CLong!
    
    var langID:CLong!
    
    @IBOutlet weak var headerlbl: UILabel!
    
    @IBOutlet weak var descrlbl: UILabel!
    
    @IBOutlet weak var backbtno: UIButton!
    
    
    fileprivate let imageNames = ["offers","offers","offers","offers","offers","offers","offers"]
    fileprivate let transformerNames = ["cross fading", "zoom out", "depth", "linear", "overlap", "ferris wheel", "inverted ferris wheel", "coverflow", "cubic"]
    fileprivate let transformerTypes: [FSPagerViewTransformerType] = [.crossFading,
                                                                      .zoomOut,
                                                                      .depth,
                                                                      .linear,
                                                                      .overlap,
                                                                      .ferrisWheel,
                                                                      .invertedFerrisWheel,
                                                                      .coverFlow,
                                                                      .cubic]
    
    fileprivate var typeIndex = 0 {
        didSet {
            let type = self.transformerTypes[0]
            self.pagerView.transformer = FSPagerViewTransformer(type:type)
            switch type {
            case .crossFading, .zoomOut, .depth:
                self.pagerView.itemSize = FSPagerView.automaticSize
                self.pagerView.decelerationDistance = 1
            case .linear,.overlap:
                let transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.pagerView.itemSize = FSPagerView.automaticSize
                
                self.pagerView.decelerationDistance = FSPagerView.automaticDistance
                
            case .ferrisWheel, .invertedFerrisWheel:
                self.pagerView.itemSize = CGSize(width: 180, height: 140)
                self.pagerView.decelerationDistance = FSPagerView.automaticDistance
            case .coverFlow:
                self.pagerView.itemSize = CGSize(width: 220, height: 170)
                self.pagerView.decelerationDistance = FSPagerView.automaticDistance
            case .cubic:
                let transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                self.pagerView.itemSize = self.pagerView.frame.size.applying(transform)
                self.pagerView.decelerationDistance = 1
            }
        }
    }
    
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.typeIndex = 0
        }
    }
    
    
    
    
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let index = self.typeIndex
        self.typeIndex = index
        
    }
    
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return imgsarr.count
    }
    
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let finimgdict:NSDictionary = imgsarr[index] as! NSDictionary
        let imgstr:String = finimgdict.value(forKey: "image") as! String
        let imgurl:URL = URL(string: imgstr)!
        let data = try? Data(contentsOf: imgurl)
        cell.imageView?.image = UIImage(data: data!)
        
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.clipsToBounds = true
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let svgimg: SVGKImage = SVGKImage(named: "arrback")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        nxtbtn.layer.cornerRadius = 4.0
        
        let appintro:NSDictionary = langDict.value(forKey: "appIntro") as! NSDictionary
        imgsarr = appintro.value(forKey: "screen") as! NSArray
        
        self.pagerView.reloadData()
        let percent:Double = Double(360/self.imgsarr.count)
        
        self.progress.animate(toAngle: percent, duration: 1.0, completion: nil)
        
        let finimgdict:NSDictionary = imgsarr[indexx] as! NSDictionary
        headerlbl.text = finimgdict.value(forKey: "header") as? String
        descrlbl.text = finimgdict.value(forKey: "description") as? String
    
        self.navigationController?.navigationBar.isHidden = true
        

        pagerView.isUserInteractionEnabled = false
        
        if imgsarr.count == 1
        {
        nxtbtn.setTitle(NSLocalizedString("Get Started", comment: ""), for: .normal)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if imgsarr.count == 1
        {
        nxtbtn.setTitle(NSLocalizedString("Get Started", comment: ""), for: .normal)
        }
    }
    
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    
    @IBAction func gotoHome(_ sender: Any) {
       
       let currAng:Double = progress.angle
       if currAng == 360
       {
         let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "buyMeds") as? buyMeds
          let questions:NSDictionary = langDict.value(forKey: "questions") as! NSDictionary
          //vc!.mastdict = questions
          vc!.imgsarr = imgsarr
          vc!.agegroupID = agegroupID
          vc!.langID = langID
         UserDefaults.standard.set(langID, forKey: "languageId")
        UserDefaults.standard.set(agegroupID, forKey: "ageGroupId")
        UserDefaults.standard.synchronize()
        
          UserDefaults.standard.set(questions, forKey: "quesdict")
          UserDefaults.standard.synchronize()
          self.navigationController?.pushViewController(vc!, animated: true)
       }
       else
       {
        let percent:Double = Double(360/imgsarr.count)
        let addAng:Double = progress.angle + percent
        indexx = indexx + 1
        pagerView.scrollToItem(at: indexx, animated: true)
        progress.animate(toAngle: addAng, duration: 1.0, completion: nil)
        let finimgdict:NSDictionary = imgsarr[indexx] as! NSDictionary
        headerlbl.text = finimgdict.value(forKey: "header") as? String
        descrlbl.text = finimgdict.value(forKey: "description") as? String
        
        if indexx == imgsarr.count - 1
        {
         nxtbtn.setTitle(NSLocalizedString("Get Started", comment: ""), for: .normal)
        }
        
        }
    
    }
    
   
    
    
    
    
    
    
    
    @IBAction func backto(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
    
    
    
}
