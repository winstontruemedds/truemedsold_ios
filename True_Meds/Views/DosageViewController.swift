//
//  DosageViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class DosageViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate {

    
    @IBOutlet weak var backbtno: UIButton!
    
    var dosagearr = NSArray()
    
    @IBOutlet weak var dosagetable: UITableView!
    
    var findosage = NSDictionary()
    
    var odhelpdict5 = NSDictionary()
    
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        GetDosageDetails()
        
    }

    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dosagearr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DosageTableViewCell") as! DosageTableViewCell
    
        findosage = dosagearr[indexPath.row] as! NSDictionary
        cell.medcinename.text = findosage.value(forKey: "medicineName") as! String
        cell.packetsize.text = findosage.value(forKey: "pack") as? String
        
        if let count1 = findosage.value(forKey: "isMorning") as? String
        {
            if count1 == "0"
            {
                cell.morview.isHidden = true
            }else{
                cell.morview.isHidden = false
                cell.morview.layer.cornerRadius = 13.0
                cell.morcount.text = String(describing: findosage.value(forKey: "isMorning")!)
                cell.morchrono.text = findosage.value(forKey: "chronology") as! String
                cell.morfreq.text = findosage.value(forKey: "frequency") as! String
                let anSVGImage: SVGKImage = SVGKImage(named: "dawn")
                cell.morimg.image = anSVGImage.uiImage
            }
        }
        
        if let count2 = findosage.value(forKey: "isAfternoon") as? String
        {
            if count2 == "0"
            {
                cell.aftview.isHidden = true
            }else{
                cell.aftview.isHidden = false
                cell.aftview.layer.cornerRadius = 13.0
                cell.aftcount.text = String(describing: findosage.value(forKey: "isAfternoon")!)
                cell.aftchrono.text = findosage.value(forKey: "chronology") as! String
                cell.aftfreq.text = findosage.value(forKey: "frequency") as! String
                let anSVGImage: SVGKImage = SVGKImage(named: "sun")
                cell.aftimg.image = anSVGImage.uiImage
            }
        }
        
        if let count3 = findosage.value(forKey: "isEvening") as? String
        {
            if count3 == "0"
            {
                cell.eveview.isHidden = true
            }else{
                cell.eveview.isHidden = false
                cell.eveview.layer.cornerRadius = 13.0
                cell.evecount.text = String(describing: findosage.value(forKey: "isEvening")!)
                cell.evechrono.text = findosage.value(forKey: "chronology") as! String
                cell.evefreq.text = findosage.value(forKey: "frequency") as! String
                let anSVGImage: SVGKImage = SVGKImage(named: "sunset")
                cell.eveimg.image = anSVGImage.uiImage
            }
        }
        
        if let count4 = findosage.value(forKey: "isNight") as? String
        {
            if count4 == "0"
            {
                cell.nigview.isHidden = true
            }else{
                cell.nigview.isHidden = false
                cell.nigview.layer.cornerRadius = 13.0
                cell.nigcount.text = String(describing: findosage.value(forKey: "isNight")!)
                cell.nigchrono.text = findosage.value(forKey: "chronology") as! String
                cell.nigfreq.text = findosage.value(forKey: "frequency") as! String
                let anSVGImage: SVGKImage = SVGKImage(named: "moon")
                cell.nigimg.image = anSVGImage.uiImage
            }
        }
        
        if let count5 = findosage.value(forKey: "isSos") as? String
        {
            if count5 == "0"
            {
                cell.onlview.isHidden = true
            }
            else{
                cell.onlview.isHidden = false
                cell.onlview.layer.cornerRadius = 13.0
                cell.onlcount.text = String(describing: findosage.value(forKey: "isSos")!)
                cell.onlchrono.text = findosage.value(forKey: "chronology") as! String
                cell.onlfreq.text = findosage.value(forKey: "frequency") as! String
                let anSVGImage: SVGKImage = SVGKImage(named: "Clock")
                cell.onlimg.image = anSVGImage.uiImage
            }
        }
        
        if cell.morview.isHidden == true && cell.aftview.isHidden == true && cell.eveview.isHidden == true && cell.nigview.isHidden == true && cell.onlview.isHidden == true
        {
            cell.scroheight.constant = 0
        }
        else
        {
            cell.scroheight.constant = 135
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
   
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
   
    
    
    func GetDosageDetails()
    {
        if Connectivity.isConnectedToInternet {
        
        let orderID:CLong = odhelpdict5.value(forKey: "orderId") as! CLong
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["orderId":String(describing: orderID)]
        
        let url =  getDosageDetails()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    DispatchQueue.main.async {
                        
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    let dosdict:NSDictionary = json as NSDictionary
                    self.dosagearr = dosdict.value(forKey: "Medicines") as! NSArray
                    self.dosagetable.reloadData()
                    if self.dosagearr.count == 0
                    {
                     self.dosagetable.isHidden = true
                    }
                    else
                    {
                     self.dosagetable.isHidden = false
                    }
                        
                    }
                    else
                    {
                    //Utility.showAlertWithFading(title: "", andMessage: "Dosage Details Service Error", onVC: self)
                    let localizedContent = NSLocalizedString("No Dosage Found!", comment: "")
                    Utility.showAlertWithAction(title: "", andMessage: localizedContent, onVC: self)
                    }
                    }
                        
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
    
}
