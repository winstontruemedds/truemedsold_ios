//
//  SubmitTicketViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class SubmitTicketViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,UITextViewDelegate{

    let reachability = Reachability()!
    
    @IBOutlet weak var backbtno: UIButton!
    
    var imgarrs = [UIImage]()
    
    var imgbase64arr = [String]()
    
    @IBOutlet weak var ticketcollection: UICollectionView!
    
    @IBOutlet weak var collectionwidthcons: NSLayoutConstraint!
    
    @IBOutlet weak var emailtf: UITextField!
    
    @IBOutlet weak var mobtf: UITextField!
    
    @IBOutlet weak var desctv: UITextView!
    
    var IssueId:CLong!
    
    var cnctype:String!
    
    var emailtxt:String!
    
    var mobiletxt:String!
    
    @IBOutlet weak var typelbl: UILabel!
    
    @IBOutlet weak var toolbr: UIToolbar!
    
    @IBOutlet weak var issuetitle: UILabel!
    
    @IBOutlet weak var issuequeslbl: UILabel!
    
    var detailstr3:String!
    
    var quesstr:String!
    
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    @IBOutlet weak var ssubmitbtno: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ssubmitbtno.layer.cornerRadius = 4.0
        
        issuetitle.text = detailstr3
        
        issuequeslbl.text = quesstr
        
        desctv.delegate = self
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        if cnctype == "email"
        {
            typelbl.text = NSLocalizedString("Email", comment: "")
            mobtf.isHidden = true
            //emailtf.placeholder = NSLocalizedString("Email", comment: "")
        }
        else
        {
            typelbl.text = NSLocalizedString("Mobile number", comment: "")
            emailtf.isHidden = true
            mobtf.inputAccessoryView = toolbr
            //mobtf.placeholder = NSLocalizedString("Mobile number", comment: "")
            mobtf.isUserInteractionEnabled = false
            if UserDefaults.standard.object(forKey: "mobileNo") != nil
            {
            mobtf.text = String(describing: UserDefaults.standard.value(forKey: "mobileNo")!)
            }
        }
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    
    @IBAction func doneact(_ sender: Any) {
        view.endEditing(true)
        
        if Utility.isValidMobNo(mobtf.text!) == true
        {
         
        }
        else
        {
          let localizedContent = NSLocalizedString("Please enter a valid mobile number", comment: "")
          Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if Utility.isValidEmail(emailtf.text!) == true
        {
            
        }else
        {
         emailtf.text = ""
         let localizedContent = NSLocalizedString("Please enter a valid email address", comment: "")
         Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            view.endEditing(true)
            return false
        }
        else
        {
            return true
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imgarrs.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "submit1CollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        
        cell.ticketimg.image = imgarrs[(indexPath.item)]
        
        cell.ticketimg.layer.cornerRadius = 4.0
        cell.ticketimg.clipsToBounds = true
        
        cell.subvisblur.layer.cornerRadius = 4.0
        cell.subvisblur.clipsToBounds = true
        
        cell.delticketimg.tag = indexPath.item
        cell.delticketimg.addTarget(self, action: #selector(delimg), for: .touchUpInside)
        
        let svgimg: SVGKImage = SVGKImage(named: "circle_cross")
        cell.delticketimg.setImage(svgimg.uiImage, for: .normal)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 120, height: 120)
    }
    
    
    @IBAction func uploadbtn(_ sender: Any) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title:NSLocalizedString("Camera", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                print("error")
            }
            
        })
        
        camera.setValue(UIColor.black, forKey: "titleTextColor")
        
        actionSheet.addAction(camera);
        
        let gallery = UIAlertAction(title:NSLocalizedString("Gallery", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                print("error")
            }
        })
        
        gallery.setValue(UIColor.black, forKey: "titleTextColor")
        
        actionSheet.addAction(gallery);
        
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: {
            (alert: UIAlertAction!) in
            
            self.dismiss(animated: true, completion: nil);
            
        })
        
        
        actionSheet.addAction(cancel);
        
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0 , height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        present(actionSheet, animated: true, completion: nil);
    }
    
    

    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let images = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        {
           imgarrs.append(images)
            
            collectionwidthcons.constant = collectionwidthcons.constant + 130
            
            ticketcollection.reloadData()
            let imageData:NSData = images.jpegData(compressionQuality: 0.4) as! NSData
            let base64:String = imageData.base64EncodedString(options: .lineLength64Characters)
            imgbase64arr.append(base64)
            
            dismiss(animated: true, completion: nil)
        }
        
        
        
    }
    
    
    
    @objc func delimg(sender:UIButton)
    {
        let localizedContent = NSLocalizedString("Are you sure you want to delete the images?", comment: "")
        let alertVC = UIAlertController(title: title, message: localizedContent, preferredStyle: .alert)
        let Action1 = UIAlertAction(title:NSLocalizedString("Yes", comment: ""), style: .default)
        { (action:UIAlertAction!) in
            self.imgarrs.remove(at: sender.tag)
            self.imgbase64arr.remove(at: sender.tag)
            self.collectionwidthcons.constant = self.collectionwidthcons.constant - 130
            self.ticketcollection.reloadData()
        }
        alertVC.addAction(Action1);
        let Action2 = UIAlertAction(title:NSLocalizedString("No", comment: ""), style: .default, handler: nil);
        alertVC.addAction(Action2);
        self.present(alertVC, animated: true, completion: nil);

    }
    
    
    @IBAction func backto(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
   
    @IBAction func submiticketbtna(_ sender: Any) {
        
        if cnctype == "email"
        {
            if Utility.isValidEmail(emailtf.text!) == true
            {
                RaiseTicket()
            }else
            {
                emailtf.text = ""
                let localizedContent = NSLocalizedString("Please enter a valid email address", comment: "")
                Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
            }
        }
        else
        {
          RaiseTicket()
        }
    
    }
    
    
    
    
    func RaiseTicket()
    {
        if Connectivity.isConnectedToInternet {
        
        actInd.startAnimating()
        
        //let custid:String = String(describing:UserDefaults.standard.value(forKey: "CustomerId")!)
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        var para2 = ["":""
                ]
       
    
        let url =  RaiseOrderTicket()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        if cnctype == "email"
        {
         emailtxt = emailtf.text!
        }
        else
        {
         mobiletxt = mobtf.text!
        }
        
        
        let json = [
            "mobile":mobtf.text!,
            "email":emailtf.text!,
            "ticketType":CLong(1),
            "orderId":"",
            "issues":[
            [
            "issueId":IssueId,
            "medicineId":"",
            "description":desctv.text!,
            "images":imgbase64arr
            ]
            ]
            ] as [String : Any]
       
        
    
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
            
        request.httpBody = jsonData
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                     DispatchQueue.main.async {
                        
                     print(json)
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {

                    self.actInd.stopAnimating()
                    
                    //NotificationCenter.default.post(name: Notification.Name("Issues"), object: nil)
                        
                        self.pushThankYou()
                        
                  
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                        
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
        
    }
    
    
    
    func pushThankYou(){
       
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankTicketViewController") as? thankTicketViewController
        
            self.navigationController?.pushViewController(vc!, animated: false)
    }

    
    
    

}




// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}






