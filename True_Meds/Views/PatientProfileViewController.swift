//
//  PatientProfileViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class PatientProfileViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,getReload {
    
    let reachability = Reachability()!
    
    @IBOutlet weak var backbtno: UIButton!
    
    var apatarr = NSArray()

    @IBOutlet weak var allpatientstable: UITableView!
    
    func reloadPatients(temp:String) {
        allpatients()
    }
    
    
    @IBOutlet weak var addbtno: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Patient Profile"
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "Vector")
        addbtno.setImage(svgimg2.uiImage, for: .normal)
        
         allpatients()
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return apatarr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientProfileTableViewCell") as! PatientProfileTableViewCell
        cell.patpropic.layer.cornerRadius = 35.0
        cell.patpropic.clipsToBounds = true
        
        let svgimg: SVGKImage = SVGKImage(named: "green_arrow")
        cell.greenarrimg.image = svgimg.uiImage
        
        let finpatdict:NSDictionary = apatarr[indexPath.row] as! NSDictionary
        
        if let name = finpatdict.value(forKey: "patientName") as? String
        {
        cell.apnamelbl.text = name
        }
        
        if let age = finpatdict.value(forKey:"age") as? String
        {
         cell.agelbl.text = String(describing:age) + ", "
        }
        
        if let gender = finpatdict.value(forKey:"genderName") as? String
        {
        cell.genderlbl.text = gender
        }
        
        let relname:String = finpatdict.value(forKey:"relationName") as! String
        cell.relationname.text = relname
        
        if relname == "GRANDPARENT" && cell.genderlbl.text == "MALE"
        {
         cell.patpropic.image = UIImage(named: "wgfather")
        }else if relname == "GRANDPARENT" && cell.genderlbl.text == "FEMALE"
        {
            cell.patpropic.image = UIImage(named: "wgmother")
        }else if relname == "PARENT" && cell.genderlbl.text == "MALE"
        {
            cell.patpropic.image = UIImage(named: "wfather")
        }else if relname == "PARENT" && cell.genderlbl.text == "FEMALE"
        {
            cell.patpropic.image = UIImage(named: "wmother")
        }else if relname == "SIBLING" && cell.genderlbl.text == "MALE"
        {
            cell.patpropic.image = UIImage(named: "wfather")
        }else if relname == "SIBLING" && cell.genderlbl.text == "FEMALE"
        {
            cell.patpropic.image = UIImage(named: "wmother")
        }else if relname == "SPOUSE" && cell.genderlbl.text == "MALE"
        {
            cell.patpropic.image = UIImage(named: "wfather")
        }else if relname == "SPOUSE" && cell.genderlbl.text == "FEMALE"
        {
            cell.patpropic.image = UIImage(named: "wmother")
        }else if relname == "CHILD" && cell.genderlbl.text == "MALE"
        {
            cell.patpropic.image = UIImage(named: "wbrother")
        }else if relname == "CHILD" && cell.genderlbl.text == "FEMALE"
        {
            cell.patpropic.image = UIImage(named: "wsister")
        }else if relname == "GRANDCHILD" && cell.genderlbl.text == "MALE"
        {
            cell.patpropic.image = UIImage(named: "wbrother")
        }else if relname == "GRANDCHILD" && cell.genderlbl.text == "FEMALE"
        {
            cell.patpropic.image = UIImage(named: "wsister")
        }
        else if relname == "OTHER" && cell.genderlbl.text == "MALE"
        {
            cell.patpropic.image = UIImage(named: "wfather")
        }else if relname == "OTHER" && cell.genderlbl.text == "FEMALE"
        {
            cell.patpropic.image = UIImage(named: "wmother")
        }
            
            
        else
        {
        cell.patpropic.image = UIImage(named: "Default profile image")
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PatientDetailViewController") as? PatientDetailViewController
        let finpatdict:NSDictionary = apatarr[indexPath.row] as! NSDictionary
        vc!.patId = finpatdict.value(forKey: "patientId") as! CLong
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addpatientbtn(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddPatientViewController") as? AddPatientViewController
        vc?.reloadelegate = self
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    func allpatients()
    {
        if Connectivity.isConnectedToInternet {
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
    
        let para2 = ["showMyself":"true"]
        
        let url =  GetAllPatients()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        //request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
               
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                        
                        print(json)
                        
                        let httpResponse = response as? HTTPURLResponse
                        
                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                        {
                            
                            let apdict:NSDictionary = json as NSDictionary
                            self.apatarr = apdict.value(forKey: "PatientList") as! NSArray
                            DispatchQueue.main.async {
                                self.allpatientstable.reloadData()
                                self.allpatientstable.tableFooterView = UIView()
                            }
                            
                            if self.apatarr.count == 0
                            {
                                self.allpatientstable.isHidden = true
                            }else
                            {
                                self.allpatientstable.isHidden = false
                            }
                            
                        }
                        else
                        {
                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
        
    }

}
