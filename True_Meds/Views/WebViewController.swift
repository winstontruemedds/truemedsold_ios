//
//  WebViewController.swift
//  True_Meds
//


import UIKit
import SVGKit

class WebViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var webviu: UIWebView!
    
    var articlesdict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        let myURL = URL(string:articlesdict.value(forKey: "url") as! String)
        let myRequest = URLRequest(url: myURL!)
        webviu.loadRequest(myRequest)
    }
    

    @IBAction func backto(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    

}
