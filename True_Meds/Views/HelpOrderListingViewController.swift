//
//  HelpOrderListingViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class HelpOrderListingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    let reachability = Reachability()!
    
    @IBOutlet weak var helporderlistableview: UITableView!
    
    @IBOutlet weak var backbtno: UIButton!
    
    var PastODarr = NSArray()
    
    var CurrentODarr = NSArray()
    
    var seccount = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()

        allorders()
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return seccount
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count:Int?
        
        if tableView == helporderlistableview
        {
            if section == 0
            {
                count = CurrentODarr.count
            }
            
            if section == 1
            {
                count = PastODarr.count
            }
            
        }
    
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : HelpOrderListTableViewCell?
        
        if tableView == helporderlistableview
        {
            if indexPath.section == 0
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "HelpOrderListTableViewCell") as! HelpOrderListTableViewCell
                let finpastdict:NSDictionary = CurrentODarr[indexPath.row] as! NSDictionary
                cell?.orderId.text = String(describing:finpastdict.value(forKey: "orderId")!)
                
                if let totalsavings = finpastdict.value(forKey: "totalSaving") as? Double
                {
                    cell?.savingslbl.text = "₹ " + String(format: "%.2f", totalsavings)
                }
                
                //cell?.savingslbl.text = "₹ " + String(describing:finpastdict.value(forKey: "totalSaving")!)
                
//                if let delieveredon = finpastdict.value(forKey: "deliveredDate") as? String
//                {
//                    cell?.presdeli.text = delieveredon
//                }
                
                 cell?.presdeli.text = finpastdict.value(forKey: "orderStatus") as? String
                
                let mydate:CLong = finpastdict.value(forKey: "orderDate") as! CLong
                let mydates:CLong = mydate/1000
                let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
                
                let dayTimePeriodFormatter = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "dd MMMM yyyy hh:mm"
                
                let dateString = dayTimePeriodFormatter.string(from: date as Date)
                cell?.odatelbl.text = dateString
                
                cell?.cvalidlbl.text = NSLocalizedString("Order Status -", comment: "")
                cell?.bgview3.layer.cornerRadius = 4.0
                cell?.bgview3.layer.borderWidth = 1.0
                cell?.bgview3.layer.borderColor = UIColor(hexString: "#EDEDED", alpha: 1.0).cgColor
                cell?.bgview3.clipsToBounds = true
                
                let statusId:CLong = finpastdict.value(forKey: "statusId") as! CLong
                if statusId == 81
                {
                cell?.erroriden.isHidden = false
                }
                else
                {
                cell?.erroriden.isHidden = true
                }
                if statusId == 39 || statusId == 1 || statusId == 81 || statusId == 57 || statusId == 2
                {
                    cell?.savingslbl.isHidden = true
                    cell?.totsav2.isHidden = true
                }
                else
                {
                    cell?.savingslbl.isHidden = false
                    cell?.totsav2.isHidden = false
                }
                
                if statusId == 142
                {
                    cell?.totsav2.text = NSLocalizedString("Estimated Savings :", comment: "")
                }
                else
                {
                    cell?.totsav2.text = NSLocalizedString("Total Savings :", comment: "")
                }
                
                
            }else
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "HelpOrderListTableViewCell") as! HelpOrderListTableViewCell
                let fincurrdict:NSDictionary = PastODarr[indexPath.row] as! NSDictionary
                cell?.orderId.text = String(describing:fincurrdict.value(forKey: "orderId")!)
                
                if let totalsavings = fincurrdict.value(forKey: "totalSaving") as? Double
                {
                    cell?.savingslbl.text = "₹ " + String(format: "%.2f", totalsavings)
                }
                
                //cell?.savingslbl.text = "₹ " + String(describing: fincurrdict.value(forKey: "totalSaving")!)
                if let delieveredon = fincurrdict.value(forKey: "deliveredDate") as? String
                {
                    cell?.odstatlbl.text = NSLocalizedString("Delivered On -", comment: "")
                    cell?.odatelbl.text = delieveredon
                }
                else
                {
                    let mydate:CLong = fincurrdict.value(forKey: "orderDate") as! CLong
                    let mydates:CLong = mydate/1000
                    let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
                    
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMMM yyyy hh:mm"
                    
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
                    cell?.odstatlbl.text = NSLocalizedString("Ordered On -", comment: "")
                    cell?.odatelbl.text = dateString
                }
                
                let statusId:CLong = fincurrdict.value(forKey: "statusId") as! CLong
                
                if statusId == 39 || statusId == 1 || statusId == 81 || statusId == 57 || statusId == 2
                {
                    cell?.savingslbl.isHidden = true
                    cell?.totsav2.isHidden = true
                }
                else
                {
                    cell?.savingslbl.isHidden = false
                    cell?.totsav2.isHidden = false
                }
                
                if statusId == 142
                {
                    cell?.totsav2.text = NSLocalizedString("Estimated Total Savings :", comment: "")
                }
                else
                {
                    cell?.totsav2.text = NSLocalizedString("Total Savings :", comment: "")
                }
                
                cell?.bgview3.layer.cornerRadius = 4.0
                cell?.bgview3.layer.borderWidth = 1.0
                cell?.bgview3.layer.borderColor = UIColor(hexString: "#EDEDED", alpha: 1.0).cgColor
                cell?.bgview3.clipsToBounds = true
                cell?.cvalidlbl.text = NSLocalizedString("Order Status -", comment: "")
                cell?.presdeli.text = fincurrdict.value(forKey: "orderStatus") as? String
            }
            
        
        }
        
       
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var headerView = UIView()
        
        if tableView == helporderlistableview
        {
            if section == 0
            {
                //let headerView = UIView()
                headerView.backgroundColor = UIColor.white
                
                let headerLabel = UILabel(frame: CGRect(x: 20, y: 10, width:
                    tableView.bounds.size.width, height: tableView.bounds.size.height))
                
                headerLabel.font = UIFont(name: "OpenSans-bold", size: 21)
                
                headerLabel.textColor = UIColor.black
                
                headerLabel.text = NSLocalizedString("Current Orders", comment: "")
                headerLabel.sizeToFit()
                headerView.addSubview(headerLabel)
                // return headerView
            }
            
            if section == 1 {
                //let headerView = UIView()
                headerView.backgroundColor = UIColor.white
                
                let headerLabel = UILabel(frame: CGRect(x: 20, y: 10, width:
                    tableView.bounds.size.width, height: tableView.bounds.size.height))
                
                headerLabel.font = UIFont(name: "OpenSans-bold", size: 21)
                headerLabel.textColor = UIColor.black
                
                headerLabel.text = NSLocalizedString("Past Orders", comment: "")
                headerLabel.sizeToFit()
                headerView.addSubview(headerLabel)
                //return headerView
                
            }
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectOrderTicketViewController") as? SelectOrderTicketViewController
            if indexPath.section == 0
            {
            let finpastdict:NSDictionary = CurrentODarr[indexPath.row] as! NSDictionary
            let statusId:CLong = finpastdict.value(forKey: "statusId") as! CLong
            if statusId == 1 || statusId == 39 || statusId == 2
            {
            let localizedContent = NSLocalizedString("This order is not yet processed. Please check again in some time.", comment: "")
            Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
            }
            else if statusId == 81
            {
             let localizedContent = NSLocalizedString("This order has errors. Kindly resolve them before raising a ticket.", comment: "")
             Utility.showAlertWithFading(title: "", andMessage: localizedContent, onVC: self)
            }
            else if statusId == 57
            {
                
            }
            else{
             vc!.selecteddict = CurrentODarr[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(vc!, animated: true)
            }
            }else{
                
            let finpastdict:NSDictionary = PastODarr[indexPath.row] as! NSDictionary
            let statusId:CLong = finpastdict.value(forKey: "statusId") as! CLong
            if statusId == 57
            {
                
            }
            else
             {
             vc!.selecteddict = PastODarr[indexPath.row] as! NSDictionary
             self.navigationController?.pushViewController(vc!, animated: true)
             }
             }
    
    }
   

    @IBAction func backto(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    func allorders()
    {
        if Connectivity.isConnectedToInternet {
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let para2 = ["":""]
        
        let url =  GetAllOrders()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        //request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                        let httpResponse = response as? HTTPURLResponse
                        
                        print(json)
                        
                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                        {
                            let OrderDict:NSDictionary = json as NSDictionary
                            self.PastODarr = OrderDict.value(forKey: "pastOrder") as! NSArray
                            self.CurrentODarr = OrderDict.value(forKey: "currentOrder") as! NSArray
                            
                            if self.PastODarr.count == 0
                            {
                                self.seccount = 1
                            }
                            else
                            {
                                self.seccount = 2
                            }
                            
                            self.helporderlistableview.reloadData()
                            
                        }
                        else
                        {
                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
}
