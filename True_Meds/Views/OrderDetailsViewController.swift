//
//  OrderDetailsViewController.swift
//  True_Meds
//


protocol  getModelIDelegate  {
    func getModelIDFromAnotherVC(temp: Int)
}


import UIKit
import SVGKit
import PaymentSDK
import Reachability
import Alamofire

class OrderDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,PGTransactionDelegate {
    
    
    let reachability = Reachability()!
    
    var custDele : getModelIDelegate?
    
    @IBOutlet weak var mbag: UIImageView!
    
    @IBOutlet weak var saveviu: UIView!
    
    @IBOutlet weak var heightcons: NSLayoutConstraint!
    
    @IBOutlet weak var billtblview: UITableView!
    
    @IBOutlet weak var backbtno: UIButton!
    
    var ordertype:String!
    
    @IBOutlet weak var orderheightcons: NSLayoutConstraint!
    
    @IBOutlet weak var reorderbtno: UIButton!
    
    var show:String!
    
    var currentquan = 1
    
    var billmedsarr = NSArray()
    
    @IBOutlet weak var totallbl: UILabel!
    
    @IBOutlet weak var deliveryfee: UILabel!
    
    @IBOutlet weak var cashandling: UILabel!
    
    @IBOutlet weak var discountlbl: UILabel!
    
    @IBOutlet weak var truemedscash: UILabel!
    
    @IBOutlet weak var truemedstotal: UILabel!
    
    var odhelpdict2 = NSDictionary()
    
    var dictionaryA = ["productCode":String(),"quantity":String()]
    
    var ArrayB = [[String:String]]()
    
    @IBOutlet weak var savelbl1: UILabel!
    
    @IBOutlet weak var savelbl2: UILabel!
    
    @IBOutlet weak var makepayo: UIButton!
    
    var checksumstr:String!
    
    var TxnAmt:String!
    
    var TxnAmt2:Int!
    
    var txnController = PGTransactionViewController()

    var serv = PGServerEnvironment()

    var bankname:String!
    var banktxnid:String!
    var checksumhash:String!
    var currency:String!
    var gatewayname:String!
    var mid:String!
    var orderid:String!
    var paymentmode:String!
    var respcode:String!
    var respmsg:String!
    var status:String!
    var txnamount:String!
    var txndate:String!
    var txnid:String!
    
    @IBOutlet weak var Ttllbl: UILabel!
    
    @IBOutlet weak var Ttldislbl: UILabel!
    
    @IBOutlet weak var Tmttlbl: UILabel!
    
    var savedstr:String!
    
    var savedstr2:String!
    
    var savedstr3:String!
    
    var savedstr4:String!
    
    var orderID:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let statId:CLong = odhelpdict2.value(forKey: "statusId") as! CLong
        
        if statId == 142
        {
        Ttllbl.text = NSLocalizedString("Estimated Total", comment: "")
        Ttldislbl.text = NSLocalizedString("Estimated Total discount", comment: "")
        Tmttlbl.text = NSLocalizedString("Estimated Truemeds total", comment: "")
        savedstr = "You will save approximately ₹ "
        savedstr2 = " on this order"
        savedstr3 = "against your original prescription cost of ₹ "
        savedstr4 = "demotext"
        }
        else
        {
        Ttllbl.text = NSLocalizedString("Total", comment: "")
        Ttldislbl.text = NSLocalizedString("Total discount", comment: "")
        Tmttlbl.text = NSLocalizedString("Truemeds total", comment: "")
        savedstr = "You saved ₹ "
        savedstr2 = " on this orders"
        savedstr3 = "against your original prescription cost of ₹ "
        savedstr4 = "demotext2"
            
        }
        
        GetOrderBill()
    
        self.title = "Bill"
        
        saveviu.layer.cornerRadius = 4.0
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        if ordertype == "Past Order"
        {
        reorderbtno.isHidden = false
        orderheightcons.constant = 60
        }
        else if ordertype == "Billing Order" || statId == 58
        {
        makepayo.isHidden = false
        reorderbtno.isHidden = true
        orderheightcons.constant = 0
        makepayo.layer.cornerRadius = 4.0
        genchecksum()
        }
        else
        {
           reorderbtno.isHidden = true
           orderheightcons.constant = 0
        }
        
       
       
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        var count:Int
        if ordertype == "Past Order"
        {
          count = billmedsarr.count
        }
        else
        {
         count = billmedsarr.count
        }
        
        return count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if ordertype == "Past Order"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReOrderTableViewCell") as! OrderDetailsTableViewCell
            
            let finbilldict:NSDictionary = billmedsarr[indexPath.row] as! NSDictionary
            cell.redsubname.text = finbilldict.value(forKey: "substituteProductName") as! String
            cell.redsubqty.text = finbilldict.value(forKey: "substituteProductOty") as! String
            
            //cell.redsubprice.text = "₹ " + String(describing: finbilldict.value(forKey: "substituteProductPrice")!)
            
            if let redsubprice = finbilldict.value(forKey: "substituteProductPrice") as? Double
            {
               cell.redsubprice.text = "₹ " + String(format: "%.2f", redsubprice)
            }
            
            cell.redorgname.text = finbilldict.value(forKey: "orderProductName") as! String
            
            if let reormedpr = finbilldict.value(forKey: "orderProducPrice") as? Double
            {
                cell.redorgprice.text = "₹ " + String(format: "%.2f", reormedpr)
            }
            
            //cell.redorgprice.text = "₹ " + String(describing: finbilldict.value(forKey: "orderProducPrice")!)
            
            cell.bgview.layer.cornerRadius = 4.0
            cell.bgview.layer.borderWidth = 1.0
            cell.bgview.layer.borderColor = UIColor(hexString: "#EDEDED", alpha: 1.0).cgColor
            cell.bgview.clipsToBounds = true
            
            if show == "yes"
            {
                cell.removebtno.isHidden = false
                cell.amstack.isHidden = false
                cell.addquan.addTarget(self, action: #selector(add), for: .touchUpInside)
                cell.minusquan.addTarget(self, action: #selector(min), for: .touchUpInside)
                cell.addquan.tag = indexPath.row
                cell.minusquan.tag = indexPath.row
                //cell.quanlbl.text = String(describing: currentquan)
                cell.removebtno.addTarget(self, action: #selector(remove), for: .touchUpInside)
                cell.removebtno.tag = indexPath.row
                cell.addmed.tag = indexPath.row
                cell.addmed.addTarget(self, action: #selector(addmedview), for: .touchUpInside)
            }
            else{
                cell.removebtno.isHidden = true
                cell.amstack.isHidden = true
                cell.blurview.isHidden = true
                cell.addmed.isHidden = true
                cell.addmed.addTarget(self, action: #selector(addmedview), for: .touchUpInside)
            }
            
           return cell
        }
        else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsTableViewCell") as! OrderDetailsTableViewCell
      
        cell.bgview.layer.cornerRadius = 4.0
        cell.bgview.layer.borderWidth = 1.0
        cell.bgview.layer.borderColor = UIColor(hexString: "#EDEDED", alpha: 1.0).cgColor
        cell.bgview.clipsToBounds = true
        let finbilldict:NSDictionary = billmedsarr[indexPath.row] as! NSDictionary
        cell.submedname.text = finbilldict.value(forKey: "substituteProductName") as! String
        cell.submedqty.text = finbilldict.value(forKey: "substituteProductOty") as! String
        
        if let subormedpr = finbilldict.value(forKey: "substituteProductPrice") as? Double
        {
        cell.submedprice.text = "₹ " + String(format: "%.2f", subormedpr)
        }
            
        cell.ormedname.text = finbilldict.value(forKey: "orderProductName") as! String
            
        if let ormedpr = finbilldict.value(forKey: "orderProducPrice") as? Double
        {
         cell.ormedprice.text = "₹ " + String(format: "%.2f", ormedpr)
        }
        
            
        return cell
        }
        
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 140.0
    }
    
    
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func ReOrderact(_ sender: Any) {
        if reorderbtno.title(for: .normal) == "Place Order"
        {
                     
          NotificationCenter.default.post(name: Notification.Name("Reorder"), object: ArrayB)
          NotificationCenter.default.post(name: Notification.Name("ReorderOID"), object: odhelpdict2.value(forKey: "orderId") as! CLong)
          
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: buyMeds.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            
        }
        else{
        reorderbtno.setTitle("Place Order", for: .normal)
        show = "yes"
        billtblview.reloadData()
        
            for i in 0..<billmedsarr.count
            {
                let variantdicts:NSDictionary = billmedsarr[i] as! NSDictionary
                
                dictionaryA.updateValue(variantdicts.value(forKey: "medicineId") as! String, forKey: "productCode")
                dictionaryA.updateValue("1", forKey: "quantity")
                
                ArrayB.append(dictionaryA)
            }
            print(ArrayB)
            
        }
    }
    
    @objc func add(sender:UIButton)
    {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        
        let cell:OrderDetailsTableViewCell = billtblview.cellForRow(at: myIndexPath as IndexPath) as! OrderDetailsTableViewCell
        
        let variantdicts:NSDictionary = billmedsarr[sender.tag] as! NSDictionary
        let vmedicode:String = variantdicts.value(forKey: "medicineId") as! String
        
        var quantity = Int(cell.quanlbl.text!)!
        quantity = quantity + 1
        cell.quanlbl.text = String(describing: quantity)
        
        for i in 0..<ArrayB.count
        {
            if ArrayB[i].values.contains(vmedicode){
                
                ArrayB[i].updateValue(cell.quanlbl.text!, forKey: "quantity")
                
            } else {
                // does not contain key
            }
        }
        print(ArrayB)
        

    }
    
    @objc func min(sender:UIButton)
    {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        
        let cell:OrderDetailsTableViewCell = billtblview.cellForRow(at: myIndexPath as IndexPath) as! OrderDetailsTableViewCell
        let variantdicts:NSDictionary = billmedsarr[sender.tag] as! NSDictionary
        let vmedicode:String = variantdicts.value(forKey: "medicineId") as! String
        
         var quantity = Int(cell.quanlbl.text!)!
        if quantity == 1
        {
            
        }else{
        quantity = quantity - 1
        }
        
        cell.quanlbl.text = String(describing: quantity)
        
        for i in 0..<ArrayB.count
        {
            if ArrayB[i].values.contains(vmedicode){
                
                ArrayB[i].updateValue(cell.quanlbl.text!, forKey: "quantity")
                
            } else {
                // does not contain key
            }
        }
    
    print(ArrayB)
        
    }
    
    
    @objc func remove(sender:UIButton)
    {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        
        let cell:OrderDetailsTableViewCell = billtblview.cellForRow(at: myIndexPath as IndexPath) as! OrderDetailsTableViewCell
        let variantdicts:NSDictionary = billmedsarr[sender.tag] as! NSDictionary
        let vmedicode:String = variantdicts.value(forKey: "medicineId") as! String
        
        ArrayB.remove(at: sender.tag)
        
        print(ArrayB)
        
        cell.quanlbl.text = "1"
        cell.blurview.isHidden = false
        cell.addmed.isHidden = false
        cell.removebtno.isHidden = true
        
    }
    
    
    @objc func addmedview(sender:UIButton)
    {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        
        let cell:OrderDetailsTableViewCell = billtblview.cellForRow(at: myIndexPath as IndexPath) as! OrderDetailsTableViewCell
        let variantdicts:NSDictionary = billmedsarr[sender.tag] as! NSDictionary
        
        dictionaryA.updateValue(variantdicts.value(forKey: "medicineId") as! String, forKey: "productCode")
        dictionaryA.updateValue("1", forKey: "quantity")
    
        ArrayB.insert(dictionaryA, at: sender.tag)
        print(ArrayB)
        
        cell.blurview.isHidden = true
        cell.addmed.isHidden = true
        cell.removebtno.isHidden = false
        
    }
    

    func GetOrderBill()
    {
        if Connectivity.isConnectedToInternet {
        
        let orderId:CLong = odhelpdict2.value(forKey: "orderId") as! CLong
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["orderId":orderId]
        
        print(parameters)
        
        let url =  GetOrderBills()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    print(json)
                    
                    DispatchQueue.main.async {
                        
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    let orderbilldict:NSDictionary = json as NSDictionary
                    if orderbilldict.object(forKey: "error") != nil
                    {
                        
                    }
                    else{
                    let mybill:NSDictionary = orderbilldict.value(forKey: "bill") as! NSDictionary
                    self.billmedsarr = mybill.value(forKey: "medicines") as! NSArray
                        
                    if let total = mybill.value(forKey: "total") as? Double
                    {
                    self.totallbl.text = "₹ " + String(format: "%.2f", total)
                    }
                        
                   
                    let deli:CLong = mybill.value(forKey: "deliveryFee") as! CLong
                    if deli == 0
                    {
                    self.deliveryfee.text = NSLocalizedString("Free", comment: "")
                    }
                    else
                    {
                      self.deliveryfee.text = "₹ " + String(describing: mybill.value(forKey: "deliveryFee")!)
                    }
                        
                    let handling:CLong = mybill.value(forKey: "handlingFee") as! CLong
                    if handling == 0
                    {
                    self.cashandling.text = NSLocalizedString("Free", comment: "")
                    }
                    else{
                    self.cashandling.text = String(describing: mybill.value(forKey: "handlingFee")!)
                    }
                        
                    if let discount = mybill.value(forKey: "discount") as? Double
                    {
                        self.discountlbl.text = "₹ " + String(format: "%.2f", discount)
                    }
                        
                        if let truemedscash = mybill.value(forKey: "truemedsCash") as? Double
                        {
                            
                            self.truemedscash.text = "₹ " + String(format: "%.2f", truemedscash)
                            
                        }
                        
                        if let truemedstotal = mybill.value(forKey: "truemedsTotal") as? Double
                        {
                            
                        self.truemedstotal.text = "₹ " + String(format: "%.2f", truemedstotal)
                            
                        }
                        
                        
                        if let savingprice = mybill.value(forKey: "savingPrice") as? Double
                        {
                            
                            self.savelbl1.text = NSLocalizedString(self.savedstr, comment: "") + String(format: "%.2f", savingprice) + NSLocalizedString(self.savedstr2, comment: "")
                            
                        }
                        
                        
                        if let totalprice = mybill.value(forKey: "orderPrice") as? Double
                        {
                            
                            self.savelbl2.text = NSLocalizedString(self.savedstr3, comment: "") + String(format: "%.2f", totalprice) + NSLocalizedString(self.savedstr4, comment: "")
                            
                        }
                        
                        
                        
                        

                        
                    }
                    
                   
                    self.billtblview.reloadData()
                    self.heightcons.constant = self.billtblview.contentSize.height
                    
                    
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
        }
        else
        {
            Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
        }
    }
    
    
    @IBAction func Billcheckout(_ sender: UIButton) {
        beginPayment()
    }
    
    
    func beginPayment() {
        
        let orderIDs:String = orderID

        let mobNumber:String = UserDefaults.standard.value(forKey: "mobileNo") as! String

        serv = serv.createProductionEnvironment()

        let type :ServerType = .eServerTypeProduction
        let order = PGOrder(orderID: "", customerID: "", amount: "", eMail: "", mobile: "")
        order.params = ["MID":"hQzDdW75292560094634",
                        "ORDER_ID": orderIDs,
                        "CUST_ID": mobNumber,
                        "MOBILE_NO": mobNumber,
                        "CHANNEL_ID": "WAP",
                        "WEBSITE": "DEFAULT",
                        "TXN_AMOUNT": TxnAmt,
                        "INDUSTRY_TYPE_ID": "Retail",
                        "CHECKSUMHASH": checksumstr,
                        "CALLBACK_URL": "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDs)"]
        
        
      //  Staging Creds  //
        
//
//        serv = serv.createStagingEnvironment()
//
//        let type :ServerType = .eServerTypeStaging
//        let order = PGOrder(orderID: "", customerID: "", amount: "", eMail: "", mobile: "")
//        order.params = ["MID":"VOmyVH09017809207504",
//                        "ORDER_ID": orderIDs,
//                        "CUST_ID": mobNumber,
//                        "MOBILE_NO": mobNumber,
//                        "CHANNEL_ID": "WAP",
//                        "WEBSITE": "WEBSTAGING",
//                        "TXN_AMOUNT": TxnAmt,
//                        "INDUSTRY_TYPE_ID": "Retail",
//                        "CHECKSUMHASH": checksumstr,
//                        "CALLBACK_URL": "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDs)"]


        self.txnController =  (self.txnController.initTransaction(for: order) as?PGTransactionViewController)!
        self.txnController.title = "Paytm Payments"
        self.txnController.setLoggingEnabled(true)

        print(order.params)

        if(type != ServerType.eServerTypeNone) {
            self.txnController.serverType = type;
        } else {
            return
        }
        self.txnController.merchant = PGMerchantConfiguration.defaultConfiguration()
        self.txnController.delegate = self
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.pushViewController(self.txnController, animated: true)
    }
    
    
    func didFinishedResponse(_ controller: PGTransactionViewController, response responseString: String) {

       // let orderIDs:CLong = odhelpdict2.value(forKey: "orderId") as! CLong

        let resdict:NSDictionary = convertStringToDictionary(json: responseString) as! NSDictionary

        print(resdict)

        let resstatus:String = resdict.value(forKey: "RESPCODE") as! String
        if resstatus == "01"
        {

            if let Bankname = resdict.value(forKey: "BANKNAME") as? String
            {
                bankname = Bankname
            }

            if let Banktxnid = resdict.value(forKey: "BANKTXNID") as? String
            {
                banktxnid = Banktxnid
            }

            if let Checksumhash = resdict.value(forKey: "CHECKSUMHASH") as? String
            {
                checksumhash = Checksumhash
            }

            if let Currency = resdict.value(forKey: "CURRENCY") as? String
            {
                currency = Currency
            }

            if let Gatewayname = resdict.value(forKey: "GATEWAYNAME") as? String
            {
                gatewayname = Gatewayname
            }

            if let Mid = resdict.value(forKey: "MID") as? String
            {
                mid = Mid
            }

            if let Orderid = resdict.value(forKey: "ORDERID") as? String
            {
                orderid = Orderid
            }

            if let Paymentmode = resdict.value(forKey: "PAYMENTMODE") as? String
            {
                paymentmode = Paymentmode
            }

            if let Respcode = resdict.value(forKey: "RESPCODE") as? String
            {
                respcode = Respcode
            }

            if let Respmsg = resdict.value(forKey: "RESPMSG") as? String
            {
                respmsg = Respmsg
            }

            if let Status = resdict.value(forKey: "STATUS") as? String
            {
                status = Status
            }

            if let Txnamount = resdict.value(forKey: "TXNAMOUNT") as? String
            {
                txnamount = Txnamount
            }

            if let Txndate = resdict.value(forKey: "TXNDATE") as? String
            {
                txndate = Txndate
            }

            if let Txnid = resdict.value(forKey: "TXNID") as? String
            {
                txnid = Txnid
            }

            savePaymentResponses()

        }
        else
        {

            if let Bankname = resdict.value(forKey: "BANKNAME") as? String
            {
            bankname = Bankname
            }

            if let Banktxnid = resdict.value(forKey: "BANKTXNID") as? String
            {
                banktxnid = Banktxnid
            }

            if let Checksumhash = resdict.value(forKey: "CHECKSUMHASH") as? String
            {
                checksumhash = Checksumhash
            }

            if let Currency = resdict.value(forKey: "CURRENCY") as? String
            {
                currency = Currency
            }

            if let Gatewayname = resdict.value(forKey: "GATEWAYNAME") as? String
            {
                gatewayname = Gatewayname
            }

            if let Mid = resdict.value(forKey: "MID") as? String
            {
                mid = Mid
            }

            if let Orderid = resdict.value(forKey: "ORDERID") as? String
            {
                orderid = Orderid
            }

            if let Paymentmode = resdict.value(forKey: "PAYMENTMODE") as? String
            {
                paymentmode = Paymentmode
            }

            if let Respcode = resdict.value(forKey: "RESPCODE") as? String
            {
                respcode = Respcode
            }

            if let Respmsg = resdict.value(forKey: "RESPMSG") as? String
            {
                respmsg = Respmsg
            }

            if let Status = resdict.value(forKey: "STATUS") as? String
            {
                status = Status
            }

            if let Txnamount = resdict.value(forKey: "TXNAMOUNT") as? String
            {
                txnamount = Txnamount
            }

            if let Txndate = resdict.value(forKey: "TXNDATE") as? String
            {
                txndate = Txndate
            }

            if let Txnid = resdict.value(forKey: "TXNID") as? String
            {
                txnid = Txnid
            }


            savePaymentResponses()


        }

    }

    //this function triggers when transaction gets cancelled
    func didCancelTrasaction(_ controller : PGTransactionViewController) {
        controller.navigationController?.popViewController(animated: true)
    }

    //Called when a required parameter is missing.
    func errorMisssingParameter(_ controller : PGTransactionViewController, error : NSError?) {
        //NotificationCenter.default.post(name: Notification.Name("PaymentFailed"), object: nil)
       // controller.navigationController?.popViewController(animated: true)

        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: OrderListingViewController.self) {
                NotificationCenter.default.post(name: Notification.Name("PaymentFailed"), object: nil)

                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }

    }
    
    
    
    func genchecksum()
    {
        if Connectivity.isConnectedToInternet {
            
        let orderId:CLong = odhelpdict2.value(forKey: "orderId") as! CLong
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["orderId":orderId] as [String : Any]
        
        let url =  GenerateChecksum()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        //request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                
                    DispatchQueue.main.async {
                        let httpResponse = response as? HTTPURLResponse
                        
                        print(json)
                        
                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                        {
                        let resdict:NSDictionary = json as NSDictionary
                        
                        print(resdict)
                            
                        self.checksumstr = resdict.value(forKey: "CHECKSUMHASH") as! String
                        self.TxnAmt = String(describing: resdict.value(forKey: "TXN_AMOUNT")!)
                        //let paytmOID:String = resdict.value(forKey: "paytmOrderId") as! String
                            
                        self.orderID = resdict.value(forKey: "paytmOrderId") as! String
                        
                        }
                        else if httpResponse?.statusCode == 401
                        {
                            let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                            Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                        }
                        else
                        {
                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
    func savePaymentResponses()
    {
        if Connectivity.isConnectedToInternet {
            
        let parameters = ["bankname":bankname,
                          "banktxnid":banktxnid,
                          "checksumhash":checksumhash,
                          "currency": currency,
                          "gatewayname":gatewayname,
                          "mid":mid,
                          "orderid":orderid,
                          "paymentmode":paymentmode,
                          "respcode": respcode,
                          "respmsg":respmsg,
                          "status": status,
                          "txnamount":txnamount,
                          "txndate":txndate,
                          "txnid": txnid] as [String : Any]
        
           // let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
            
            let para2 = ["":""]
            
            let url =  savePaytmResponse()
            
            var items = [URLQueryItem]()
            var myURL = URLComponents(string: url)
            
            let session = URLSession.shared
            
            for (key,value) in para2 {
                items.append(URLQueryItem(name: key, value: value))
            }
            
            myURL?.queryItems = items
            
            var request = URLRequest(url: (myURL?.url)!)
            
            request.httpMethod = "POST"
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("a", forHTTPHeaderField: "transactionId")
            // request.addValue(authtok, forHTTPHeaderField: "Authorization")
            
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                if let data = data {
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                        print(json)
                        
                        DispatchQueue.main.async {
                            let httpResponse = response as? HTTPURLResponse
                            
                            if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                            {
                                let paytmdict:NSDictionary = json as NSDictionary
                                if paytmdict.allKeys.first as! String == "ERROR"
                                {
                                    for controller in self.navigationController!.viewControllers as Array {
                                        if controller.isKind(of: OrderListingViewController.self) {
                                            NotificationCenter.default.post(name: Notification.Name("PaymentFailed"), object: nil)
                                            
                                            self.navigationController!.popToViewController(controller, animated: true)
                                            break
                                        }
                                    }
                                }
                                else
                                {
                                    for controller in self.navigationController!.viewControllers as Array {
                                        
                                        if controller.isKind(of:buyMeds.self) {
                                            NotificationCenter.default.post(name: Notification.Name("PaymentSuccess"), object: nil)
                                      self.navigationController!.popToViewController(controller, animated: true)
                                            break
                                        }
                                    }
                                }
                            }
                            else if httpResponse?.statusCode == 400
                            {
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: OrderListingViewController.self) {
                                NotificationCenter.default.post(name: Notification.Name("PaymentFailed"), object: nil)
                                
                            self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                    }
                                }
                            }
                            else
                            {
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: OrderListingViewController.self) {
                                        NotificationCenter.default.post(name: Notification.Name("PaymentFailed"), object: nil)
                                        
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            }
                        }
                        
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
                }
            })
            task.resume()
}
else
{
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
}
            
    }
        
    
    
        
        
    
    
    
}
