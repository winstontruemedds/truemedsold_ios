//
//  ArticlesDetailsViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import SDWebImage
import Reachability
import Alamofire

class ArticlesDetailsViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,UITextViewDelegate {

    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var sharebtno: UIButton!
    
    @IBOutlet weak var relartleftcons: NSLayoutConstraint!
    
    @IBOutlet weak var detailimg: UIImageView!
    
    @IBOutlet weak var detailtitle: UILabel!
    
    @IBOutlet weak var detailauthor: UILabel!
    
    @IBOutlet weak var detaildate: UILabel!
    
    
    @IBOutlet weak var detailsLbl: UILabel!
    
//    @IBOutlet weak var detaildesc: UITextView!
    
    @IBOutlet weak var relatedcollection: UICollectionView!
    
//    @IBOutlet weak var artdescheight: NSLayoutConstraint!
    
    @IBOutlet weak var relartcolheight: NSLayoutConstraint!
    
    var articlesdict = NSDictionary()
    
    var relatedarts = NSArray()
    
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    @IBOutlet weak var urllink: UILabel!
    
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "share")
        sharebtno.setImage(svgimg2.uiImage, for: .normal)
        
        getArticledetails()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        urllink.isUserInteractionEnabled = true
        urllink.addGestureRecognizer(tap)
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        
        guard let url = URL(string: urllink.text!) else { return }
        UIApplication.shared.open(url)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return relatedarts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "relatecollectioncell", for: indexPath) as! ImageCollectionViewCell
        cell.relartimg.layer.cornerRadius = 8.0
        cell.relartimg.clipsToBounds = true
        cell.relartblur.layer.cornerRadius = 8.0
        cell.relartblur.clipsToBounds = true
        let finreldict:NSDictionary = relatedarts[indexPath.row] as! NSDictionary
        cell.relarttitle.text = finreldict.value(forKey: "name") as! String
        cell.relauth.text = finreldict.value(forKey: "author") as! String
        
//        let imgurl:URL = URL(string: finreldict.value(forKey: "image") as! String)!
//
//        if let data = try? Data(contentsOf: imgurl)
//        {
//        let images: UIImage = UIImage(data: data)!
//        let imageData = images.pngData()
//        cell.relartimg.image = UIImage(data: imageData!)
//        }
        
        let imgurl:URL = URL(string: finreldict.value(forKey: "image") as! String)!
        cell.relartimg.sd_setImage(with: imgurl, completed: nil)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let finreldict:NSDictionary = relatedarts[indexPath.row] as! NSDictionary
        let type:CLong = finreldict.value(forKey: "type") as! CLong
        if type == 1
        {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ArticlesDetailsViewController") as? ArticlesDetailsViewController
        vc!.articlesdict = relatedarts[indexPath.row] as! NSDictionary
        self.navigationController?.pushViewController(vc!, animated: true)
        }else{
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
        vc!.articlesdict = relatedarts[indexPath.row] as! NSDictionary
        self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.bounds.size.width * 0.8, height: 140)
    }
    
    
    @IBAction func backto(_ sender: Any) {
       //self.navigationController?.popViewController(animated: true)
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: ArticlesViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
        
        
    }
    

    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {

        var yVelocity: CGFloat = scrollView.panGestureRecognizer.velocity(in: scrollView.superview).x
        if yVelocity < 0 {
            print("left")
            relartleftcons.constant = 0
        } else if yVelocity > 0 {
            print("right")
            relartleftcons.constant = 20
        } else {
            print("Can't determine direction as velocity is 0")
        }

    }
    
    
    
    @IBAction func shareart(_ sender: Any)
    {
        let authorname:String = detailauthor.text! + " Says...\n"
        let first4 = authorname + String(detailsLbl.text!.prefix(250)) + "\n\n Read more at Truemeds App:"
        
        if let myWebsite = NSURL(string: "http://bit.ly/TruemedsDL") {
            let objectsToShare = [first4, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = sender as! UIView
            self.present(activityVC, animated: true, completion: nil)
    }
        
    }
    
    
    func getArticledetails()
    {
        if Connectivity.isConnectedToInternet {
        
        actInd.startAnimating()
        
        let articleID:CLong = articlesdict.value(forKey: "id") as! CLong
        
        let para2 = ["id":articleID]
        
        let url =  GetArticleDetails()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        // request.addValue("T1", forHTTPHeaderField: "accessToken")
        
        //request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                
                print(json)
                
                DispatchQueue.main.async {
                        
                let httpResponse = response as? HTTPURLResponse
                    
                if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                {
                    
                let dict:NSDictionary = json as NSDictionary
                let resultdict:NSDictionary = dict.value(forKey: "result") as! NSDictionary
                self.relatedarts = resultdict.value(forKey: "intrestedActicles") as! NSArray
                let imgurl:URL = URL(string: resultdict.value(forKey: "image") as! String)!
                self.detailimg.sd_setImage(with: imgurl, completed: nil)
                self.detailtitle.text = resultdict.value(forKey: "name") as! String
                self.detailauthor.text = resultdict.value(forKey: "authorName") as! String
                    
                self.detaildate.text = resultdict.value(forKey: "createdOn") as? String
                
                self.detailsLbl.text = resultdict.value(forKey: "description") as! String
                
                if self.relatedarts.count == 0
                {
                self.relartcolheight.constant = 0
                }
                  
                if let urltext = resultdict.value(forKey: "url") as? String
                {
                self.urllink.text = urltext
                }
                
                self.relatedcollection.reloadData()
//                self.artdescheight.constant = self.detaildesc.contentSize.height
                                    
                }
                else
                {
                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                    
                self.actInd.stopAnimating()
                    
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
        }
        else
        {
            Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
        }
        
    }
    

}

