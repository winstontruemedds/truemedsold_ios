//
//  PastOrderViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class PastOrderViewController: UIViewController,UITableViewDataSource,UITableViewDelegate  {
    
    let reachability = Reachability()!
    
    var ordertype:String!
    
    @IBOutlet weak var backbtno: UIButton!
    
    var PastODarr = NSArray()
    
    @IBOutlet weak var pastordertable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        allorders()
        
        self.title = "Past Orders"
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PastODarr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PastOrderTableViewCell") as! OrderListTableViewCell
        let finpastdict:NSDictionary = PastODarr[indexPath.row] as! NSDictionary
        cell.popatlbl.text = String(describing:finpastdict.value(forKey: "orderId")!)
        cell.posavinglbl.text = String(describing: finpastdict.value(forKey: "totalSaving")!)
        
        if let delieveredon = finpastdict.value(forKey: "deliveredDate") as? String
        {
            cell.pastostatlbl.text = NSLocalizedString("Delivered On -", comment: "")
            cell.delionlbl.text = delieveredon
        }
        else
        {
            let mydate:CLong = finpastdict.value(forKey: "orderDate") as! CLong
            let mydates:CLong = mydate/1000
            let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
            
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "dd MMMM yyyy hh:mm"
            
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            cell.pastostatlbl.text = NSLocalizedString("Ordered On -", comment: "")
            cell.delionlbl.text = dateString
        }
        
        let statusId:CLong = finpastdict.value(forKey: "statusId") as! CLong
        
        if statusId == 39 || statusId == 1 || statusId == 81 || statusId == 57 || statusId == 2
        {
            cell.posavinglbl.isHidden = true
            cell.pastotsavelbl.isHidden = true
        }
        else
        {
            cell.posavinglbl.isHidden = false
            cell.pastotsavelbl.isHidden = false
        }
        
        if statusId == 142
        {
            cell.pastotsavelbl.text = NSLocalizedString("Estimated Savings :", comment: "")
        }
        else
        {
            cell.pastotsavelbl.text = NSLocalizedString("Total Savings :", comment: "")
        }
        
        
        if statusId == 81
        {
            cell.pasterrorid.isHidden = false
        }
        else
        {
            cell.pasterrorid.isHidden = true
        }
        
        if statusId == 58
        {
            cell.ppaynow.isHidden = false
            cell.ppaynow.layer.cornerRadius = 4.0
            cell.ppaynow.layer.borderWidth = 0.8
            cell.ppaynow.layer.borderColor = UIColor(hexString: "#0071BC", alpha: 1.0).cgColor
            cell.ppaynow.tag = indexPath.row
            cell.ppaynow.addTarget(self, action: #selector(toPastPayment), for: .touchUpInside)
            
        }
        else
        {
            cell.ppaynow.isHidden = true
        }
        
        
        cell.bgview.layer.cornerRadius = 4.0
        cell.bgview.layer.borderWidth = 1.0
        cell.bgview.layer.borderColor = UIColor(hexString: "#EDEDED", alpha: 1.0).cgColor
        cell.bgview.clipsToBounds = true
        cell.pastvalidlbl.text = NSLocalizedString("Order Status -", comment: "")
        cell.povalidtill.text = finpastdict.value(forKey: "orderStatus") as? String
        
        
        return cell
    }
    
    @objc func toPastPayment(sender:UIButton)
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
        vc?.ordertype = "Billing Order"
        vc?.odhelpdict2 = self.PastODarr[sender.tag] as! NSDictionary
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let finpastdict:NSDictionary = PastODarr[indexPath.row] as! NSDictionary
        let statusId:CLong = finpastdict.value(forKey: "statusId") as! CLong
        if statusId == 58
        {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
            vc?.ordertype = "Billing Order"
            vc?.odhelpdict2 = self.PastODarr[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else if statusId == 57
        {
            
        }
        else
        {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
             vc?.ordertype = "Past Order"
             vc?.odhelpdict2 = self.PastODarr[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }

    
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func allorders()
    {
        if Connectivity.isConnectedToInternet {
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let para2 = ["":""]
        
        let url =  GetAllOrders()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        //request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    DispatchQueue.main.async {
                        
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    let OrderDict:NSDictionary = json as NSDictionary
                    self.PastODarr = OrderDict.value(forKey: "pastOrder") as! NSArray
                   
                    DispatchQueue.main.async {
                      self.pastordertable.reloadData()
                      self.pastordertable.tableFooterView = UIView()
                    }
                    if self.PastODarr.count == 0
                    {
                     self.pastordertable.isHidden = true
                    }else
                    {
                     self.pastordertable.isHidden = false
                    }
                        
                        
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
}
