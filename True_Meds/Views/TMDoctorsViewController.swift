//
//  TMDoctorsViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class TMDoctorsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    let reachability = Reachability()!
    
    var doctarr = NSArray()
    
    @IBOutlet weak var doctlisttable: UITableView!
    
    @IBOutlet weak var backbtno: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        getdoctors()
    
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return doctarr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "doctorlist") as! PresImgTableViewCell
        let findoctdict:NSDictionary = doctarr[indexPath.row] as! NSDictionary
        cell.doctname.text = findoctdict.value(forKey: "doctorName") as? String
        cell.doctexp.text = findoctdict.value(forKey: "experience") as? String
        if let hmantra = findoctdict.value(forKey: "healthMantra") as? String
        {
        cell.doctmantra.text = "\"" + hmantra + "\""
        }
        
        if let imgstr = findoctdict.value(forKey: "image") as? String
        {
        if imgstr == "" || imgstr == nil
        {
            
        }else
        {
        let imgurl:URL = URL(string: imgstr)!
        cell.doctimage.sd_setImage(with: imgurl, completed: nil)
        }
        }
        cell.doctimage.layer.cornerRadius = 50.0
        cell.doctimage.clipsToBounds = true
        
        let qualarr:[String] = findoctdict.value(forKey: "qualification") as! [String]
        let strrs = qualarr.joined(separator:",")
        cell.doctqual.text = strrs
        
        let specarr:[String] = findoctdict.value(forKey: "specialist") as! [String]
        let strrs2 = specarr.joined(separator:",")
        
         cell.doctspec.text = strrs2
        
        let rating:CLong = findoctdict.value(forKey: "rating") as! CLong
        if rating == 0
        {
            cell.star1.image = UIImage(named: "emptystar")
            cell.star2.image = UIImage(named: "emptystar")
            cell.star3.image = UIImage(named: "emptystar")
            cell.star4.image = UIImage(named: "emptystar")
            cell.star5.image = UIImage(named: "emptystar")
        }
        else if rating == 1
        {
         cell.star1.image = UIImage(named: "filledstar")
        }
        else if rating == 2
        {
         cell.star1.image = UIImage(named: "filledstar")
         cell.star2.image = UIImage(named: "filledstar")
        }
        else if rating == 3
        {
            cell.star1.image = UIImage(named: "filledstar")
            cell.star2.image = UIImage(named: "filledstar")
            cell.star3.image = UIImage(named: "filledstar")
        }
        else if rating == 4
        {
            cell.star1.image = UIImage(named: "filledstar")
            cell.star2.image = UIImage(named: "filledstar")
            cell.star3.image = UIImage(named: "filledstar")
            cell.star4.image = UIImage(named: "filledstar")
        }
        else
        {
            cell.star1.image = UIImage(named: "filledstar")
            cell.star2.image = UIImage(named: "filledstar")
            cell.star3.image = UIImage(named: "filledstar")
            cell.star4.image = UIImage(named: "filledstar")
            cell.star5.image = UIImage(named: "filledstar")
        }
        
        return cell
    }
    
    
    @IBAction func backto(_ sender: UIButton) {
       self.navigationController?.popViewController(animated: true)
    }
    
    
    func getdoctors()
    {
        if Connectivity.isConnectedToInternet {
        
        let urlstr = GetDoctorsList()
        let url =  URL(string: urlstr)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    let resdict:NSDictionary = json as NSDictionary
                    self.doctarr = resdict.value(forKey: "payload") as! NSArray
                    DispatchQueue.main.async {
                        self.doctlisttable.reloadData()
                        self.doctlisttable.tableFooterView = UIView()
                    }
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
}
