//
//  StatusViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Alamofire

class StatusViewController: UIViewController {

    var dateString = String()
    
    var timeString = String()
    
    var odhelpdict4 = NSDictionary()
   
    @IBOutlet weak var saveView: UIView!
    
    @IBOutlet weak var actInd: UIActivityIndicatorView!

    @IBOutlet weak var estview: UIView!
    
    @IBOutlet weak var estextlbl: UILabel!
    
    var realoadeCount = Int()
    
    
    var savedstr:String!
       
       var savedstr2:String!
       
       var savedstr3:String!
       
       var savedstr4:String!
    
    @IBOutlet weak var savelbl1: UILabel!
    
    @IBOutlet weak var savelbl2: UILabel!
    
     var openCloseStatus = [Bool]()
        
        var selectedSectionTag = Int()
        
        var sectionToShow = Int()
        
        var orderStatusHeaderArray = NSArray()

        var orderStatusCellArray = NSArray()
        
        @IBOutlet weak var orderStatusTable: UITableView!

         @IBOutlet weak var cancelobtno: UIButton!
    @IBOutlet weak var backbtno: UIButton!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
         
            // Do any additional setup after loading the view.
        }
        
        
    
    override func viewWillAppear(_ animated: Bool) {
           //reloadCountFor onece reloade to show list open beacause cell array ceate inside table view head
                realoadeCount = 0
                OrderStatus()
                 print("selected order dict\(odhelpdict4)")
                 
                 
                 let svgimg: SVGKImage = SVGKImage(named: "back")
                        backbtno.setImage(svgimg.uiImage, for: .normal)
                 
                 let statusId:CLong = odhelpdict4.value(forKey: "statusId") as! CLong
                        
                        if statusId == 1 || statusId == 2 || statusId == 39 || statusId == 57 || statusId == 3 || statusId == 4
                        {
                        saveView.isHidden = true
                        }
                        
                        if statusId == 142
                        {
                            savedstr = "You will save approximately ₹ "
                            savedstr2 = " on this order"
                            savedstr3 = "against your original prescription cost of ₹ "
                            savedstr4 = "demotext"
                        }
                        else
                        {
                            savedstr = "You saved ₹ "
                            savedstr2 = " on this orders"
                            savedstr3 = "against your original prescription cost of ₹ "
                            savedstr4 = "demotext2"
                        }
                        
                 
                 saveView.layer.cornerRadius = 4.0
                 cancelobtno.layer.cornerRadius = 4.0
                        
                        self.title = "Status"
                        
                        cancelobtno.setTitle(NSLocalizedString("Cancel Order", comment: ""), for: .normal)
    }

        
        func OrderStatus(){
            let orderID:CLong = odhelpdict4.value(forKey: "orderId") as! CLong
              
              let parameters = ["orderId":String(describing: orderID)]
              
              let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
              
              let url =  GetOrderStats()
              
              var items = [URLQueryItem]()
              var myURL = URLComponents(string: url)
              
              let session = URLSession.shared
              
              for (key,value) in parameters {
                  items.append(URLQueryItem(name: key, value: String(describing:value)))
              }
              myURL?.queryItems = items
              
              var request = URLRequest(url: (myURL?.url)!)
              request.httpMethod = "POST"
              
              request.addValue("application/json", forHTTPHeaderField: "Content-Type")
              request.addValue("a", forHTTPHeaderField: "transactionId")
              request.addValue(authtok, forHTTPHeaderField: "Authorization")
                                      AF.request(request).responseJSON { response in
    //                                    print(response)
                                        switch response.result {
                                                case .success(let value):
                                                    
                                                     switch response.response?.statusCode {
                                                                 case 200,201:
                                                                      print("200")
                                                        
                                                        let dict = value as? NSDictionary
                        let orderTrackingDict = dict?.value(forKey: "orderTracking") as? NSDictionary
                        self.orderStatusHeaderArray = orderTrackingDict?.value(forKey: "orderStatus") as? NSArray ?? []
                                                                                                         
                    let statusId:CLong = orderTrackingDict?.value(forKey: "statusId") as! CLong
                let iscancellable:Bool = orderTrackingDict?.value(forKey: "isCancelable") as! Bool
                                                                      
                                                                      
            let estDDate:String = orderTrackingDict?.value(forKey: "edd") as? String ?? ""
                        self.estextlbl.text = estDDate
                                                                                                          
        if iscancellable == true{
                                                                                                                                
            if statusId == 1 || statusId == 2 || statusId == 39 || statusId == 58 || statusId == 66 || statusId == 142
            {
            self.cancelobtno.isHidden = false
            }
            else
            {
            self.cancelobtno.isHidden = true
            }
            }
            else
            {
            if statusId == 1 || statusId == 2 || statusId == 39 || statusId == 58 || statusId == 142
            {
            self.cancelobtno.isHidden = false
            }
            else
                {
                                  self.cancelobtno.isHidden = true
           }
              }
                                                                      
                                        if let savingprice = orderTrackingDict?.value(forKey: "savingPrice") as? Double
                                                                                      {
                                                                                              
            self.savelbl1.text = NSLocalizedString(self.savedstr, comment: "") + String(format: "%.2f", savingprice) + NSLocalizedString(self.savedstr2, comment: "")
                                                                                              
                                                                                      }
                                                                                          
                                                                      
                                                                      if let totalprice = orderTrackingDict?.value(forKey: "orderPrice") as? Double
                                                                                      {
                                                                                          
                                                                                       self.savelbl2.text = NSLocalizedString(self.savedstr3, comment: "") + String(format: "%.2f", totalprice) + NSLocalizedString(self.savedstr4, comment: "")
                                                                          
                                                                                      }
                                                                      
                                                                      
                            print( "test\(self.orderStatusHeaderArray)")
                    for _ in 0..<self.orderStatusHeaderArray.count{
                                self.openCloseStatus.append(false)
                                                                    }
                                                                      
                                                                                                        
                DispatchQueue.main.async {
                            self.orderStatusTable.reloadData()
                    
                                        }
                                                                                                  
                                                             
                 case 401:
                print("401")
                 let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
               Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                  case 400:
                 print("400")
                   let adddict = value as? NSDictionary
                       if adddict?.allKeys.first as! String == "400"
                            {
                    Utility.showAlertWithTitle(title: "", andMessage: adddict?.value(forKey: "400") as? String, onVC: self)
                            }
                case 500:
               print("500")
                        let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
                                                                                         
                                                     self.addChild(popupVC)
                                                    popupVC.view.frame = self.view.frame
                                                     self.view.addSubview(popupVC.view)
                                                     popupVC.didMove(toParent: self)
                        default:
                     print("may be 500")
                    let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                                                                                                                                                 
                            }
                                                  
                                            
                                            
                                            
                                                         case .failure(let error):
                                                             print(error)
                                                         }
                                              
                                          }
                    
                    
        }
    

 
    
  
    
 
    
    
    
    
  
  
    

  
    
    
    
    
    @IBAction func backto(_ sender: Any) {
       // NotificationCenter.default.post(name: Notification.Name("OnHold"), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
 
    
    
    
    
    
    
 
    
    @IBAction func cancelbtna(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CancelReasonsViewController") as? CancelReasonsViewController
        vc!.cancelOrderId = odhelpdict4.value(forKey: "orderId") as! CLong
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    
}



extension StatusViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
         orderStatusHeaderArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = Int()
       
        if sectionToShow == section{
            if openCloseStatus[section] != true{
            count = orderStatusCellArray.count
                
                   
        }else{
             count = 0
        }
        
       }else{
             count = 0
        }
       
        
        return count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
     
      
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTableViewCell") as! cellTableViewCell
                   if openCloseStatus[indexPath.section] != true{
                   let index = orderStatusCellArray.count - 1
                    let dict = orderStatusCellArray[index - indexPath.row] as? NSDictionary
                    let StatusDate = dict?.value(forKey: "StatusDate") as? String ?? ""
                    let StatusTime = dict?.value(forKey: "StatusTime") as? String ?? ""
                    cell.dateTimeLbl.text = "\(StatusDate) | \(StatusTime)"
                    cell.lbl.text = dict?.value(forKey: "Comment") as? String ?? ""
                   }
               
        if indexPath.row == orderStatusCellArray.count - 1 {
            cell.lineView.isHidden = true
        }else{
            cell.lineView.isHidden = false
        }
                   
            return cell
        
      
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let head = tableView.dequeueReusableCell(withIdentifier: "sectionTableViewCell") as! sectionTableViewCell
        let statusDict = orderStatusHeaderArray[section] as? NSDictionary
        head.setionLbl.text = statusDict?.value(forKey: "status") as? String ?? ""
        
        if let mydate:CLong = statusDict?.value(forKey: "date") as? CLong {
            let mydates:CLong = mydate/1000
            let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "dd MMMM yy"
            dateString = dayTimePeriodFormatter.string(from: date as Date)
        }
        
      
        if  let myTime:CLong = statusDict?.value(forKey: "modifiedOn") as? CLong {
            let myTimes:CLong = myTime/1000
            let Time = NSDate(timeIntervalSince1970: TimeInterval(myTimes))
            let dayTimePeriodFormatter2 = DateFormatter()
            dayTimePeriodFormatter2.dateFormat = "HH:mm"
            timeString = dayTimePeriodFormatter2.string(from: Time as Date)
        }
      
       
        
        
         head.dateTimeLbl.text = "\(dateString) | \(timeString)"
        
        
        if statusDict?.value(forKey: "getShipmentSummaryResponse") as? NSDictionary != nil{

        let dict = statusDict?.value(forKey: "getShipmentSummaryResponse") as? NSDictionary
           
        if dict?.value(forKey: "ShipmentSummary") as? NSArray != nil{
            sectionToShow = section
            orderStatusCellArray = dict?.value(forKey: "ShipmentSummary") as? NSArray ?? []
           
            if realoadeCount == 0{
                self.orderStatusTable.reloadData()
                realoadeCount = 1
            }
            
        }
        
                                                                                       
        }
       
        head.openCloseBtn.addTarget(self, action: #selector(openCloseBtnPress(sender:)), for: .touchUpInside)
        head.openCloseBtn.tag = section
        if section == orderStatusHeaderArray.count - 1 && openCloseStatus[orderStatusHeaderArray.count - 1] == false
        {
            if orderStatusCellArray.count == 0 && orderStatusHeaderArray.count != 5{
                              head.lineView.isHidden = true
                          }else{
                              head.lineView.isHidden = false
                          }
        }else{
            if section == 4 {
              head.lineView.isHidden = true
            }
            
        }
        
       
        
        
        
        return head
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
     
   
    @objc func openCloseBtnPress(sender:UIButton){
        
        if orderStatusHeaderArray.count != 1{
            if sectionToShow == sender.tag{
                print(sender.tag)
                selectedSectionTag = sender.tag
                openCloseStatus[sender.tag] = !openCloseStatus[sender.tag]
                dateString = ""
                timeString = ""
                self.orderStatusTable.reloadData()
                
            }

        }
        
//            print(sender.tag)
        
           
                   
       
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == selectedSectionTag{
            cell.alpha = 0

            UIView.animate(
                withDuration: 1,
                delay: 0.05 * Double(indexPath.row),
                animations: {
                    cell.alpha = 1
            })
        }
        
    }
    
    
    
    
    
    
    
}


















