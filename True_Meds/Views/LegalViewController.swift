//
//  LegalViewController.swift
//  True_Meds
//


import UIKit
import SVGKit

class LegalViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var arr = ["Privacy Policy","Terms & Conditions"]
    
    var subarr = ["Our Privacy Policy","Our Terms & Conditions"]
    
    var svgarr = ["privacypol","tnc"]
    
    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var legaltable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.legaltable.tableFooterView = UIView()

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LegalTableViewCell") as! LegalTableViewCell
        
        cell.legmainlbl.text = NSLocalizedString(arr[indexPath.row], comment: "")
        //cell.legsublbl.text = NSLocalizedString(subarr[indexPath.row], comment: "")
        

        let svgimg2: SVGKImage = SVGKImage(named: svgarr[indexPath.row])
        cell.legic.image = svgimg2.uiImage
        
        let svgimg3: SVGKImage = SVGKImage(named: "green_arrow")
        cell.legrearr.image = svgimg3.uiImage
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as? PrivacyPolicyViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else if indexPath.row == 1
        {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TNCViewController") as? TNCViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
