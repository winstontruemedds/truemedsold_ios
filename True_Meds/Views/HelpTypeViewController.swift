//
//  HelpTypeViewController.swift
//  True_Meds
//


import UIKit
import SVGKit

class HelpTypeViewController: UIViewController {

    @IBOutlet weak var orderhelpbtno: UIButton!
    
    @IBOutlet weak var otherhelpbtno: UIButton!
    
    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var questext: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mastdict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        let dict:NSDictionary = mastdict.value(forKey: "30") as! NSDictionary
        let text:String = dict.value(forKey: "question") as! String
        
        questext.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
        orderhelpbtno.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
        otherhelpbtno.setTitle(btn2.value(forKey: "text") as! String, for: .normal)
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)

        orderhelpbtno.layer.borderWidth = 0.8
        orderhelpbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        orderhelpbtno.layer.cornerRadius = 4.0
        
        otherhelpbtno.layer.borderWidth = 0.8
        otherhelpbtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        otherhelpbtno.layer.cornerRadius = 4.0
    }
    
    @IBAction func helporder(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpOrderListingViewController") as? HelpOrderListingViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func helpother(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpViewController") as? HelpViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func backto(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
    
}
