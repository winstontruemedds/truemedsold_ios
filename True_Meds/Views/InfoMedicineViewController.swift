//
//  InfoMedicineViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class InfoMedicineViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate {

    let reachability = Reachability()!
    
    @IBOutlet weak var backbtno: UIButton!
    
    var qtycount:Int = 1
    
    @IBOutlet weak var InfoMedTable: UITableView!
    
    @IBOutlet weak var addmorebtno: UIButton!
    
    @IBOutlet weak var finalisebtno: UIButton!
    
    @IBOutlet weak var addfinstickyviu: UIView!
    
    var MyMed = NSArray()
    
    var findosage = NSDictionary()
    
    var dictionaryA = ["productCode":String(),"quantity":String(),"pack":String(),"medicineName":String()]
    
    var ArrayB = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        GetInfo()
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        addmorebtno.layer.borderWidth = 0.8
        addmorebtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        addmorebtno.layer.cornerRadius = 4.0
        
        finalisebtno.layer.borderWidth = 0.8
        finalisebtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        finalisebtno.layer.cornerRadius = 4.0
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MyMed.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoMedicineTableViewCell") as! InfoMedicineTableViewCell
        findosage = self.MyMed[indexPath.row] as! NSDictionary
        cell.infomednamelbl.text = findosage.value(forKey: "medicineName") as! String
        cell.infomedcomp.text = findosage.value(forKey: "companyName") as! String
        let presreqflag:CLong = findosage.value(forKey: "prescriptionRequired") as! CLong
        if presreqflag == 0
        {
         cell.infopresreq.text = NSLocalizedString("Prescription Not Required", comment: "")
        }
        else
        {
        cell.infopresreq.text = NSLocalizedString("Prescription Required", comment: "")
        }
        
        let svgimg: SVGKImage = SVGKImage(named: "prescription")
        cell.infopresamimg.image = svgimg.uiImage
        
       
        
        
        
        let compo:NSArray = findosage.value(forKey: "composition") as? NSArray ?? []
        
        if compo.count == 0{
            cell.infocompos.text = findosage.value(forKey: "medicineName") as? String ?? ""
        }else{
           cell.infocompos.text = compo.componentsJoined(by: ",")
        }
       
       
        
        cell.infomedsize.text = findosage.value(forKey: "pack") as? String
        
        if let mprice = findosage.value(forKey: "price") as? Double
        {
        cell.infomedprice.text = "₹ " + String(format: "%.2f", mprice)
        }
        

        if let count1 = findosage.value(forKey: "isMorning") as? String
        {
            if count1 == "0"
            {
            cell.morview.isHidden = true
            }else{
                cell.morview.isHidden = false
                cell.morview.layer.cornerRadius = 13.0
                cell.morcount.text = String(describing: findosage.value(forKey: "isMorning")!)
                cell.morchrono.text = findosage.value(forKey: "chronology") as! String
                cell.morfreq.text = findosage.value(forKey: "frequency") as! String
                let anSVGImage: SVGKImage = SVGKImage(named: "dawn")
                cell.morimg.image = anSVGImage.uiImage
            }
        }
        
        if let count2 = findosage.value(forKey: "isAfternoon") as? String
        {
            if count2 == "0"
            {
            cell.aftview.isHidden = true
            }else{
                cell.aftview.isHidden = false
                cell.aftview.layer.cornerRadius = 13.0
                cell.aftcount.text = String(describing: findosage.value(forKey: "isAfternoon")!)
                cell.aftchrono.text = findosage.value(forKey: "chronology") as! String
                cell.aftfreq.text = findosage.value(forKey: "frequency") as! String
                let anSVGImage: SVGKImage = SVGKImage(named: "sun")
                cell.aftimg.image = anSVGImage.uiImage
            }
        }
        
        if let count3 = findosage.value(forKey: "isEvening") as? String
        {
            if count3 == "0"
            {
            cell.eveview.isHidden = true
            }else{
                cell.eveview.isHidden = false
                cell.eveview.layer.cornerRadius = 13.0
                cell.evecount.text = String(describing: findosage.value(forKey: "isEvening")!)
                cell.evechrono.text = findosage.value(forKey: "chronology") as! String
                cell.evefreq.text = findosage.value(forKey: "frequency") as! String
                let anSVGImage: SVGKImage = SVGKImage(named: "sunset")
                cell.eveimg.image = anSVGImage.uiImage
            }
        }
        
        if let count4 = findosage.value(forKey: "isNight") as? String
        {
            if count4 == "0"
            {
            cell.nigview.isHidden = true
            }else{
                cell.nigview.isHidden = false
                cell.nigview.layer.cornerRadius = 13.0
                cell.nigcount.text = String(describing: findosage.value(forKey: "isNight")!)
                cell.nigchrono.text = findosage.value(forKey: "chronology") as! String
                cell.nigfreq.text = findosage.value(forKey: "frequency") as! String
                let anSVGImage: SVGKImage = SVGKImage(named: "moon")
                cell.nigimg.image = anSVGImage.uiImage
            }
        }
        
        if let count5 = findosage.value(forKey: "isSos") as? String
        {
            if count5 == "0"
            {
            cell.onlview.isHidden = true
            }
            else{
                cell.onlview.isHidden = false
                cell.onlview.layer.cornerRadius = 13.0
                cell.onlcount.text = String(describing: findosage.value(forKey: "isSos")!)
                cell.onlchrono.text = findosage.value(forKey: "chronology") as! String
                cell.onlfreq.text = findosage.value(forKey: "frequency") as! String
                let anSVGImage: SVGKImage = SVGKImage(named: "Clock")
                cell.onlimg.image = anSVGImage.uiImage
            }
        }
        
        if cell.morview.isHidden == true && cell.aftview.isHidden == true && cell.eveview.isHidden == true && cell.nigview.isHidden == true && cell.onlview.isHidden == true
        {
           cell.scroheight.constant = 0
        }
        else
        {
         cell.scroheight.constant = 135
        }
        
        cell.minqty.addTarget(self, action: #selector(minqty), for: .touchUpInside)
        
        cell.addqty.addTarget(self, action: #selector(addqty), for: .touchUpInside)
        
        cell.addcartbtno.addTarget(self, action: #selector(addcart), for: .touchUpInside)
        cell.addcartbtno.layer.borderWidth = 0.8
        cell.addcartbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        cell.addcartbtno.layer.cornerRadius = 4.0
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func addcart(sender:UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.InfoMedTable)
        let indexPath = self.InfoMedTable.indexPathForRow(at:buttonPosition)
        let cell = self.InfoMedTable.cellForRow(at: indexPath!) as! InfoMedicineTableViewCell
        cell.minqty.isHidden = false
        cell.addqty.isHidden = false
        cell.qtylbl.isHidden = false
        cell.addcartbtno.isHidden = true
        addfinstickyviu.isHidden = false
        addfinstickyviu.alpha = 1
        
        let variantdicts:NSDictionary = MyMed[sender.tag] as! NSDictionary
        
        if dictionaryA.count == 0
        {
            dictionaryA = ["productCode":String(),"quantity":String(),"pack":String(),"medicineName":String()]
        }
        else
        {
            dictionaryA.updateValue(String(describing:variantdicts.value(forKey: "productCode")!), forKey: "productCode")
            dictionaryA.updateValue("1", forKey: "quantity")
            dictionaryA.updateValue(variantdicts.value(forKey: "medicineName") as! String, forKey: "medicineName")
            dictionaryA.updateValue(variantdicts.value(forKey: "pack") as! String, forKey: "pack")
            // dictionaryA.updateValue(variantdicts.value(forKey: "price") as! String, forKey: "price")
        }
        ArrayB.append(dictionaryA)
        
        
    }
    
    
    @objc func addqty(sender:UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.InfoMedTable)
        let indexPath = self.InfoMedTable.indexPathForRow(at:buttonPosition)
        let cell = self.InfoMedTable.cellForRow(at: indexPath!) as! InfoMedicineTableViewCell
        
        let variantdicts:NSDictionary = MyMed[sender.tag] as! NSDictionary
        let vmediname:String = variantdicts.value(forKey: "medicineName") as! String
        
        var quantity = Int(cell.qtylbl.text!)!
        
        quantity = quantity + 1
        cell.qtylbl.text = String(describing: quantity)
        
        for i in 0..<ArrayB.count
        {
            if ArrayB[i].values.contains(vmediname){
                
                ArrayB[i].updateValue(cell.qtylbl.text!, forKey: "quantity")
                print(ArrayB)
            } else {
                // does not contain key
            }
        }
        
       // InfoMedTable.reloadData()
    }
    
    @objc func minqty(sender:UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.InfoMedTable)
        let indexPath = self.InfoMedTable.indexPathForRow(at:buttonPosition)
        let cell = self.InfoMedTable.cellForRow(at: indexPath!) as! InfoMedicineTableViewCell
        
        let variantdicts:NSDictionary = MyMed[sender.tag] as! NSDictionary
        let vmediname:String = variantdicts.value(forKey: "medicineName") as! String
        
        var quantity = Int(cell.qtylbl.text!)!
        
        if quantity == 1
        {
          cell.addcartbtno.isHidden = false
          cell.minqty.isHidden = true
          cell.addqty.isHidden = true
          cell.qtylbl.isHidden = true
          addfinstickyviu.isHidden = true
          addfinstickyviu.alpha = 0
            
            if ArrayB[sender.tag].values.contains(vmediname){
                
                ArrayB.remove(at: sender.tag)
                print(ArrayB)
            } else {
                // does not contain key
            }
            
        }else
        {
            quantity = quantity - 1
            
            for i in 0..<ArrayB.count
            {
                if ArrayB[i].values.contains(vmediname){
                    
                    ArrayB[i].updateValue(String(describing: quantity), forKey: "quantity")
                    print(ArrayB)
                    
                } else {
                    // does not contain key
                }
            }
            
            cell.qtylbl.text = String(describing: quantity)
        }
        
       // InfoMedTable.reloadData()
    }
    
    
    @IBAction func finalizeorder(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("MedInfoFinalize"), object: ArrayB)
     
       UserDefaults.standard.set(ArrayB, forKey: "MedicineArr")
       UserDefaults.standard.synchronize()
               
               if UserDefaults.standard.value(forKey:"custname") == nil || UserDefaults.standard.value(forKey:"custname") as? String == ""
               {
               let vc = storyboard?.instantiateViewController(withIdentifier: "PeronalinfoViewController") as! PeronalinfoViewController
               navigationController?.pushViewController(vc, animated: false)
               }
               else if UserDefaults.standard.value(forKey:"isAddAvail") as? String == "No"
               {
                  let vc = storyboard?.instantiateViewController(withIdentifier: "deliveryPincodeViewController") as! deliveryPincodeViewController
                  navigationController?.pushViewController(vc, animated: false)
               }
               else
               {
                   //Address Listing
                   let vc = storyboard?.instantiateViewController(withIdentifier: "addressListViewController") as! addressListViewController
                   navigationController?.pushViewController(vc, animated: false)
               }
    }
    
    
    @IBAction func addmoreorder(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("MedInfoAddMore"), object: ArrayB)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
 
    
    func GetInfo()
    {
        if Connectivity.isConnectedToInternet {
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let para2 = ["":""]
        
        let url =  GetInfoOnMedicines()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: value))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
    
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                
                    DispatchQueue.main.async {
                        let httpResponse = response as? HTTPURLResponse
                        
                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                        {
                            print(json)
                            
                            let infomed:NSDictionary = json as NSDictionary
                            self.MyMed = infomed.value(forKey: "MyMedicines") as! NSArray
                            if self.MyMed.count == 0
                            {
                                self.InfoMedTable.isHidden = true
                            }
                            
                            DispatchQueue.main.async {
                                self.InfoMedTable.reloadData()
                            }
                        }
                        else
                        {
                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
}



