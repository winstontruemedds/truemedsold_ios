//
//  ArticlesViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import TTGTagCollectionView
import SDWebImage
import Reachability
import Alamofire

class ArticlesViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,TTGTextTagCollectionViewDelegate{
    
    @IBOutlet weak var taglistheight: NSLayoutConstraint!
    
    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var tagview: TTGTextTagCollectionView!
   
    var articlesarr = NSArray()
    
    var categoryarr = NSArray()
    
    var categoryListarr = [String]()
   
    @IBOutlet weak var articlestable: UICollectionView!
    
    @IBOutlet weak var articleheight: NSLayoutConstraint!
    
    @IBOutlet weak var expandbtno: UIButton!
    
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    var filterarr = NSArray()
    
    var isActive:Bool!
    
    var indArr = [Int]()
    
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SDImageCache.shared.clearMemory()
        SDImageCache.shared.clearDisk()
        

        getArticles()
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "arrow_down")
        expandbtno.setImage(svgimg2.uiImage, for: .normal)
        
        DispatchQueue.main.async {
        
            self.tagview.alignment = .center
            self.tagview.scrollView.isScrollEnabled = false
        
            var config: TTGTextTagConfig? = self.tagview.defaultConfig
        config?.textFont = UIFont.boldSystemFont(ofSize: 13.0)
        
        config?.textColor = UIColor.black
        config?.selectedTextColor = UIColor.white
        
        config?.backgroundColor = UIColor.white
        config?.selectedBackgroundColor = UIColor(hexString: "#22B573")
        
        config?.shadowOffset = CGSize(width: 0.0, height: 0.0)
        
        config?.borderColor = UIColor(hexString: "#22B573")
        config?.selectedBorderColor = UIColor(hexString: "#22B573")
        config?.borderWidth = 0.8
        config?.selectedBorderWidth = 0.8
        
        config?.exactHeight = 33
        
        self.tagview.delegate = self
        
        self.tagview.reload()
        }

    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        articleheight.constant = articlestable.collectionViewLayout.collectionViewContentSize.height
        
        self.view.layoutIfNeeded()
    }
    
    
    func textTagCollectionView(_ textTagCollectionView: TTGTextTagCollectionView!, didTapTag tagText: String!, at index: UInt, selected: Bool, tagConfig config: TTGTextTagConfig!) {
        
       // tagview.selectionLimit = 1
        

        if indArr.contains(Int(index))
        {
            DispatchQueue.main.async {
                if let indexx = self.indArr.firstIndex(of: Int(index)) {
                    
                    self.indArr.remove(at: indexx)
            }
                
                self.isActive = false
            
            let heightss = self.articlestable.collectionViewLayout.collectionViewContentSize.height
            self.articleheight.constant = heightss
             self.articlestable.reloadData()
             self.tagview.reload()
             self.view.layoutIfNeeded()
            }
        }
        else
        {
            DispatchQueue.main.async {
                
                self.indArr.removeAll()
                
                self.indArr.append(Int(index))
                
                let catsdict:NSDictionary = self.categoryarr[Int(index)] as! NSDictionary
                let catids:CLong = catsdict.value(forKey: "id") as! CLong
                
                self.filterarr = self.articlesarr.filter { (($0 as! NSDictionary)["categoryId"] as! CLong) == catids } as NSArray
                
                self.isActive = true
               
               let heightss = self.articlestable.collectionViewLayout.collectionViewContentSize.height
               self.articleheight.constant = heightss
                self.articlestable.reloadData()
                self.tagview.reload()
               self.view.layoutIfNeeded()
            }
            
            
        }
    }

    
    
    @IBAction func expandtaglist(_ sender: Any) {
       
        if taglistheight.constant == 40
        {
           taglistheight.constant = tagview.contentSize.height
            let svgimg3: SVGKImage = SVGKImage(named: "arrow_up")
            expandbtno.setImage(svgimg3.uiImage, for: .normal)
        }
        else
        {
         taglistheight.constant = 40
            let svgimg2: SVGKImage = SVGKImage(named: "arrow_down")
            expandbtno.setImage(svgimg2.uiImage, for: .normal)
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var counts:Int!
        
        if isActive == true
        {
        counts = filterarr.count
        }
        else
        {
        counts = articlesarr.count
        }
        
        return counts
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "artcollectioncell", for: indexPath) as! ImageCollectionViewCell
        cell.artimg.layer.cornerRadius = 8.0
        cell.artimg.clipsToBounds = true
        cell.artblur.layer.cornerRadius = 8.0
        cell.artblur.clipsToBounds = true
        
        if isActive == true
        {
            let finartList:NSDictionary = filterarr[indexPath.row] as! NSDictionary
            cell.artauthor.text = finartList.value(forKey: "author") as! String
            let imgurl:URL = URL(string: finartList.value(forKey: "image") as! String)!
            cell.artimg.sd_setImage(with: imgurl, completed: nil)
            cell.arttitle.text = finartList.value(forKey: "name") as! String
            
            cell.artdatelbl.text = finartList.value(forKey: "createdOn") as? String
            
            
            return cell
        }
        else
        {
        
        let finartList:NSDictionary = articlesarr[indexPath.row] as! NSDictionary
        cell.artauthor.text = finartList.value(forKey: "author") as! String
        let imgurl:URL = URL(string: finartList.value(forKey: "image") as! String)!
        cell.artimg.sd_setImage(with: imgurl, completed: nil)
        cell.arttitle.text = finartList.value(forKey: "name") as! String
        
        cell.artdatelbl.text = finartList.value(forKey: "createdOn") as? String
       
        return cell
            
        }
        
        //return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isActive == true
        {
            let finartList:NSDictionary = filterarr[indexPath.row] as! NSDictionary
            let type:CLong = finartList.value(forKey: "type") as! CLong
            if type == 1
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ArticlesDetailsViewController") as? ArticlesDetailsViewController
                vc!.articlesdict = filterarr[indexPath.row] as! NSDictionary
                self.navigationController?.pushViewController(vc!, animated: true)
            }else
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
                vc!.articlesdict = filterarr[indexPath.row] as! NSDictionary
                self.navigationController?.pushViewController(vc!, animated: true)
            }
        }
        else{
        let finartList:NSDictionary = articlesarr[indexPath.row] as! NSDictionary
        let type:CLong = finartList.value(forKey: "type") as! CLong
        if type == 1
        {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ArticlesDetailsViewController") as? ArticlesDetailsViewController
         vc!.articlesdict = articlesarr[indexPath.row] as! NSDictionary
        self.navigationController?.pushViewController(vc!, animated: true)
        }else
        {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
        vc!.articlesdict = articlesarr[indexPath.row] as! NSDictionary
        self.navigationController?.pushViewController(vc!, animated: true)
        }
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.bounds.size.width, height: 140)
    }
    
    
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    
    func getArticles()
    {
        if Connectivity.isConnectedToInternet {
        
        actInd.startAnimating()
        
        let url = GetArticles()
        
        let serviceurl = URL(string: url)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: serviceurl!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    let artlistdict:NSDictionary = json as NSDictionary
                    let resultdict:NSDictionary = artlistdict.value(forKey: "result") as! NSDictionary
                    self.articlesarr = resultdict.value(forKey: "article") as! NSArray
                    self.categoryarr = resultdict.value(forKey: "category") as! NSArray
                   
                        DispatchQueue.main.async {
                    
                        self.articlesarr = resultdict.value(forKey: "article") as! NSArray
                        self.categoryarr = resultdict.value(forKey: "category") as! NSArray
                        if self.articlesarr.count == 0
                        {
                            self.articlestable.isHidden = true
                        }
                        else
                        {
                            self.articlestable.isHidden = false
                        }
                        
                        self.articlestable.reloadData()
                            
                        }
                        
                    for i in 0..<self.categoryarr.count
                    {
                        let catdict:NSDictionary = self.categoryarr[i] as! NSDictionary
                        self.categoryListarr.append(catdict.value(forKey: "name") as! String)
                    }
                    
                    DispatchQueue.main.async {
                    self.tagview.addTags(self.categoryListarr)
                            
                    self.tagview.reload()
                    
                        
//                    self.tagview.addTags(self.categoryListarr)
//
//                    self.tagview.reload()

                    //self.articleheight.constant = self.articlestable.contentSize.height
                        
                    self.actInd.stopAnimating()
                        }
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                    }
                
            } catch let error {
                print(error.localizedDescription)
            }
            }
            
        })
        
        DispatchQueue.main.async {
        self.articlestable.reloadData()
        self.tagview.reload()
        }
        
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
}
