//
//  OrderHelpViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import SideMenu

class OrderHelpViewController: UIViewController {

    @IBOutlet weak var statusbtno: UIButton!
    
    @IBOutlet weak var billbtno: UIButton!
    
    @IBOutlet weak var summbtno: UIButton!
    
    @IBOutlet weak var dosagebtno: UIButton!
    
    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var menubtno: UIButton!
    
    @IBOutlet weak var tmlogoimg: UIImageView!
    
    @IBOutlet weak var editorderbtno: UIButton!
    
    var odhelpdict = NSDictionary()
    
    @IBOutlet weak var helptypeques: UILabel!
    
    @IBOutlet weak var dosheight: NSLayoutConstraint!
    
    @IBOutlet weak var sumheight: NSLayoutConstraint!
    
     var statusID:CLong = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
           NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforNoitemFoundOnSumm(notification:)), name: Notification.Name("noItemFoundOnsummary"), object: nil)

        let svgimg: SVGKImage = SVGKImage(named: "logo-svg")
        tmlogoimg.image = svgimg.uiImage
        
        let svgimg2: SVGKImage = SVGKImage(named: "menu")
        menubtno.setImage(svgimg2.uiImage, for: .normal)
        
        let svgimg3: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg3.uiImage, for: .normal)
        
        
//        let menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! UISideMenuNavigationController
//
//
//        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
        
       
  //      SideMenuManager.default.menuFadeStatusBar = false
    
        statusbtno.layer.borderWidth = 0.8
        statusbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        statusbtno.layer.cornerRadius = 4.0
        
        billbtno.layer.borderWidth = 0.8
        billbtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        billbtno.layer.cornerRadius = 4.0
        
        summbtno.layer.borderWidth = 0.8
        summbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        summbtno.layer.cornerRadius = 4.0
        
        dosagebtno.layer.borderWidth = 0.8
        dosagebtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        dosagebtno.layer.cornerRadius = 4.0
        
        editorderbtno.layer.borderWidth = 0.8
        editorderbtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        editorderbtno.layer.cornerRadius = 4.0
        
        
        summbtno.addTarget(self, action: #selector(toSummary), for: .touchUpInside)
        
        billbtno.addTarget(self, action: #selector(toBill), for: .touchUpInside)
        
        statusbtno.addTarget(self, action: #selector(toStatus), for: .touchUpInside)
        
        dosagebtno.addTarget(self, action: #selector(toDosage), for: .touchUpInside)
        
        editorderbtno.addTarget(self, action: #selector(toEditOrder), for: .touchUpInside)
        
        
        
        if  odhelpdict.value(forKey: "orderStatusId") != nil{
          statusID = odhelpdict.value(forKey: "orderStatusId") as! CLong
        }
        if odhelpdict.value(forKey: "statusId") != nil{
           statusID = odhelpdict.value(forKey: "statusId") as! CLong
        }
        
    
        
        
        if statusID == 81
        {
        billbtno.isHidden = true
        summbtno.isHidden = true
         dosagebtno.isHidden = true
            editorderbtno.isHidden = false
            
            statusbtno.isHidden = false
            
           
            
            let mastdict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
            let dict:NSDictionary = mastdict.value(forKey: "29") as! NSDictionary
            
            let text:String = dict.value(forKey: "question") as! String
            
            helptypeques.text = text.replacingOccurrences(of: "\\n", with: "\n")
            let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
            let btn1:NSDictionary = btnarr[0] as! NSDictionary
            let btn2:NSDictionary = btnarr[1] as! NSDictionary
            
            statusbtno.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
            editorderbtno.setTitle(btn2.value(forKey: "text") as! String, for: .normal)
            
        }
            
        if statusID == 1 || statusID == 2 || statusID == 39 || statusID == 57  {
            editorderbtno.isHidden = true
            dosagebtno.isHidden = true
            statusbtno.isHidden = false
            billbtno.isHidden = true
            summbtno.isHidden = false
            
               let mastdict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
                     let dict:NSDictionary = mastdict.value(forKey: "28") as! NSDictionary
                     
                     let text:String = dict.value(forKey: "question") as! String
                     
                     helptypeques.text = text.replacingOccurrences(of: "\\n", with: "\n")
                     let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
                     let btn1:NSDictionary = btnarr[0] as! NSDictionary
                  
                     let btn3:NSDictionary = btnarr[2] as! NSDictionary
                   
                     
                     statusbtno.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
                     
                     summbtno.setTitle(btn3.value(forKey: "text") as! String, for: .normal)
                    
                       
            
            
        }
        if statusID != 1 && statusID != 2 && statusID != 39 && statusID != 57 && statusID != 81
        {
         editorderbtno.isHidden = true
         dosagebtno.isHidden = false
         statusbtno.isHidden = false
         billbtno.isHidden = false
         summbtno.isHidden = false

            let mastdict:NSDictionary = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
            let dict:NSDictionary = mastdict.value(forKey: "28") as! NSDictionary

            let text:String = dict.value(forKey: "question") as! String

            helptypeques.text = text.replacingOccurrences(of: "\\n", with: "\n")
            let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
            let btn1:NSDictionary = btnarr[0] as! NSDictionary
            let btn2:NSDictionary = btnarr[1] as! NSDictionary
            let btn3:NSDictionary = btnarr[2] as! NSDictionary
            let btn4:NSDictionary = btnarr[3] as! NSDictionary

            statusbtno.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
            billbtno.setTitle(btn2.value(forKey: "text") as! String, for: .normal)
            summbtno.setTitle(btn3.value(forKey: "text") as! String, for: .normal)
            dosagebtno.setTitle(btn4.value(forKey: "text") as! String, for: .normal)

        }
    }
    

    
    
    @objc func NotificationReceivedforNoitemFoundOnSumm(notification: Notification) {
          let localizedContent = NSLocalizedString("No Items Found", comment: "")
                                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
        
         }
    
    
    
    @objc func toSummary()
    {
        
         if statusID == 1 || statusID == 2 || statusID == 39 || statusID == 57  {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "summaryViewController") as? summaryViewController
                       vc?.odhelpdictsumm = odhelpdict
                       self.navigationController?.pushViewController(vc!, animated: true)
         }else{
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderDescriptionViewController") as? OrderDescriptionViewController
            vc?.odhelpdict3 = odhelpdict
            self.navigationController?.pushViewController(vc!, animated: true)
        }

    }
    
    @objc func toBill()
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
        vc?.ordertype = "Current Order"
        vc?.odhelpdict2 = odhelpdict
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func toDosage()
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DosageViewController") as? DosageViewController
        vc?.odhelpdict5 = odhelpdict
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @objc func toStatus()
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StatusViewController") as? StatusViewController
        vc?.odhelpdict4 = odhelpdict
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func toEditOrder()
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditOrderViewController") as? EditOrderViewController
        vc?.odhelpdict6 = odhelpdict
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func backto(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func openmenu(_ sender: Any) {
          let menu = storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
              
               let menu2 = SideMenuNavigationController(rootViewController: menu)
              
               menu2.statusBarEndAlpha = 0
              
               present(menu2, animated: true, completion: nil)
    }
    
    
}
