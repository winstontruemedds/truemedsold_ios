//
//  SelectOrderTicketViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class SelectOrderTicketViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var selectedItems = [Int]()
    
    let reachability = Reachability()!
    
    @IBOutlet weak var bottomheight: NSLayoutConstraint!
    
    @IBOutlet weak var raiseview: UIView!
    
    @IBOutlet weak var mainraiseviu: UIView!
    
    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var SOTtableview: UITableView!
    
    var marr = [Any]()
    
    @IBOutlet weak var orraisebtno: UIButton!
    
    @IBOutlet weak var emailo: UIButton!
    
    @IBOutlet weak var callo: UIButton!
    
    var selecteddict = NSDictionary()
    
    var helpbillmedsarr = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailo.layer.cornerRadius = 4.0
        emailo.layer.borderColor = UIColor.white.cgColor
        emailo.layer.borderWidth = 0.8
        
        callo.layer.cornerRadius = 4.0
        callo.layer.borderColor = UIColor.white.cgColor
        callo.layer.borderWidth = 0.8
        
        orraisebtno.layer.cornerRadius = 4.0
        
        raiseview.layer.cornerRadius = 4.0

        raiseview.isHidden = true
        
        bottomheight.constant = 20
        
        mainraiseviu.isHidden = true
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "email")
        emailo.setImage(svgimg2.uiImage, for: .normal)
        
        let svgimg3: SVGKImage = SVGKImage(named: "call")
        callo.setImage(svgimg3.uiImage, for: .normal)
        
        GetOrderBill()
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helpbillmedsarr.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectOrderTicketTableViewCell") as! SelectOrderTicketTableViewCell
         let mydict = helpbillmedsarr[indexPath.row] as! NSDictionary
        
        
        //cell.medordcheck.setImage(UIImage(named: ""), for: .normal)
        
        cell.mednlbl.text = mydict.value(forKey: "substituteProductName") as! String
        cell.mednoslbl.text = mydict.value(forKey: "substituteProductOty") as! String
        cell.medamt.text =  "₹ " + String(describing:mydict.value(forKey: "substituteProductPrice")!)
        cell.medordcheck.addTarget(self, action: #selector(checkmed), for: .touchUpInside)
        cell.medordcheck.tag = indexPath.row
        
        if (selectedItems.contains(indexPath.row)) {
            cell.medordcheck.setBackgroundImage(UIImage(named: "tick"), for: .normal)
        }
        else {
            cell.medordcheck.setBackgroundImage(UIImage(named: "untick"), for: .normal)
        }
    
        return cell
    }

    
    @IBAction func raiseQuery(_ sender: Any) {
        UIView.animate(withDuration: 1.0, animations: {
            
            if self.bottomheight.constant == 20
            {
                self.bottomheight.constant = 137
                self.raiseview.isHidden = false
            }
            else
            {
                self.bottomheight.constant = 20
                self.raiseview.isHidden = true
            }
            
        }, completion:nil)
    }
    

    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func checkmed(sender:UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.SOTtableview)
        let indexPath = self.SOTtableview.indexPathForRow(at:buttonPosition)
        let cell = self.SOTtableview.cellForRow(at: indexPath!) as! SelectOrderTicketTableViewCell
        
        if (self.selectedItems.contains(sender.tag)) {
            let index = self.selectedItems.firstIndex(of: sender.tag)
            self.selectedItems.remove(at: index!)
        }
        else {
            self.selectedItems.append(sender.tag)
        }
        
        if cell.medordcheck.backgroundImage(for: .normal) == UIImage(named: "untick")
        {
          let mydict2 = helpbillmedsarr[sender.tag] as! NSDictionary
          marr.append(mydict2)
         if marr.count >= 1
         {
           mainraiseviu.isHidden = false
         }
         else
         {
           mainraiseviu.isHidden = true
          }
            
            cell.medordcheck.setBackgroundImage(UIImage(named: "tick"), for: .normal)
        }
        else
        {
          marr.remove(at: sender.tag)
            if marr.count == 0
            {
                mainraiseviu.isHidden = true
            }
            else
            {
                mainraiseviu.isHidden = false
            }
            cell.medordcheck.setBackgroundImage(UIImage(named: "untick"), for: .normal)
        }
        SOTtableview.reloadData()
    }
    
    
    @IBAction func emailus(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "IssueDetailsViewController") as? IssueDetailsViewController
        vc?.passarr = marr
        vc?.cnctype = "email"
        vc?.selecteddict2 = selecteddict
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func callus(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "IssueDetailsViewController") as? IssueDetailsViewController
        vc?.passarr = marr
        vc?.cnctype = "call"
        vc?.selecteddict2 = selecteddict
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    func GetOrderBill()
    {
        if Connectivity.isConnectedToInternet {
        
        let orderID:CLong = selecteddict.value(forKey: "orderId") as! CLong
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["orderId":String(describing: orderID)]
    
        print(parameters)
        
        let url =  GetOrderBills()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    let orderbilldict:NSDictionary = json as NSDictionary
                    if orderbilldict.object(forKey: "error") != nil
                    {
                        
                    }
                    else{
                        let mybill:NSDictionary = orderbilldict.value(forKey: "bill") as! NSDictionary
                        self.helpbillmedsarr = mybill.value(forKey: "medicines") as! NSArray
                    }
 
                    DispatchQueue.main.async {
                        self.SOTtableview.reloadData()
                        self.SOTtableview.tableFooterView = UIView()
                    }
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    
}
    
}
