//
//  AgeSelectVC.swift
//  True_Meds
//


import UIKit
import SVGKit

class AgeSelectVC: UIViewController {

    @IBOutlet weak var agetext: UILabel!
    
    @IBOutlet weak var aseniorbtno: UIButton!
    
    @IBOutlet weak var anotseniorbtno: UIButton!
    
    var langID:CLong!
    
    var langDict = NSDictionary()
    
    @IBOutlet weak var ageBgimg: UIImageView!
    
    @IBOutlet weak var backbtno: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let svgimg: SVGKImage = SVGKImage(named: "arrback")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        let selagelan:NSDictionary = langDict.value(forKey: "questions") as! NSDictionary
        let aques:NSDictionary = selagelan.value(forKey:"2") as! NSDictionary
        let btnarr:NSArray = aques.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        let text:String = aques.value(forKey: "question") as! String
        agetext.text = text.replacingOccurrences(of: "\\n", with: "\n")
        aseniorbtno.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
        anotseniorbtno.setTitle(btn2.value(forKey: "text") as! String, for: .normal)
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        aseniorbtno.layer.borderWidth = 0.8
        aseniorbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        aseniorbtno.layer.cornerRadius = 4.0
        
        anotseniorbtno.layer.borderWidth = 0.8
        anotseniorbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        anotseniorbtno.layer.cornerRadius = 4.0
        
        let appintro:NSDictionary = langDict.value(forKey: "appIntro") as! NSDictionary
        let imgstr:String = appintro.value(forKey: "screenAge") as! String
        let imgurl:URL = URL(string: imgstr)!
        ageBgimg.sd_setImage(with: imgurl, completed: nil)
        
        let menuItems:NSArray = langDict.value(forKey: "menuItems") as! NSArray
        let newuser:NSArray = menuItems.filter{!(($0 as! NSDictionary)["login"] as! Bool)} as NSArray
        
        UserDefaults.standard.set(newuser, forKey: "menunew")
        UserDefaults.standard.set(menuItems, forKey: "menulogin")
        UserDefaults.standard.synchronize()
        
    }
    
    
    @IBAction func nosenioract(_ sender: Any) {
        let NewVC = self.storyboard?.instantiateViewController(withIdentifier: "WalkthroughViewController") as? WalkthroughViewController
        NewVC?.langDict = langDict
        NewVC?.agegroupID = 41
        NewVC?.langID = langID
        self.navigationController?.pushViewController(NewVC!, animated: true)
    }
    
    
    @IBAction func senioract(_ sender: Any) {
        let NewVC = self.storyboard?.instantiateViewController(withIdentifier: "WalkthroughViewController") as? WalkthroughViewController
        NewVC?.langDict = langDict
        NewVC?.agegroupID = 40
        NewVC?.langID = langID
        self.navigationController?.pushViewController(NewVC!, animated: true)
    }
    
    
    @IBAction func backto(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    
    
    
}
