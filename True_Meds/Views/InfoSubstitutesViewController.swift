//
//  InfoSubstitutesViewController.swift
//  True_Meds
//
//  Created by Office on 12/9/19.
//

import UIKit
import SVGKit

class InfoSubstitutesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var backbtno: UIButton!
    
    var MyMed = NSArray()
    
    var findosage = NSDictionary()
    
    var resdict = NSDictionary()
    
    @IBOutlet weak var subsTable: UITableView!
    
    var SorderId:CLong!
    
    var recommended = NSDictionary()
    
    var prescribed = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        getInfoSubsMed()
    }

    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MyMed.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        findosage = self.MyMed[indexPath.row] as! NSDictionary
        recommended = findosage.value(forKey: "recommended") as! NSDictionary
        let pcode:String = recommended.value(forKey: "productCode") as? String ?? ""
        prescribed = findosage.value(forKey: "prescribed") as! NSDictionary
        let pcode2:String = prescribed.value(forKey: "productCode") as? String ?? ""
        
        if pcode == pcode2
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoSubstitutesTableViewCell2") as! InfoSubstitutesTableViewCell
            cell.infocardviu.layer.cornerRadius = 4.0
            cell.infocardviu.layer.borderWidth = 1.0
            cell.infocardviu.layer.borderColor = UIColor(hexString: "#CCCCCC", alpha: 1.0).cgColor
            cell.infocardviu.clipsToBounds = true
            cell.compositionviu.layer.cornerRadius = 4.0
            cell.compositionviu.clipsToBounds = true
            
            let compo:NSArray = findosage.value(forKey: "composition") as? NSArray ?? []
          
            
          
                  
                  if compo.count == 0{
                     cell.compositionlbl.text = prescribed.value(forKey: "medicineName") as? String ?? ""
                  }else{
                       cell.compositionlbl.text = compo.componentsJoined(by: " + ")
                  }
           
            cell.presmedcompany.text = prescribed.value(forKey: "companyName") as? String ?? ""
            cell.presmedname.text = prescribed.value(forKey: "medicineName") as? String ?? ""
            if let price = prescribed.value(forKey: "price") as? Double
            {
             cell.presmedprice.text = "₹ " + String(format: "%.2f", price)
            }
            
            //cell.presmedprice.text = "₹ " + String(describing: prescribed.value(forKey: "price")!)
            //cell.presmedisprice.text = "₹ " + String(describing: prescribed.value(forKey: "discountedPrice")!)
            
            if let disprice = prescribed.value(forKey: "discountedPrice") as? Double
            {
             cell.presmedisprice.text = "₹ " + String(format: "%.2f", disprice)
            }
            
            return cell
            
        }
        else
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoSubstitutesTableViewCell") as! InfoSubstitutesTableViewCell
       
        cell.infocardviu.layer.cornerRadius = 4.0
        cell.infocardviu.layer.borderWidth = 1.0
        cell.infocardviu.layer.borderColor = UIColor(hexString: "#CCCCCC", alpha: 1.0).cgColor
        cell.infocardviu.clipsToBounds = true
        cell.compositionviu.layer.cornerRadius = 4.0
        cell.compositionviu.clipsToBounds = true
        
        let compo:NSArray = findosage.value(forKey: "composition") as? NSArray ?? []
        cell.compositionlbl.text = compo.componentsJoined(by: " + ")
        
        cell.recmedcompany.text = recommended.value(forKey: "companyName") as? String ?? ""
        cell.recmedname.text = recommended.value(forKey: "medicineName") as? String ?? ""
        //cell.recmedprice.text = "₹ " + String(describing: recommended.value(forKey: "price")!)
            
        if let price = recommended.value(forKey: "price") as? Double
        {
        cell.recmedprice.text = "₹ " + String(format: "%.2f", price)
        }
            
        //cell.recmedisprice.text = "₹ " + String(describing: recommended.value(forKey: "discountedPrice")!)
            
            if let disprice = recommended.value(forKey: "discountedPrice") as? Double
            {
            cell.recmedisprice.text = "₹ " + String(format: "%.2f", disprice)
            }
        
        cell.presmedcompany.text = prescribed.value(forKey: "companyName") as? String ?? ""
        cell.presmedname.text = prescribed.value(forKey: "medicineName") as? String ?? ""
        //cell.presmedprice.text = "₹ " + String(describing: prescribed.value(forKey: "price")!)
            
        if let price2 = prescribed.value(forKey: "price") as? Double
        {
        cell.presmedprice.text = "₹ " + String(format: "%.2f", price2)
        }
            
       // cell.presmedisprice.text = "₹ " + String(describing: prescribed.value(forKey: "discountedPrice")!)
            
        if let disprice2 = prescribed.value(forKey: "discountedPrice") as? Double
            {
            cell.presmedisprice.text = "₹ " + String(format: "%.2f", disprice2)
            }
        
        return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 345.0
    }
    
    func getInfoSubsMed()
        {
            let parameters = ["orderId": SorderId!] as [String : Any]
            print(parameters)
            ApiManager().requestApiWithDataType( methodType: HGET, urlString:GetInfoOnSubstitutes(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
                
                print(response)
                
                if cStatus == 200 || cStatus == 201
                {
                self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
                print(self.resdict)
                self.MyMed = self.resdict.value(forKey: "MyMedicines") as! NSArray
                if self.MyMed.count == 0
                {
                self.subsTable.isHidden = true
                }
                DispatchQueue.main.async {
                self.subsTable.reloadData()
                }
                }
                else if cStatus == 401
                {
                    let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                    Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                }
                else if cStatus == 400
                {
                    let adddict:NSDictionary = convertStringToDictionary(json: response as! String)! as NSDictionary
                    if adddict.allKeys.first as! String == "400"
                    {
                        Utility.showAlertWithTitle(title: "", andMessage: adddict.value(forKey: "400") as! String, onVC: self)
                    }
                 }
                else if cStatus == 500
                {
                    let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                    self.addChild(popupVC)
                    popupVC.view.frame = self.view.frame
                    self.view.addSubview(popupVC.view)
                    popupVC.didMove(toParent: self)
                }
                else
                {
                    let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                }
            }
        }
    
    
}
