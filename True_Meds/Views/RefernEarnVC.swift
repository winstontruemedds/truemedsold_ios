//
//  RefernEarnVC.swift
//  True_Meds
//


struct InvitedContact {
    let contact: CNContact
}


import UIKit
import SVGKit
import Contacts
import Reachability
import Alamofire

class RefernEarnVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
     let store = CNContactStore()
    
    @IBOutlet weak var progressOfContact: UIProgressView!
    
    var selectedItems = [Int]()
    
    var selectedCNCs = [String]()
    
    let reachability = Reachability()!
 
    @IBOutlet weak var registeredcountlbl: UIButton!
    
    @IBOutlet weak var backbtno: UIButton!
    
    var contactsarr = ["name":String(),"number":String()]
    
    var listcncarr = [[String:String]]()
    
    var refercncarr = [[String:String]]()
    
    var favoritableContacts = [InvitedContact]()
    
    var fetcnc = [String]()
    
    @IBOutlet weak var cncList: UITableView!
    
    @IBOutlet weak var installcnclist: UITableView!
    
    @IBOutlet weak var SIpopviu: UIView!
    
    @IBOutlet weak var mulinvitebtn: UIButton!
    
    @IBOutlet weak var searchbarr: UISearchBar!
    
    var searchActive = false
    
    var filterArray = NSMutableArray()
    
    var filterArray2 = NSMutableArray()
    
    var filterArray3 = NSMutableArray()
    
    var notInstall = NSArray()
    
    var Invited = NSArray()
    
    var Reginstalled = NSArray()
    
    var someProtocol = [String : Int]()
    
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    var checked = [Bool]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
//         if UserDefaults.standard.object(forKey: "access_token") != nil
//         {
//             store.requestAccess(for: .contacts) { (granted, err) in
//                       if let err = err {
//                           print("Failed to request access:", err)
//                           return
//                       }
//             }
//
//         }
        
        progressOfContact.transform = progressOfContact.transform.scaledBy(x: 1, y: 8)
        progressOfContact.progress = 0.0
        progressOfContact.layer.cornerRadius = 10
        progressOfContact.clipsToBounds = true
        progressOfContact.layer.sublayers![1].cornerRadius = 10
        progressOfContact.subviews[1].clipsToBounds = true
        
        
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        mulinvitebtn.layer.borderWidth = 0.8
        mulinvitebtn.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        mulinvitebtn.layer.cornerRadius = 4.0

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        DispatchQueue.main.async {
            self.fetchContacts()
            
            if self.listcncarr.count != 0
            {
                self.SyncCNC()
            }
        }
        
        
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    
    func applicationDidReceiveMemoryWarning(application: UIApplication) {
        URLCache.shared.removeAllCachedResponses()
    }

    
    func searchBar(_ searchBar: UISearchBar, textDidChange textSearched: String)
    {
        if installcnclist.isHidden == false
        {
            let dict1 = Reginstalled.filter { dict in
                let dictionaryDataTable = convertDictionaryToString(json: dict)
                
                if((dictionaryDataTable?.contains(textSearched, options: .caseInsensitive))!)
                {
                    return true
                }
                
                return false
            }
            
            filterArray.removeAllObjects()
            filterArray = NSMutableArray(array: dict1)
            
            searchActive = true
            
            self.installcnclist.reloadData()

        }
        else
        {
            let dict2 = Invited.filter { dict in
                let dictionaryDataTable = convertDictionaryToString(json: dict)
                
                if((dictionaryDataTable?.contains(textSearched, options: .caseInsensitive))!)
                {
                    return true
                }
                
                return false
            }
            
            filterArray2.removeAllObjects()
            filterArray2 = NSMutableArray(array: dict2)
            
            
            let dict3 = notInstall.filter { dict in
                let dictionaryDataTable = convertDictionaryToString(json: dict)

                if((dictionaryDataTable?.contains(textSearched, options: .caseInsensitive))!)
                {
                    return true
                }

                return false
            }
            
            filterArray3.removeAllObjects()
            filterArray3 = NSMutableArray(array: dict3)
            
            searchActive = true
            
            self.cncList.reloadData()
        }
        
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchbarr.resignFirstResponder()
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchbarr.endEditing(true)
        self.searchbarr.text = ""
        searchbarr.showsCancelButton = false
        searchActive = false
        
        if installcnclist.isHidden == false
        {
         self.installcnclist.reloadData()
        }
        
        if installcnclist.isHidden == true
        {
         self.cncList.reloadData()
        }
        
       // self.cncList.reloadData()
       // self.installcnclist.reloadData()
    }
    
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchbarr.showsCancelButton = true
        
        return true
    }
    
    

    private func fetchContacts() {

        let store = CNContactStore()
        
        store.requestAccess(for: .contacts) { (granted, err) in
            if let err = err {
                print("Failed to request access:", err)
                return
            }

            //if granted {
            
//            DispatchQueue.main.async {
//            self.actInd.startAnimating()
//            }
            
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])

                do {

                    try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointerIfYouWantToStopEnumerating) in

                        self.favoritableContacts.append(InvitedContact(contact: contact))
                       
                        let number:String = contact.phoneNumbers.first?.value.stringValue ?? ""

                        let dictionary = ["+": "", " ": "", "(": "", ")": "", "-": ""]
                        let mobileResult = number.replaceDictionary(dictionary)

                        let last10:String = String(mobileResult.suffix(10))

                        let name:String = contact.givenName + " " + contact.familyName

                        if last10 == "" || last10.count < 10
                        {
                            
                        }
                        else
                        {
                         print("siddh")
                        self.listcncarr.append(["name":name,"mobile":last10])
                        }

                    })
                    
                } catch let err {
                    print("Failed to enumerate contacts:", err)
                }


        }
    }

    @IBAction func showInstalled(_ sender: UIButton) {
        if installcnclist.isHidden == true
        {
          installcnclist.isHidden = false
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == cncList
        {
        if section == 0
        {
        if searchActive == true
        {
        return filterArray2.count
        }
        return Invited.count
        }
        else
        {
        if searchActive == true
        {
        return filterArray3.count
        }
        return notInstall.count
        }
        }
        else
        {
        if searchActive == true
        {
        return filterArray.count
        }
        return Reginstalled.count
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == cncList
        {
        return 2
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
     if tableView == cncList
     {
     if indexPath.section == 0
     {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
        
        var InvDict:NSDictionary?
        
        if searchActive == true
        {
            InvDict = filterArray2[indexPath.row] as! NSDictionary
        }
        else
        {
            InvDict = Invited[indexPath.row] as! NSDictionary
        }
        
        cell.cncName.text = InvDict!.value(forKey: "name") as! String
    
        cell.cncNumber.text = InvDict!.value(forKey: "mobile") as! String
        
        cell.invitebtno.isHidden = true
        cell.invitesentlbl.isHidden = false
        
        return cell
     }
     else
     {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
        
        var NoIDict:NSDictionary?

        if searchActive == true
        {
        NoIDict = filterArray3[indexPath.row] as! NSDictionary
        }
        else
        {
        NoIDict = notInstall[indexPath.row] as! NSDictionary
            
        }
            
        cell.cncName.text = NoIDict!.value(forKey: "name") as! String
        
        cell.cncNumber.text = NoIDict!.value(forKey: "mobile") as! String

        cell.invitebtno.tag = indexPath.row
        cell.invitebtno.addTarget(self, action: #selector(selcnc), for: .touchUpInside)
        cell.invitesentlbl.isHidden = true
        cell.invitebtno.isHidden = false
        cell.invitebtno.layer.cornerRadius = 2.0
    
        if (selectedCNCs.contains(cell.cncNumber.text!)) {
                cell.invitebtno.setBackgroundImage(UIImage(named: "tick"), for: .normal)
        }
        else {
        cell.invitebtno.setBackgroundImage(UIImage(named: "untick"), for: .normal)
        }
        
        
        return cell
    }
    }
    else
    {
    let cell = tableView.dequeueReusableCell(withIdentifier: "installedContactCell") as! ContactCell

    var RegDict:NSDictionary?
     
    if searchActive == true
    {
    RegDict = filterArray[indexPath.row] as! NSDictionary
    }
    else
    {
    RegDict = Reginstalled[indexPath.row] as! NSDictionary
    }
    
    cell.installname.text = RegDict!.value(forKey: "name") as! String

    cell.installnumber.text = RegDict!.value(forKey: "mobile") as! String
        
    return cell
    }
        
        
    return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
    
    
    @objc func selcnc(sender:UIButton)
    {
    
        let buttonPosition = sender.convert(CGPoint.zero, to: self.cncList)
        let indexPath = self.cncList.indexPathForRow(at:buttonPosition)
        let cell = self.cncList.cellForRow(at: indexPath!) as! ContactCell
        
        if (self.selectedItems.contains(sender.tag)) {
            let index = self.selectedItems.firstIndex(of: sender.tag)
            self.selectedItems.remove(at: index!)
        }
        else {
            self.selectedItems.append(sender.tag)
        }
        
        if cell.invitebtno.backgroundImage(for: .normal) == UIImage(named: "untick")
        {
            if searchActive == true
            {
            refercncarr.append(filterArray3[sender.tag] as! [String : String])
            let invD = filterArray3[sender.tag] as! NSDictionary
                selectedCNCs.append(invD.value(forKey: "mobile") as! String)
            }
            else
            {
            refercncarr.append(notInstall[sender.tag] as! [String : String])
            let invD = notInstall[sender.tag] as! NSDictionary
                selectedCNCs.append(invD.value(forKey: "mobile") as! String)
            }
            
//            sendRefCNC()

            if refercncarr.count >= 1
            {
                SIpopviu.isHidden = false
            }
            else
            {
                SIpopviu.isHidden = true
            }
           
            cell.invitebtno.setBackgroundImage(UIImage(named: "tick"), for: .normal)
            
        }
        else
        {
            if searchActive == true
            {
                if refercncarr.contains(filterArray3[sender.tag] as! [String : String])
                {

                    if let index = refercncarr.firstIndex(of: filterArray3[sender.tag] as! [String : String]) {
                        refercncarr.remove(at: index)
                        selectedCNCs.remove(at: index)
                    } else {
                        // not found
                    }
                }
            }
            else
            {
            if refercncarr.contains(notInstall[sender.tag] as! [String : String])
            {

                if let index = refercncarr.firstIndex(of: notInstall[sender.tag] as! [String : String]) {
                    refercncarr.remove(at: index)
                    selectedCNCs.remove(at: index)
                } else {
                    // not found
                }
            }
            }
        
            if refercncarr.count == 0
            {
                SIpopviu.isHidden = true
            }
            else
            {
                SIpopviu.isHidden = false
            }
            

            
            cell.invitebtno.setBackgroundImage(UIImage(named: "untick"), for: .normal)
        }
        
        cncList.reloadData()
        
    }
    

    @IBAction func backto(_ sender: Any) {
        if installcnclist.isHidden == false
        {
          installcnclist.isHidden = true
        }
        else
        {
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @IBAction func sendInvite(_ sender: Any) {
        sendRefCNC()
        SIpopviu.isHidden = true
    }
    
    
    func SyncCNC()
    {
        if Connectivity.isConnectedToInternet {
            
        actInd.startAnimating()
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["mobile":listcncarr]
        
        let para2 = ["":""]
        
        let url = syncContacts()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
            
                    DispatchQueue.main.async {
                        let httpResponse = response as? HTTPURLResponse
                        
                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                        {
                        self.actInd.stopAnimating()
                        let resdict:NSDictionary = json as NSDictionary
                        let payload:NSDictionary = resdict.value(forKey: "payload") as! NSDictionary
                        self.notInstall = payload.value(forKey: "notInstalled") as! NSArray
                        self.Invited = payload.value(forKey: "alreadyInvited") as! NSArray
                        self.Reginstalled = payload.value(forKey: "installed") as! NSArray
                        self.registeredcountlbl.setTitle(String(describing: self.Reginstalled.count) + NSLocalizedString(" Existing Truemeds users", comment: ""), for: .normal)
                        
                            self.cncList.reloadData()
                            self.installcnclist.reloadData()
                            
                        }
                        else if httpResponse?.statusCode == 401
                        {
                            let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                            Utility.showAlertWithTitle(title: "", andMessage:localizedContent, onVC: self)
                        }
                        else
                        {
                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
    func sendRefCNC()
    {
        if Connectivity.isConnectedToInternet {
            
        view.isUserInteractionEnabled = false
        actInd.startAnimating()
        cncList.isScrollEnabled = false
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["mobile":refercncarr]
        
        let para2 = ["":""]
        
        let url = sendReff()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    print(json)
                    
                    DispatchQueue.main.async {
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    self.view.isUserInteractionEnabled = true
                    
                    self.actInd.stopAnimating()
                    
                    self.cncList.isScrollEnabled = true
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                        self.actInd.stopAnimating()
                        self.searchActive = false
                        self.listcncarr.removeAll()
                        self.fetcnc.removeAll()
                        self.fetchContacts()
                        self.SyncCNC()
                    }
                    else if httpResponse?.statusCode == 401
                    {
                    
                        let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                        Utility.showAlertWithTitle(title: "", andMessage:localizedContent, onVC: self)
                    }
                    else
                    {
                
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
    

}

extension String{
    func replaceDictionary(_ dictionary: [String: String]) -> String{
        var result = String()
        var i = -1
        for (of , with): (String, String)in dictionary{
            i += 1
            if i<1{
                result = self.replacingOccurrences(of: of, with: with)
            }else{
                result = result.replacingOccurrences(of: of, with: with)
            }
        }
        return result
    }
}



extension String {
    func contains(_ string: String, options: CompareOptions) -> Bool {
        return range(of: string, options: options) != nil
    }
}
