//
//  SubstitutesListViewController.swift
//  True_Meds
//
//  Created by Welborn Machado on 16/12/19.
//

import UIKit
import SVGKit

class SubstitutesListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var backbtno: UIButton!

    var resdict = NSDictionary()
    
    var MyorderList = NSArray()
    
    var Listdict = NSDictionary()
    
    @IBOutlet weak var subsListTable: UITableView!
    
    @IBOutlet weak var selectlbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        getInfoSubsList()
    }
    
    
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return MyorderList.count
   }
   
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "InfoSubstitutesList") as! InfoSubstitutesTableViewCell
       Listdict = self.MyorderList[indexPath.row] as! NSDictionary
       cell.subslistmainviu.layer.cornerRadius = 4.0
       cell.subslistmainviu.layer.borderWidth = 1.0
       cell.subslistmainviu.layer.borderColor = UIColor(hexString: "#98C6E5", alpha: 1.0).cgColor
       cell.subslistmainviu.clipsToBounds = true
       cell.ordernolbl.text = String(describing: Listdict.value(forKey: "orderId")!)
       let meds:NSArray = Listdict.value(forKey: "medicines") as? NSArray ?? []
       cell.subslistmedicine.text = meds.componentsJoined(by: " , ")
       
     if let listprice = Listdict.value(forKey: "orderamount") as? Double
     {
      cell.subslistprice.text = "₹ " + String(format: "%.2f", listprice)
     }
       if let totalsavings = Listdict.value(forKey: "totalSaving") as? Double
       {
        cell.subslistsaving.text = "Saved ₹ " + String(format: "%.2f", totalsavings)
       }
       
       let statusId:CLong = Listdict.value(forKey: "statusId") as? CLong ?? 55
       if statusId == 55
       {
        cell.subsliststatus.text = "Delivered"
       }
       else
       {
       cell.subsliststatus.text = "Est. Delivery"
       }
       
       if let mydate = Listdict.value(forKey: "date") as? CLong
       {
       let mydates:CLong = mydate/1000
       let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
       
       let dayTimePeriodFormatter = DateFormatter()
       dayTimePeriodFormatter.dateFormat = "dd MMMM yy"
       
       let dateString = dayTimePeriodFormatter.string(from: date as Date)
       
       cell.subslistdatelbl.text = dateString
        }
        else
       {
       cell.subslistdatelbl.text = ""
       }
    
       return cell
   }
   
    
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableView.automaticDimension
   }
   
    
   func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
       return 169.0
   }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "InfoSubstitutesViewController") as? InfoSubstitutesViewController
         let odict:NSDictionary = self.MyorderList[indexPath.row] as! NSDictionary
         vc?.SorderId = odict.value(forKey: "orderId") as! CLong
         self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    func getInfoSubsList()
    {
        let parameters = ["": ""] as [String : Any]
        print(parameters)
        ApiManager().requestApiWithDataType( methodType: HGET, urlString:GetInfoOnSubsList(),parameters: parameters as [String : AnyObject]) { (response,cStatus, error) in
            
            print(response)
            
            if cStatus == 200 || cStatus == 201
            {
            self.resdict = convertStringToDictionary(json: response as! String) as! NSDictionary
            
            print(self.resdict)
                
            self.MyorderList = self.resdict.value(forKey: "orderList") as? NSArray ?? []
            if self.MyorderList.count == 0
            {
            self.selectlbl.isHidden = true
            self.subsListTable.isHidden = true
            }
            else
            {
            self.selectlbl.isHidden = false
            self.subsListTable.isHidden = false
            }
            DispatchQueue.main.async {
            self.subsListTable.reloadData()
            }
            }
            else if cStatus == 401
            {
                let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
            }
            else if cStatus == 400
            {
                let adddict:NSDictionary = convertStringToDictionary(json: response as! String)! as NSDictionary
                if adddict.allKeys.first as! String == "400"
                {
                    Utility.showAlertWithTitle(title: "", andMessage: adddict.value(forKey: "400") as! String, onVC: self)
                }
             }
            else if cStatus == 500
            {
                let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                self.addChild(popupVC)
                popupVC.view.frame = self.view.frame
                self.view.addSubview(popupVC.view)
                popupVC.didMove(toParent: self)
            }
            else
            {
                let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
            }
        }
    }
    
    
}
