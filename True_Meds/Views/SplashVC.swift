//
//  SplashVC.swift
//  True_Meds
//


import UIKit

import Reachability
import Alamofire

class SplashVC: UIViewController {
    
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var waitLbl: UILabel!
    
    var custdict = NSDictionary()
    
    var quesdict = NSDictionary()
    
    var masterdict = NSDictionary()
    
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        waitLbl.isHidden = true
        waitLbl.isHidden = false
        activity.startAnimating()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        if Connectivity.isConnectedToInternet {
            
            self.GetMasters()
            
            if UserDefaults.standard.object(forKey: "CustomerId") != nil
            {
                self.getCustomerD()
            }
        }
            
        else
        {
         let showAlert = UIAlertController(title: NSLocalizedString("Please check your internet connectivity", comment: ""), message: nil, preferredStyle: .alert)
         let imageView = UIImageView(frame: CGRect(x: 0, y: 90, width: 250, height: 140))
         imageView.image = UIImage(named: "Group 50")
         imageView.contentMode = .scaleAspectFit
         showAlert.view.addSubview(imageView)
         let height = NSLayoutConstraint(item: showAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 280)
         let width = NSLayoutConstraint(item: showAlert.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 280)
                 
         showAlert.view.addConstraint(height)
         showAlert.view.addConstraint(width)

         showAlert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
             // your actions here...
         }))
                 self.present(showAlert, animated: true, completion: nil)
        }
        
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       // self.loadNextVC()
    }
    
    
    
    func loadNextVC() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            
            let defaults = UserDefaults.standard
            if defaults.object(forKey: "CustomerId") == nil {
                let NewVC = self.storyboard?.instantiateViewController(withIdentifier: "LangSelectVC") as? LangSelectVC
                NewVC!.mastdicts = self.masterdict
                self.navigationController?.pushViewController(NewVC!, animated: true)
                
            }else
            {
                let NewVC = self.storyboard?.instantiateViewController(withIdentifier: "buyMeds") as? buyMeds
                let langpref:String = UserDefaults.standard.value(forKey: "langpref") as! String
                if langpref == "English"
                {
                 let sindict:NSDictionary = self.masterdict.value(forKey: "English") as! NSDictionary
                 let finsindict:NSDictionary = sindict.value(forKey: "questions") as! NSDictionary
                 let appintro:NSDictionary = sindict.value(forKey: "appIntro") as! NSDictionary
                    print(appintro)
                    let userDefaults = UserDefaults.standard
                      userDefaults.set(appintro, forKey: "appIntro")
                      userDefaults.synchronize()
//                  self.setDict(dict: appintro)
                 NewVC!.imgsarr = appintro.value(forKey: "screen") as! NSArray
//                    UserDefaults.standard.setValue(appintro, forKey: "appIntro")
//                 UserDefaults.standard.set(appintro as! NSDictionary, forKey: "appIntro")
                   
                 UserDefaults.standard.set(finsindict, forKey: "quesdict")
                 UserDefaults.standard.synchronize()
                 self.navigationController?.pushViewController(NewVC!, animated: true)
                }
                else if langpref == "Hindi"
                {
                 let sindict:NSDictionary = self.masterdict.value(forKey: "Hindi") as! NSDictionary
                 let finsindict:NSDictionary = sindict.value(forKey: "questions") as! NSDictionary
                 let appintro:NSDictionary = sindict.value(forKey: "appIntro") as! NSDictionary
                               let userDefaults = UserDefaults.standard
                                      userDefaults.setValue(appintro, forKey: "appIntro")
                                      userDefaults.synchronize()
                //                  self.setDict(dict: appintro)

                 NewVC!.imgsarr = appintro.value(forKey: "screen") as! NSArray
//                 UserDefaults.standard.set(appintro as! NSDictionary, forKey: "appIntro")
//                     UserDefaults.standard.setValue(appintro, forKey: "appIntro")
                   
                 UserDefaults.standard.set(finsindict, forKey: "quesdict")
                 UserDefaults.standard.synchronize()
                 self.navigationController?.pushViewController(NewVC!, animated: true)
                }
                else
                {
                 let sindict:NSDictionary = self.masterdict.value(forKey: "Marathi") as! NSDictionary
                 let finsindict:NSDictionary = sindict.value(forKey: "questions") as! NSDictionary
                 let appintro:NSDictionary = sindict.value(forKey: "appIntro") as! NSDictionary
                                 let userDefaults = UserDefaults.standard
                                       userDefaults.setValue(appintro, forKey: "appIntro")
                                       userDefaults.synchronize()
                 //                  self.setDict(dict: appintro)

                 NewVC!.imgsarr = appintro.value(forKey: "screen") as! NSArray
//                 UserDefaults.standard.set(appintro as! NSDictionary, forKey: "appIntro")
//                     UserDefaults.standard.setValue(appintro, forKey: "appIntro")
                     
                 UserDefaults.standard.set(finsindict, forKey: "quesdict")
                 UserDefaults.standard.synchronize()
                 self.navigationController?.pushViewController(NewVC!, animated: true)
                }
            }
        }
    }
    
    
    func GetMasters()
    {
        
        if Connectivity.isConnectedToInternet {
        
        let url =  GetUserMasters()
    
        let session = URLSession.shared
        
        let serviceUrl = URL(string: url)
        
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue("T1", forHTTPHeaderField: "accessToken")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
          print(response)
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    

                    DispatchQueue.main.async {
                        let httpResponse = response as? HTTPURLResponse
                        
                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                        {
                            
                            self.masterdict = json as NSDictionary
//                            self.waitLbl.isHidden = true
//                            self.activity.stopAnimating()
                            self.loadNextVC()
                            
                        }
                        else
                        {
                            let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP

                            self.addChild(popupVC)
                            popupVC.view.frame = self.view.frame
                            self.view.addSubview(popupVC.view)
                            popupVC.didMove(toParent: self)
                        }
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
                
            }
        })
        task.resume()
        }
        else
        {
            let showAlert = UIAlertController(title: NSLocalizedString("Please check your internet connectivity", comment: ""), message: nil, preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: 0, y: 90, width: 250, height: 140))
            imageView.image = UIImage(named: "Group 50")
            imageView.contentMode = .scaleAspectFit
            showAlert.view.addSubview(imageView)
            let height = NSLayoutConstraint(item: showAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 280)
            let width = NSLayoutConstraint(item: showAlert.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 280)
                    
            showAlert.view.addConstraint(height)
            showAlert.view.addConstraint(width)

            showAlert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
                // your actions here...
            }))
                    self.present(showAlert, animated: true, completion: nil)
        }
    }
    
    
    func getCustomerD()
    {
        if Connectivity.isConnectedToInternet {
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["":""]
        
        let url =  GetCustDetails()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    print(json)
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                        let resultdict:NSDictionary = json as NSDictionary
                        self.custdict = resultdict.value(forKey: "CustomerDetails") as! NSDictionary
                        if let custname = self.custdict.value(forKey: "customerName") as? String
                        {
                            UserDefaults.standard.set(custname, forKey: "custname")
                            UserDefaults.standard.synchronize()
                        }
                        if let ageId = self.custdict.value(forKey: "ageGroupId") as? CLong
                        {
                            UserDefaults.standard.set(ageId, forKey: "ageGroupId")
                            UserDefaults.standard.synchronize()
                        }
                        if let langId = self.custdict.value(forKey: "languageId") as? CLong
                        {
                            UserDefaults.standard.set(langId, forKey: "languageId")
                            UserDefaults.standard.synchronize()
                        }
                        if let mobNo = self.custdict.value(forKey: "mobileNo") as? String
                        {
                            UserDefaults.standard.set(mobNo, forKey: "mobileNo")
                            UserDefaults.standard.synchronize()
                        }
                        
                        
                    }
                    else
                    {
                        
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    let showAlert = UIAlertController(title: NSLocalizedString("Please check your internet connectivity", comment: ""), message: nil, preferredStyle: .alert)
    let imageView = UIImageView(frame: CGRect(x: 0, y: 90, width: 250, height: 140))
    imageView.image = UIImage(named: "Group 50")
    imageView.contentMode = .scaleAspectFit
    showAlert.view.addSubview(imageView)
    let height = NSLayoutConstraint(item: showAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 280)
    let width = NSLayoutConstraint(item: showAlert.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 280)
            
    showAlert.view.addConstraint(height)
    showAlert.view.addConstraint(width)

    showAlert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
        // your actions here...
    }))
            self.present(showAlert, animated: true, completion: nil)
    }
    }
    

}
