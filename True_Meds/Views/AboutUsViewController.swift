//
//  AboutUsViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import SDWebImage
import Reachability
import Alamofire

class AboutUsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    let reachability = Reachability()!
    
    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var akpic: UIImageView!
    
    @IBOutlet weak var kunpic: UIImageView!
    
    @IBOutlet weak var name1: UILabel!
    
    @IBOutlet weak var name2: UILabel!
    
    @IBOutlet weak var desgn1: UILabel!
    
    @IBOutlet weak var desgn2: UILabel!
    
    @IBOutlet weak var qual1: UILabel!
    
    @IBOutlet weak var qual2: UILabel!
    
   var aboutusDict = NSDictionary()
    
   var aboutdetails = NSArray()
    
   var leaderarr = NSArray()
    
    var leader1 = NSDictionary()
    
    var leader2 = NSDictionary()
    
    @IBOutlet weak var AboutUstable: UITableView!
    
    @IBOutlet weak var tableheightcons: NSLayoutConstraint!
    
    @IBOutlet weak var fb1: UIButton!
    
    @IBOutlet weak var lk1: UIButton!
    
    @IBOutlet weak var fb2: UIButton!
    
    @IBOutlet weak var lk2: UIButton!
    
    @IBOutlet weak var versiontag: UILabel!
    
    @IBOutlet weak var urltagger: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            
           if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
               self.versiontag.text = ("V " + version) + " " + "(" + build + ")"
            }
            
        }
        
        if Live_IP == "https://stage-services.truemeds.in"
        {
         urltagger.text = "STAGE"
        }
        else
        {
         urltagger.text = ""
        }
        
        AboutUs()
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "facebook")
        fb1.setImage(svgimg2.uiImage, for: .normal)
        
        let svgimg3: SVGKImage = SVGKImage(named: "linkedin")
        lk1.setImage(svgimg3.uiImage, for: .normal)
        
        let svgimg4: SVGKImage = SVGKImage(named: "facebook")
        fb2.setImage(svgimg4.uiImage, for: .normal)
        
        let svgimg5: SVGKImage = SVGKImage(named: "linkedin")
        lk2.setImage(svgimg5.uiImage, for: .normal)
        
        akpic.layer.cornerRadius = 50.0
        akpic.clipsToBounds = true
        kunpic.layer.cornerRadius = 50.0
        kunpic.clipsToBounds = true
        
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let height2 = AboutUstable.contentSize.height
        tableheightcons.constant = height2
        
        self.view.layoutIfNeeded()
        
    }
    
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return aboutdetails.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "aboutuscell") as! LegalTableViewCell
    
        let finaboutdict:NSDictionary = aboutdetails[indexPath.row] as! NSDictionary
        cell.aboutheader.text = finaboutdict.value(forKey: "header") as! String
        cell.aboutdesc.text = finaboutdict.value(forKey: "description") as! String
        
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    @IBAction func openakshatfb(_ sender: Any) {
        UIApplication.tryURL(urls: [
            self.leader1.value(forKey: "facebook") as! String // Website if app fails
            ])
    }
    
    @IBAction func openakshatlinkedin(_ sender: Any) {
        UIApplication.tryURL(urls: [
           self.leader1.value(forKey: "linkedin") as! String
        ])
    }
    
    @IBAction func openkunalfb(_ sender: Any) {
        UIApplication.tryURL(urls: [
            self.leader2.value(forKey: "facebook") as! String
            ])
    }
    
    @IBAction func openkunalinked(_ sender: Any) {
        UIApplication.tryURL(urls: [
            self.leader2.value(forKey: "linkedin") as! String
            ])
    }
    
    func AboutUs()
    {
        if Connectivity.isConnectedToInternet {
        
        let url = AllAboutUs()
        let serviceurl = URL(string: url)
        let session = URLSession.shared
        
        var request = URLRequest(url: serviceurl!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    print(json)
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    let aboutdict:NSDictionary = json as NSDictionary
                    self.aboutusDict = aboutdict.value(forKey: "aboutus") as! NSDictionary
                    self.aboutdetails = self.aboutusDict.value(forKey: "aboutUs") as! NSArray
                    self.leaderarr = self.aboutusDict.value(forKey: "leadership") as! NSArray
                    self.leader1 = self.leaderarr[0] as! NSDictionary
                    self.leader2 = self.leaderarr[1] as! NSDictionary
                    
                    DispatchQueue.main.async {
                    
                        self.desgn1.text = self.leader1.value(forKey: "designation") as! String
                        let url1:URL = URL(string: self.leader1.value(forKey: "image") as! String)!
                        self.akpic.sd_setImage(with: url1, completed:nil)
                        self.name1.text = self.leader1.value(forKey: "name") as! String
                        self.qual1.text = self.leader1.value(forKey: "qualification") as! String
                        self.desgn2.text = self.leader2.value(forKey: "designation") as! String
                        
                        let url2:URL = URL(string: self.leader2.value(forKey: "image") as! String)!
                        self.kunpic.sd_setImage(with: url2, completed:nil)
                        self.name2.text = self.leader2.value(forKey: "name") as! String
                        self.qual2.text = self.leader2.value(forKey: "qualification") as! String
                        
                       self.AboutUstable.reloadData()
                        
                    }
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
            
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
        
        
        self.AboutUstable.reloadData()
    }
    
    
}

extension UIApplication {
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if application.canOpenURL(URL(string: url)!) {
                application.openURL(URL(string: url)!)
                return
            }
        }
    }
    
}
