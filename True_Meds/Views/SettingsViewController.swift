//
//  SettingsViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire
import LanguageManager_iOS

class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    
    let reachability = Reachability()!
    
    var TYPE_CELL_IDENTIFIER : String!
    
    @IBOutlet weak var settingsTableView: UITableView!
    
    var currentRow = 0
    
    let setarray = [1,2,3,4,5,6,7]
    
    var ScreenID:Int = 1
    
    @IBOutlet weak var setTMhome: UIButton!
    
    var langID:Int!
    
    var agegroupID:Int!
    
    @IBOutlet weak var prevbtn: UIButton!
    
    var custdict = NSDictionary()
    
    var genId:CLong!
    
    var base64:String!
    
    let pickarr = ["Male","Female","Others"]
    
    
    var dayArr = [Any]()
    
    var mydict = [String]()

    var texttag:Int!
    
    var textstr:String!
    
    @IBOutlet weak var genpicker: UIPickerView!
    
    @IBOutlet weak var toolbr: UIToolbar!
    
    var masterdict = NSDictionary()
    
    var lanDict = NSDictionary()
    
    var engDict = NSDictionary()
    
    var hinDict = NSDictionary()
    
    var marDict = NSDictionary()
    
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    var isfrommenu:String!
    
   var mastdict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GetMasters()

        masterdict = UserDefaults.standard.value(forKey: "quesdict") as! NSDictionary
        
        if UserDefaults.standard.object(forKey: "CustomerId") != nil
        {
        getCustomerD()
        }
    
        if isfrommenu == "yes"
        {
        currentRow = 1
        ScreenID = 4
        prevbtn.isHidden = false
        
        }
        else
        {
        prevbtn.isHidden = false
        }
        
        prevbtn.layer.borderWidth = 0.8
        prevbtn.layer.borderColor = UIColor.darkGray.cgColor
        prevbtn.layer.cornerRadius = 4.0
        
        prevbtn.addTarget(self, action: #selector(prevcell), for: .touchUpInside)
        
        
        setTMhome.addTarget(self, action: #selector(popvc), for: .touchUpInside)
        hideKeyboard()
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickarr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        textstr = pickarr[row]
        return textstr
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return setarray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if ScreenID == 1 {
        TYPE_CELL_IDENTIFIER = "OptionTableViewCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TYPE_CELL_IDENTIFIER) as! OptionTableViewCell
        let dict:NSDictionary = masterdict.value(forKey: "25") as! NSDictionary
        let text:String = dict.value(forKey: "question") as! String
            
        cell.questext.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        let btn3:NSDictionary = btnarr[2] as! NSDictionary
            
        cell.langselbtno.tag = 1
        cell.langselbtno.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
        cell.langselbtno.addTarget(self, action: #selector(changerow(sender:)), for: .touchUpInside)
        cell.ageselbtno.tag = 2
        cell.ageselbtno.setTitle(btn2.value(forKey: "text") as! String, for: .normal)
        cell.ageselbtno.addTarget(self, action: #selector(changerow(sender:)), for: .touchUpInside)
        cell.editprobtno.tag = 3
        cell.editprobtno.setTitle(btn3.value(forKey: "text") as! String, for: .normal)
        cell.editprobtno.addTarget(self, action: #selector(changerow(sender:)), for: .touchUpInside)
        if UserDefaults.standard.object(forKey: "CustomerId") == nil
        {
        cell.editprobtno.isHidden = true
        }
        
        return cell
        }
        else if ScreenID == 2
        {
        TYPE_CELL_IDENTIFIER = "OptionTableViewCell"
            
        let cell = tableView.dequeueReusableCell(withIdentifier: TYPE_CELL_IDENTIFIER) as! OptionTableViewCell
        let dict:NSDictionary = masterdict.value(forKey: "1") as! NSDictionary
        let text:String = dict.value(forKey: "question") as! String
        cell.questext.text = text.replacingOccurrences(of: "\\n", with: "\n")
        let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
        let btn1:NSDictionary = btnarr[0] as! NSDictionary
        let btn2:NSDictionary = btnarr[1] as! NSDictionary
        let btn3:NSDictionary = btnarr[2] as! NSDictionary
            
        cell.langselbtno.tag = 42
        cell.langselbtno.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
        cell.langselbtno.addTarget(self, action: #selector(changerow(sender:)), for:.touchUpInside)
        cell.ageselbtno.tag = 43
        cell.ageselbtno.setTitle(btn2.value(forKey: "text") as! String, for: .normal)
        cell.ageselbtno.addTarget(self, action: #selector(changerow(sender:)), for:.touchUpInside)
        cell.editprobtno.tag = 44
        cell.editprobtno.addTarget(self, action: #selector(changerow(sender:)), for:.touchUpInside)
        cell.editprobtno.setTitle(btn3.value(forKey: "text") as! String, for: .normal)
        if UserDefaults.standard.object(forKey: "CustomerId") == nil
        {
        cell.editprobtno.isHidden = false
        }
            
            
        return cell
        }
        else if ScreenID == 3
        {
            TYPE_CELL_IDENTIFIER = "AgeGroupTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: TYPE_CELL_IDENTIFIER) as! AgeGroupTableViewCell
            let dict:NSDictionary = masterdict.value(forKey: "2") as! NSDictionary
            let text:String = dict.value(forKey: "question") as! String
            cell.qlbl3.text = text.replacingOccurrences(of: "\\n", with: "\n")
            let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
            let btn1:NSDictionary = btnarr[0] as! NSDictionary
            let btn2:NSDictionary = btnarr[1] as! NSDictionary
        
            cell.seniorbtno.tag = 40
            cell.seniorbtno.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
            cell.seniorbtno.addTarget(self, action: #selector(changerow(sender:)), for: .touchUpInside)
            cell.noseniorbtno.tag = 41
            cell.noseniorbtno.setTitle(btn2.value(forKey: "text") as! String, for: .normal)
            cell.noseniorbtno.addTarget(self, action: #selector(changerow(sender:)), for: .touchUpInside)
            
            return cell
        }
        else if ScreenID == 4
        {
            TYPE_CELL_IDENTIFIER = "EditProfileTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: TYPE_CELL_IDENTIFIER) as! EditProfileTableViewCell
            cell.nametf.delegate = self
            cell.emailtf.delegate = self
            cell.agetf.delegate = self
           
            cell.nametf.text = custdict.value(forKey: "customerName") as? String
            cell.emailtf.text = custdict.value(forKey: "emailAddress") as? String
            
            if let myage = custdict.value(forKey: "age") as? CLong
            {
            cell.agetf.text = String(describing:myage)
            }
            
            if let propicurl = custdict.value(forKey: "profileImageUrl") as? String
            {
            if propicurl == ""
            {
            
            }else
            {
            let imgurl:URL = URL(string: propicurl)!
            cell.eProPic.sd_setImage(with: imgurl, completed: nil)
            }
            }
            if let genderId = custdict.value(forKey: "gender") as? CLong
            {
                if genderId == 8
                {
                cell.gendertf.text = "Male"
                genId = 8
                }
                else if genderId == 9
                {
                cell.gendertf.text = "Female"
                genId = 9
                }
                else if genderId == 10
                {
                    cell.gendertf.text = "Others"
                    genId = 10
                }
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            cell.eProPic.isUserInteractionEnabled = true
            cell.eProPic.addGestureRecognizer(tap)
            cell.esavebtno.addTarget(self, action: #selector(updateProfile), for: .touchUpInside)
            cell.ecancelbtno.addTarget(self, action: #selector(prevcell), for: .touchUpInside)
            cell.gendertf.delegate = self
            cell.gendertf.tag = indexPath.row
            
            cell.gendertf.addTarget(self, action: #selector(openpicker(_:)), for: .editingDidBegin)
            cell.gendertf.inputAccessoryView = toolbr
            
            return cell
        }
        else if ScreenID == 5
        {
            TYPE_CELL_IDENTIFIER = "AgeGroupTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: TYPE_CELL_IDENTIFIER) as! AgeGroupTableViewCell
            
            var dict:NSDictionary
            
            if langID == 42
            {
            let changedict:NSDictionary = engDict.value(forKey: "questions") as! NSDictionary
            dict = changedict.value(forKey: "27") as! NSDictionary
            }
            else if langID == 43
            {
            let changedict:NSDictionary = hinDict.value(forKey: "questions") as! NSDictionary
            dict = changedict.value(forKey: "27") as! NSDictionary
            }
            else if langID == 44
            {
            let changedict:NSDictionary = marDict.value(forKey: "questions") as! NSDictionary
            dict = changedict.value(forKey: "27") as! NSDictionary
            }
            else
            {
            dict = masterdict.value(forKey: "27") as! NSDictionary
            }
            let text:String = dict.value(forKey: "question") as! String
            cell.qlbl3.text = text.replacingOccurrences(of: "\\n", with: "\n")
            let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
            let btn1:NSDictionary = btnarr[0] as! NSDictionary
            let btn2:NSDictionary = btnarr[1] as! NSDictionary
            cell.seniorbtno.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
            cell.seniorbtno.addTarget(self, action: #selector(updateselection(sender:)), for: .touchUpInside)
            cell.noseniorbtno.setTitle(btn2.value(forKey: "text") as! String, for: .normal)
            cell.noseniorbtno.addTarget(self, action: #selector(prevcell), for: .touchUpInside)
            
            return cell
        }
        else if ScreenID == 6
        {
            TYPE_CELL_IDENTIFIER = "AgeGroupTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: TYPE_CELL_IDENTIFIER) as! AgeGroupTableViewCell
            let dict:NSDictionary = masterdict.value(forKey: "26") as! NSDictionary
            let text:String = dict.value(forKey: "question") as! String
            cell.qlbl3.text = text.replacingOccurrences(of: "\\n", with: "\n")
            let btnarr:NSArray = dict.value(forKey: "buttons") as! NSArray
            let btn1:NSDictionary = btnarr[0] as! NSDictionary
            let btn2:NSDictionary = btnarr[1] as! NSDictionary
            cell.seniorbtno.setTitle(btn1.value(forKey: "text") as! String, for: .normal)
            cell.seniorbtno.addTarget(self, action: #selector(updateselection2(sender:)), for: .touchUpInside)
            cell.noseniorbtno.setTitle(btn2.value(forKey: "text") as! String, for: .normal)
            cell.noseniorbtno.addTarget(self, action: #selector(prevcell), for: .touchUpInside)
            
            return cell
        }
        
        
        return UITableViewCell()
        
    }
    
    
    @IBAction func donepick(_ sender: UIBarButtonItem) {
        view.endEditing(true)
        
        let myIndexPath = NSIndexPath(row: texttag, section: 0)
        
        let cell:EditProfileTableViewCell = settingsTableView.cellForRow(at: myIndexPath as IndexPath) as! EditProfileTableViewCell
        
        if textstr == ""
        {
         cell.gendertf.text = "Male"
         genId = 8
        }
        else if textstr == "Male"
        {
         cell.gendertf.text = "Male"
         genId = 8
        }
        else if textstr == "Female"
        {
        cell.gendertf.text = "Female"
        genId = 9
        }
        else if textstr == "Others"
        {
            cell.gendertf.text = "Others"
            genId = 10
        }
        
    }
    
    
    
    @objc func openpicker(_ textField: UITextField) {
        textField.inputView = genpicker
        
        texttag = textField.tag
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return settingsTableView.bounds.size.height
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: NSLocalizedString("Camera", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                print("error")
            }
            
        })
        
        camera.setValue(UIColor.black, forKey: "titleTextColor")
        
        actionSheet.addAction(camera);
        
        let gallery = UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                print("error")
            }
        })
        
        gallery.setValue(UIColor.black, forKey: "titleTextColor")
        
        actionSheet.addAction(gallery);
        
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: {
            (alert: UIAlertAction!) in
            
            self.dismiss(animated: true, completion: nil);
            
        })
        
        
        actionSheet.addAction(cancel);
        
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0 , height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        present(actionSheet, animated: true, completion: nil);
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let images = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        {
           // imgarrs.append(images)
            
         let myIndexPath = NSIndexPath(row: currentRow, section: 0)
            
           let cell:EditProfileTableViewCell = settingsTableView.cellForRow(at: myIndexPath as IndexPath) as! EditProfileTableViewCell
            cell.eProPic.image = images
    
            let imageData:NSData = images.jpegData(compressionQuality: 0.4) as! NSData
           // UserDefaults.standard.set(imageData, forKey: "imageData")

          base64 = imageData.base64EncodedString(options: .lineLength64Characters)
           
            
            dismiss(animated: true, completion: nil)
        }
        
        
        
    }
    
    
    
    func updateSetts()
    {
        if Connectivity.isConnectedToInternet {
        
        actInd.startAnimating()
        
        let custid:String = String(describing:UserDefaults.standard.value(forKey: "CustomerId")!)
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        
            
//            let parameters = ["ageGroupId":UserDefaults.value(forKey: "ageGroupId") as! CLong,
//                          "languageId":UserDefaults.value(forKey: "languageId") as! CLong,
//                          "customerId":custid
//            ] as [String : Any]
        
            
            let parameters = ["ageGroupId": CLong(agegroupID),
                          "languageId": CLong(langID),
                          "customerId":custid
            ] as [String : Any]
        let url =  UpdateSettings()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                        self.actInd.stopAnimating()
                        
                        if self.lanDict == nil
                        {
                            
                        }
                        else
                        {
                            if self.lanDict == self.engDict
                            {
                                //L102Language.setAppleLAnguageTo(lang: "en")
                                
                                Locale.updateLanguage(code: "en")
                            
                                UserDefaults.standard.set("English", forKey: "langpref")
                                
                                self.mastdict = self.lanDict.value(forKey: "questions") as! NSDictionary
                                
                                let menuItems:NSArray = self.lanDict.value(forKey: "menuItems") as! NSArray
                                let newuser:NSArray = menuItems.filter{!(($0 as! NSDictionary)["login"] as! Bool)} as NSArray
                                
                            let appintro:NSDictionary = self.lanDict.value(forKey: "appIntro") as! NSDictionary
                            let userDefaults = UserDefaults.standard
                                userDefaults.set(appintro, forKey: "appIntro")
//                                userDefaults.synchronize()
                                
                                UserDefaults.standard.set(self.mastdict, forKey: "quesdict")
                                UserDefaults.standard.set(newuser, forKey: "menunew")
                                UserDefaults.standard.set(menuItems, forKey: "menulogin")
                                
                                UserDefaults.standard.synchronize()
                            }
                            else if self.lanDict == self.hinDict
                            {
                                //L102Language.setAppleLAnguageTo(lang: "hi")
                                
                                Locale.updateLanguage(code: "hi")
                                
                                UserDefaults.standard.set("Hindi", forKey: "langpref")
                                
                                self.mastdict = self.lanDict.value(forKey: "questions") as! NSDictionary
                                
                                let menuItems:NSArray = self.lanDict.value(forKey: "menuItems") as! NSArray
                                let newuser:NSArray = menuItems.filter{!(($0 as! NSDictionary)["login"] as! Bool)} as NSArray
                                
                                let appintro:NSDictionary = self.lanDict.value(forKey: "appIntro") as! NSDictionary
                                                          let userDefaults = UserDefaults.standard
                                                              userDefaults.set(appintro, forKey: "appIntro")
//                                                              userDefaults.synchronize()
                                
                                UserDefaults.standard.set(self.mastdict, forKey: "quesdict")
                                UserDefaults.standard.set(newuser, forKey: "menunew")
                                UserDefaults.standard.set(menuItems, forKey: "menulogin")
                                
                                UserDefaults.standard.synchronize()
                        
                            }
                            else if self.lanDict == self.marDict
                            {
                                //L102Language.setAppleLAnguageTo(lang: "mr-IN")
                                
                                Locale.updateLanguage(code: "mr-IN")
                                
                                UserDefaults.standard.set("Marathi", forKey: "langpref")
                                
                                self.mastdict = self.lanDict.value(forKey: "questions") as! NSDictionary
                                
                                let menuItems:NSArray = self.lanDict.value(forKey: "menuItems") as! NSArray
                                let newuser:NSArray = menuItems.filter{!(($0 as! NSDictionary)["login"] as! Bool)} as NSArray
                                
                                let appintro:NSDictionary = self.lanDict.value(forKey: "appIntro") as! NSDictionary
                                                          let userDefaults = UserDefaults.standard
                                                              userDefaults.set(appintro, forKey: "appIntro")
//                                                              userDefaults.synchronize()
                                
                                UserDefaults.standard.set(self.mastdict, forKey: "quesdict")
                                UserDefaults.standard.set(newuser, forKey: "menunew")
                                UserDefaults.standard.set(menuItems, forKey: "menulogin")
                                
                                UserDefaults.standard.synchronize()
                            
                            }
                        
//                            NotificationCenter.default.post(name: Notification.Name("UpdateMaster"), object: self.lanDict)
                        }
//                    self.navigationController?.popViewController(animated: true)
//                        for controller in self.navigationController!.viewControllers as Array {
//                                                                            if controller.isKind(of: buyMeds.self) {
//
//                                                                                self.navigationController!.popToViewController(controller, animated: false)
//                                                                                break
//                                                                            }
//                                                                        }
                        
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "buyMeds") as? buyMeds
                               
                                 self.navigationController?.pushViewController(vc!, animated: true)
                        
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
        

    }
    
    
    func getCustomerD()
    {
        if Connectivity.isConnectedToInternet {
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["":""]
        
        let url =  GetCustDetails()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    let resultdict:NSDictionary = json as NSDictionary
                    self.custdict = resultdict.value(forKey: "CustomerDetails") as! NSDictionary
                   
                        self.settingsTableView.reloadData()
                    
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
    func GetMasters()
    {
        if Connectivity.isConnectedToInternet {
        
        let url =  GetUserMasters()
        
        let session = URLSession.shared
        
        let serviceUrl = URL(string: url)
        
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue("T1", forHTTPHeaderField: "accessToken")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    print(json)
                    
                    DispatchQueue.main.async {
                        let httpResponse = response as? HTTPURLResponse
                        
                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                        {
                            
                            let masterdicts:NSDictionary = json as NSDictionary
                            self.engDict = masterdicts.value(forKey: "English") as! NSDictionary
                            self.hinDict = masterdicts.value(forKey: "Hindi") as! NSDictionary
                            self.marDict = masterdicts.value(forKey: "Marathi") as! NSDictionary
                            
                            
                        }
                        else
                        {
                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
        
    }
    
    
    @objc func updateProfile()
    {
        let myIndexPath = NSIndexPath(row: currentRow, section: 0)
        
        let cell:EditProfileTableViewCell = settingsTableView.cellForRow(at: myIndexPath as IndexPath) as! EditProfileTableViewCell
       
        if cell.agetf.text! == ""
        {
          let localizedContent = NSLocalizedString("Please enter valid age", comment: "")
          Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
        else if cell.nametf.text! == ""
        {
         let localizedContent = NSLocalizedString("Please enter a valid name", comment: "")
         Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
//        else if cell.emailtf.text! == ""
//        {
//          Utility.showAlertWithTitle(title: "", andMessage: "Please Enter Email", onVC: self)
//        }
        else if cell.gendertf.text! == ""
        {
         let localizedContent = NSLocalizedString("Select your gender", comment: "")
         Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
        else{
        var vage = Int(cell.agetf.text!)
        
        if vage! < 18
        {
            cell.agetf.text = ""
            let localizedContent = NSLocalizedString("Sorry, Truemeds is only for people older than 18 years", comment: "")
            Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }else
        {
            
        if Connectivity.isConnectedToInternet {
        
        let custid:String = String(describing:UserDefaults.standard.value(forKey: "CustomerId")!)
            
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["customerId":custid]
        
        let json = ["customerName":cell.nametf.text!,
                    "mobileNo":"",
                    "ageGroupId":nil ,
                    "ageGroupName":nil,
                    "languageId":nil,
                    "languageName":nil,
                    "emailAddress":cell.emailtf.text!,
                    "profileImage":base64,
                    "profileImageUrl":nil,
                    "age":CLong(cell.agetf.text!),
                    "gender":genId,
                    "genderName":"",
                    "addressId":nil
            ] as [String : Any?]
        
        
        let url =  UpdateName()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: value))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        request.httpBody = jsonData
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                        
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                     self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
            }
            else
            {
                Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
            }
    
        }
    }
    }
    
    @objc func updateselection(sender:UIButton)
    {
      if ScreenID == 5
      {
        
        if UserDefaults.standard.object(forKey: "ageGroupId") == nil
        {
            
        }else
        {
        agegroupID = UserDefaults.standard.value(forKey: "ageGroupId") as! Int
        }
        
        if UserDefaults.standard.object(forKey: "CustomerId") != nil
        {
        updateSetts()
        }
        else
        {
            if self.lanDict == self.engDict
            {
                 Locale.updateLanguage(code: "en")
                
                UserDefaults.standard.set("English", forKey: "langpref")
                UserDefaults.standard.set(42, forKey: "languageId")
                

                
                self.mastdict = self.lanDict.value(forKey: "questions") as! NSDictionary
                
                let menuItems:NSArray = self.lanDict.value(forKey: "menuItems") as! NSArray
                let newuser:NSArray = menuItems.filter{!(($0 as! NSDictionary)["login"] as! Bool)} as NSArray
                
                                            let appintro:NSDictionary = self.lanDict.value(forKey: "appIntro") as! NSDictionary
                                            let userDefaults = UserDefaults.standard
                                                userDefaults.set(appintro, forKey: "appIntro")
                //                                userDefaults.synchronize()
                
                UserDefaults.standard.set(self.mastdict, forKey: "quesdict")
                UserDefaults.standard.set(newuser, forKey: "menunew")
                UserDefaults.standard.set(menuItems, forKey: "menulogin")
                
                UserDefaults.standard.synchronize()
            }
            else if self.lanDict == self.hinDict
            {
                Locale.updateLanguage(code: "hi")
                UserDefaults.standard.set("Hindi", forKey: "langpref")
                UserDefaults.standard.set(43, forKey: "languageId")
                self.mastdict = self.lanDict.value(forKey: "questions") as! NSDictionary
                
                let menuItems:NSArray = self.lanDict.value(forKey: "menuItems") as! NSArray
                let newuser:NSArray = menuItems.filter{!(($0 as! NSDictionary)["login"] as! Bool)} as NSArray
                
                                            let appintro:NSDictionary = self.lanDict.value(forKey: "appIntro") as! NSDictionary
                                            let userDefaults = UserDefaults.standard
                                                userDefaults.set(appintro, forKey: "appIntro")
                //                                userDefaults.synchronize()
                
                UserDefaults.standard.set(self.mastdict, forKey: "quesdict")
                UserDefaults.standard.set(newuser, forKey: "menunew")
                UserDefaults.standard.set(menuItems, forKey: "menulogin")
                
                UserDefaults.standard.synchronize()
            }
            else if self.lanDict == self.marDict
            {
                 Locale.updateLanguage(code: "mr-IN")
                UserDefaults.standard.set("Marathi", forKey: "langpref")
                UserDefaults.standard.set(44, forKey: "languageId")
                
                self.mastdict = self.lanDict.value(forKey: "questions") as! NSDictionary
                
                let menuItems:NSArray = self.lanDict.value(forKey: "menuItems") as! NSArray
                let newuser:NSArray = menuItems.filter{!(($0 as! NSDictionary)["login"] as! Bool)} as NSArray
                
                                            let appintro:NSDictionary = self.lanDict.value(forKey: "appIntro") as! NSDictionary
                                            let userDefaults = UserDefaults.standard
                                                userDefaults.set(appintro, forKey: "appIntro")
                //                                userDefaults.synchronize()
                
                UserDefaults.standard.set(self.mastdict, forKey: "quesdict")
                UserDefaults.standard.set(newuser, forKey: "menunew")
                UserDefaults.standard.set(menuItems, forKey: "menulogin")
                
                UserDefaults.standard.synchronize()
            }
            
            
//            NotificationCenter.default.post(name: Notification.Name("UpdateMaster"), object: self.lanDict)
            
//             for controller in self.navigationController!.viewControllers as Array {
//                                                                                       if controller.isKind(of: buyMeds.self) {
//
//                                                                                           self.navigationController!.popToViewController(controller, animated: false)
//                                                                                           break
//                                                                                       }
//                                                                                   }
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "buyMeds") as? buyMeds
                                         
                                           self.navigationController?.pushViewController(vc!, animated: true)
        }
        
    
      }
        
    }
    
    
    @objc func updateselection2(sender:UIButton)
    {
        if ScreenID == 6
        {
            
            if UserDefaults.standard.object(forKey: "languageId") == nil
            {
                
            }else
            {
                langID = UserDefaults.standard.value(forKey: "languageId") as! Int
            }
            
            if UserDefaults.standard.object(forKey: "CustomerId") != nil
            {
                updateSetts()
            }
            else
            {
                
            UserDefaults.standard.set(agegroupID, forKey: "ageGroupId")
            UserDefaults.standard.synchronize()
            NotificationCenter.default.post(name: Notification.Name("UpdateAgeGroup"), object: nil)
            self.navigationController?.popViewController(animated: true)
            }
            
        }
    }
    
    
    @objc func changerow(sender:AnyObject)
    {
        prevbtn.isHidden = false
        
        if sender.tag == 1
        {
        ScreenID = 2
        
        if currentRow < setarray.count - 1 {
            DispatchQueue.main.async {
                self.settingsTableView.scrollToRow(at: NSIndexPath(row: self.currentRow + 1, section: 0) as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                self.currentRow += 1
            }
        }
        settingsTableView.reloadData()
        }
        
        if sender.tag == 2
        {
         let localizedContent = NSLocalizedString("This feature is coming soon", comment: "")
         Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
            
//            ScreenID = 3
//
//            if currentRow < setarray.count - 1 {
//                DispatchQueue.main.async {
//                    self.settingsTableView.scrollToRow(at: NSIndexPath(row: self.currentRow + 1, section: 0) as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
//                    self.currentRow += 1
//                }
//            }
//            settingsTableView.reloadData()
        }
        
        if sender.tag == 3
        {
            ScreenID = 4
            
            if currentRow < setarray.count - 1 {
                DispatchQueue.main.async {
                    self.settingsTableView.scrollToRow(at: NSIndexPath(row: self.currentRow + 1, section: 0) as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    self.currentRow += 1
                }
            }
            settingsTableView.reloadData()
        }
        
        if sender.tag == 42
        {
            ScreenID = 5
            langID = sender.tag
            lanDict = engDict
            
            if currentRow < setarray.count - 1 {
                DispatchQueue.main.async {
                    self.settingsTableView.scrollToRow(at: NSIndexPath(row: self.currentRow + 1, section: 0) as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    self.currentRow += 1
                }
            }
            settingsTableView.reloadData()
        }
        
        if sender.tag == 43
        {
            ScreenID = 5
            langID = sender.tag
            lanDict = hinDict
            
            if currentRow < setarray.count - 1 {
                DispatchQueue.main.async {
                    self.settingsTableView.scrollToRow(at: NSIndexPath(row: self.currentRow + 1, section: 0) as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    self.currentRow += 1
                }
            }
            settingsTableView.reloadData()
        }
        
        if sender.tag == 44
        {
            ScreenID = 5
            langID = sender.tag
            lanDict = marDict
            
            if currentRow < setarray.count - 1 {
                DispatchQueue.main.async {
                    self.settingsTableView.scrollToRow(at: NSIndexPath(row: self.currentRow + 1, section: 0) as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    self.currentRow += 1
                }
            }
            settingsTableView.reloadData()
        }
        
        if sender.tag == 40
        {
            ScreenID = 6
            agegroupID = 40
            
            if currentRow < setarray.count - 1 {
                DispatchQueue.main.async {
                    self.settingsTableView.scrollToRow(at: NSIndexPath(row: self.currentRow + 1, section: 0) as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    self.currentRow += 1
                }
            }
            settingsTableView.reloadData()
        }
        
        if sender.tag == 41
        {
            ScreenID = 6
            agegroupID = 41
            
            if currentRow < setarray.count - 1 {
                DispatchQueue.main.async {
                    self.settingsTableView.scrollToRow(at: NSIndexPath(row: self.currentRow + 1, section: 0) as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    self.currentRow += 1
                }
            }
            settingsTableView.reloadData()
        }
        
        
    }
    
    @objc func prevcell()
    {
        //ScreenID = 1
        view.endEditing(true)
        
        lanDict = [:]
        
        if currentRow == 0 || ScreenID == 1
        {
           // prevbtn.isHidden = true
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            ScreenID = 1
            
                settingsTableView.scrollToRow(at: NSIndexPath(row: currentRow - 1, section: 0) as IndexPath, at: UITableView.ScrollPosition.top, animated: true)
                currentRow -= 1
                prevbtn.isHidden = false
        }
    
    }
    
    
    @objc func popvc()
    {
     self.navigationController?.popViewController(animated: true)
    }

}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
