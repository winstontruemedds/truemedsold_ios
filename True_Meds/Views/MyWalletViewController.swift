//
//  MyWalletViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Alamofire
import Contacts
import Reachability

class MyWalletViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,FSPagerViewDataSource,FSPagerViewDelegate{
    
    
    let store = CNContactStore()
    
    let reachability = Reachability()!
    
    @IBOutlet weak var referearnbtno: UIButton!
    
    @IBOutlet weak var viewtransbtno: UIButton!
    
    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var tmcashlbl: UILabel!
    
    var ledgerarr = NSArray()
    
    @IBOutlet weak var transtable: UITableView!
    
    @IBOutlet weak var transview: UIView!
    
    @IBOutlet weak var dismissviu: UIButton!
    
    var imgsarr = NSArray()
    

    fileprivate let imageNames = ["offers","offers","offers","offers","offers","offers","offers"]
    fileprivate let transformerNames = ["cross fading", "zoom out", "depth", "linear", "overlap", "ferris wheel", "inverted ferris wheel", "coverflow", "cubic"]
    fileprivate let transformerTypes: [FSPagerViewTransformerType] = [.crossFading,
                                                                      .zoomOut,
                                                                      .depth,
                                                                      .linear,
                                                                      .overlap,
                                                                      .ferrisWheel,
                                                                      .invertedFerrisWheel,
                                                                      .coverFlow,
                                                                      .cubic]
    
    fileprivate var typeIndex = 0 {
        didSet {
            let type = self.transformerTypes[0]
            self.pagerView.transformer = FSPagerViewTransformer(type:type)
            switch type {
            case .crossFading, .zoomOut, .depth:
                self.pagerView.itemSize = FSPagerView.automaticSize
                self.pagerView.decelerationDistance = 1
            case .linear,.overlap:
                let transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.pagerView.itemSize = FSPagerView.automaticSize
                
                self.pagerView.decelerationDistance = FSPagerView.automaticDistance
                
            case .ferrisWheel, .invertedFerrisWheel:
                self.pagerView.itemSize = CGSize(width: 180, height: 140)
                self.pagerView.decelerationDistance = FSPagerView.automaticDistance
            case .coverFlow:
                self.pagerView.itemSize = CGSize(width: 220, height: 170)
                self.pagerView.decelerationDistance = FSPagerView.automaticDistance
            case .cubic:
                let transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                self.pagerView.itemSize = self.pagerView.frame.size.applying(transform)
                self.pagerView.decelerationDistance = 1
            }
        }
    }
    
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.typeIndex = 0
        }
    }
    
    
    
    
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let index = self.typeIndex
        self.typeIndex = index
        
    }
    
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return imgsarr.count
    }
    
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        let imgstr:String = imgsarr[index] as! String
        let imgurl:URL = URL(string: imgstr)!
        let data = try? Data(contentsOf: imgurl)
        cell.imageView?.image = UIImage(data: data!)
        
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.clipsToBounds = true
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dismissviu.isHidden = true
        
        transview.isHidden = true

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        
        let svgimg1: SVGKImage = SVGKImage(named: "back")
        dismissviu.setImage(svgimg1.uiImage, for: .normal)
        
        referearnbtno.layer.borderWidth = 0.8
        referearnbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        referearnbtno.layer.cornerRadius = 4.0
        
        viewtransbtno.layer.borderWidth = 0.8
        viewtransbtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        viewtransbtno.layer.cornerRadius = 4.0
        
       
        getwalletdetails()
        
        gettransdetails()
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }

    @IBAction func backto(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func viewtransbtna(_ sender: UIButton) {
       if ledgerarr.count == 0
       {
       let localizedContent = NSLocalizedString("No transactions found", comment: "")
       Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
       }
       else{
        transview.isHidden = false
        dismissviu.isHidden = false
        }
    }
    
    
    @IBAction func refernearnbtna(_ sender: UIButton) {
        
    //Utility.showAlertWithTitle(title:"", andMessage: "Refer & Earn coming soon", onVC: self)
     CNContactStore().requestAccess(for: .contacts) { (access, error) in
                    print("Access: \(access)")
                   if access == false{
                       DispatchQueue.main.async {
                           print("no")
                          
                           let localizedContent = NSLocalizedString("Please enable contacts permission from settings", comment: "")
                           Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
                       }
                   }else{
                       
                       DispatchQueue.main.async {
                             print("yes")
                           
                             let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RefernEarnVC") as? RefernEarnVC
                            self.navigationController?.pushViewController(vc!, animated: true)
                       }
                       
                   }
                   
                  }
                       

        

//        store.requestAccess(for: .contacts) { (granted, err) in
//            if let err = err {
//                print("Failed to request access:", err)
//                let localizedContent = NSLocalizedString("Please enable contacts permission from settings", comment: "")
//                Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
//                return
//            }
//
//            if granted {
//                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RefernEarnVC") as? RefernEarnVC
//                self.navigationController?.pushViewController(vc!, animated: true)
//            } else {
//                let localizedContent = NSLocalizedString("Please enable contacts permission from settings", comment: "")
//                Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
//            }
//        }

    }
    
    @IBAction func showmainviu(_ sender: Any) {
        if transview.isHidden == false
        {
           transview.isHidden = true
           dismissviu.isHidden = true
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ledgerarr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactioncell") as! PresImgTableViewCell
        let finledgedict:NSDictionary = self.ledgerarr[indexPath.row] as! NSDictionary
        cell.trname.text = finledgedict.value(forKey: "transaction") as? String
        cell.trdate.text = finledgedict.value(forKey: "transactionDate") as? String
       
        if let spent = finledgedict.value(forKey: "spent") as? Double
        {
        cell.trspent.text = String(format: "%.2f", spent)
        }
        
        if cell.trspent.text == "0.00"
        {
        cell.trspent.text = ""
        }
        
        if let earned = finledgedict.value(forKey: "earned") as? Double
        {
            cell.trearned.text = String(format: "%.2f", earned)
        }
        
        if cell.trearned.text == "0.00"
        {
            cell.trearned.text = ""
        }
        
        if let balance = finledgedict.value(forKey: "balance") as? Double
        {
            cell.trbalance.text = String(format: "%.2f", balance)
        }
        
        if cell.trbalance.text == "0.00"
        {
            cell.trbalance.text = ""
        }
        
        return cell
    }
    
    
    func getwalletdetails()
    {
        if Connectivity.isConnectedToInternet {
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["":""]
        
        let url =  GetWalletDetails()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                
                    DispatchQueue.main.async {
                        let httpResponse = response as? HTTPURLResponse
                        
                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                        {
                            print(json)
                            
                            let resdict:NSDictionary = json as NSDictionary
                            let finwalletdict:NSDictionary = resdict.value(forKey: "payload") as! NSDictionary
                            
                            if let truemedsCash = finwalletdict.value(forKey: "truemedsCash") as? Double
                            {
                            self.tmcashlbl.text = "₹ " + String(format: "%.2f", truemedsCash)
                            }
                            
                            self.imgsarr = resdict.value(forKey: "howToEarn") as! NSArray
                           
                            self.pagerView.reloadData()
                            
                        }
                        else
                        {
                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    

    func gettransdetails()
    {
        if Connectivity.isConnectedToInternet {
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["":""]
        
        let url =  GetMyTransactions()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                     DispatchQueue.main.async {
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    print(json)
                        
                    let resdict:NSDictionary = json as NSDictionary
                    let payloadict:NSDictionary = resdict.value(forKey: "payload") as! NSDictionary
                
                    self.ledgerarr = payloadict.value(forKey: "ledger") as! NSArray
                    
                    self.transtable.reloadData()
                    
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    

}
