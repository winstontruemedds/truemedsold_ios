//
//  OrderDescriptionViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import SDWebImage
import Reachability
import Alamofire

class OrderDescriptionViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate {
   
    @IBOutlet weak var sumorderdetview: UIView!

    @IBOutlet weak var sumdosageview: UIView!
    
    @IBOutlet weak var saveviu: UIView!
    
    @IBOutlet weak var saveviu2: UIView!
    
    @IBOutlet weak var segcontrol: UISegmentedControl!
    
    @IBOutlet weak var backto: UIButton!
    
    var ordersumarr = NSArray()
    
    @IBOutlet weak var sumordertable: UITableView!
    
    @IBOutlet weak var sumtotalbl: UILabel!
    
    @IBOutlet weak var sumdelilbl: UILabel!
    
    @IBOutlet weak var sumcashandling: UILabel!
    
    @IBOutlet weak var sumdiscount: UILabel!
    
    @IBOutlet weak var sumTMcash: UILabel!
    
    @IBOutlet weak var sumTMtotal: UILabel!
    
    @IBOutlet weak var sumtableheight: NSLayoutConstraint!
    
    @IBOutlet weak var sumdosagetable: UITableView!
    
    @IBOutlet weak var sumdosheight: NSLayoutConstraint!
    
    var findosage = NSDictionary()
    
    var dosagearr = NSArray()
    
    var orderinfoarr = NSArray()
    
    @IBOutlet weak var orderIdlbl: UILabel!
    
    @IBOutlet weak var orderstatuslbl: UILabel!
    
    @IBOutlet weak var orderpresimg: UILabel!
    
    @IBOutlet weak var orderdatelbl: UILabel!
    
    @IBOutlet weak var headingorderIdlbl: UILabel!
    
    @IBOutlet weak var presimgstable: UITableView!
    
    var odhelpdict3 = NSDictionary()
    
    var pimgarr = NSArray()
    
    @IBOutlet weak var savelbl1: UILabel!
    
    @IBOutlet weak var savelbl2: UILabel!
    
    @IBOutlet weak var dossavelbl1: UILabel!
    
    @IBOutlet weak var dossavelbl2: UILabel!
    
    @IBOutlet weak var Ttllbl: UILabel!
    
    @IBOutlet weak var Ttldislbl: UILabel!
    
    @IBOutlet weak var Tmtllbl: UILabel!
    
    let reachability = Reachability()!
    
    var savedstr:String!
    
    var savedstr2:String!
    
    var savedstr3:String!
    
    var savedstr4:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.sumdosagetable.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        
        
        let statId:CLong = odhelpdict3.value(forKey: "statusId") as! CLong
        
        if statId == 142
        {
            Ttllbl.text = NSLocalizedString("Estimated Total", comment: "")
            Ttldislbl.text = NSLocalizedString("Estimated Total discount", comment: "")
            Tmtllbl.text = NSLocalizedString("Estimated Truemeds total", comment: "")
            savedstr = "You will save approximately ₹ "
            savedstr2 = " on this order"
            savedstr3 = "against your original prescription cost of ₹ "
            savedstr4 = "demotext"
        }
        else
        {
            Ttllbl.text = NSLocalizedString("Total", comment: "")
            Ttldislbl.text = NSLocalizedString("Total discount", comment: "")
            Tmtllbl.text = NSLocalizedString("Truemeds total", comment: "")
            savedstr = "You saved ₹ "
            savedstr2 = " on this orders"
            savedstr3 = "against your original prescription cost of ₹ "
            savedstr4 = "demotext2"
        }
        
        presimgstable.isHidden = true
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backto.setImage(svgimg.uiImage, for: .normal)
        
        sumorderdetview.isHidden = false
        sumdosageview.isHidden = true
       
        GetOrderSummary()
        
        saveviu.layer.cornerRadius = 4.0
        
        saveviu2.layer.cornerRadius = 4.0
        
        orderpresimg.isUserInteractionEnabled = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(didtapimglbl))
        orderpresimg.addGestureRecognizer(tap)
        
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }

    @objc func didtapimglbl()
    {
       if pimgarr.count == 0
       {
       let localizedContent = NSLocalizedString("No prescriptions found", comment: "")
       Utility.showAlertWithTitle(title: "", andMessage:localizedContent, onVC: self)
       }
       else
       {
       presimgstable.isHidden = false
       }
    }
    
    
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        sumdosagetable.layer.removeAllAnimations()
        sumdosheight.constant = sumdosagetable.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.sumdosagetable.updateConstraints()
            self.sumdosagetable.layoutIfNeeded()
        }

    }
    
    
    @IBAction func changetab(_ sender: Any) {
        if segcontrol.selectedSegmentIndex == 0
        {
            sumorderdetview.isHidden = false
            sumdosageview.isHidden = true
        }
        else
        {
            if self.dosagearr.count == 0
            {
             segcontrol.selectedSegmentIndex = 0
             let localizedContent = NSLocalizedString("Dosage details not found for this order", comment: "")
             Utility.showAlertWithTitle(title: "", andMessage:localizedContent, onVC: self)
            }else{
            sumorderdetview.isHidden = true
            sumdosageview.isHidden = false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count:Int
        if tableView == sumordertable
        {
        count = ordersumarr.count
        }
        else if tableView == presimgstable
        {
          count = pimgarr.count
        }
        else
        {
        count = dosagearr.count
        }
        return count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if tableView == sumordertable
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsSumTableViewCell") as! OrderDetailsTableViewCell
        
        cell.bgview.layer.cornerRadius = 4.0
        cell.bgview.layer.borderWidth = 1.0
        cell.bgview.layer.borderColor = UIColor(hexString: "#EDEDED", alpha: 1.0).cgColor
        cell.bgview.clipsToBounds = true
        let finsumdict:NSDictionary = ordersumarr[indexPath.row] as! NSDictionary
        cell.summedname.text = finsumdict.value(forKey: "substituteProductName") as! String
        cell.summedqty.text = finsumdict.value(forKey: "substituteProductOty") as! String
            
        if let summedprice = finsumdict.value(forKey: "substituteProductPrice") as? Double
        {
        cell.summedprice.text = "₹ " + String(format: "%.2f", summedprice)
        }
            
        cell.sumormedname.text = finsumdict.value(forKey: "orderProductName") as! String
            
        if let sumormedp = finsumdict.value(forKey: "orderProducPrice") as? Double
        {
        cell.sumormedprice.text = "₹ " + String(format: "%.2f", sumormedp)
        }
            
        return cell
        }
        else if tableView == presimgstable
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PresImgTableViewCell") as! PresImgTableViewCell
        let pimgdict:NSDictionary = pimgarr[indexPath.row] as! NSDictionary
        if let imgstr:String = pimgdict.value(forKey: "imagePath") as? String
        {
       
        let imgurl:URL = URL(string: imgstr)!
        cell.RxImages.sd_setImage(with: imgurl, completed: nil)
        }
        
            
        return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DosageTableViewCell") as! DosageTableViewCell
            
           findosage = dosagearr[indexPath.row] as! NSDictionary
           cell.summedcinename.text = findosage.value(forKey: "medicineName") as! String
           cell.sumpacketsize.text = findosage.value(forKey: "pack") as? String
           
            if let count1 = findosage.value(forKey: "isMorning") as? String
            {
                if count1 == "0"
                {
                    cell.morview2.isHidden = true
                }else{
                    cell.morview2.isHidden = false
                    cell.morview2.layer.cornerRadius = 13.0
                    cell.morcount2.text = String(describing: findosage.value(forKey: "isMorning")!)
                    cell.morchrono2.text = findosage.value(forKey: "chronology") as! String
                    cell.morfreq2.text = findosage.value(forKey: "frequency") as! String
                    let anSVGImage: SVGKImage = SVGKImage(named: "dawn")
                    cell.morimg2.image = anSVGImage.uiImage
                }
            }
            
            if let count2 = findosage.value(forKey: "isAfternoon") as? String
            {
                if count2 == "0"
                {
                    cell.aftview2.isHidden = true
                }else{
                    cell.aftview2.isHidden = false
                    cell.aftview2.layer.cornerRadius = 13.0
                    cell.aftcount2.text = String(describing: findosage.value(forKey: "isAfternoon")!)
                    cell.aftchrono2.text = findosage.value(forKey: "chronology") as! String
                    cell.aftfreq2.text = findosage.value(forKey: "frequency") as! String
                    let anSVGImage: SVGKImage = SVGKImage(named: "sun")
                    cell.aftimg2.image = anSVGImage.uiImage
                }
            }
            
            if let count3 = findosage.value(forKey: "isEvening") as? String
            {
                if count3 == "0"
                {
                    cell.eveview2.isHidden = true
                }else{
                    cell.eveview2.isHidden = false
                    cell.eveview2.layer.cornerRadius = 13.0
                    cell.evecount2.text = String(describing: findosage.value(forKey: "isEvening")!)
                    cell.evechrono2.text = findosage.value(forKey: "chronology") as! String
                    cell.evefreq2.text = findosage.value(forKey: "frequency") as! String
                    let anSVGImage: SVGKImage = SVGKImage(named: "sunset")
                    cell.eveimg2.image = anSVGImage.uiImage
                }
            }
            
            if let count4 = findosage.value(forKey: "isNight") as? String
            {
                if count4 == "0"
                {
                    cell.nigview2.isHidden = true
                }else{
                    cell.nigview2.isHidden = false
                    cell.nigview2.layer.cornerRadius = 13.0
                    cell.nigcount2.text = String(describing: findosage.value(forKey: "isNight")!)
                    cell.nigchrono2.text = findosage.value(forKey: "chronology") as! String
                    cell.nigfreq2.text = findosage.value(forKey: "frequency") as! String
                    let anSVGImage: SVGKImage = SVGKImage(named: "moon")
                    cell.nigimg2.image = anSVGImage.uiImage
                }
            }
            
            if let count5 = findosage.value(forKey: "isSos") as? String
            {
                if count5 == "0"
                {
                    cell.onlview2.isHidden = true
                }
                else{
                    cell.onlview2.isHidden = false
                    cell.onlview2.layer.cornerRadius = 13.0
                    cell.onlcount2.text = String(describing: findosage.value(forKey: "isSos")!)
                    cell.onlchrono2.text = findosage.value(forKey: "chronology") as! String
                    cell.onlfreq2.text = findosage.value(forKey: "frequency") as! String
                    let anSVGImage: SVGKImage = SVGKImage(named: "Clock")
                    cell.onlimg2.image = anSVGImage.uiImage
                }
            }
            
            if cell.morview2.isHidden == true && cell.aftview2.isHidden == true && cell.eveview2.isHidden == true && cell.nigview2.isHidden == true && cell.onlview2.isHidden == true
            {
                cell.scroheight2.constant = 0
            }
            else
            {
                cell.scroheight2.constant = 135
            }
            
            
            
            return cell
        
        }
        
        return UITableViewCell()
    }
    
    
   func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var theight:CGFloat

        if tableView == sumordertable
        {
         theight = 140.0
        }
        else if tableView == sumdosagetable
        {
         theight = UITableView.automaticDimension
        }
        else
        {
         theight = presimgstable.bounds.size.height/2
        }

        return theight
    }
 
 
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var theight:CGFloat
        
        if tableView == sumordertable
        {
            theight = 140.0
        }
        else if tableView == sumdosagetable
        {
            theight = UITableView.automaticDimension
        }
        else
        {
          theight = presimgstable.bounds.size.height/2
        }
        
        return theight
    }
    

    @IBAction func backbtn(_ sender: Any) {
        if presimgstable.isHidden == false
        {
         presimgstable.isHidden = true
        }
        else
        {
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func GetOrderSummary()
    {
        if Connectivity.isConnectedToInternet {
        
        let orderID:CLong = odhelpdict3.value(forKey: "orderId") as! CLong
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["orderId":String(describing: orderID)]
        
        let url =  getOrderSummary()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                   
                    DispatchQueue.main.async {
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                        
                    print(json)
                    
                    let orderbilldict:NSDictionary = json as NSDictionary
                    let summybill:NSDictionary = orderbilldict.value(forKey: "OrderDetails") as! NSDictionary
                    self.ordersumarr = summybill.value(forKey: "medicines") as! NSArray
                    self.dosagearr = orderbilldict.value(forKey: "OrderDosage") as! NSArray
                    self.pimgarr = orderbilldict.value(forKey: "OrderImages") as! NSArray
                    
                    if let sumtotalbl = summybill.value(forKey: "total") as? Double
                    {
                     self.sumtotalbl.text = "₹ " + String(format: "%.2f", sumtotalbl)
                    }
                        
                    let deli:CLong = summybill.value(forKey: "deliveryFee") as! CLong
                    if deli == 0
                    {
                        self.sumdelilbl.text = NSLocalizedString("Free", comment: "")
                    }
                    else{
                        self.sumdelilbl.text = "₹ " + String(describing: summybill.value(forKey: "deliveryFee")!)
                    }
                        
                    let handling:CLong = summybill.value(forKey: "handlingFee") as! CLong
                    if handling == 0
                    {
                        self.sumcashandling.text = NSLocalizedString("Free", comment: "")
                    }
                    else{
                        self.sumcashandling.text = String(describing: summybill.value(forKey: "handlingFee")!)
                    }
                    
                    if let sumdiscount = summybill.value(forKey: "discount") as? Double
                    {
                     self.sumdiscount.text = "₹ " + String(format: "%.2f", sumdiscount)
                    }
                        
                    if let sumTMcash = summybill.value(forKey: "truemedsCash") as? Double
                    {
                    self.sumTMcash.text = "₹ " + String(format: "%.2f", sumTMcash)
                    }
                    
                    if let sumTMtotal = summybill.value(forKey: "truemedsTotal") as? Double
                    {
                    self.sumTMtotal.text = "₹ " + String(format: "%.2f", sumTMtotal)
                    }
                    
                    
                    
                    self.orderinfoarr = orderbilldict.value(forKey: "OrderInfo") as! NSArray
                    let OrderInfo:NSDictionary = self.orderinfoarr[0] as! NSDictionary
                    self.orderIdlbl.text = String(describing:OrderInfo.value(forKey: "OrderId")!)
                    self.orderstatuslbl.text = OrderInfo.value(forKey: "OrderStatus") as! String
                    
                    let mydate:CLong = OrderInfo.value(forKey: "OrderedOn") as! CLong
                    
                    let date = NSDate(timeIntervalSince1970: TimeInterval(mydate/1000))
                    
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMMM yyyy hh:mm"
                    
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
                    self.orderdatelbl.text = dateString
                    
                    self.headingorderIdlbl.text = String(describing:OrderInfo.value(forKey: "OrderId")!)
                        
                        if let savingprice = OrderInfo.value(forKey: "SavingPrice") as? Double
                        {
                            
                            self.savelbl1.text = NSLocalizedString(self.savedstr, comment: "") + String(format: "%.2f", savingprice) + NSLocalizedString(self.savedstr2, comment: "")
                            
                        }
                        
                        
                        if let totalprice = OrderInfo.value(forKey: "OrderPrice") as? Double
                        {
                            
                            self.savelbl2.text = NSLocalizedString(self.savedstr3, comment: "") + String(format: "%.2f", totalprice) + NSLocalizedString(self.savedstr4, comment: "")
                            
                        }
                        
                        if let savingprice2 = OrderInfo.value(forKey: "SavingPrice") as? Double
                        {
                            
                            self.dossavelbl1.text = NSLocalizedString(self.savedstr, comment: "") + String(format: "%.2f", savingprice2) + NSLocalizedString(self.savedstr2, comment: "")
                            
                        }
                        
                        
                        if let totalprice2 = OrderInfo.value(forKey: "OrderPrice") as? Double
                        {
                            
                            self.dossavelbl2.text = NSLocalizedString(self.savedstr3, comment: "") + String(format: "%.2f", totalprice2) + NSLocalizedString(self.savedstr4, comment: "")
                            
                        }
                        
                        
                    DispatchQueue.main.async {
                        self.sumordertable.reloadData()
                        self.sumtableheight.constant = self.sumordertable.contentSize.height
                        self.sumdosagetable.reloadData()
                        self.sumdosheight.constant = self.sumdosagetable.contentSize.height
                        self.presimgstable.reloadData()
                    }
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }

}
