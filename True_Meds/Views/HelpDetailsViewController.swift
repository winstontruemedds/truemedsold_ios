//
//  HelpDetailsViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class HelpDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    let reachability = Reachability()!
    
    @IBOutlet weak var backbtno: UIButton!
    
    var hcdetailarr = NSArray()
    
    var hid:String!
    
    @IBOutlet weak var Detailheader: UILabel!
    
    @IBOutlet weak var helpdetailstable: UITableView!
    
    var detailstr:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Detailheader.text = detailstr
        
         getHelpCatdetails()
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hcdetailarr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpDetailsTableViewCell") as! HelpTableViewCell
        let finissuedict:NSDictionary = self.hcdetailarr[indexPath.row] as! NSDictionary
        cell.hissuelbl.text = finissuedict.value(forKey: "issues") as! String
        let svgimg2: SVGKImage = SVGKImage(named: "green_arrow")
        cell.hgrarr.image = svgimg2.uiImage
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RaiseTicketViewController") as? RaiseTicketViewController
        vc!.hcdetailsdict = self.hcdetailarr[indexPath.row] as! NSDictionary
        vc!.detailstr2 = detailstr
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    
    func getHelpCatdetails()
    {
        if Connectivity.isConnectedToInternet {
        
        let para2 = ["id":hid]
        
        let url =  GetHelpCatDet()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        // request.addValue("T1", forHTTPHeaderField: "accessToken")
        
        //request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                print(json)
                    
                let httpResponse = response as? HTTPURLResponse
                    
                if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                {
                let dict:NSDictionary = json as NSDictionary
                self.hcdetailarr = dict.value(forKey: "Category") as? NSArray ?? []
                    DispatchQueue.main.async {
                        self.helpdetailstable.reloadData()
                        self.helpdetailstable.tableFooterView = UIView()
                    }
                }
                else
                {
                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
               
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
        
    }

}
