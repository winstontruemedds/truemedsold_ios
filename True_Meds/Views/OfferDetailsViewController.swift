//
//  OfferDetailsViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire




class OfferDetailsViewController: UIViewController {
    
    
 

    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var applybtnins: UIButton!
    
    var offId:CLong!
    
    var offdetails = NSDictionary()
    
    @IBOutlet weak var detimg: UIImageView!
    
    @IBOutlet weak var dettitle: UILabel!
    
    @IBOutlet weak var detdesc: UILabel!
    
    @IBOutlet weak var detvalid: UILabel!
    
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.value(forKey: "applyCoupen") as! Bool == true{
            applybtnins.isHidden = false
              }else{
            applybtnins.isHidden = true
              }

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        applybtnins.layer.cornerRadius = 4.0
        
        getofferdetails()
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }

    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func applycoupbtn(_ sender: Any) {
        
        
        print(offdetails)
        
        let dict = offdetails.value(forKey: "offersDetails") as? NSDictionary
        
        let applicableOn = dict?.value(forKey: "applicableOn") as? String
        let Offertitle = dict?.value(forKey: "title") as? String
        let description = dict?.value(forKey: "description") as? String
       
        
       
         UserDefaults.standard.set(offId, forKey: "offerId")
        UserDefaults.standard.set(applicableOn, forKey: "applicableOn")
        UserDefaults.standard.set(Offertitle, forKey: "offertitel")
        UserDefaults.standard.set(description, forKey: "descriptionCoupen")
         UserDefaults.standard.synchronize()
        
        
        if applybtnins.backgroundColor == UIColor(hexString: "#0071BC")
        {
          applybtnins.backgroundColor = UIColor(hexString: "#22B573")
        }
        else
        {
           applybtnins.backgroundColor = UIColor(hexString: "#0071BC")
        }
        
        for controller in self.navigationController!.viewControllers as Array {
                       if controller.isKind(of: orderPaymentViewController.self) {
                           
                           self.navigationController!.popToViewController(controller, animated: false)
                           break
                       }
                   }
    }
    
    
    func getofferdetails()
    {
        if Connectivity.isConnectedToInternet {
        
        let para2 = ["offersId":CLong(offId)] as [String : Any]
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let url =  GetOfferDetails()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
//                    print(json)
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    let offdet:NSDictionary = json as NSDictionary
                        
                        DispatchQueue.main.async {
                        self.offdetails = offdet.value(forKey: "offersDetails") as! NSDictionary
                        let findetails:NSDictionary = self.offdetails.value(forKey: "offersDetails") as! NSDictionary
                            self.detdesc.text = findetails.value(forKey: "description") as? String
                            self.dettitle.text = findetails.value(forKey: "title") as? String
                        let imgstr:String = findetails.value(forKey: "image") as! String
                        let imgurl:URL = URL(string: imgstr)!
                        self.detimg.sd_setImage(with: imgurl, completed: nil)
                        let mydate:CLong = findetails.value(forKey: "validity") as! CLong
                        let mydates:CLong = mydate/1000
                        let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
                            
                        let dayTimePeriodFormatter = DateFormatter()
                        dayTimePeriodFormatter.dateFormat = "dd MMMM yyyy"
                            
                        let dateString = dayTimePeriodFormatter.string(from: date as Date)
                        self.detvalid.text = "Valid till: \(dateString)"
                        }
                   
                    
                   
                        
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
        
    }
    
}
