//
//  EditOrderViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class EditOrderViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    let reachability = Reachability()!
    
    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var prevarrowbtn: UIButton!
    
    @IBOutlet weak var nextarrowbtn: UIButton!
    
    @IBOutlet weak var deletebtno: UIButton!
    
    @IBOutlet weak var reuploadbtno: UIButton!
    
    @IBOutlet weak var errorimgs: UIImageView!
    
    @IBOutlet weak var symbolimg: UIImageView!
    
    @IBOutlet weak var symbolviu: UIView!
    
    @IBOutlet weak var commentlbl: UILabel!
    
    @IBOutlet weak var epatientnamelbl: UILabel!
    
    @IBOutlet weak var cardviu1: UIView!
    
    @IBOutlet weak var cardviu2: UIView!
    
    var isFrom = String()
    
    var errorArr = NSArray()
    
    var currInt:Int = 0
    
    var odhelpdict6 = NSDictionary()
    
    var imgIds2 = [CLong]()
    
    var orderID:CLong!
    
    var PatientID:CLong!
    
    var filenameID:Int! = 0
    
    var imgbase64arr:String!
    
    var reImgId:CLong!
    
    @IBOutlet weak var errcountlbl: UILabel!
    
    @IBOutlet weak var actInd: UIActivityIndicatorView!

    var EditDict = ["patientName":String(),"patientId":CLong(),"imagePath":String(),"imageId":CLong(),"errorReason":String(),"error":CLong()] as [String : Any]
    
    var EditArr = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkEditOrder()

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        reuploadbtno.layer.borderWidth = 0.8
        reuploadbtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        reuploadbtno.layer.cornerRadius = 4.0
        
        deletebtno.layer.borderWidth = 0.8
        deletebtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        deletebtno.layer.cornerRadius = 4.0
        
        prevarrowbtn.isHidden = true
        
        cardviu1.layer.cornerRadius = 4.0
        cardviu1.layer.borderWidth = 0.8
        cardviu1.layer.borderColor = UIColor.darkGray.cgColor
        
        cardviu2.layer.cornerRadius = 4.0
        cardviu2.layer.borderWidth = 0.8
        cardviu2.layer.borderColor = UIColor.darkGray.cgColor
        
        symbolviu.layer.cornerRadius = 4.0
        
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    @IBAction func backto(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func nextImage(_ sender: UIButton) {
        
        self.currInt = self.currInt + 1
        
        if EditArr.count > currInt
        {
            prevarrowbtn.isHidden = false
            
            let finpatdict:NSDictionary = self.EditArr[self.currInt] as NSDictionary
            
            self.epatientnamelbl.text = finpatdict.value(forKey: "patientName") as? String
            PatientID = finpatdict.value(forKey: "patientId") as? CLong
            let imgstr:String = finpatdict.value(forKey: "imagePath") as! String
            let imgstr2:String = imgstr.replacingOccurrences(of: "\\", with: "/")
            let finurl:String = imgstr2
            
            let imgurl:URL = URL(string: finurl)!
            self.errorimgs.sd_setImage(with: imgurl, completed: nil)
            let errReason:String = finpatdict.value(forKey: "errorReason") as! String
            
            self.commentlbl.text = "Comment: " + errReason
            
            let errorcode:CLong = finpatdict.value(forKey: "error") as! CLong
            if errorcode == 0
            {
                let svgimg4: SVGKImage = SVGKImage(named: "correctimg")
                self.symbolimg.image = svgimg4.uiImage
                self.symbolviu.backgroundColor = UIColor(hexString: "#22B573")
                self.reuploadbtno.isHidden = true
                self.deletebtno.isHidden = true
            }else
            {
                let svgimg4: SVGKImage = SVGKImage(named: "error")
                self.symbolimg.image = svgimg4.uiImage
                self.symbolviu.backgroundColor = UIColor.red
                self.reuploadbtno.isHidden = false
                self.deletebtno.isHidden = false
                self.imgIds2 = [finpatdict.value(forKey: "imageId") as! CLong]
                self.reImgId = (finpatdict.value(forKey: "imageId") as! CLong)
                
            }
            
            if EditArr.count - 1 == currInt
            {
             nextarrowbtn.isHidden = true
            }
            
        }
        
    }
    
    
    @IBAction func previousImage(_ sender: UIButton) {
        
        self.currInt = self.currInt - 1
        
        if currInt >= 0
        {
            nextarrowbtn.isHidden = false
            
            let finpatdict:NSDictionary = self.EditArr[self.currInt] as NSDictionary
            
            self.epatientnamelbl.text = finpatdict.value(forKey: "patientName") as? String
            PatientID = finpatdict.value(forKey: "patientId") as? CLong
            let imgstr:String = finpatdict.value(forKey: "imagePath") as! String
            let imgstr2:String = imgstr.replacingOccurrences(of: "\\", with: "/")
            let finurl:String = imgstr2
            
            let imgurl:URL = URL(string: finurl)!
            self.errorimgs.sd_setImage(with: imgurl, completed: nil)
            let errReason:String = finpatdict.value(forKey: "errorReason") as! String
            
            self.commentlbl.text = "Comment: " + errReason
            
            let errorcode:CLong = finpatdict.value(forKey: "error") as! CLong
            if errorcode == 0
            {
                let svgimg4: SVGKImage = SVGKImage(named: "correctimg")
                self.symbolimg.image = svgimg4.uiImage
                self.symbolviu.backgroundColor = UIColor(hexString: "#22B573")
                self.reuploadbtno.isHidden = true
                self.deletebtno.isHidden = true
            }else
            {
                let svgimg4: SVGKImage = SVGKImage(named: "error")
                self.symbolimg.image = svgimg4.uiImage
                self.symbolviu.backgroundColor = UIColor.red
                self.reuploadbtno.isHidden = false
                self.deletebtno.isHidden = false
                self.imgIds2 = [finpatdict.value(forKey: "imageId") as! CLong]
                self.reImgId = (finpatdict.value(forKey: "imageId") as! CLong)
                
            }
            
            if currInt == 0
            {
            prevarrowbtn.isHidden = true
            }
            
        }
        
        
    }
    
    
    
    
    @IBAction func ReuploadImage(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title:NSLocalizedString("Camera", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                print("error")
            }
            
        })
        
        camera.setValue(UIColor.black, forKey: "titleTextColor")
        
        actionSheet.addAction(camera);
        
        let gallery = UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                print("error")
            }
        })
        
        gallery.setValue(UIColor.black, forKey: "titleTextColor")
        
        actionSheet.addAction(gallery);
        
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: {
            (alert: UIAlertAction!) in
            
            self.dismiss(animated: true, completion: nil);
            
        })
        
        
        actionSheet.addAction(cancel);
        
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0 , height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        present(actionSheet, animated: true, completion: nil);
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let images = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        {
            
            let imageData:NSData = images.jpegData(compressionQuality: 0.4) as! NSData
            imgbase64arr = imageData.base64EncodedString(options: .lineLength64Characters)
            
            UploadErrorRX()
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    
    
    @IBAction func DeleteImage(_ sender: UIButton) {
        let localizedContent = NSLocalizedString("Are you sure you want to delete the images?", comment: "")
        let alertVC = UIAlertController(title: title, message: localizedContent, preferredStyle: .alert)
        let Action1 = UIAlertAction(title:NSLocalizedString("Yes", comment: ""), style: .default)
        { (action:UIAlertAction!) in
        
            self.Deleteimgs()
        }
        alertVC.addAction(Action1);
        let Action2 = UIAlertAction(title:NSLocalizedString("No", comment: ""), style: .default, handler: nil);
        alertVC.addAction(Action2);
        self.present(alertVC, animated: true, completion: nil);

    }
    
    
    
    func checkEditOrder()
    {
        if Connectivity.isConnectedToInternet {
        
        let orderID:CLong = odhelpdict6.value(forKey: "orderId") as! CLong
        
        let para2 = ["orderId":String(describing: orderID)]
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let url =  checkEditO()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    print(json)
                    
                    DispatchQueue.main.async {
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                        
                        let resdict:NSDictionary = json as NSDictionary
                        let errorlist:NSDictionary = resdict.value(forKey: "OrderList") as! NSDictionary
                        self.orderID = errorlist.value(forKey: "orderId") as! CLong
                        self.errorArr = errorlist.value(forKey: "patientList") as! NSArray
                        if self.errorArr.count == 0
                        {
                            
                            if self.isFrom == "buyMeds"{
                                 self.navigationController?.popViewController(animated: true)
                            }else{
                                for controller in self.navigationController!.viewControllers as Array {
                                                              if controller.isKind(of: OrderListingViewController.self) {
                                                                  NotificationCenter.default.post(name: Notification.Name("OnHold"), object: nil)
                                                                  self.navigationController!.popToViewController(controller, animated: true)
                                                                  break
                                                              }
                            }
                          
                            }
                        }
                        else
                        {
                            
                        self.currInt = 0
                        
                        self.EditArr.removeAll()
                        
                        for i in 0..<self.errorArr.count
                        {
                            
                        let edpatdict:NSDictionary = self.errorArr[i] as! NSDictionary
                        self.EditDict.updateValue(edpatdict.value(forKey: "patientName") as! String, forKey: "patientName")
                        self.EditDict.updateValue(edpatdict.value(forKey: "patientId") as! CLong, forKey: "patientId")
                        let edimgarr:NSArray = edpatdict.value(forKey: "imageList") as! NSArray
                         for j in 0..<edimgarr.count
                         {
                          let imgpatdict:NSDictionary = edimgarr[j] as! NSDictionary
                          self.EditDict.updateValue(imgpatdict.value(forKey: "imagePath") as! String, forKey: "imagePath")
                          self.EditDict.updateValue(imgpatdict.value(forKey: "imageId") as! CLong, forKey: "imageId")
                          self.EditDict.updateValue(imgpatdict.value(forKey: "errorReason") as! String, forKey: "errorReason")
                          self.EditDict.updateValue(imgpatdict.value(forKey: "error") as! CLong, forKey: "error")
                          self.EditArr.append(self.EditDict)
                            
                         }
                            
                        }
                            
                        if self.EditArr.count == 1
                        {
                            self.prevarrowbtn.isHidden = true
                            self.nextarrowbtn.isHidden = true
                        }
                            
                        let newuser:NSArray = self.EditArr.filter{(($0 as NSDictionary)["error"] as! CLong) == 1} as NSArray
                            
                        self.errcountlbl.text = String(describing:newuser.count) + "/" + String(describing:self.EditArr.count)
                            
                        let finpatdict:NSDictionary = self.EditArr[self.currInt] as NSDictionary
                            
                        self.epatientnamelbl.text = finpatdict.value(forKey: "patientName") as? String
                        
                        let imgstr:String = finpatdict.value(forKey: "imagePath") as! String
                        let imgstr2:String = imgstr.replacingOccurrences(of: "\\", with: "/")
                        let finurl:String = imgstr2
                            
                        let imgurl:URL = URL(string: finurl)!
                        self.errorimgs.sd_setImage(with: imgurl, completed: nil)
                        let errReason:String = finpatdict.value(forKey: "errorReason") as! String
                            
                        self.commentlbl.text = "Comment: " + errReason
                            
                        let errorcode:CLong = finpatdict.value(forKey: "error") as! CLong
                        if errorcode == 0
                        {
                            let svgimg4: SVGKImage = SVGKImage(named: "correctimg")
                            self.symbolimg.image = svgimg4.uiImage
                            self.symbolviu.backgroundColor = UIColor(hexString: "#22B573")
                            self.reuploadbtno.isHidden = true
                            self.deletebtno.isHidden = true
                        }else
                        {
                            let svgimg4: SVGKImage = SVGKImage(named: "error")
                            self.symbolimg.image = svgimg4.uiImage
                            self.symbolviu.backgroundColor = UIColor.red
                            self.reuploadbtno.isHidden = false
                            self.deletebtno.isHidden = false
                            self.imgIds2 = [finpatdict.value(forKey: "imageId") as! CLong]
                            self.reImgId = (finpatdict.value(forKey: "imageId") as! CLong)
                            self.PatientID = finpatdict.value(forKey: "patientId") as! CLong
                         }
                       }
                    //}
                }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
            
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
    
    func Deleteimgs()
    {
        if Connectivity.isConnectedToInternet {
        
        actInd.startAnimating()
        
        var para2 = ["orderId":CLong(orderID),
                     "edit":true] as [String : Any]
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        print(para2)
        
        let url =  DeleteImgs()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        let json = imgIds2
        
        print(json)
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        request.httpBody = jsonData
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                   
                    DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                        // Utility.showAlertWithFading(title: "", andMessage: "RX deleted succesfully", onVC: self)
                        
                        self.checkEditOrder()
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
    
    func UploadErrorRX()
    {
        if Connectivity.isConnectedToInternet {
        
        actInd.startAnimating()
        let parameters = ["orderId":orderID,
                          "patientId":PatientID,
                          "image":imgbase64arr,
                          "imageId":CLong(reImgId),
                          "fileName":"IMG\(String(describing: filenameID!))"] as [String : Any]
        
        let url =  ReUploadRX()
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let session = URLSession.shared
        
        let myurl:URL = URL(string: url)!
        
        var request = URLRequest(url: myurl)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                   
                    DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                     self.filenameID = self.filenameID + 1
                    
                     self.checkEditOrder()
                        
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
}
else
{
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
}
        
    }
    

}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
