//
//  TNCViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class TNCViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var backbtno: UIButton!
    
    var tncarr = NSArray()
    
    @IBOutlet weak var tnctabledata: UITableView!
    
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getTnC()
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tncarr.count
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tnctabledata.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TnCCell") as! TnCCell
        let fintncdict:NSDictionary = tncarr[indexPath.row] as! NSDictionary
        let descstr:String = fintncdict.value(forKey: "description") as! String
        
        cell.tncdesc.text = descstr
        
        cell.tncheader.text = fintncdict.value(forKey:"header") as! String
       
        
        return cell
    }

    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
  
    
    func getTnC()
    {
        if Connectivity.isConnectedToInternet {
        
        let url = TnC()
        let serviceurl = URL(string: url)
        let session = URLSession.shared
        
        var request = URLRequest(url: serviceurl!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    let tncdict:NSDictionary = json as NSDictionary
                    self.tncarr = tncdict.value(forKey: "Legals") as! NSArray
                   
//                    DispatchQueue.main.async {
//                    self.tnctabledata.reloadData()
//
//                    }
                        
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
        self.tnctabledata.reloadData()
        }
        else
        {
            Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
        }
    }
    
}
