//
//  OrderListingViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import SideMenu
import Alamofire

class OrderListingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var orderlisttableview: UITableView!
    
    @IBOutlet weak var backbtno: UIButton!
    
    var ordertype:String!
    
    var PastODarr = NSArray()
    
    var CurrentODarr = NSArray()
    
    var seccount = 2
    
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    @IBOutlet weak var payfailviu: UIView!
    
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
    }
    
    
    
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    @objc func NotificationReceivedforPaymentFailed(notification: Notification) {
        payerrorpop()
        
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.payfailviu.removeFromSuperview()
            self.visualEffectView.removeFromSuperview()
        }
        
    }
    
    
    @objc func NotificationReceivedforOrderOnHold(notification: Notification) {
        allorders()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        visualEffectView.alpha = 0.3
               
               payfailviu.layer.cornerRadius = 4.0
               
               NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforPaymentFailed(notification:)), name: Notification.Name("PaymentFailed"), object: nil)
               
               NotificationCenter.default.addObserver(self, selector: #selector(self.NotificationReceivedforOrderOnHold(notification:)), name: Notification.Name("OnHold"), object: nil)
               
               allorders()
               
               let svgimg: SVGKImage = SVGKImage(named: "back")
               backbtno.setImage(svgimg.uiImage, for: .normal)
               
               self.title = "My Orders"
       
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == orderlisttableview
        {
        return seccount
        }
        return 1
    }

 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count:Int?
        
        if tableView == orderlisttableview
        {
        if section == 0
        {
          count = CurrentODarr.count
        }
        
        if section == 1
        {
          count = PastODarr.count
        }
            
        }
        
        return count!
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : OrderListTableViewCell?
        
        if tableView == orderlisttableview
        {
        if indexPath.section == 0
        {
        cell = tableView.dequeueReusableCell(withIdentifier: "OrderListTableViewCell") as! OrderListTableViewCell
        let finpastdict:NSDictionary = CurrentODarr[indexPath.row] as! NSDictionary
        cell?.orderId.text = String(describing:finpastdict.value(forKey: "orderId")!)
            
        if let totalsavings = finpastdict.value(forKey: "totalSaving") as? Double
        {
        cell?.totalsavings.text = "₹ " + String(format: "%.2f", totalsavings)
        }
            
        //cell?.totalsavings.text = "₹ " + String(describing:finpastdict.value(forKey: "totalSaving")!)
        
        cell?.presvalidtill.text = finpastdict.value(forKey: "orderStatus") as? String
            
        let mydate:CLong = finpastdict.value(forKey: "orderDate") as! CLong
        let mydates:CLong = mydate/1000
        let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMMM yyyy hh:mm"
            
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        cell?.deliveredonlbl.text = dateString
        cell?.cvalidlbl.text = NSLocalizedString("Order Status -", comment: "")
        cell?.bgview.layer.cornerRadius = 4.0
        cell?.bgview.layer.borderWidth = 1.0
        cell?.bgview.layer.borderColor = UIColor(hexString: "#EDEDED", alpha: 1.0).cgColor
        cell?.bgview.clipsToBounds = true
            
        let statusId:CLong = finpastdict.value(forKey: "statusId") as! CLong
          if statusId == 81
          {
           cell?.errorIden.isHidden = false
          }
          else
          {
          cell?.errorIden.isHidden = true
          }
            
          if statusId == 58
          {
          cell?.paynowo.isHidden = false
          cell?.paynowo.layer.cornerRadius = 4.0
          cell?.paynowo.layer.borderWidth = 0.8
          cell?.paynowo.layer.borderColor = UIColor(hexString: "#0071BC", alpha: 1.0).cgColor
          cell?.paynowo.tag = indexPath.row
          cell?.paynowo.addTarget(self, action: #selector(toPayment), for: .touchUpInside)
          
          }
          else
          {
          cell?.paynowo.isHidden = true
          }
            
            if statusId == 39 || statusId == 1 || statusId == 81 || statusId == 57 || statusId == 2
            {
                cell?.totalsavings.isHidden = true
                cell?.totsavelbl.isHidden = true
            }
            else
            {
                cell?.totalsavings.isHidden = false
                cell?.totsavelbl.isHidden = false
            }
           
            if statusId == 142
            {
                cell?.totsavelbl.text = NSLocalizedString("Estimated Savings :", comment: "")
            }
            else
            {
                cell?.totsavelbl.text = NSLocalizedString("Total Savings :", comment: "")
            }
            
        }else
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "OrderListTableViewCell") as! OrderListTableViewCell
            let fincurrdict:NSDictionary = PastODarr[indexPath.row] as! NSDictionary
            cell?.orderId.text = String(describing:fincurrdict.value(forKey: "orderId")!)
            
            if let totalsavings = fincurrdict.value(forKey: "totalSaving") as? Double
            {
             cell?.totalsavings.text = "₹ " + String(format: "%.2f", totalsavings)
            }
            
            //cell?.totalsavings.text = "₹ " + String(describing:fincurrdict.value(forKey: "totalSaving")!)
            if let delieveredon = fincurrdict.value(forKey: "deliveredDate") as? String
            {
            cell?.oldstatlbl.text = NSLocalizedString("Delivered On -", comment: "")
            cell?.deliveredonlbl.text = delieveredon
            }
            else
            {
                let mydate:CLong = fincurrdict.value(forKey: "orderDate") as! CLong
                let mydates:CLong = mydate/1000
                let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
                
                let dayTimePeriodFormatter = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "dd MMMM yyyy hh:mm"
                
                let dateString = dayTimePeriodFormatter.string(from: date as Date)
                cell?.oldstatlbl.text = NSLocalizedString("Ordered On -", comment: "")
                cell?.deliveredonlbl.text = dateString
            }
            
            cell?.errorIden.isHidden = true
            cell?.bgview.layer.cornerRadius = 4.0
            cell?.bgview.layer.borderWidth = 1.0
            cell?.bgview.layer.borderColor = UIColor(hexString: "#EDEDED", alpha: 1.0).cgColor
            cell?.bgview.clipsToBounds = true
            cell?.cvalidlbl.text = NSLocalizedString("Order Status -", comment: "")
            cell?.presvalidtill.text = fincurrdict.value(forKey: "orderStatus") as? String
            let statusId:CLong = fincurrdict.value(forKey: "statusId") as! CLong
            if statusId == 58
            {
                cell?.paynowo.isHidden = false
                cell?.paynowo.layer.cornerRadius = 4.0
                cell?.paynowo.layer.borderWidth = 0.8
                cell?.paynowo.layer.borderColor = UIColor(hexString: "#0071BC", alpha: 1.0).cgColor
                cell?.paynowo.tag = indexPath.row
                cell?.paynowo.addTarget(self, action: #selector(toPastPayment), for: .touchUpInside)
                
            }
            else
            {
                cell?.paynowo.isHidden = true
            }
            
            if statusId == 142
            {
            cell?.totsavelbl.text = NSLocalizedString("Estimated Total Savings :", comment: "")
            }
            else
            {
            cell?.totsavelbl.text = NSLocalizedString("Total Savings :", comment: "")
            }
            
            if statusId == 39 || statusId == 1 || statusId == 81 || statusId == 57 || statusId == 2
            {
                cell?.totalsavings.isHidden = true
                cell?.totsavelbl.isHidden = true
            }
            else
            {
                cell?.totalsavings.isHidden = false
                cell?.totsavelbl.isHidden = false
            }
            
            
        }
        }
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var headerView = UIView()
        
        if tableView == orderlisttableview
        {
        if section == 0
        {
            //let headerView = UIView()
            headerView.backgroundColor = UIColor.white
            
            let headerLabel = UILabel(frame: CGRect(x: 20, y: 10, width:
                tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            headerLabel.font = UIFont(name: "OpenSans-bold", size: 21)
            
            headerLabel.textColor = UIColor.black
            
            headerLabel.text = NSLocalizedString("Current Orders", comment: "")
            headerLabel.sizeToFit()
            headerView.addSubview(headerLabel)
           // return headerView
        }
        
        if section == 1 {
        //let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 10, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        
        headerLabel.font = UIFont(name: "OpenSans-bold", size: 21)
        headerLabel.textColor = UIColor.black
        
        headerLabel.text = NSLocalizedString("Past Orders", comment: "")
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        //return headerView
        
        }
        }
        
           return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if ordertype == "Past Order"
        {
           
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
            vc?.ordertype = "Past Order"
            self.navigationController?.pushViewController(vc!, animated: true)
        }else
        {
            
            if indexPath.section == 0
            {
            let finpastdict:NSDictionary = CurrentODarr[indexPath.row] as! NSDictionary
            let statusId:CLong = finpastdict.value(forKey: "statusId") as! CLong
            if statusId == 49
            {
            let localizedContent = NSLocalizedString("This order is not yet processed. Please check again in some time.", comment: "")
            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
            }
            else if statusId == 1 || statusId == 39 || statusId == 2
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderHelpViewController") as? OrderHelpViewController
                               vc!.odhelpdict = self.CurrentODarr[indexPath.row] as! NSDictionary
                               self.navigationController?.pushViewController(vc!, animated: true)
//                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StatusViewController") as? StatusViewController
//                vc!.odhelpdict4 = self.CurrentODarr[indexPath.row] as! NSDictionary
//                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if statusId == 58
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderHelpViewController") as? OrderHelpViewController
                vc!.odhelpdict = self.CurrentODarr[indexPath.row] as! NSDictionary
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            
            else
            {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderHelpViewController") as? OrderHelpViewController
            vc!.odhelpdict = self.CurrentODarr[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(vc!, animated: true)
            }
            }
            else
            {
            let finpastdict:NSDictionary = PastODarr[indexPath.row] as! NSDictionary
            let statusId:CLong = finpastdict.value(forKey: "statusId") as! CLong
            if statusId == 1 || statusId == 2
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderHelpViewController") as? OrderHelpViewController
                               vc!.odhelpdict = self.PastODarr[indexPath.row] as! NSDictionary
                               self.navigationController?.pushViewController(vc!, animated: true)
//            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StatusViewController") as? StatusViewController
//            vc!.odhelpdict4 = self.PastODarr[indexPath.row] as! NSDictionary
//            self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if statusId == 58
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderHelpViewController") as? OrderHelpViewController
                vc!.odhelpdict = self.PastODarr[indexPath.row] as! NSDictionary
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if statusId == 57
            {
                
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderHelpViewController") as? OrderHelpViewController
                               vc!.odhelpdict = self.PastODarr[indexPath.row] as! NSDictionary
                               self.navigationController?.pushViewController(vc!, animated: true)
//                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StatusViewController") as? StatusViewController
//                vc!.odhelpdict4 = self.PastODarr[indexPath.row] as! NSDictionary
//                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if statusId == 55
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderHelpViewController") as? OrderHelpViewController
                vc!.odhelpdict = self.PastODarr[indexPath.row] as! NSDictionary
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            
            }
            
        }
    }
    
   
    
    @objc func toPayment(sender:UIButton)
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
        vc?.ordertype = "Billing Order"
        vc?.odhelpdict2 = self.CurrentODarr[sender.tag] as! NSDictionary
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func toPastPayment(sender:UIButton)
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
        vc?.ordertype = "Billing Order"
        vc?.odhelpdict2 = self.PastODarr[sender.tag] as! NSDictionary
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    

    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func allorders()
    {
        if Connectivity.isConnectedToInternet {
        
        actInd.startAnimating()
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        print(authtok)
        
        let para2 = ["":""]
        
        let url =  GetAllOrders()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                        
                    print(json)
                        
                    self.actInd.stopAnimating()
                        
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    let OrderDict:NSDictionary = json as NSDictionary
                    self.PastODarr = OrderDict.value(forKey: "pastOrder") as! NSArray
                    self.CurrentODarr = OrderDict.value(forKey: "currentOrder") as! NSArray
                    
                    if self.PastODarr.count == 0
                    {
                        self.seccount = 1
                    }
                    else
                    {
                        self.seccount = 2
                    }
                        
                    self.orderlisttableview.reloadData()
                    
                    }
                    else if httpResponse?.statusCode == 500
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
        }
        else
        {
            Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
        }
    }
    
    
    func payerrorpop()
    {
        
        payfailviu.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(visualEffectView)
        
        self.view.addSubview(payfailviu)
        
        let leadingConstraint = NSLayoutConstraint(item: payfailviu, attribute: .leading, relatedBy: .equal,
                                                   toItem: payfailviu.superview, attribute: .leading,
                                                   multiplier: 1.0, constant: 20.0)
        
        let trailingConstraint = NSLayoutConstraint(item: payfailviu.superview!, attribute: .trailing, relatedBy: .equal,
                                                    toItem: payfailviu, attribute: .trailing,
                                                    multiplier: 1.0, constant: 20.0)
        
        let horizontalConstraint = NSLayoutConstraint(item: payfailviu, attribute: .centerX, relatedBy: .equal,
                                                      toItem: payfailviu.superview, attribute: .centerX,
                                                      multiplier: 1.0, constant: 0.0)
        
        let verticalConstraint = NSLayoutConstraint(item: payfailviu, attribute: .centerY, relatedBy: .equal,
                                                    toItem: payfailviu.superview, attribute: .centerY,
                                                    multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraint,trailingConstraint,horizontalConstraint,verticalConstraint])
        
        let leadingConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .leading, relatedBy: .equal,
                                                          toItem: visualEffectView.superview, attribute: .leading,
                                                          multiplier: 1.0, constant: 0.0)
        
        let trailingConstraintOverlay = NSLayoutConstraint(item: visualEffectView.superview!, attribute: .trailing, relatedBy: .equal,
                                                           toItem: visualEffectView, attribute: .trailing,
                                                           multiplier: 1.0, constant: 0.0)
        
        let topConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .top, relatedBy: .equal,
                                                      toItem: visualEffectView.superview, attribute: .top,
                                                      multiplier: 1.0, constant: 0.0)
        
        let bottomConstraintOverlay = NSLayoutConstraint(item: visualEffectView, attribute: .bottom, relatedBy: .equal,
                                                         toItem: visualEffectView.superview, attribute: .bottom,
                                                         multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([leadingConstraintOverlay,trailingConstraintOverlay,topConstraintOverlay,bottomConstraintOverlay])
        
    }
    
    
    
}

extension Double {
    
    
    // returns the date formatted.
    var dateFormatted : String? {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.none //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.short //Set date style
        return dateFormatter.string(from: date)
    }
    
    // returns the date formatted according to the format string provided.
    func dateFormatted(withFormat format : String) -> String{
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
}
