//
//  CancelReasonsViewController.swift
//  True_Meds
//
//  Created by Mangesh Toraskar on 10/12/19.
//

import UIKit
import SVGKit
import Reachability
import Alamofire

class CancelReasonsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate {

    
    var cancelOrderId = CLong()
    
    let reachability = Reachability()!
    
    var reasonId:CLong!
    
    @IBOutlet weak var othertv: UITextView!
    
    @IBOutlet weak var reasonsTable: UITableView!
    
    var finresdict = NSArray()
    
    var selectedItems2 = [Int]()
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var textfViewHeight: NSLayoutConstraint!
    

    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var reascancelo: UIButton!
    
    @IBOutlet weak var reasconfirmo: UIButton!
    
    @IBOutlet weak var reasonsviu: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldView.isHidden = true
        textfViewHeight.constant = 0
        
        GetCReasons()
        
        othertv.layer.borderColor = UIColor.lightGray.cgColor
        othertv.layer.borderWidth = 0.8
        othertv.layer.cornerRadius = 4.0
        
        reascancelo.layer.borderWidth = 0.8
        reascancelo.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        reascancelo.layer.cornerRadius = 4.0
        
        reasconfirmo.layer.borderWidth = 0.8
        reasconfirmo.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        reasconfirmo.layer.cornerRadius = 4.0
    }
    
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            othertv.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    @IBAction func cancelBackPress(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func confirmbtna(_ sender: Any) {
         if reasonId == 14 && othertv.text == ""
         {
         Utility.showAlertWithTitle(title: "", andMessage: "Please enter your reasons", onVC: self)
         }
         else if reasonId == nil
         {
         Utility.showAlertWithTitle(title: "", andMessage: "Please select your reason", onVC: self)
         }
         else
         {
         self.cancelOrder()
         }
     }
     
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return finresdict.count
     }
     
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "CancelReasonsTVC") as! CancelReasonsTVC
         
         let mydict:NSDictionary = finresdict[indexPath.row] as! NSDictionary
         cell.reasonlbl.text = mydict.value(forKey: "value") as? String ?? ""
         
       //  cell.reasonchecko.addTarget(self, action: #selector(checkrea), for: .touchUpInside)
       //    cell.reasonchecko.tag = indexPath.row
                
         if (selectedItems2.contains(indexPath.row)) {
         cell.reasonchecko.setBackgroundImage(UIImage(named: "Ellipse 141"), for: .normal)
         }
         else {
         cell.reasonchecko.setBackgroundImage(UIImage(named: "Ellipse 142"), for: .normal)
         }
             
         
         return cell
     }

     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
         let cell = self.reasonsTable.cellForRow(at: indexPath) as! CancelReasonsTVC
         
         let mydict:NSDictionary = finresdict[indexPath.row] as! NSDictionary
         self.reasonId = mydict.value(forKey: "reasonId") as! CLong

         self.selectedItems2.removeAll()
         
         if (self.selectedItems2.contains(indexPath.row)) {
             let index = self.selectedItems2.firstIndex(of: indexPath.row)
                 self.selectedItems2.remove(at: index!)
                 cell.reasonchecko.setBackgroundImage(UIImage(named: "Ellipse 142"), for: .normal)
             }
             else {
             self.selectedItems2.append(indexPath.row)
                 cell.reasonchecko.setBackgroundImage(UIImage(named: "Ellipse 141"), for: .normal)
             }
         
             print(self.reasonId)
         
         if(indexPath.row == finresdict.count - 1)
         {
             textFieldView.isHidden = false
         textfViewHeight.constant = 120
         }
         else
         {
             textFieldView.isHidden = true
        textfViewHeight.constant = 0
         othertv.text = ""
         view.endEditing(true)
         }
         
             reasonsTable.reloadData()
         
     }
     
    @objc func checkrea(sender:UIButton)
     {
         let buttonPosition = sender.convert(CGPoint.zero, to: self.reasonsTable)
         let indexPath = self.reasonsTable.indexPathForRow(at:buttonPosition)
         let cell = self.reasonsTable.cellForRow(at: indexPath!) as! CancelReasonsTVC
         
         let mydict:NSDictionary = finresdict[sender.tag] as! NSDictionary
         self.reasonId = mydict.value(forKey: "reasonId") as! CLong
         
         self.selectedItems2.removeAll()
         if (self.selectedItems2.contains(sender.tag)) {
             let index = self.selectedItems2.firstIndex(of: sender.tag)
             self.selectedItems2.remove(at: index!)
             cell.reasonchecko.setBackgroundImage(UIImage(named: "Ellipse 142"), for: .normal)
         }
         else {
             self.selectedItems2.append(sender.tag)
             cell.reasonchecko.setBackgroundImage(UIImage(named: "Ellipse 141"), for: .normal)
         }
         
         print(self.reasonId)
     
         reasonsTable.reloadData()
     }

    func GetCReasons(){
            
                 let para2 = ["":""] as [String : Any]
                 
                ApiManager().requestApiWithDataType(methodType:HPOST, urlString: getCancelReasons(),parameters: para2 as [String : AnyObject]) { (response,cStatus, error) in
                                 
                    if cStatus == 200 || cStatus == 201
                    {
                    let resdict:NSDictionary = convertStringToDictionary(json: response as! String)! as NSDictionary
                    self.finresdict = resdict.value(forKey: "reasonList") as! NSArray
                        DispatchQueue.main.async {
                            self.reasonsTable.reloadData()
                            self.tableViewHeight.constant = CGFloat(self.finresdict.count) * 51
                        }
                   
                    self.view.layoutIfNeeded()

                    }
                    else if cStatus == 401
                    {
                        let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                        Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                    }
                    else if cStatus == 500
                    {
    //                    let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
    //                    Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        
                        let popupVC = UIStoryboard.init(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "unexpectedemp") as! UnexpectedPOP
                                    
                                    self.addChild(popupVC)
                                    popupVC.view.frame = self.view.frame
                                    self.view.addSubview(popupVC.view)
                                    popupVC.didMove(toParent: self)
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                         }
            }
    
    
    func cancelOrder()
    {
        if Connectivity.isConnectedToInternet {
            
        //actInd.startAnimating()
        
             let orderID:CLong = cancelOrderId
//        let orderID:CLong = odhelpdict4.value(forKey: "orderId") as! CLong
        
          
            
        let parameters = ["orderId":String(describing: orderID),
                              "reasonId":reasonId!,
                              "notes":othertv.text!] as [String : Any]
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let url =  RCancelOrders()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    DispatchQueue.main.async {
                        
                    //self.actInd.stopAnimating()
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                        
                        
                         self.navigationController?.popViewController(animated: true)
                        
//                        for controller in self.navigationController!.viewControllers as Array {
//                            if controller.isKind(of: OrderListingViewController.self) {
//                                NotificationCenter.default.post(name: Notification.Name("OnHold"), object: nil)
//
//                                self.navigationController!.popToViewController(controller, animated: true)
//                                break
//                            }
//                        }
                        
                        
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                        
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
        }
        else
        {
            Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
        }
    }

}
