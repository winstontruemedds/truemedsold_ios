//
//  RaiseTicketViewController.swift
//  True_Meds
//


import UIKit
import SVGKit

class RaiseTicketViewController: UIViewController{

    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var bottomheight: NSLayoutConstraint!
    
    @IBOutlet weak var raiseview: UIView!
    
    @IBOutlet weak var raisebtno: UIButton!
    
    @IBOutlet weak var emailbtno: UIButton!
    
    @IBOutlet weak var callbtno: UIButton!
    
    @IBOutlet weak var queslbl: UILabel!
    
    @IBOutlet weak var answlbl: UITextView!
    
    var hcdetailsdict = NSDictionary()
    
    var detailstr2:String!
    
    @IBOutlet weak var detailheader: UILabel!
    
    @IBOutlet weak var fullraiseviu: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        detailheader.text = detailstr2
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        let svgimg2: SVGKImage = SVGKImage(named: "email")
        emailbtno.setImage(svgimg2.uiImage, for: .normal)
        
        let svgimg3: SVGKImage = SVGKImage(named: "call")
        callbtno.setImage(svgimg3.uiImage, for: .normal)
        
        raisebtno.layer.cornerRadius = 4.0
        raiseview.layer.cornerRadius = 4.0
        emailbtno.layer.cornerRadius = 4.0
        callbtno.layer.cornerRadius = 4.0
        
        emailbtno.layer.borderWidth = 0.8
        emailbtno.layer.borderColor = UIColor.white.cgColor
        
        callbtno.layer.borderWidth = 0.8
        callbtno.layer.borderColor = UIColor.white.cgColor
        
        raiseview.isHidden = true
        
        bottomheight.constant = 20
        
        queslbl.text = hcdetailsdict.value(forKey: "issues") as! String
        answlbl.text = hcdetailsdict.value(forKey: "answers") as! String
        
        if UserDefaults.standard.object(forKey: "CustomerId") == nil
        {
         fullraiseviu.isHidden = true
        }
        else
        {
         fullraiseviu.isHidden = false
        }
        
    }
    
    
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func RaiseQuery(_ sender: Any) {
        
        UIView.animate(withDuration: 1.0, animations: {
            
            if self.bottomheight.constant == 20
            {
                self.bottomheight.constant = 137
                self.raiseview.isHidden = false
            }
            else
            {
                self.bottomheight.constant = 20
                self.raiseview.isHidden = true
            }
            
        }, completion:nil)
        
    }
    
    
    @IBAction func emailus(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubmitTicketViewController") as? SubmitTicketViewController
        vc!.IssueId = hcdetailsdict.value(forKey: "issuesId") as! CLong
        vc!.cnctype = "email"
        vc!.detailstr3 = detailstr2
        vc!.quesstr = hcdetailsdict.value(forKey: "issues") as! String
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func callus(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubmitTicketViewController") as? SubmitTicketViewController
         vc!.IssueId = hcdetailsdict.value(forKey: "issuesId") as! CLong
        vc!.cnctype = "call"
        vc!.detailstr3 = detailstr2
        vc!.quesstr = hcdetailsdict.value(forKey: "issues") as! String
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
}
