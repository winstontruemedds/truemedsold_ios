//
//  MenuViewController.swift
//  True_Meds
//



import UIKit
import SideMenu
import SVGKit
import SDWebImage
import Contacts
import Reachability
import Alamofire

class MenuViewController:
UIViewController,UITableViewDelegate,UITableViewDataSource {
    
     let store = CNContactStore()
    
    
   // var menulogin = ["Help","My Orders","Info On Medicines","Health Articles","Subscriptions","Patients Profiles","Trumeds Doctors","Wallet","Refer & Earn","Settings","Legal","About Us","Logout"]
    
    var menuiclogin = ["help","myorders","medicineinfo","healarticle","subscriptions","trusteddoc","trusteddoc","wallet","refernearn","settings","legal","help","wallet","settings"]
    
   // var menunew = ["Help","Health Articles","Trumeds Doctors","Settings","Legal","About Us"]
    
    var menuicnew = ["help","healarticle","trusteddoc","settings","legal","help"]

    @IBOutlet weak var propic: UIImageView!
    
    @IBOutlet weak var menubtno: UIButton!
    
    @IBOutlet weak var custname: UILabel!
    
    @IBOutlet weak var custmobno: UILabel!
    
    @IBOutlet weak var custemail: UILabel!
    
    @IBOutlet weak var menutable: UITableView!
    
    @IBOutlet weak var savingconsheight: NSLayoutConstraint!

    @IBOutlet weak var saveviu: UIView!
    
    var PastODarr = NSArray()
    
    @IBOutlet weak var savinglbl: UILabel!
    
    var mastdict = NSDictionary()
    
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    var menulogin = NSArray()
    
    var menunew = NSArray()
    
    @IBOutlet weak var propicheight: NSLayoutConstraint!
    
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(handleClick))
//        propic.isUserInteractionEnabled = true
//        propic.addGestureRecognizer(tap)
    
    
        
        self.navigationController?.navigationBar.isHidden = true
        
        menubtno.layer.cornerRadius = 20.0
        propic.layer.cornerRadius = 50.0
        propic.clipsToBounds = true
        
        if UserDefaults.standard.object(forKey: "CustomerId") == nil
        {
        self.savingconsheight.constant = 0
        self.saveviu.isHidden = true
        menunew = UserDefaults.standard.value(forKey: "menunew") as! NSArray
            
        menutable.reloadData()
        propicheight.constant = 0
            
        }else{
            
        menulogin = UserDefaults.standard.value(forKey: "menulogin") as! NSArray
        propicheight.constant = 100
            
        getCustomerD()
        allorders()
        }
    }
    
    
    @objc func handleClick(_ sender: UITapGestureRecognizer) {
        if UserDefaults.standard.object(forKey: "CustomerId") == nil
        {
            
        }
        else
        {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController
            vc?.isfrommenu = "yes"
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
     }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.object(forKey: "CustomerId") == nil
        {
           menunew = UserDefaults.standard.value(forKey: "menunew") as! NSArray
           propicheight.constant = 0
           
        }else{
            
            menulogin = UserDefaults.standard.value(forKey: "menulogin") as! NSArray
            propicheight.constant = 100
            getCustomerD()
            allorders()
            
        }
        menutable.reloadData()
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count:Int
        if UserDefaults.standard.object(forKey: "CustomerId") == nil
        {
          count = menunew.count
        }
        else{
         count = menulogin.count
        }
        return count
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        var rowHeight:CGFloat = UITableView.automaticDimension
//
//        if(indexPath.row == 1){
//            rowHeight = 0.0
//        }
//
//        return rowHeight
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        if UserDefaults.standard.object(forKey: "CustomerId") == nil
        {
            let dict:NSDictionary = menunew[indexPath.row] as! NSDictionary
            cell.mlbl.text = dict.value(forKey: "text") as! String
            
            let svgimg: SVGKImage = SVGKImage(named: menuicnew[indexPath.row])
            cell.menudesic.image = svgimg.uiImage
        }
        else
        {
         let dict:NSDictionary = menulogin[indexPath.row] as! NSDictionary
         cell.mlbl.text = dict.value(forKey: "text") as! String
       
        let svgimg: SVGKImage = SVGKImage(named: menuiclogin[indexPath.row])
        cell.menudesic.image = svgimg.uiImage
        }
        
//        if indexPath.row == 1
//        {
//         cell.isHidden = true
//        }
//        else
//        {
//         cell.isHidden = false
//        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if UserDefaults.standard.object(forKey: "CustomerId") == nil
        {
            if indexPath.row == 0
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpViewController") as? HelpViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 1
            {
                
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ArticlesViewController") as? ArticlesViewController
                self.navigationController?.pushViewController(vc!, animated: true)
                
            }
            
            else if indexPath.row == 2
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TMDoctorsViewController") as? TMDoctorsViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            
                
            else if indexPath.row == 3
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 4
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LegalViewController") as? LegalViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
                
            else if indexPath.row == 5
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AboutUsViewController") as? AboutUsViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            
            
            
        }
        else
        {
            if indexPath.row == 0
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpTypeViewController") as? HelpTypeViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 1
            {
                
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderListingViewController") as? OrderListingViewController
                self.navigationController?.pushViewController(vc!, animated: true)
                
            }
            else if indexPath.row == 2
            {
                
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubstitutesListViewController") as? SubstitutesListViewController
                self.navigationController?.pushViewController(vc!, animated: true)
                
            }
                
                
            else if indexPath.row == 3
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ArticlesViewController") as? ArticlesViewController
                
                self.navigationController?.pushViewController(vc!, animated: true)
            }
                
            else if indexPath.row == 4
            {
                let localizedContent = NSLocalizedString("This feature is coming soon", comment: "")
                Utility.showAlertWithTitle(title:"", andMessage: localizedContent, onVC: self)
            }
                
                
                
            else if indexPath.row == 5
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PatientProfileViewController") as? PatientProfileViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 6
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TMDoctorsViewController") as? TMDoctorsViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
                
                
            else if indexPath.row == 7
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MyWalletViewController") as? MyWalletViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 8
            {
                
         //   Utility.showAlertWithTitle(title:"Refer & Earn coming soon", andMessage: "", onVC: self)
                
           CNContactStore().requestAccess(for: .contacts) { (access, error) in
             print("Access: \(access)")
            if access == false{
                DispatchQueue.main.async {
                    print("no")
                       
                    let localizedContent = NSLocalizedString("Please enable contacts permission from settings", comment: "")
                    Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
                }
            }else{
                
                DispatchQueue.main.async {
                      print("yes")
                     
                      let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RefernEarnVC") as? RefernEarnVC
                     self.navigationController?.pushViewController(vc!, animated: true)
                }
                
            }
            
           }
                

//                      store.requestAccess(for: .contacts) { (granted, err) in
//                          if let err = err {
//                              print("Failed to request access:", err)
//                              let localizedContent = NSLocalizedString("Please enable contacts permission from settings", comment: "")
//                             CNContactStore().requestAccess(for: .contacts) { (access, error) in
//                               print("Access: \(access)")
//                             }
//                              return
//                          }
//
//                          if granted {
//                              let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RefernEarnVC") as? RefernEarnVC
//                              self.navigationController?.pushViewController(vc!, animated: true)
//                          } else {
//                              let localizedContent = NSLocalizedString("Please enable contacts permission from settings", comment: "")
//                              Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
//                          }
//                      }
                
            }
            else if indexPath.row == 9
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 10
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LegalViewController") as? LegalViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 11
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AboutUsViewController") as? AboutUsViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 12
            {
                UserDefaults.standard.set(false, forKey: "applyCoupen")
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OffersViewController") as? OffersViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 13
            {
                let localizedContent = NSLocalizedString("Are you sure you want to logout?", comment: "")
                let alertVC = UIAlertController(title: title, message: localizedContent, preferredStyle: .alert)
                let Action1 = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default)
                { (action:UIAlertAction!) in
                    
                    //L102Language.setAppleLAnguageTo(lang: "en")
                    
                    UserDefaults.standard.removeObject(forKey: "CustomerId")
                    UserDefaults.standard.removeObject(forKey: "ageGroupId")
                    UserDefaults.standard.removeObject(forKey: "languageId")
                    UserDefaults.standard.removeObject(forKey: "custname")
                    UserDefaults.standard.removeObject(forKey: "access_token")
                    UserDefaults.standard.removeObject(forKey: "mobileNo")
                    UserDefaults.standard.removeObject(forKey: "langpref")
                    UserDefaults.standard.removeObject(forKey: "menunew")
                    UserDefaults.standard.removeObject(forKey: "menulogin")
                    UserDefaults.standard.synchronize()
                    
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SplashVC") as? SplashVC
                    
                    self.navigationController?.pushViewController(vc!, animated: true)
                    
                }
                alertVC.addAction(Action1);
                let Action2 = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil);
                alertVC.addAction(Action2);
                self.present(alertVC, animated: true, completion: nil);
                
            }
        }
    
    }
    
    
    @IBAction func closemenu(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func getCustomerD()
    {
        if Connectivity.isConnectedToInternet {
        
        actInd.startAnimating()
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let parameters = ["":""]
        
        let url =  GetCustDetails()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in parameters {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                        let httpResponse = response as? HTTPURLResponse
                        
                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                        {
                            DispatchQueue.main.async {
                                self.actInd.stopAnimating()
                                
                                let resultdict:NSDictionary = json as NSDictionary
                                let custdict:NSDictionary = resultdict.value(forKey: "CustomerDetails") as! NSDictionary
                                self.custname.text = custdict.value(forKey: "customerName") as? String
                                self.custemail.text = custdict.value(forKey: "emailAddress") as? String
                                self.custmobno.text = custdict.value(forKey: "mobileNo") as! String
                                
                                if let propicurl = custdict.value(forKey: "profileImageUrl") as? String
                                {
                                    if propicurl == ""
                                    {
                                        
                                    }else
                                    {
                                        let imgurl:URL = URL(string: propicurl)!
                                        self.propic.sd_setImage(with: imgurl, completed: nil)
                                    }
                                }
                                
                                if let mytotalsaving = custdict.value(forKey: "totalSavingsTillDate") as? Double
                                {
                                self.savinglbl.text = String(format: "%.2f", mytotalsaving)
                                }
                                
            
                                if let mobNo = custdict.value(forKey: "mobileNo") as? String
                                {
                                    UserDefaults.standard.set(mobNo, forKey: "mobileNo")
                                    UserDefaults.standard.synchronize()
                                }
                                
                                self.menutable.reloadData()
                                
                            }
                            
                        }
                        else if httpResponse?.statusCode == 401
                        {
                            let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                            Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                        }
                        else if httpResponse?.statusCode == 500
                        {
                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                        else
                        {
                            let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
    func allorders()
    {
        if Connectivity.isConnectedToInternet {
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let para2 = ["":""]
        
        let url =  GetAllOrders()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                        let httpResponse = response as? HTTPURLResponse
                        
    
                        if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                        {
                            DispatchQueue.main.async {
                                let OrderDict:NSDictionary = json as NSDictionary
                                self.PastODarr = OrderDict.value(forKey: "pastOrder") as! NSArray
                                if self.PastODarr.count == 0
                                {
                                    self.savingconsheight.constant = 0
                                    self.saveviu.isHidden = true
                                }
                                else
                                {
                                    self.savingconsheight.constant = 65
                                    self.saveviu.isHidden = false
                                }
                                
                            }
                        }
                        else if httpResponse?.statusCode == 401
                        {
                            let localizedContent = NSLocalizedString("Your login has expired, please login again", comment: "")
                            Utility.showAlertWithLogoutAction(title: "", andMessage:localizedContent, onVC: self)
                        }
                        else if httpResponse?.statusCode == 500
                        {
                            let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                        else
                        {
                            let localizedContent = NSLocalizedString("Unexpected Error ! Please try after some time", comment: "")
                            Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
}
else
{
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
}
    }
    
    
}
