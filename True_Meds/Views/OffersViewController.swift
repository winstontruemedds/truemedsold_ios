//
//  OffersViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import SDWebImage
import Reachability
import Alamofire




class OffersViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
   
    
    @IBOutlet weak var backbtno: UIButton!
    
    var offerarr = NSArray()
    
    var applyArray = [Bool]()
    var applyCoupenRow = Int()
    
    @IBOutlet weak var offertable: UITableView!
    
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        getalloffers()
         applyCoupenRow = 100
        
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offerarr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OffersTableViewCell") as! OffersTableViewCell
        let offerdict:NSDictionary = offerarr[indexPath.row] as! NSDictionary
        cell.titlelbl.text = offerdict.value(forKey: "title") as! String
        cell.desclbl.text = offerdict.value(forKey: "description") as! String
        let imgstr:String = offerdict.value(forKey: "image") as! String
        let imgurl:URL = URL(string: imgstr)!
        cell.logoimg.sd_setImage(with: imgurl, completed: nil)
        
        let mydate:CLong = offerdict.value(forKey: "validity") as! CLong
        let mydates:CLong = mydate/1000
        let date = NSDate(timeIntervalSince1970: TimeInterval(mydates))
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMMM yyyy"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        cell.datelbl.text = "Valid till: \(dateString)"
        
        if UserDefaults.standard.value(forKey: "applyCoupen") as! Bool == true{
            cell.applybtno.isHidden = false
        }else{
            cell.applybtno.isHidden = true
        }
       
        if applyCoupenRow == indexPath.row{
            cell.applybtno.backgroundColor = UIColor(hexString: "#22B573")
            cell.applybtno.setTitleColor(UIColor.white, for: .normal)
            cell.applybtno.layer.borderColor = UIColor(hexString: "#22B573").cgColor
        }else{
            cell.applybtno.backgroundColor = UIColor.white
            cell.applybtno.setTitleColor(UIColor(hexString: "#0071BC"), for: .normal)
            cell.applybtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        }
        
        cell.applybtno.layer.cornerRadius = 4.0
        cell.applybtno.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        cell.applybtno.layer.borderWidth = 0.8
        cell.offerviu.layer.cornerRadius = 4.0
        cell.offerviu.layer.borderColor = UIColor(hexString: "#EDEDED").cgColor
        cell.offerviu.layer.borderWidth = 0.8
        
        cell.viewdetailbtno.tag = indexPath.row
        cell.viewdetailbtno.addTarget(self, action: #selector(todetail), for: .touchUpInside)
        
        cell.applybtno.tag = indexPath.row
        cell.applybtno.addTarget(self, action: #selector(applycoupon), for: .touchUpInside)
        
        return cell
    }
    
    @objc func applycoupon(sender:UIButton)
    {
        let dict = self.offerarr[sender.tag] as? NSDictionary
        print(dict)
        let offerId = dict?.value(forKey: "offerId") as? CLong
        let applicableOn = dict?.value(forKey: "applicableOn") as? String
        let Offertitle = dict?.value(forKey: "title") as? String
        let description = dict?.value(forKey: "description") as? String
        print(applicableOn!)
        
        UserDefaults.standard.set(Offertitle, forKey: "offertitel")
          UserDefaults.standard.set(description, forKey: "descriptionCoupen")
        
        if applyCoupenRow == sender.tag{
            applyCoupenRow = 100
            UserDefaults.standard.set(0, forKey: "offerId")
            UserDefaults.standard.set("", forKey: "applicableOn")
        }else{
           applyCoupenRow = sender.tag
           UserDefaults.standard.set(offerId, forKey: "offerId")
            UserDefaults.standard.set(applicableOn, forKey: "applicableOn")
        }
       
        UserDefaults.standard.synchronize()
        

      
        
          self.navigationController?.popViewController(animated: true)
        
        print(offerId ?? 0)
//        applyArray.remove(at: sender.tag)
//        applyArray.insert(true, at: sender.tag)
//        print(applyArray)
//        print(applyArray.count)
        
        self.offertable.reloadData()
        
//        if sender.backgroundColor == UIColor.white
//        {
//            sender.backgroundColor = UIColor(hexString: "#22B573")
//           sender.setTitleColor(UIColor.white, for: .normal)
//          sender.layer.borderColor = UIColor(hexString: "#22B573").cgColor
//        }
//        else
//        {
//          sender.backgroundColor = UIColor.white
//          sender.setTitleColor(UIColor(hexString: "#0071BC"), for: .normal)
//          sender.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
//        }
    }
    
    
    
    @objc func todetail(sender:UIButton)
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OfferDetailsViewController") as? OfferDetailsViewController
       
        let offerdict:NSDictionary = offerarr[sender.tag] as! NSDictionary
        vc?.offId = offerdict.value(forKey: "offerId") as! CLong
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    

    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getalloffers()
    {
        if Connectivity.isConnectedToInternet {
        
        let url =  GetAllOffers()
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let session = URLSession.shared
        
        let serviceUrl = URL(string: url)
        
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                     DispatchQueue.main.async {
                    let httpResponse = response as? HTTPURLResponse
                    
                    print(httpResponse?.statusCode)
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    let offdict : NSDictionary = json as NSDictionary
                    self.offerarr = offdict.value(forKey: "offers") as! NSArray
                        
                        
                       
//                    for _ in 0..<self.offerarr.count {
//                                self.applyArray.append(false)
//                            }
//
//                        print(self.applyArray)
//                        print(self.offerarr.count)
//                        print(self.applyArray.count)
                    
                    
                    self.offertable.reloadData()
                    
                    }else if httpResponse?.statusCode == 400
                    {
                    self.offertable.isHidden = true
                        
                        let offdict : NSDictionary = json as NSDictionary
                        
                        if offdict.allKeys.first as! String == "400"
                        {
                        
                        let alertVC = UIAlertController(title: "", message: offdict.value(forKey: "400") as! String, preferredStyle: .alert)
                        let Action1 = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default)
                        { (action:UIAlertAction!) in
                         self.navigationController?.popViewController(animated: false)
                        }
                        alertVC.addAction(Action1);

                        self.present(alertVC, animated: true, completion: nil);
                        }
                        
                    }
                    else
                    {
                        
                    }
                }
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }

}
