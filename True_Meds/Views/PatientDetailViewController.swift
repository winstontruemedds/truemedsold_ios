//
//  PatientDetailViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class PatientDetailViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource {
    
    let reparr = ["Physical Examination","Endoscopy","Colonoscopy","Haemoglobin A1C"]
    
    let repdatearr = ["12 January 2019","10 January 2019","5 January 2019","2 January 2019"]
    
    let reachability = Reachability()!
    
    @IBOutlet weak var prescollectionview: UICollectionView!
    
    @IBOutlet weak var collectionheight: NSLayoutConstraint!
    
    @IBOutlet weak var patdetailimg: UIImageView!
    
    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var patName: UILabel!
    
    @IBOutlet weak var patRelation: UILabel!
    
    @IBOutlet weak var patAge: UILabel!
    
    @IBOutlet weak var patGender: UILabel!
    
    var patId:CLong!
    
    var activeRXlist = NSArray()
    
    var deactiveRXlist = NSArray()
    
    var PatientLabList = NSArray()
    
    var seccount = 2
    
    @IBOutlet weak var labtable: UITableView!
    
    @IBOutlet weak var labtabheight: NSLayoutConstraint!
    
    @IBOutlet weak var closebtno: UIButton!
    
    @IBOutlet weak var visblur: UIVisualEffectView!
    
    @IBOutlet weak var prespopviu: UIView!
    
    @IBOutlet weak var prespopimg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        visblur.isHidden = true
        prespopviu.isHidden = true
        prespopimg.isHidden = true
        closebtno.isHidden = true

        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
        patdetailimg.layer.cornerRadius = 45.0
        patdetailimg.clipsToBounds = true
        
        getpatientDetails()
        
        
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    @IBAction func closePresbtna(_ sender: UIButton) {
        visblur.isHidden = true
        prespopviu.isHidden = true
        prespopimg.isHidden = true
        closebtno.isHidden = true
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let height = prescollectionview.collectionViewLayout.collectionViewContentSize.height
        collectionheight.constant = height
        
        let height2 = labtable.contentSize.height
        labtabheight.constant = height2
        
        self.view.layoutIfNeeded()
    
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return seccount
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        var count:Int!
        if section == 0
        {
         count = activeRXlist.count
        }
        else
        {
         count = deactiveRXlist.count
        }
        
        return count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PatientDetailsCollectionViewCell", for: indexPath) as! PatientDetailsCollectionViewCell
    
        if indexPath.section == 0
        {
        let pdict:NSDictionary = activeRXlist[indexPath.row] as! NSDictionary
        let imgstr:String = pdict.value(forKey: "imagePath") as! String
        let imgstr2:String = imgstr.replacingOccurrences(of: "\\", with: "/")
        let finurl:String = Live_IP + imgstr2
        let url:URL = URL(string: finurl)!
        cell.adpresimg.sd_setImage(with: url, completed: nil)
        cell.adpresimg.layer.cornerRadius = 4.0
        cell.adpresimg.clipsToBounds = true
        }
        else{
                let pdict:NSDictionary = deactiveRXlist[indexPath.row] as! NSDictionary
                let imgstr:String = pdict.value(forKey: "imagePath") as! String
                let imgstr2:String = imgstr.replacingOccurrences(of: "\\", with: "/")
                let finurl:String = Live_IP + imgstr2
                let url:URL = URL(string: finurl)!
                cell.adpresimg.sd_setImage(with: url, completed: nil)
                cell.adpresimg.layer.cornerRadius = 4.0
                cell.adpresimg.clipsToBounds = true
        }

        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        var headerView:ProfDetailsCollectionReusableView?
        
        if indexPath.section == 0
        {
        headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ProfDetailsCollectionReusableView", for: indexPath) as! ProfDetailsCollectionReusableView
    
        headerView?.headerlbl.text = NSLocalizedString("Active Rx", comment: "")
        }
        else
        {
            headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ProfDetailsCollectionReusableView", for: indexPath) as! ProfDetailsCollectionReusableView
            
            headerView?.headerlbl.text = NSLocalizedString("Expired Rx", comment: "")
        }
            
            
        return headerView!
    }
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (prescollectionview.bounds.size.width/3) - 10, height: 100.0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        visblur.isHidden = false
        prespopviu.isHidden = false
        prespopimg.isHidden = false
        closebtno.isHidden = false
        
        if indexPath.section == 0
        {
            let pdict:NSDictionary = activeRXlist[indexPath.row] as! NSDictionary
            let imgstr:String = pdict.value(forKey: "imagePath") as! String
            let imgstr2:String = imgstr.replacingOccurrences(of: "\\", with: "/")
            let finurl:String = Live_IP + imgstr2
            let url:URL = URL(string: finurl)!
            prespopimg.sd_setImage(with: url, completed: nil)
            
        }
        else{
            let pdict:NSDictionary = deactiveRXlist[indexPath.row] as! NSDictionary
            let imgstr:String = pdict.value(forKey: "imagePath") as! String
            let imgstr2:String = imgstr.replacingOccurrences(of: "\\", with: "/")
            let finurl:String = Live_IP + imgstr2
            let url:URL = URL(string: finurl)!
            prespopimg.sd_setImage(with: url, completed: nil)
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PatientLabList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientDetailsTableViewCell") as! PatientProfileTableViewCell
        let labdict:NSDictionary = PatientLabList[indexPath.row] as! NSDictionary
        
        cell.reportnamelbl.text = labdict.value(forKey: "name") as! String
       // cell.reportdatelbl.text = repdatearr[indexPath.row]
        
        cell.bgview.layer.cornerRadius = 4.0
        cell.bgview.layer.borderWidth = 0.8
        
        cell.bgview.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        
        return cell
    }
    
    @IBAction func backto(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
    
    
    func getpatientDetails()
    {
        if Connectivity.isConnectedToInternet {
        
        let para2 = ["patientId":CLong(patId)] as [String : Any]
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        let url =  GetPatientDetails()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                    
                    let httpResponse = response as? HTTPURLResponse
                        
                    print(json)
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                        
                    let resdict:NSDictionary = json as NSDictionary
                        
                    print(resdict)
                    
                    let finpatdetdict:NSDictionary = resdict.value(forKey: "PatientDto") as! NSDictionary
                        
                    self.patName.text = finpatdetdict.value(forKey: "patientName") as? String
                    self.patRelation.text = finpatdetdict.value(forKey: "relationName") as? String
                    if let patage = finpatdetdict.value(forKey: "age") as? CLong
                    {
                    self.patAge.text = String(describing:patage) + ", "
                    }
                        
                    if let patage2 = finpatdetdict.value(forKey: "age") as? String
                    {
                    self.patAge.text = String(describing:patage2) + ", "
                    }
                        
                    self.patGender.text = finpatdetdict.value(forKey: "genderName") as? String
                   
                    let genname = finpatdetdict.value(forKey: "genderName") as? String
                    
                    if let relname:String = finpatdetdict.value(forKey: "relationName") as? String
                    {
                    if relname == "GRANDPARENT" && genname == "MALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wgfather")
                    }else if relname == "GRANDPARENT" && genname == "FEMALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wgmother")
                    }else if relname == "PARENT" && genname == "MALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wfather")
                    }else if relname == "PARENT" && genname == "FEMALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wmother")
                    }else if relname == "SIBLING" && genname == "MALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wfather")
                    }else if relname == "SIBLING" && genname == "FEMALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wmother")
                    }else if relname == "SPOUSE" && genname == "MALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wfather")
                    }else if relname == "SPOUSE" && genname == "FEMALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wmother")
                    }else if relname == "CHILD" && genname == "MALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wbrother")
                    }else if relname == "CHILD" && genname == "FEMALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wsister")
                    }else if relname == "GRANDCHILD" && genname == "MALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wbrother")
                    }else if relname == "GRANDCHILD" && genname == "FEMALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wsister")
                    }
                    else if relname == "OTHER" && genname == "MALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wfather")
                    }else if relname == "OTHER" && genname == "FEMALE"
                    {
                        self.patdetailimg.image = UIImage(named: "wmother")
                    }
                    else
                    {
                      self.patdetailimg.image = UIImage(named: "Default profile image")
                    }
                    }
                    
                    self.activeRXlist = resdict.value(forKey: "ActiveRx") as! NSArray
                    
                    self.deactiveRXlist = resdict.value(forKey: "ExpiredRx") as! NSArray
                        
                    self.PatientLabList = resdict.value(forKey: "PatientLabList") as! NSArray
                   
                    if self.activeRXlist.count == 0 && self.deactiveRXlist.count == 0
                    {
                        self.seccount = 0
                    }
                    else if self.deactiveRXlist.count == 0
                    {
                     self.seccount = 1
                    }
                        
                    self.prescollectionview.reloadData()
                    self.labtable.reloadData()

                    }
                    else if httpResponse?.statusCode == 400
                    {
                    let resdict:NSDictionary = json as NSDictionary
                    Utility.showAlertWithTitle(title: "", andMessage: resdict.value(forKey: "400") as! String, onVC: self)
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                     
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
        }
        else
        {
            Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
        }
    }
    
}
