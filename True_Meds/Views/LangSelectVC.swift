//
//  LangSelectVC.swift
//  True_Meds
//


import UIKit

class LangSelectVC: UIViewController {

    @IBOutlet weak var langtext: UILabel!
    
    @IBOutlet weak var lang1: UIButton!
    
    @IBOutlet weak var lang2: UIButton!
    
    @IBOutlet weak var lang3: UIButton!
    
    var mastdicts = NSDictionary()
    
    var engDict = NSDictionary()
    
    var hinDict = NSDictionary()
    
    var marDict = NSDictionary()
    
    @IBOutlet weak var langBgimg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        lang1.layer.borderWidth = 0.8
        lang1.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        lang1.layer.cornerRadius = 4.0
        
        lang2.layer.borderWidth = 0.8
        lang2.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        lang2.layer.cornerRadius = 4.0
        
        lang3.layer.borderWidth = 0.8
        lang3.layer.borderColor = UIColor(hexString: "#0071BC").cgColor
        lang3.layer.cornerRadius = 4.0
        
    
        engDict = mastdicts.value(forKey: "English") as! NSDictionary
        let appintro:NSDictionary = engDict.value(forKey: "appIntro") as! NSDictionary
        let imgstr:String = appintro.value(forKey: "screenLanguage") as! String
        let imgurl:URL = URL(string: imgstr)!
        langBgimg.sd_setImage(with: imgurl, completed: nil)
        
        hinDict = mastdicts.value(forKey: "Hindi") as! NSDictionary
        marDict = mastdicts.value(forKey: "Marathi") as! NSDictionary
    
    }
    

    @IBAction func lang1act(_ sender: Any) {
        let NewVC = self.storyboard?.instantiateViewController(withIdentifier: "AgeSelectVC") as? AgeSelectVC
        NewVC!.langID = 42
        NewVC!.langDict = engDict
        //L102Language.setAppleLAnguageTo(lang: "en")
        Locale.updateLanguage(code: "en")
        UserDefaults.standard.set("English", forKey: "langpref")
        let appintro:NSDictionary = engDict.value(forKey: "appIntro") as! NSDictionary
        let userDefaults = UserDefaults.standard
        userDefaults.set(appintro, forKey: "appIntro")
        userDefaults.synchronize()
        print(userDefaults.value(forKey: "appIntro"))
        //UserDefaults.standard.synchronize()
        self.navigationController?.pushViewController(NewVC!, animated: true)
    }
    
    @IBAction func lang2act(_ sender: Any) {
        let NewVC = self.storyboard?.instantiateViewController(withIdentifier: "AgeSelectVC") as? AgeSelectVC
        NewVC!.langID = 43
        NewVC!.langDict = hinDict
        //L102Language.setAppleLAnguageTo(lang: "hi")
        Locale.updateLanguage(code: "hi")
        UserDefaults.standard.set("Hindi", forKey: "langpref")
        let appintro:NSDictionary = hinDict.value(forKey: "appIntro") as! NSDictionary
        let userDefaults = UserDefaults.standard
        userDefaults.set(appintro, forKey: "appIntro")
        userDefaults.synchronize()
        print(userDefaults.value(forKey: "appIntro"))
//        UserDefaults.standard.set(appintro as! NSDictionary, forKey: "appIntro")
//         UserDefaults.standard.setValue(appintro, forKey: "appIntro")
       // UserDefaults.standard.synchronize()
//                            let userDefaults = UserDefaults.standard
//                               userDefaults.setValue(appintro, forKey: "appIntro")
//                               userDefaults.synchronize()
       
         //                  self.setDict(dict: appintro)

        self.navigationController?.pushViewController(NewVC!, animated: true)
    }
    
    @IBAction func lang3act(_ sender: Any) {
        let NewVC = self.storyboard?.instantiateViewController(withIdentifier: "AgeSelectVC") as? AgeSelectVC
        NewVC!.langID = 44
        NewVC!.langDict = marDict
        //L102Language.setAppleLAnguageTo(lang: "mr-IN")
        Locale.updateLanguage(code: "mr-IN")
        UserDefaults.standard.set("Marathi", forKey: "langpref")
        let appintro:NSDictionary = marDict.value(forKey: "appIntro") as! NSDictionary
        let userDefaults = UserDefaults.standard
        userDefaults.set(appintro, forKey: "appIntro")
        userDefaults.synchronize()
//       UserDefaults.standard.set(appintro as! NSDictionary, forKey: "appIntro")
//         UserDefaults.standard.setValue(appintro, forKey: "appIntro")
//        UserDefaults.standard.synchronize()
//                         let userDefaults = UserDefaults.standard
//                               userDefaults.setValue(appintro, forKey: "appIntro")
//                               userDefaults.synchronize()
        
         //                  self.setDict(dict: appintro)

        self.navigationController?.pushViewController(NewVC!, animated: true)
    }
    

}
