//
//  IssueDetailsViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class IssueDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UITextViewDelegate{
    
    let reachability = Reachability()!
    
    var cnctype:String!
    
    var issimgarrs = [UIImage]()
    
    var mainimgarr = [[UIImage]]()
    
    var singlecellarr = [UIImage]()
    
    var mainimg64str = [[String]]()
    
    var singlecell64str = [String]()
    
    var passarr = [Any]()
    
    @IBOutlet weak var issuetabledata: UITableView!
    
    @IBOutlet weak var issuePicker: UIPickerView!
    
    @IBOutlet weak var toolbr: UIToolbar!
    
    var texttag:Int!
    
    var textstr:String!
    
    var pickarr = NSArray()
    
    @IBOutlet weak var typelbl: UILabel!
    
    @IBOutlet weak var backbtno: UIButton!
    
    @IBOutlet weak var issuetableheight: NSLayoutConstraint!
    
    @IBOutlet weak var emailtf: UITextField!
    
    @IBOutlet weak var mobtf: UITextField!
    
    var selecteddict2 = NSDictionary()
    
    var dictionaryA = ["issueId":CLong(),"medicineId":String(),"description":"","images":""] as [String : Any]
    
    var ArrayB = [[String:Any]]()

    var emailtxt:String!
    
    var mobiletxt:String!
    
    var tableimgtag:Int!
    
    @IBOutlet weak var toolbr2: UIToolbar!
    
    var issIDarr = [CLong]()
    
    var issID:CLong!
    
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    @IBOutlet weak var submitbtno: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        submitbtno.layer.cornerRadius = 4.0
        
        getIssues()
        
        if cnctype == "email"
        {
            typelbl.text = NSLocalizedString("Email", comment: "")
            mobtf.isHidden = true
            //emailtf.placeholder = NSLocalizedString("Email", comment: "")
        }
        else
        {
            typelbl.text = NSLocalizedString("Mobile number", comment: "")
            emailtf.isHidden = true
            //mobtf.placeholder = NSLocalizedString("Mobile number", comment: "")
            mobtf.inputAccessoryView = toolbr2
            mobtf.isUserInteractionEnabled = false
            if UserDefaults.standard.object(forKey: "mobileNo") != nil
            {
                mobtf.text = String(describing: UserDefaults.standard.value(forKey: "mobileNo")!)
            }
            
        }
        
        issuetableheight.constant = CGFloat(410 * passarr.count)
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
        
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickarr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        let pickdict:NSDictionary = pickarr[row] as! NSDictionary
        textstr = pickdict.value(forKey: "categoryName") as! String
        issID = pickdict.value(forKey: "categoryId") as! CLong
        return textstr
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        let pickdict:NSDictionary = pickarr[row] as! NSDictionary
        textstr = pickdict.value(forKey: "categoryName") as! String
        issID = pickdict.value(forKey: "categoryId") as! CLong
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return passarr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IssueDetailsTableViewCell") as! IssueDetailsTableViewCell
       
        let isdict = passarr[indexPath.row] as! NSDictionary
        cell.medissnlbl.text = isdict.value(forKey: "substituteProductName") as! String
        cell.uploadbtno.tag = indexPath.row
        cell.uploadbtno.addTarget(self, action: #selector(selectimage), for: .touchUpInside)
        
        
        cell.issuepickertf.delegate = self
        cell.issuepickertf.tag = indexPath.row
       
        cell.issuepickertf.addTarget(self, action: #selector(openpicker(_:)), for: .editingDidBegin)
        cell.issuepickertf.inputAccessoryView = toolbr
        
        if mainimgarr.count == 0
        {
            
        }
        else
        {
        if mainimgarr.indices.contains(indexPath.row)
        {
        cell.fillCollectionView3(with: mainimgarr[indexPath.row])
        }
        }
        

        return cell
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if Utility.isValidEmail(emailtf.text!) == true
        {
            
        }else
        {
            emailtf.text = ""
            let localizedContent = NSLocalizedString("Please enter a valid email address", comment: "")
            Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            view.endEditing(true)
            return false
        }
        else
        {
            return true
        }
    }
    
    
    @IBAction func donepick(_ sender: Any) {
        view.endEditing(true)
        
        let myIndexPath = NSIndexPath(row: texttag, section: 0)
        
        let cell:IssueDetailsTableViewCell = issuetabledata.cellForRow(at: myIndexPath as IndexPath) as! IssueDetailsTableViewCell
        
        cell.issuepickertf.text = textstr
        
        print(textstr)
        print(issID)
        
        if issIDarr.indices.contains(texttag)
        {
          issIDarr[texttag] = issID
        }
        else
        {
          issIDarr.append(issID)
        }
        
        
     
    }
    
    @IBAction func donepick2(_ sender: Any) {
        view.endEditing(true)
        if Utility.isValidMobNo(mobtf.text!) == true
        {
            
        }
        else
        {
            mobtf.text = ""
            let localizedContent = NSLocalizedString("Please enter a valid mobile number", comment: "")
            Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
    }
    
    
    
    
    @objc func openpicker(_ textField: UITextField) {
        textField.inputView = issuePicker
        
       texttag = textField.tag
    }
    

    @objc func selectimage(sender: UIButton)
    {
        tableimgtag = sender.tag
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: NSLocalizedString("Camera", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                print("error")
            }
            
        })
        
        camera.setValue(UIColor.black, forKey: "titleTextColor")
        
        actionSheet.addAction(camera);
        
        let gallery = UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                print("error")
            }
        })
        
        gallery.setValue(UIColor.black, forKey: "titleTextColor")
        
        actionSheet.addAction(gallery);
        
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: {
            (alert: UIAlertAction!) in
            
            self.dismiss(animated: true, completion: nil);
            
        })
        
        
        actionSheet.addAction(cancel);
        
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0 , height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        present(actionSheet, animated: true, completion: nil);
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let images = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        {
           if mainimgarr.indices.contains(tableimgtag)
           {
            mainimgarr[tableimgtag].insert(images, at: 0)
           }
           else
           {
            singlecellarr.removeAll()
            singlecellarr.append(images)
            mainimgarr.append(singlecellarr)
            }
            
            
            issuetabledata.reloadData()
            
            let imageData:NSData = images.jpegData(compressionQuality: 0.4) as! NSData
            let base64:String = imageData.base64EncodedString(options: .lineLength64Characters)
            
            if mainimg64str.indices.contains(tableimgtag)
            {
                mainimg64str[tableimgtag].insert(base64, at: 0)
            }
            else
            {
                singlecell64str.removeAll()
                singlecell64str.append(base64)
                mainimg64str.append(singlecell64str)
            }
            
            
            
            dismiss(animated: true, completion: nil)
        }
        
        
        
    }
    
    
    @IBAction func backto(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func getIssues()
    {
        if Connectivity.isConnectedToInternet {
        
        let url =  GetOrderIssues()
        
        var myURL = URL(string: url)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: myURL!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    print(json)
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    let issuecat:NSDictionary = json as NSDictionary
                    self.pickarr = issuecat.value(forKey: "Category") as! NSArray
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }
    
    
    
    func RaiseTicket()
    {
        if Connectivity.isConnectedToInternet {
        
        actInd.startAnimating()
        
        //let custid:String = String(describing:UserDefaults.standard.value(forKey: "CustomerId")!)
        
        let authtok:String = UserDefaults.standard.value(forKey: "access_token") as! String
        
        var para2 = ["":""]
        
        let url =  RaiseOrderTicket()
        
        var items = [URLQueryItem]()
        var myURL = URLComponents(string: url)
        
        let session = URLSession.shared
        
        for (key,value) in para2 {
            items.append(URLQueryItem(name: key, value: String(describing:value)))
        }
        
        myURL?.queryItems = items
        
        var request = URLRequest(url: (myURL?.url)!)
        
        let orderID:CLong = selecteddict2.value(forKey: "orderId") as! CLong
        
        for i in 0..<passarr.count
        {
          let myIndexPath = NSIndexPath(row: i, section: 0)
            
          let cell:IssueDetailsTableViewCell = issuetabledata.cellForRow(at: myIndexPath as IndexPath) as! IssueDetailsTableViewCell
          let isdict = passarr[i] as! NSDictionary
          dictionaryA.updateValue(issIDarr[i], forKey: "issueId")
          dictionaryA.updateValue(isdict.value(forKey: "medicineId"), forKey: "medicineId")
          dictionaryA.updateValue(cell.tellustv.text!, forKey: "description")
          dictionaryA.updateValue(mainimg64str[i], forKey: "images")
          ArrayB.append(dictionaryA)
        }
        
        
        let json = [
            "mobile":mobtf.text!,
            "email":emailtf.text!,
            "ticketType":CLong(2),
            "orderId":String(describing: orderID),
            "issues":ArrayB
            ] as [String : Any]
        
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        request.httpBody = jsonData
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        request.addValue(authtok, forHTTPHeaderField: "Authorization")
        
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                    
                    print(json)
                        
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    self.actInd.stopAnimating()
                    
//                        NotificationCenter.default.post(name: Notification.Name("Issues"), object: nil)
//                        for controller in self.navigationController!.viewControllers as Array {
//                            if controller.isKind(of: ViewController.self) {
//
//                                self.navigationController!.popToViewController(controller, animated: true)
//                                break
//                            }
//                        }
                        self.pushThankYou()
                    
                }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                        }
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
        
    }
    
    
    func pushThankYou(){
         
              let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankTicketViewController") as? thankTicketViewController
          
              self.navigationController?.pushViewController(vc!, animated: false)
      }
    
    
    @IBAction func submitTicket(_ sender: Any) {
        
        if cnctype == "email"
        {
        if Utility.isValidEmail(emailtf.text!) == true
        {
         RaiseTicket()
        }else
        {
            emailtf.text = ""
            let localizedContent = NSLocalizedString("Please enter a valid email address", comment: "")
            Utility.showAlertWithTitle(title: "", andMessage: localizedContent, onVC: self)
        }
        }
        else
        {
        RaiseTicket()
        }
    }
    
    
    
    
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}



