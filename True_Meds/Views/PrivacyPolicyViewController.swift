//
//  PrivacyPolicyViewController.swift
//  True_Meds
//


import UIKit
import SVGKit
import Reachability
import Alamofire

class PrivacyPolicyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    let reachability = Reachability()!
    
    @IBOutlet weak var backbtno: UIButton!
    
    var pparr = NSArray()
    
    @IBOutlet weak var pptabledata: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getPrivacy()
        
        let svgimg: SVGKImage = SVGKImage(named: "back")
        backbtno.setImage(svgimg.uiImage, for: .normal)
    }
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pparr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrivacyCell") as! LegalTableViewCell
        let finppdict:NSDictionary = pparr[indexPath.row] as! NSDictionary
        cell.pdesc.text = finppdict.value(forKey: "description") as! String
        cell.pheader.text = finppdict.value(forKey: "header") as! String
        
        return cell
    }
    
    
    
    
    @IBAction func backto(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    
    func getPrivacy()
    {
        if Connectivity.isConnectedToInternet {
    
        let url = Privacy()
        let serviceurl = URL(string: url)
        let session = URLSession.shared
        
        var request = URLRequest(url: serviceurl!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a", forHTTPHeaderField: "transactionId")
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            if let data = data {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200 || httpResponse?.statusCode == 201
                    {
                    
                    let ppdict:NSDictionary = json as NSDictionary
                    self.pparr = ppdict.value(forKey: "Legals") as! NSArray
                    
                    DispatchQueue.main.async {
                         self.pptabledata.reloadData()
                    }
                    }
                    else
                    {
                        let localizedContent = NSLocalizedString("We are facing some technical difficulties. Please try again after some time", comment: "")
                        Utility.showAlertWithFading(title: "", andMessage:localizedContent, onVC: self)
                    }
                    
                }
            } catch let error {
                print(error.localizedDescription)
            }
            }
        })
        task.resume()
    }
    else
    {
    Utility.showAlertWithTitle(title: "", andMessage: NSLocalizedString("Please check your internet connectivity", comment: ""), onVC: self)
    }
    }

}
