//
//  cellTableViewCell.swift
//  orderStatus
//
//  Created by Mangesh Toraskar on 29/11/19.
//  Copyright © 2019 Mangesh Toraskar. All rights reserved.
//

import UIKit

class cellTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var dateTimeLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
