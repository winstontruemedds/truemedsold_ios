//
//  pXCollectionViewCell.swift
//  summary
//
//  Created by Office on 11/4/19.
//  Copyright © 2019 Office. All rights reserved.
//

import UIKit

class pXCollectionViewCell: UICollectionViewCell {
    
    
    
    @IBOutlet weak var PxImageView: UIImageView!
    
    
    @IBOutlet weak var deletePxBtn: UIButton!
    
    
    override func awakeFromNib() {
           super.awakeFromNib()
        PxImageView.layer.cornerRadius = 10
        //rightBottom(object: PxImageView)
           // Initialization code
       }
    
    func rightBottom(object:UIImageView){
          if #available(iOS 11.0, *){
              object.clipsToBounds = false
              object.layer.cornerRadius = 10
              object.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner,.layerMinXMaxYCorner]
          }else{
              let rectShape = CAShapeLayer()
              rectShape.bounds = object.frame
              rectShape.position = object.center
              rectShape.path = UIBezierPath(roundedRect: object.bounds,    byRoundingCorners: [.topLeft , .topRight , .bottomLeft], cornerRadii: CGSize(width: 20, height: 20)).cgPath
              object.layer.mask = rectShape
          }
        
         
          
      }
    
}
