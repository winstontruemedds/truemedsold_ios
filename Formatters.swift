//
//  Formatters.swift
//  languagetry
//
//  Created by Office on 12/13/19.
//  Copyright © 2019 Office. All rights reserved.
//

import Foundation

extension NumberFormatter {

    static let moneyFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.generatesDecimalNumbers = true
        nf.maximumFractionDigits = 2
        nf.minimumFractionDigits = 2
        nf.numberStyle = .decimal
        return nf
    }()

    static func resetupCashed() {
        moneyFormatter.locale = Locale.current
    }
}


extension DateFormatter {

    static let dobFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateStyle = .full
        return df
    }()

    static func resetupCashed() {
        dobFormatter.locale = Locale.current
    }
}
